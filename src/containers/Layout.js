import { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import PageContent from "./PageContent"
import ModalLayout from "./ModalLayout"
import RightSidebar from './RightSidebar'
import FormLeftbar from "../form/FormLeftbar/FormLeftbar"
import { removeNotificationMessage } from "../features/common/headerSlice"

function Layout() {
    const dispatch = useDispatch();
    const { newNotificationMessage, newNotificationStatus } = useSelector(state => state.header);

    useEffect(() => {
        if (newNotificationMessage !== "") {
            if (newNotificationStatus === 3) NotificationManager.warning(newNotificationMessage, 'Peringatan');
            if (newNotificationStatus === 2) NotificationManager.info(newNotificationMessage, 'Info');
            if (newNotificationStatus === 1) NotificationManager.success(newNotificationMessage, 'Sukses');
            if (newNotificationStatus === 0) NotificationManager.error(newNotificationMessage, 'Kesalahan');

            dispatch(removeNotificationMessage());
        }
    }, [dispatch, newNotificationMessage]);

    return (
        <>
            <div className="drawer drawer-mobile lg:drawer-open">
                <input id="left-sidebar-drawer" type="checkbox" className="drawer-toggle" />
                <FormLeftbar/>
                <PageContent/>
            </div>

            <RightSidebar />

            <NotificationContainer />

            <ModalLayout />
        </>
    );
}

export default Layout;