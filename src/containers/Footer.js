import logoCollega from '../assets/images/collega-text-icon.png';

export function Footer() {
    return(
        <div className="w-full flex justify-center items-center pb-10 bg-base-200">
            <h2 className="font-semibold mr-4">Powered by </h2>
            <img alt='' src={logoCollega} className="w-[6rem]"/>
        </div>
    )
}