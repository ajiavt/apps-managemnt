function SuspenseContent(){
    return(
        <div className="flex justify-center items-center w-full h-screen text-gray-300 dark:text-gray-200 bg-base-100">
            <h1 className='text-white'><strong>Loading ...</strong></h1>
        </div>
    )
}

export default SuspenseContent