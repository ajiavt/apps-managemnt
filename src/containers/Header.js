import React, {  } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Bars3Icon  from '@heroicons/react/24/outline/Bars3Icon'
import {useNavigate} from 'react-router-dom'
import FormUserData from "../form/FormUserData/FormUserData";
import {LogoutOutlined, PersonOutline, InfoOutlined} from "@mui/icons-material";
import {handleLogout} from '../redux/authLocalStorage/authLocalStorageRedirect';

// v1 - Old version of Smart Branch
// v2 - Add template, upgrading from old version
// v2.1 - Add Tree/Details List for Generate Menu
// v2.2 - Add Interactive UI Upload like Banner and Ads
// v2.3 - Add Export to Pdf and Tables for the export
// v2.4 - Add Preview Print, and add logic word wrap
// v2.5 - Add Bang Imam's form and component
// v2.6 - CUT OFF 1
// v3.0 - versi 3 adalah versi CUT OFF 1. Sudah include Pemeliharaan Parameter baru, Approval Transaksi, dan Generate Modul
// v3.0 - Renaming to Hybrid from Smart

const APP_VERSION = "Version v3.150224";

function Header() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { noOfNotifications, pageTitle } = useSelector(state => state.header);

    const handleLogoutClick = () => {
        handleLogout(dispatch, navigate);
    };

    return (
        <>
            <div className="navbar -mb-5 bg-base-200 flex justify-between">
                <div className="z-10">
                    <label htmlFor="left-sidebar-drawer" className="btn btn-primary ml-2 drawer-button lg:hidden">
                    <Bars3Icon className="h-5 inline-block w-5"/></label>
                    <h1 className="text-2xl ml-[10px] font-semibold">{pageTitle}</h1>
                </div>
                <div className="order-last">
                    <div className="tooltip tooltip-bottom" data-tip={APP_VERSION}>
                        <button className="btn btn-ghost btn-circle">
                            <InfoOutlined className='h-5 w-5'/>
                        </button>
                    </div>
                    <div className="tooltip tooltip-bottom" data-tip="User Profil">
                        <details className="dropdown dropdown-hover dropdown-end dropdown-bottom">
                            <summary tabIndex={0} className="btn btn-ghost btn-circle avatar">
                                <PersonOutline className='h-5 w-5'/>
                            </summary>
                            <ul tabIndex={0} className="shadow shadow-md shadow-gray-400 dropdown-content z-[60] menu  bg-base-100 rounded-box w-96 -mr-11 mt-3">
                                <FormUserData/>
                            </ul>
                        </details>
                    </div>
                    <div className="tooltip tooltip-bottom" data-tip="Logout">
                        <button className="btn btn-ghost btn-circle mr-2" onClick={handleLogoutClick}>
                            <LogoutOutlined className='h-5 w-5'/>
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Header