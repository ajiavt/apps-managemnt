import Header from "./Header"
import {Route, Routes, useLocation} from 'react-router-dom'
import routes from '../routes'
import { Suspense, lazy, useEffect, useRef, useMemo } from 'react'
import SuspenseContent from "./SuspenseContent"
import {useDispatch, useSelector} from 'react-redux'
import {setActivePath} from "../redux/pathSet/pathSetSlice";
import { memo } from 'react';

const Page404 = lazy(() => import('../pages/protected/404'))
const LoadingSkeleton = lazy(() => import('../pages/LoadingSkeleton'))
const FormHome = lazy(() => import("../form/FormHome/FormHome"))

function PageContent(){

    const location = useLocation();
    const dispatch = useDispatch();
    const mainContentRef = useRef(null);
    const {pageTitle} = useSelector(state => state.header);
    const {pathSet} = useSelector(state => state.pathSet);

    const currentPathSet = useMemo(() => new Set(pathSet), [pathSet]);

    useEffect(() => {
        const appRoute = location.pathname;
        dispatch(setActivePath(appRoute));
        mainContentRef.current.scroll({
            top: 0,
            behavior: "smooth"
          });
    }, [location.pathname, dispatch, pageTitle]);

    return(
        <div className="drawer-content flex flex-col ">
            <Header/>
            <main className="flex-1 overflow-y-auto px-5 bg-base-200" ref={mainContentRef}>
                <Suspense fallback={<SuspenseContent />}>
                    <Routes>
                        {
                            routes.map((route, key) => {
                                const appRoute = route.path;
                                const component = currentPathSet.has(appRoute) ? <route.component /> : <LoadingSkeleton />;
                                return(
                                    <Route
                                        key={key}
                                        exact={true}
                                        path={appRoute}
                                        element={component}
                                    />
                                )
                            })
                        }

                        {/* Redirecting unknown url to 404 page */}
                        <Route path="/" element={<FormHome />} />
                        <Route path="*" element={<Page404 />} />
                    </Routes>
                </Suspense>
            </main>

            {/*<Footer/>*/}
        </div> 
    )
}

export default memo(PageContent);