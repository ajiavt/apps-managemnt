import { lazy } from 'react'

const Page404 = lazy(() => import('../pages/protected/404'))
const FormHome = lazy(() => import('../form/FormHome/FormHome'))
const FormPemeliharaanUser = lazy(() => import('../form/Form010100'))
const FormAktifUser = lazy(() => import('../form/Form010200'))
const FormPemeliharaanWewenang = lazy(() => import('../form/Form020100'))
const FormPemeliharaanMenu = lazy(() => import('../form/Form030100'))
const FormPemeliharaanPerangkat = lazy(() => import('../form/Form050200'))
const FormPemeliharaanPathAPI = lazy(() => import('../form/Form040100'))
const FormPemeliharaanParameter = lazy(() => import('../form/Form050100'))
const FormPemeliharaanNegara = lazy(() => import('../form/FormAddress'))
const FormPemeliharaanProvinsi = lazy(() => import('../form/FormAddress'))
const FormPemeliharaanKotaKabupaten = lazy(() => import('../form/FormAddress'))
const FormPemeliharaanKecamatan = lazy(() => import('../form/FormAddress'))
const FormPemeliharaanKelurahanDesa = lazy(() => import('../form/FormAddress'))
const FormGenerateAksesApi = lazy(() => import('../form/Form020200'))
const FormGenerateMenu = lazy(() => import('../form/Form020300'))
const FormInformasiProdukBank = lazy(() => import('../form/Form050800'))
const FormInformasiBanner = lazy(() => import('../form/Form050900'))
const FormRincianAktivitasPerTeller = lazy(() => import('../form/Form060200'))
const FormRincianAktivitasTeller = lazy(() => import('../form/Form060300'))
const FormPemeliharaanFingerprint = lazy(() => import('../form/Form010400'))
const FormKonfigurasiPrinter = lazy(() => import('../form/Form051200'))
const FormKonfigurasiButabPrinter = lazy(() => import('../form/Form051300'))
const FormGenerateAksesModul = lazy(() => import('../form/Form020400'))
const FormGenerateUserAksesModul = lazy(() => import('../form/Form010300'))
const FormApprovalTransaksi = lazy(() => import('../form/Form070100'))
const FormPemeliharaanCache = lazy(() => import('../form/Form051400'))
const FormApprovalTransaksiPerCabang = lazy(() => import('../form/Form070100'))
const FormDashboardTransaksi = lazy(() => import('../form/FormDashboard/FormDashboard'))
const FormMappingWewenangCore = lazy(() => import('../form/Form020500'))
const Forbidden = lazy(() => import('../pages/protected/Forbidden'))

const routes = [
  {
    path: '/app/404',
    component: Page404,
  },
  {
    path: '/app/forbidden',
    component: Forbidden,
  },
  {
    path: '/hbam',
    component: FormHome,
  },
  {
    path: '/app/menu-id=010100',
    component: FormPemeliharaanUser,
  },
  {
    path: '/app/menu-id=010200',
    component: FormAktifUser,
  },
  {
    path: '/app/menu-id=020100',
    component: FormPemeliharaanWewenang,
  },
  {
    path: '/app/menu-id=030100',
    component: FormPemeliharaanMenu,
  },
  {
    path: '/app/menu-id=050200',
    component: FormPemeliharaanPerangkat,
  },
  {
    path: '/app/menu-id=040100',
    component: FormPemeliharaanPathAPI,
  },
  {
    path: '/app/menu-id=050100',
    component: FormPemeliharaanParameter,
  },
  {
    path: '/app/menu-id=050300',
    component: FormPemeliharaanNegara,
  },
  {
    path: '/app/menu-id=050400',
    component: FormPemeliharaanProvinsi,
  },
  {
    path: '/app/menu-id=050500',
    component: FormPemeliharaanKotaKabupaten,
  },
  {
    path: '/app/menu-id=050600',
    component: FormPemeliharaanKecamatan,
  },
  {
    path: '/app/menu-id=050700',
    component: FormPemeliharaanKelurahanDesa,
  },
  {
    path: '/app/menu-id=020200',
    component: FormGenerateAksesApi,
  },
  {
    path: '/app/menu-id=020300',
    component: FormGenerateMenu,
  },
  {
    path: '/app/menu-id=050800',
    component: FormInformasiProdukBank,
  },
  {
    path: '/app/menu-id=050900',
    component: FormInformasiBanner,
  },
  {
    path: '/app/menu-id=060200',
    component: FormRincianAktivitasPerTeller,
  },
  {
    path: '/app/menu-id=060300',
    component: FormRincianAktivitasTeller,
  },
  {
    path: '/app/menu-id=010400',
    component: FormPemeliharaanFingerprint,
  },
  {
    path: '/app/menu-id=051200',
    component: FormKonfigurasiPrinter,
  },
  {
    path: '/app/menu-id=051300',
    component: FormKonfigurasiButabPrinter,
  },
  {
    path: '/app/menu-id=020400',
    component: FormGenerateAksesModul,
  },
  {
    path: '/app/menu-id=010300',
    component: FormGenerateUserAksesModul,
  },
  {
    path: '/app/menu-id=070100',
    component: FormApprovalTransaksi,
  },
  {
    path: '/app/menu-id=051400',
    component: FormPemeliharaanCache,
  },
  {
    path: '/app/menu-id=070200',
    component: FormApprovalTransaksiPerCabang,
  },
  {
    path: '/app/menu-id=000100',
    component: FormDashboardTransaksi,
  },
  {
    path: '/app/menu-id=020500',
    component: FormMappingWewenangCore,
  },
]

export default routes
