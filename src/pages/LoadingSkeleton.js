import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { setPageTitle } from '../features/common/headerSlice'
import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';

function InternalPage(){

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setPageTitle({ title : ""}))
      }, [])

    return(
        <div className="hero h-4/5 bg-base-200">
            <div className="hero-content text-red-700 text-center">
                <div className="max-w-md">
                    <Stack spacing={2}>
                        <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                        <Skeleton variant="rectangular" width={500} height={90} />
                        <Skeleton variant="rectangular" width={500} height={90} />
                        <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                        <Skeleton variant="rectangular" width={500} height={90} />
                    </Stack>
                </div>
            </div>
        </div>
    )
}
export default InternalPage