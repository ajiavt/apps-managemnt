import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { setPageTitle } from '../../features/common/headerSlice'

function InternalPage() {
    const dispatch = useDispatch();
    const [randomEmoji, setRandomEmoji] = useState('');

    useEffect(() => {
        dispatch(setPageTitle({ title: '' }));
        // Generate a random index to select an emoji
        const emojis = ['🫣', '🧐', '🤔', '🔎', '😱', '😭', '🥺'];
        const randomIndex = Math.floor(Math.random() * emojis.length);
        setRandomEmoji(emojis[randomIndex]);
    }, []);

    return (
        <div className="hero h-4/5 bg-base-200">
            <div className="hero-content text-[#ffb02e] text-center">
                <div className="w-full">
                    <span className="text-[100px]">{randomEmoji}</span>
                    <h1 className="text-[60px] font-bold">404 - Not Found</h1>
                </div>
            </div>
        </div>
    );
}

export default InternalPage