import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { setPageTitle } from '../../features/common/headerSlice'
import { NoSymbolIcon } from '@heroicons/react/24/outline'

function InternalPage(){

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setPageTitle({ title : ""}))
      }, [])

    return(
        <div className="hero h-4/5 bg-base-200">
            <div className="hero-content text-red-700 text-center">
                <div className="max-w-md">
                <NoSymbolIcon className="h-48 w-48 inline-block"/>
                <h1 className="text-5xl  font-bold">Access Forbidden</h1>
                </div>
            </div>
        </div>
    )
}

export default InternalPage