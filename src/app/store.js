import { configureStore } from '@reduxjs/toolkit'
import headerSlice from '../features/common/headerSlice'
import modalSlice from '../features/common/modalSlice'
import rightDrawerSlice from '../features/common/rightDrawerSlice'
import leadsSlice from '../features/leads/leadSlice'
import authLocalStorageSlice from '../redux/authLocalStorage/authLocalStorageSlice'
import pathSetSlice from '../redux/pathSet/pathSetSlice'

const combinedReducer = {
  header : headerSlice,
  rightDrawer : rightDrawerSlice,
  modal : modalSlice,
  lead : leadsSlice,
  authLocalStorage: authLocalStorageSlice,
  pathSet: pathSetSlice,
}

export default configureStore({
    reducer: combinedReducer
})