import axios from "axios";

export const apiClient = axios.create({
    baseURL: 
        `${process.env.REACT_APP_BASE_URL}`,
    headers: {
        'Content-Type': 'application/json',
        'x-Appl-id': 'FE_BE',
        'Authorization': `${process.env.REACT_APP_BASIC_AUTH}`,
    }
});

export const apiClientDashboard = axios.create({
    baseURL:
        `${process.env.REACT_APP_BASE_URL}`,
    headers: {
        'Content-Type': 'application/json',
        'x-Appl-id': 'FE_BE'
    }
})

export const apiClientUserData = axios.create({
    baseURL:
        `${process.env.REACT_APP_BASE_URL}`,
    headers: {
        'Content-Type': 'application/json',
        'x-Appl-id': 'FE_BE'
    }
})


export const apiClientAgent = axios.create({
    baseURL:
        `${process.env.REACT_APP_AGENT_BASE_URL}`,
    headers: {
        'Content-Type': 'application/json',
        'x-Appl-id': 'AG',
    }
});
