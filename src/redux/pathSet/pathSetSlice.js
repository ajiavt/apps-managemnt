import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    pathSet: [],
    appRoute: '',
}

const pathSetSlice = createSlice({
    name: 'pathSet',
    initialState,
    reducers: {
        setPathSet: (state, action) => {
            state.pathSet = action.payload;
        },

        setActivePath: (state, action) => {
            state.activePath = action.payload;
        },
    },
});

export const { setPathSet, setActivePath } = pathSetSlice.actions;

export default pathSetSlice.reducer;