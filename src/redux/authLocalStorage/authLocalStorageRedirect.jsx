import React, { lazy, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setIsAuth } from './authLocalStorageSlice';
import { handleAuthLocalStorageAsync } from './authLocalStorageAction';
import handleExpires from '../../utils/localStorages/handleExpires';
import { useLocation, useNavigate } from 'react-router-dom';
import { authService } from "../../form/_repository/authService";
import { showNotification } from "../../features/common/headerSlice";

const FormLogin = lazy(() => import('../../form/FormLogin/FormLogin'));

export const handleLogout = async (dispatch, navigate) => {
    const accessToken = localStorage.getItem("access_token");
    const tokenType = localStorage.getItem("token_type");

    if (accessToken && tokenType) {
        const { response, status } = await authService("LOGOUT_TELLER", null, accessToken, tokenType);

        if (status === 0) {
            dispatch(showNotification({ message: response?.data?.message, status: 0 }));
        }
    }

    localStorage.clear();
    dispatch(setIsAuth(false));
    navigate("/hbam");
};

const AuthLocalStorageRedirect = ({ children }) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();
    const { isAuth } = useSelector((state) => state.authLocalStorage);

    // Handle expires
    handleExpires();

    // Check and refresh token
    useEffect(() => {
        const REFRESH_TOKEN = localStorage.getItem("refresh_token");

        if (!REFRESH_TOKEN) {
            handleLogout(dispatch, navigate);
        }

        if (!isAuth && REFRESH_TOKEN) {
            dispatch(handleAuthLocalStorageAsync({ REFRESH_TOKEN }))
                .unwrap()
                .then((originalPromiseResult) => {
                    console.log(originalPromiseResult);
                    dispatch(setIsAuth(true));
                })
                .catch((rejectedValueOrSerializedError) => {
                    console.log(rejectedValueOrSerializedError);
                    handleLogout(dispatch, navigate);
                });
        }

        if (isAuth && location.pathname === '/login') {
            navigate("/hbam");
        }

    }, [dispatch, navigate, isAuth, location.pathname]);

    return isAuth ? children : <FormLogin />;
};

export default AuthLocalStorageRedirect;
