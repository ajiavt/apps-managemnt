import { createSlice } from "@reduxjs/toolkit";
import { handleAuthLocalStorageAsync } from "./authLocalStorageAction";

const REFRESH_TOKEN = localStorage.getItem("refresh_token");

const initialState = {
    accessToken: '',
    refreshToken: '',
    isAuth: REFRESH_TOKEN ? true : false,
    isLoading: false,
    isSuccess: false,
    isError: false,
    error: null,
}

const authLocalStorageSlice = createSlice({
    name: 'authLocalStorage',
    initialState,
    reducers: {
      setIsAuth: (state, action) => {
        state.isAuth = action.payload;
      }
    },
    extraReducers: (builder) => {
        builder
          .addCase(handleAuthLocalStorageAsync.pending, (state) => {
            state.isLoading = true;
          })
          .addCase(handleAuthLocalStorageAsync.fulfilled, (state, action) => {
            state.isAuth = true;
            state.accessToken = action.payload.accessToken;
            state.refreshToken = action.payload.refreshToken;
            state.isSuccess = true;
            state.isLoading = false;
          })
          .addCase(handleAuthLocalStorageAsync.rejected, (state, action) => {
            state.isAuth = false;
            state.isLoading = false;
            state.isError = true;
            state.isSuccess = false;
            state.error = action.error;
          })
    },
});

export const { setIsAuth } = authLocalStorageSlice.actions;
export default authLocalStorageSlice.reducer;
