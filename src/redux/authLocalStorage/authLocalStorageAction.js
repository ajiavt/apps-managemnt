import { createAsyncThunk } from "@reduxjs/toolkit";
import { apiClient } from "../../app/axios";
import { apiURL } from "../../app/url";
import setLocalStorage from "../../utils/localStorages/setLocalStorage";

export const handleAuthLocalStorageAsync = createAsyncThunk('auth/handleAuth', async ({refreshToken}, {rejectWithValue}) => {

    const data = {"refresh_token": refreshToken, "grant_type": "refresh_token"};

    const response = await apiClient.post(
        apiURL.GET_TOKEN,
        data,
    )

    const formData = response?.data;
    formData['access_token'] = formData['access_token']
    formData['refresh_token'] = formData['refresh_token']
    setLocalStorage(formData);
    
    return {"accessToken": formData['access_token'], "refreshToken": formData['refresh_token']};
    
});