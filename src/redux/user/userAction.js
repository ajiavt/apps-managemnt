import { createAsyncThunk } from "@reduxjs/toolkit";
import { apiClientUserData } from "../../app/axios";
import { apiURL } from "../../app/url";

export const fetchUserInfoAsync = createAsyncThunk('user/fetchUserInfo', async (cookies, {rejectWithValue}) => {
    try {
        const token = cookies.accessToken;
        const response = await apiClientUserData.get(
            apiURL.GET_USER_INFO, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
            }
        )
        const responseData = response ? response?.data.data : null;
        return {userInfo : responseData};
    } catch (error) {
        console.log("Fetch User Info Error", error);
        rejectWithValue(error);
    }
});