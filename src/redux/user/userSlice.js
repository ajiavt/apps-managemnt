import { createSlice } from "@reduxjs/toolkit";
import { fetchUserInfoAsync } from "./userAction";

const initialState = { 
  userInfo: null,
  isLoading: false,
  isSuccess: false,
  isError: false,
}

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUserInfo: (state, action) => {
           state.userInfo = action.payload;
        },
    },
    extraReducers: (builder) => {
      builder
        .addCase(fetchUserInfoAsync.pending, (state) => {
          state.isLoading = true;
        })
        .addCase(fetchUserInfoAsync.fulfilled, (state, action) => {
          const userInfo = action && action.payload ? action.payload.userInfo : null;
          if(userInfo) {
            state.userInfo = userInfo;
            state.isSuccess = true;
          }
          state.isLoading = false;
        })
        .addCase(fetchUserInfoAsync.rejected, (state) => {
          state.isLoading = false;
          state.isError = true;
        })
    },
});

export const { setUserInfo } = userSlice.actions;
export default userSlice.reducer;