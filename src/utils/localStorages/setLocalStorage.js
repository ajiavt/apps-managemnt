const setLocalStorage = (response) => {
    const now = new Date();
    const accessExpires = 1000 * response['expires_in'];
    const refreshExpires = 1000 * response['expires_refresh_in'];
    const ttlAccess = now.getTime() + accessExpires;
    const ttlRefresh = now.getTime() + refreshExpires;

    try {
        localStorage.setItem("access_token", response['access_token'])
        localStorage.setItem("refresh_token", response['refresh_token'])
        localStorage.setItem("token_type", response['token_type'])
        localStorage.setItem("expires_in", ttlAccess)
        localStorage.setItem("expires_refresh_in", ttlRefresh)
    } catch(e) {
        console.log(e);
    }
    
}

export default setLocalStorage;