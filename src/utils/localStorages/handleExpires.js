const handleExpires = () => {
    const now = new Date();
    const ttlAccess = localStorage.getItem("expires_in");
    const ttlRefresh = localStorage.getItem("expires_refresh_in");
    if(ttlAccess < now.getTime()) localStorage.removeItem("access_token");
    if(ttlRefresh < now.getTime()) localStorage.removeItem("refresh_token");
}

export default handleExpires;