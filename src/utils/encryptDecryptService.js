import CryptoJS from 'crypto-js';

class Encryptor {
    static encrypt(value, key) {
        return CryptoJS.AES.encrypt(value, key).toString();
    }
}

class Decryptor {
    static decrypt(ciphertext, key) {
        try {
            const bytes = CryptoJS.AES.decrypt(ciphertext, key);
            const originalValue = bytes.toString(CryptoJS.enc.Utf8);
            return originalValue;
        } catch (error) {
            console.error("Failed to decrypt:", error);
        }
    }
}

export { Encryptor, Decryptor };