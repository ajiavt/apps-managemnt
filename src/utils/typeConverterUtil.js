
const convertAnyToBoolean = (anyValue) => {
    if (typeof anyValue === "string") {
        if (anyValue.toLowerCase() === "true") {
            return true
        }
    } else if (typeof anyValue == "boolean") {
        return anyValue
    } else if (typeof anyValue == "number") {
        if (anyValue === 1) {
            return true
        }
    } else {
        //throw new Error("tipe data tidak dapat dikonversi ke boolean")
    }
    //console.log(trueStr, typeof trueStr); // false "boolean"
    return false
}

const convertBoolToStr = (boolValue) => {
    return "" + boolValue
}

export {convertAnyToBoolean,convertBoolToStr}