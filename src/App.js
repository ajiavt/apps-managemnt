import React, { lazy, useEffect } from 'react'
import './App.css';
import { MemoryRouter as Router, Route, Routes, } from 'react-router-dom'
import { themeChange } from 'theme-change'
import handleExpires from './utils/localStorages/handleExpires';
import initializeApp from './app/init';
import AuthLocalStorageRedirect from './redux/authLocalStorage/authLocalStorageRedirect';

// Importing our own forms
const Layout = lazy(() => import('./containers/Layout'))
const FormLogin = lazy(() => import('./form/FormLogin/FormLogin'))


// Initializing different libraries
initializeApp()

function App() {

  handleExpires();

  useEffect(() => {
    themeChange(false)
  }, [])


  return (
    <>
      <Router>
        <Routes>
          <Route path="/login" element={
            <AuthLocalStorageRedirect>
              <FormLogin />
            </AuthLocalStorageRedirect>
          } />

          <Route path="*" element={
            <AuthLocalStorageRedirect>
              <Layout />
            </AuthLocalStorageRedirect>
          }/>
        </Routes>
      </Router>
    </>
  )
}

export default App
