'use strict'

import React from 'react'
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color'
import FormLabel from "@mui/joy/FormLabel";
import FormControl from "@mui/joy/FormControl";

export default class SketchExample extends React.Component {
    state = {
        displayColorPicker: false,
        color: {
            r: '234',
            g: '234',
            b: '234',
            a: '1',
        },
    };

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleClose = () => {
        this.setState({ displayColorPicker: false })
    };

    handleChange = (color) => {
        this.setState({ color: color.rgb }, () => {
            if (this.props.onChange) {
                this.props.onChange(`rgba(${color.rgb.r}, ${color.rgb.g}, ${color.rgb.b}, ${color.rgb.a})`);
            }
        });
    };;

    render() {
        const styles = reactCSS({
            'default': {
                color: {
                    width: '100%',
                    height: '24px',
                    borderRadius: '5px',
                    background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
                },
                swatch: {
                    padding: '5px',
                    background: '#fff',
                    borderRadius: '5px',
                    boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                    display: 'inline-block',
                    cursor: 'pointer',
                },
                popover: {
                    position: 'absolute',
                    zIndex: '2',
                },
                cover: {
                    position: 'fixed',
                    top: '0px',
                    right: '0px',
                    bottom: '0px',
                    left: '0px',
                },
            },
        });

        const { className, label, disabled, hidden } = this.props; // Menambahkan props className dan label

        return (
            <div className={hidden ? className + " hidden" : className}>
                <FormControl disabled={disabled}>
                    <FormLabel>
                    <span className="mb-[-5px] text-[14px] font-Poppins">
                        {label}
                    </span>
                    </FormLabel>

                    <div style={ styles.swatch } onClick={ disabled ? null : this.handleClick }>
                        <div style={ styles.color } />
                    </div>

                    {
                        this.state.displayColorPicker && !disabled ?
                            <div style={ styles.popover }>
                                <div style={ styles.cover } onClick={ this.handleClose }/>
                                <SketchPicker color={ this.state.color } onChange={ this.handleChange } />
                            </div>
                            : null
                    }
                </FormControl>
            </div>
        )
    }
}
