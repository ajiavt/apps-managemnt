import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import LinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

function LinearProgressWithLabel(props) {
    return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Box sx={{ width: '100%', mr: 1 }}>
                <LinearProgress variant="determinate" {...props} />
            </Box>
            <Box sx={{ minWidth: 35 }}>
                <Typography variant="body2" color="text.secondary">{`${Math.round(
                    props.value,
                )}%`}</Typography>
            </Box>
        </Box>
    );
}

LinearProgressWithLabel.propTypes = {
    value: PropTypes.number.isRequired,
};

export default function LinearWithValueLabel() {
    const [progress, setProgress] = useState(10);

    useEffect(() => {
        let elapsedTime = 0;
        let reached50Percent = false;

        const timer = setInterval(() => {
            elapsedTime += 1200; // Increment elapsed time by the interval time

            // Set progress to around 50% after 3 seconds
            if (!reached50Percent && elapsedTime >= 3000) {
                const randomIncrement = (Math.random() * 10) + 50; // Random value between 50 and 60
                setProgress(randomIncrement);
                reached50Percent = true;
            } else {
                setProgress((prevProgress) => {
                    const randomIncrement = Math.random() * 10;
                    const newProgress = prevProgress + randomIncrement;
                    return newProgress >= 95 ? 100 : newProgress;
                });
            }
        }, 1200);

        const fetchData = async () => {
            try {
                await new Promise((resolve) => setTimeout(resolve, 2000));
            } catch (error) {
                console.error("Error fetching data:", error);
            }
        };

        fetchData();

        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <Box sx={{ width: '100%' }}>
            <LinearProgressWithLabel value={progress} />
        </Box>
    );
}
