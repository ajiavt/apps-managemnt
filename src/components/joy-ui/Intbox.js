import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import FormHelperText from '@mui/joy/FormHelperText';
import Input from '@mui/joy/Input';
import Tooltip from '@mui/joy/Tooltip';
import {useEffect, useState} from 'react';
import {QuestionMarkCircleIcon} from "@heroicons/react/24/outline";
import Alert from '@mui/joy/Alert';
import ExclamationTriangleIcon from "@heroicons/react/24/outline/ExclamationTriangleIcon";

const animateShowError = `
    @keyframes slide-down {
        from {height: 0}
        to {height: 25px}
    }
    
    .slide-down {
        animation-name: slide-down;
        animation-duration: 1s;      
    }
`;

function Intbox({id
        , name
        , label
        , width
        , mandatory
        , disabled
        , maxLength
        , startWith
        , endWith
        , placeHolder
        , value: propValue
        , tooltip
        , error
        , readOnly
        , autoFocus
        , allowNegative = false
        , onChange
        , onBlur
        , hidden
        , className = ""
        , span
        , inClassName
        , inStyle}) {

    const [inputValue, setInputValue] = useState('');
    const [showError, setShowError] = useState(true);

    useEffect(() => {
        setInputValue(propValue !== undefined ?
            String(propValue)
            : ''
        );
    }, [propValue]);

    const handleInputChange = (event) => {
        if (!allowNegative) {
            event.target.value = Math.max(0, parseInt(event.target.value));
        }

        let newValue = event.target.value;

        // Fungsi Maxlength
        if(maxLength && newValue.length > maxLength) {
            newValue = newValue.slice(0, maxLength);
        }

        setInputValue(newValue);

        // Panggil fungsi onChange jika ada
        if (onChange) {
            const newEvent = {...event};  // Buat salinan event
            newEvent.target.value = newValue;  // Ganti nilai dengan nilai baru yang telah dipotong

            onChange(newEvent);  // Panggil fungsi onChange dengan event yang telah dimodifikasi
        }
    };

    // TODO bug nilai tidak realtime terupdate
    const handleInputBlur = (event) => {
        if (onBlur) {
            const newEvent = {...event}
            newEvent.target.value = inputValue
            onBlur(newEvent)
        }
    }

    return(
        <>
            <div className={hidden ? "hidden" : className + (span ?` col-span-${span} ` : '')}>
                {/*Inline CSS*/}
                <style>{animateShowError}</style>

                {
                    tooltip ?
                        <div className="relative">
                            <div className="absolute right-0 z-10">
                                <Tooltip placement="left-start" title={tooltip} variant="solid">
                                    <QuestionMarkCircleIcon color="rgba(0, 0, 0, 0.5)" className="inline-block h-[16px] w-[16px] mb-[5px]"/>
                                </Tooltip>
                            </div>
                        </div>
                        : null
                }

                <FormControl
                    size="sm"
                    color="neutral"
                    id={id}
                    required={mandatory}
                    disabled={disabled}
                    className="mb-[8px]"
                    error={error}
                >
                    <FormLabel>
                    <span className="text-[14px] font-Poppins">
                        {label}
                    </span>
                    </FormLabel>

                    <Input
                        type="number"
                        variant="outlined"
                        className={"-mt-[6.5px] z-20 " + inClassName}
                        style={inStyle}
                        autoComplete="off"
                        autoCorrect="false"
                        spellCheck={false}
                        readOnly={readOnly}
                        autoFocus={autoFocus}
                        name={name}
                        sx={!error ?
                            {
                                maxWidth: width,
                                fontSize: "16px",

                                // BORDER COLOR
                                '--Input-focusedHighlight': 'rgba(87,13,248,.25)',
                                '&::before': {transition: 'box-shadow .15s ease-in-out',},
                                '&:focus-within': {borderColor: '#86b7fe',},
                            } : {
                                maxWidth: width,
                                fontSize: "16px",
                            }}

                        startDecorator={startWith}
                        endDecorator={endWith}
                        value={propValue?propValue:inputValue}
                        onChange={handleInputChange}
                        placeholder={placeHolder}
                    />

                    {error && (
                        <FormHelperText className={`${showError ? 'slide-down' : ''}`}>
                            <Alert
                                startDecorator={<ExclamationTriangleIcon className="w-5 h-5 pt-[3px]" />}
                                variant="soft"
                                color="danger"
                                className="min-w-full -mt-[12px] z-1"
                                size="sm"
                            >
                                <span className="pt-[3px]">{error}</span>
                            </Alert>
                        </FormHelperText>
                    )}
                </FormControl>
            </div>
        </>
    )
}

export default Intbox