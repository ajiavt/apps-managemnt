import * as React from 'react';
import Select from '@mui/joy/Select';
import Option, { optionClasses } from '@mui/joy/Option';
import List from '@mui/joy/List';
import ListItem from '@mui/joy/ListItem';
import Typography from '@mui/joy/Typography';
import Tooltip from "@mui/joy/Tooltip";
import {QuestionMarkCircleIcon} from "@heroicons/react/24/outline";
import FormLabel from "@mui/joy/FormLabel";
import FormHelperText from "@mui/joy/FormHelperText";
import Alert from "@mui/joy/Alert";
import ExclamationTriangleIcon from "@heroicons/react/24/outline/ExclamationTriangleIcon";
import {useEffect, useState} from "react";
import FormControl from '@mui/joy/FormControl';

const animateShowError = `
    @keyframes slide-down {
        from {height: 0}
        to {height: 25px}
    }
    
    .slide-down {
        animation-name: slide-down;
        animation-duration: 1s;      
    }
`;

export default function Selectbox({datas = {}
                , placeHolder=<span className={"italic"}>-- Silahkan Pilih --</span>
                , hidden
                , tooltip
                , id
                , mandatory
                , disabled
                , error
                , label
                , value: propValue
                , name
                , onChange
                , idKey
                , nameKey
                , className
                , inputValue
                , setInputValue
                , span
                , slClassName
                , showValueInOption = true
                }) {

    const [showError, setShowError] = useState(true);

    useEffect(() => {
        setInputValue(propValue || '');
    }, [propValue]);

    const handleInputChange = (event, child) => {
        if(child) {
            setInputValue(child);
            onChange?.(child);
        }
    };

    return (
        <>
            <div className={className + (span ?` col-span-${span} ` : '')}>
                <div className={hidden ? "hidden" : ""}>
                    {/*Inline CSS*/}
                    <style>{animateShowError}</style>

                    {
                        tooltip ?
                            <div className="relative">
                                <div className="absolute right-0 z-10">
                                    <Tooltip placement="left-start" title={tooltip} variant="solid">
                                        <QuestionMarkCircleIcon color="rgba(0, 0, 0, 0.5)" className="inline-block h-[16px] w-[16px] mb-[5px]"/>
                                    </Tooltip>
                                </div>
                            </div>
                            : null
                    }

                    <FormControl
                        color="neutral"
                        id={id}
                        required={mandatory}
                        disabled={disabled}
                        className="mb-[8px]"
                        error={error}
                    >
                        <FormLabel>
                        <span className="text-[14px] font-Poppins">
                            {label}
                        </span>
                        </FormLabel>

                        <Select
                            className={"-mt-[6.5px] z-20 " + slClassName}
                            placeholder={placeHolder}
                            slotProps={{
                                listbox: {
                                    component: 'div',
                                    sx: {
                                        maxHeight: 240,
                                        overflow: 'auto',
                                        '--List-padding': '0px',
                                        '--ListItem-radius': '0px',
                                    },
                                },
                            }}
                            name={name}
                            value={inputValue}
                            onChange={handleInputChange}
                        >
                            {datas && Object.entries(datas).map(([parentName, children], index) => (
                                <React.Fragment key={`${parentName}-${index}`}>
                                    <List
                                        aria-labelledby={`select-group-${parentName}`}
                                        sx={{ '--ListItemDecorator-size': '28px' }}
                                    >
                                        <ListItem sx={{marginTop: "-2px", marginBottom: "2px"}} id={`select-group-${parentName}`} sticky>
                                            <Typography level="body-xs" textTransform="uppercase">
                                                {/** Placeholder Listitem **/}
                                                {parentName} ({(children || []).length})
                                            </Typography>
                                        </ListItem>

                                        {(children || []).map((child, childIndex) => (
                                            <Option
                                                key={`${child[idKey]}-${childIndex}`}
                                                value={child[idKey]}
                                                label={
                                                    (nameKey !== '') ?
                                                        showValueInOption ? `${child[idKey]} - ${child[nameKey]}` : `${child[nameKey]}`
                                                        : `${child[idKey]}`
                                                }
                                                sx={{
                                                    '& .JoySelect-root': {
                                                        fontSize: "16px",
                                                        fontFamily: "Poppins",
                                                    },
                                                    [`&.${optionClasses.selected}`]: {
                                                        opacity: 1,
                                                    },
                                                }}
                                            >
                                                {/** Isian Listitem **/}
                                                {(nameKey !== '') ?
                                                    showValueInOption ? `${child[idKey]} - ${child[nameKey]}` : `${child[nameKey]}`
                                                    : `${child[idKey]}`}
                                            </Option>
                                        ))}
                                    </List>
                                </React.Fragment>
                            ))}
                        </Select>
                        {error && (
                            <FormHelperText className={`${showError ? 'slide-down' : ''}`}>
                                <Alert
                                    startDecorator={<ExclamationTriangleIcon className="w-5 h-5 pt-[3px]" />}
                                    variant="soft"
                                    color="danger"
                                    className="min-w-full -mt-[12px] z-1"
                                    size="sm"
                                >
                                    <span className="pt-[3px]">{error}</span>
                                </Alert>
                            </FormHelperText>
                        )}
                    </FormControl>
                </div>
            </div>
        </>
    );
}
