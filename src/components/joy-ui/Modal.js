import * as React from 'react';
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import Modal from '@mui/joy/Modal';
import ModalDialog from '@mui/joy/ModalDialog';
import Typography from '@mui/joy/Typography';
import {ModalOverflow} from "@mui/joy";

export default function ResponsiveModal
    ({
         title,
         desc,
         open,
         yesAction,
         noAction,
         color,
         children,
         yesLabel='Ya',
         noLabel='Tidak',
         width,
         enableCenteredButton = false,
         loadingState = false
     }) {

    return (
        <React.Fragment>
            <Modal open={open} onClose={noAction}>
                <ModalOverflow>
                    <ModalDialog
                        aria-labelledby="nested-modal-title"
                        aria-describedby="nested-modal-description"
                        sx={(theme) => ({
                            [theme.breakpoints.only('xs')]: {
                                top: 'unset',
                                bottom: 0,
                                left: 0,
                                right: 0,
                                borderRadius: 0,
                                transform: 'none',
                                maxWidth: 'unset',
                            },
                        })}
                        style={{
                            width: width
                        }}
                    >
                        {title &&(
                            <Typography id="nested-modal-title" level="h2">
                                {title}
                            </Typography>
                        )}

                        {desc && (
                            <Typography id="nested-modal-description" textColor="text.tertiary">
                                {desc}
                            </Typography>
                        )}

                        {children ?
                            (children) : (null)
                        }

                        <Box
                            sx={{
                                mt: 1,
                                display: 'flex',
                                gap: 1,
                                flexDirection: { xs: 'column', sm: 'row-reverse' },
                            }}
                            className={`w-full ${enableCenteredButton ? "justify-center items-center text-center" : ""}`}
                        >
                            <Button
                                sx={{ width: "100px" }} variant={color === "primary" ? "solid" : "outlined"} color={color}
                                onClick={yesAction}
                                loading={loadingState}
                            >
                                {yesLabel}
                            </Button>
                            <Button
                                sx={{ width: "100px" }} variant="outlined" color ="neutral"
                                onClick={noAction}
                                disabled={loadingState}
                            >
                                {noLabel}
                            </Button>
                        </Box>
                    </ ModalDialog>
                </ModalOverflow>
            </ Modal>
        </ React.Fragment >
    );
}