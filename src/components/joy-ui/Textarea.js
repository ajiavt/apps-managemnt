import Textbox from './Textbox'; // Import komponen Textbox

function Textarea(props) {
    // Anda dapat mewarisi prop dari Textbox dengan menyebar operator (...)
    return <Textbox {...props} textarea={true} />;
}

export default Textarea; // Export komponen Textarea