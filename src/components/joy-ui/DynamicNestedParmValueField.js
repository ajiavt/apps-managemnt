import React, {useCallback, useEffect, useState} from "react";
import TitleCard from "../Cards/TitleCard";
import Card from "@mui/joy/Card";
import Selectbox from "./Selectbox";
import Intbox from "./Intbox";
import Textarea from "./Textarea";
import Button from "@mui/joy/Button";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import {Add} from "@mui/icons-material";
import Textbox from "./Textbox";
import ParmValueField from "./ParmValueField";
import {IconButton} from "@mui/joy";
import CancelIcon from "@mui/icons-material/Close";
import Box from "@mui/joy/Box";
import {showNotification} from "../../features/common/headerSlice";
import {useDispatch} from "react-redux";
import ResponsiveModal from "./Modal";

const DynamicNestedParmValueField = ({
                                         disabled = true,
                                         typeData, parmValue,
                                         detailedSelectedData,
                                         items = [],
                                         setItems,
                                         setParmRefs,
                                         onUpdateValue, // deprecated
                                         onUpdateRefs // deprecated
}) => {
    const dispatch = useDispatch();

    // const [items, setItems] = useState([])

    const [selectedDetailType, setSelectedDetailType] = useState('') // untuk non-json
    const [isTypeDisabled, setIsTypeDisabled] = useState(true)
    const [mapArrayType] = useState({
        "array_string": "string",
        "array_integer": "integer",
        "array_boolean": "boolean",
        "array_decimal": "decimal",
        "array_json": "json"
    })
    const [detailTypes] = useState({
        'Daftar Tipe Data': [
            { id: 'string', name: 'String' },
            { id: 'integer', name: 'Integer' },
            { id: 'decimal', name: 'Decimal' },
            { id: 'boolean', name: 'Boolean' },
            { id: 'json', name: 'JSON' }
        ]
    })
    const [jsonTypes] = useState({
        'Tipe Data Properti': [
            { id: 'string', name: 'String' },
            { id: 'integer', name: 'Integer' },
            { id: 'decimal', name: 'Decimal' },
            { id: 'boolean', name: 'Boolean' }
        ]
    })

    const [showModalRecordDelete, setShowModalRecordDelete] = useState(false)
    const [showModalPropDelete, setShowModalPropDelete] = useState(false);
    const [showModalPropChange, setShowModalPropChange] = useState(false)
    const [selectedRecord, setSelectedRecord] = useState({ parentIndex: -1, deleteMsg: '' })
    const [selectedProp, setSelectedProp] = useState({parentIndex: -1, childIndex: -1})
    const [cfgParmReffs, setCfgParmReffs] = useState([])

    /** ********************* effects ********************* */
    // mapping `parmValue` existing from parent-form to internal `items`
    useEffect(() => {
        const mapType = {
            "array_string": "string",
            "array_integer": "integer",
            "array_decimal": "decimal",
            "array_json": "json"
        }
        const mappedType = mapType[typeData]
        setSelectedDetailType(mappedType)

        let parsedJson = parmValue ? JSON.parse(parmValue) : []
        if (Array.isArray(parsedJson)) {
            if (['array_string', 'array_integer', 'array_decimal'].includes(typeData)) {
                let tmpItems = []
                for (let x of parsedJson) {
                    tmpItems.push({ detailType: mappedType, value: x, isNew: false})
                }
                setItems(tmpItems)
            } else if (['array_json'].includes(typeData)) {
                let tmpItems = []

                for (let x of parsedJson) {
                    let subDetails = []
                    if (typeof x === 'object') {
                        Object.entries(x).forEach(kv => {
                            subDetails.push({ key: kv[0], value: kv[1], propType: '', isNew: false })
                        });
                    }

                    tmpItems.push({ detailType: 'json', value: x, isNew: false, subDetails: subDetails })
                }

                setItems(tmpItems)
            }
        }
    }, [parmValue])

    // mapping extracted param refs (detailedSelectedData.cfgParmReffs) to internal.
    useEffect(() => {
        if (typeData !== "array_json") {
            return
        }

        let selectedParamRef = []
        if (detailedSelectedData?.cfgParmReffs && detailedSelectedData.cfgParmReffs.length > 0) {
            selectedParamRef = [...detailedSelectedData.cfgParmReffs]
        }

        const parmRefMap = new Map()
        if (selectedParamRef.length > 0) {
            for (let eachRef of selectedParamRef) {
                parmRefMap.set(eachRef.key, { type: eachRef.typeData, key: eachRef.key, seq: eachRef?.seq, desc: eachRef.descr });
            }
        }

        // isi tipe data properti
        for (let item of items) {
            if (item.subDetails && Array.isArray(item.subDetails) && item.subDetails.length > 0) {
                let updatedPropItems = []
                for (let propItem of item.subDetails) {
                    const type = parmRefMap.has(propItem.key) ? parmRefMap.get(propItem.key)['type'] : 'string'
                    updatedPropItems.push({ ...propItem, propType: type })
                }

                item.subDetails = updatedPropItems
            }
        }

        setCfgParmReffs(selectedParamRef)
    }, [detailedSelectedData])

    // 🧪 TODO syncing internal `param refs` change to parent
    useEffect(() => {
        if (setParmRefs) {
            setParmRefs(cfgParmReffs)
        }
    }, [cfgParmReffs])
    /** ********************* ./effects ********************* */

    /** ********************* event handlers  ********************* */
    const handleClickAddDetail = useCallback((data) => {
        const detailType = mapArrayType[typeData]
        setSelectedDetailType(selectedDetailType)

        // setup props (json) by paramRef
        let subDetailsTemplate = []
        if (detailType === 'json') {
            if (cfgParmReffs && cfgParmReffs.length > 0) {
                cfgParmReffs.forEach((v, i) => {
                    subDetailsTemplate.push({ propType: v.typeData, key: v.key, value: '', isNew: true })
                })
            }
        }

        setItems([...items, { detailType: selectedDetailType ? selectedDetailType : detailType, value: '', subDetails: subDetailsTemplate, isNew: true}])
    }, [items]);

    const handleClickDeleteDetail = (index) => { // useCallback((index) => {
        const delMsg = 'Apakah Anda yakin ingin menghapus data record parameter ?'
        setSelectedRecord({ ...selectedRecord, parentIndex: index, deleteMsg: delMsg })
        setShowModalRecordDelete(true)
    } // , [items])

    // JSON key-values
    const handleClickAddSubDetail = useCallback((parentIndex) => {
        let updatedItems = [...items]
        updatedItems[parentIndex]['subDetails'].push({ propType: '', key: '', value: '', isNew: true })

        // update cfgParmReffs
        if (parentIndex === 0) {
            let updateParmRefs = []
            updatedItems[parentIndex]['subDetails'].forEach((v, i) => {
                if (v?.propType) {
                    const seqOneBased = i + 1
                    updateParmRefs.push({ typeData: v.propType, key: v.key, descr: v.key, seq: seqOneBased})
                }
            })

            setCfgParmReffs(updateParmRefs)
        }

        setItems(updatedItems)
    }, [items]);

    const handleClickDeleteSubDetail = useCallback((parentIndex, childIndex) => {
        if (parentIndex !== 0) {
            dispatch(showNotification({message : `tidak dapat menghapus properti JSON apabila bukan baris record pertama!`, status : 0}));
            return
        }

        setSelectedProp({ parentIndex: parentIndex, childIndex: childIndex })
        setShowModalPropDelete(true)
    }, [items])

    // TODO test fungsi updateParmRefsByUpdatedProp agar tidak rebuild paramref saat hanya 1 properti terpengaruh
    const handleChangeSubDetailPropType = useCallback((parentIndex, childIndex, typeValue) => {
        const updatedItems = [...items]

        let updatedProp = {}
        if (updatedItems[parentIndex]['subDetails'][childIndex]) {
            updatedItems[parentIndex]['subDetails'][childIndex]['propType'] = typeValue

            updatedProp = updatedItems[parentIndex]['subDetails'][childIndex]
        }

        if (parentIndex === 0) {
            updateParmRefsByUpdatedProp(updatedItems[parentIndex]['subDetails'], childIndex, updatedProp) // updateParmRefs(updatedItems[parentIndex]['subDetails']) // <--- OLD IMPLEMENTATION
        }
        setItems(updatedItems)
    }, [items])

    const handleChangePropKey = (parentIndex, childIndex, key) => {
        setSelectedProp({
            ...selectedProp, parentIndex: parentIndex, childIndex: childIndex,
            editingProp: { newKey: key }
        })

        const updatedItems = [ ...items ]

        // TODO test add new prop cascade
        // change key cascade to other props
        if (updatedItems[parentIndex]['subDetails'][childIndex]) {
            updatedItems[parentIndex]['subDetails'][childIndex]['key'] = key
        }
        const propAcuan = updatedItems[parentIndex]['subDetails'][childIndex]
        updatedItems.forEach((v, i) => {
            if (!(v.subDetails && Array.isArray(v.subDetails) && v.subDetails.length > 0))
                return false

            if (i == 0) {
                return false
            }

            if (Array.isArray(v.subDetails) && v.subDetails.length > 0) {
                if (v.subDetails[childIndex]) {
                    v.subDetails[childIndex]['key'] = propAcuan.key
                } else {
                    v.subDetails.push({...propAcuan})
                }
            }

            return true
        })

        if (parentIndex === 0) {
            updateParmRefs(updatedItems[parentIndex]['subDetails'])
        }
        setItems(updatedItems)
    }

    const handleLeaveChangePropKey = (parentIndex, childIndex, newKey) => {
        const oldKey = items[parentIndex]['subDetails'][childIndex]?.key

        const isNew = items[parentIndex]['subDetails'][childIndex]?.isNew
        if (isNew) {
            handleChangePropKey(parentIndex, childIndex, newKey)
            return;
        }


        if (newKey === oldKey) {
            return
        }

        const msg = 'Penyesuaian key pada properti JSON ini akan mempengaruhi record lain. \nApakah Anda yakin ingin mengubah key properti ?'
        setSelectedProp({
            ...selectedProp, parentIndex: parentIndex, childIndex: childIndex,
            changeMsg: msg,
            editingProp: { newKey: newKey, oldKey: oldKey }
        })
        setShowModalPropChange(true)
    }

    const handleConfirmChangePropKey = () => {
        const parentIndex = selectedProp?.parentIndex
        const childIndex = selectedProp?.childIndex
        const newKey = selectedProp?.editingProp?.newKey

        const updatedItems = [ ...items ]

        // isi nilai key baru dengan object key lama, lalu hapus key lama
        updatedItems.forEach((v, i) => {
            if (!(v.subDetails && Array.isArray(v.subDetails) && v.subDetails.length > 0)) {
                return false
            }

            v.subDetails[childIndex]['key'] = newKey

            return true
        })

        if (parentIndex === 0) {
            updateParmRefs(updatedItems[parentIndex]['subDetails'])
        }

        setItems(updatedItems)
        resetSelectedProp()
    }

    const handleCancelChangePropKey = () => {
        const parentIndex = selectedProp?.parentIndex
        const childIndex = selectedProp?.childIndex
        const newKey = selectedProp?.editingProp?.newKey
        const oldKey = selectedProp?.editingProp?.oldKey

        // revert to previous value
        const updatedItems = [...items]
        updatedItems[parentIndex]['subDetails'][childIndex]['key'] = oldKey
        setItems(updatedItems)

        resetSelectedProp()
    }

    const handleChangePropValue = (parentIndex, childIndex, value) => {
        const updatedItems = [ ...items ]
        if (updatedItems[parentIndex]['subDetails'][childIndex]) {
            updatedItems[parentIndex]['subDetails'][childIndex]['value'] = value
        }
        setItems(updatedItems)
    }

    const handleConfirmPropDelete = async (e) => {
        const updatedItems = [...items]

        // ✅ TODO delete cascade same prop on another records
        updatedItems.forEach((v) => {
            if (!(v.subDetails && Array.isArray(v.subDetails) && v.subDetails.length > 0)) {
                return false
            }

            v.subDetails.splice(selectedProp.childIndex, 1)
            return true
        })

        setItems(updatedItems)
        updateParmRefs(updatedItems[selectedProp.parentIndex]['subDetails'])

        setShowModalPropDelete(false)
        setSelectedProp({ parentIndex: -1, childIndex: -1})
    }

    const handleConfirmRecordDelete = async (e) => {
        const updatedItems = [...items];
        updatedItems.splice(selectedRecord.parentIndex, 1)
        setItems(updatedItems)

        setShowModalRecordDelete(false)
        setSelectedRecord({ parentIndex: -1 })
    }

    /** ********************* ./event handlers  ********************* */

    /** ********************* utility methods  ********************* */
    const resetSelectedProp = () => {
        setSelectedProp({ parentIndex: -1, childIndex: -1, changeMsg: 'Yakin ubah properti ?', editingProp: { } })
        setShowModalPropChange(false)
        setShowModalPropDelete(false)
    }

    const updateItemByIndex = (array, index, newValue) => {
        return array.map((value, i) => {
            if (i === index) return newValue;

            return value;
        })
    }

    // update by rebuild
    const updateParmRefs = (subDetails) => {
        let updateParmRefs = []
        subDetails.forEach((v, i) => {
            if (v?.propType || v?.typeData) { // TODO setting penamaan field yg sama di semua proses
                const seqOneBased = i + 1
                updateParmRefs.push({
                    typeData: v?.propType ? v.propType : v.typeData,
                    key: v.key,
                    descr: v.key,
                    seq: seqOneBased})
            }
        })
        setCfgParmReffs(updateParmRefs)
        return updateParmRefs
    }

    // update by one updated prop
    const updateParmRefsByUpdatedProp = (propItems, propIndex, updatedProp) => {
        if (Array.isArray(propItems) && propItems[propIndex]) { // let propItems = items?.subDetails
            let updateCfgParmReffs = [...cfgParmReffs]

            updateCfgParmReffs[propIndex] = {
                ...updateCfgParmReffs[propIndex],
                typeData:  updatedProp.propType ? updatedProp.propType : updatedProp.typeData, // note: setting penamaan field yg sama di semua proses
                key: updatedProp.key,
                descr: updatedProp.key,
            }

            setCfgParmReffs(updateParmRefs)
        }
    }
    /** ********************* ./utility methods  ********************* */


    // returning view by typeData
    if (['array_boolean','array_integer','array_decimal', 'array_string'].includes(typeData)) {
        // <Card className="grid grid-cols-12 gap-x-1 gap-y-1 mt-1.5" variant="outlined">...child comps here...</Card>
        return <>
            <TitleCard>
            <span color="info" className="col-span-12">Total Detail Parameter ({items.length})</span>
            <div className="grid grid-cols-12 gap-x-1 gap-y-1 h-[380px] overflow-y-auto">
                {items.map((item, index) => (
                    <Card key={"parmDtl_"+index} className="rounded col-span-12 pb-0" size="sm">
                        <div className="grid grid-cols-12 pb-1 pt-1 gap-x-1">
                            <Selectbox
                                label="Tipe Data"
                                className="col-span-3"
                                idKey="id"
                                nameKey="name"
                                disabled={disabled || isTypeDisabled}
                                showValueInOption={false}
                                datas={detailTypes}
                                inputValue={item?.detailType ? item.detailType : selectedDetailType}
                                setInputValue={setSelectedDetailType}
                                onChange={(eventValue) => {}}
                            />

                            <div className="col-span-7">
                                { (item?.detailType === "integer") ? (
                                    <div>
                                        <Intbox
                                            label="Nilai Parameter Integer"
                                            value={item?.value}
                                            disabled={disabled}
                                            onChange={(e) => {
                                                const rawInt = parseInt(e.target.value)
                                                const updatedItems = updateItemByIndex(items, index, {
                                                    ...items[index],
                                                    value: rawInt
                                                });

                                                setItems(updatedItems)
                                            }}
                                        />
                                    </div>
                                ) : (item?.detailType === "string") ? (
                                    <Textarea
                                        disabled={disabled}
                                        label="Nilai Parameter String"
                                        mandatory
                                        id="txtParmValueStr"
                                        placeHolder="Isi nilai teks"
                                        tooltip="Isi nilai dengan teks"
                                        value={item?.value}
                                        onChange={(e) => {
                                            const updatedItems = updateItemByIndex(items, index, {
                                                ...items[index],
                                                value: e.target.value
                                            });
                                            setItems(updatedItems)
                                        }}
                                    />
                                ) : (
                                    <div>Tipe data belum dipilih</div>
                                ) }
                            </div>

                            <div className="col-span-2 h-full flex items-center justify-center align-middle">
                                <Button
                                    disabled={disabled}
                                    variant="outlined"
                                    color="danger"
                                    onClick={(e) => {
                                        handleClickDeleteDetail(index)
                                    }}
                                    size="sm" startDecorator={<DeleteIcon />}
                                />
                            </div>
                        </div>
                    </Card>
                ))}
            </div>
            <div className="col-span-12">
                <Button
                    size="sm" color="neutral" variant="outlined" startDecorator={<Add />}
                    disabled={disabled}
                    onClick={handleClickAddDetail}
                >
                    Tambah Detail
                </Button>
            </div>
            {/*</div>*/}
        </TitleCard>
            <ResponsiveModal
                key="modalDelRecord"
                color={"danger"}
                open={showModalRecordDelete}
                title="Konfirmasi"
                desc={selectedRecord?.deleteMsg ? selectedRecord?.deleteMsg : 'Yakin hapus record ?'}
                yesAction={handleConfirmRecordDelete}
                noAction={() => setShowModalRecordDelete(false)}
            />
        </>
    } else if (['array_json'].includes(typeData)) {
        return <>
            <TitleCard>
                <span color="info" className="col-span-12">Total Record Parameter  ({items.length})</span>
                <div className="grid grid-cols-12 gap-x-1 gap-y-1 h-[380px] overflow-y-auto">
                    {items.map((item, index) => (
                        <Card key={"parmDtlJson_"+index} className="rounded col-span-12 pb-0" size="sm">
                            <div className="grid grid-cols-12 pb-1 pt-1 gap-x-1">
                                <Selectbox
                                    className="col-span-2"
                                    datas={detailTypes}
                                    inputValue={item?.detailType ? item.detailType : selectedDetailType}
                                    setInputValue={setSelectedDetailType}
                                    disabled={isTypeDisabled}
                                    idKey="id"
                                    nameKey="name"
                                    label="Tipe Data"
                                    showValueInOption={false}
                                    onChange={(eventValue) => {}}
                                />
                                <div className="col-span-9">
                                    { (item?.detailType === "json") ? (
                                        <div className="grid grid-cols-12 mb-2 gap-x-0.5 gap-y-0.5">
                                            <div className="col-span-12">Properti Key-values JSON ({item?.subDetails?.length})</div>
                                            <div className="col-span-12">
                                                {item?.subDetails.map((subitem, subindex) => ( // nested loop for sub details json
                                                    <Card key={`dtljson_${index}_${subindex}`} size="sm">
                                                        <div className="grid grid-cols-12 pb-1 mt-2.5 pt-1 gap-x-0.5 gap-y-1">
                                                            <Selectbox
                                                                className="col-span-2"
                                                                disabled={disabled || index !== 0}
                                                                datas={jsonTypes}
                                                                inputValue={subitem?.propType}
                                                                setInputValue={(newState) => {}}
                                                                idKey="id"
                                                                nameKey="name"
                                                                label="Tipe Data"
                                                                showValueInOption={false}
                                                                onChange={(eventValue) => {
                                                                    handleChangeSubDetailPropType(index, subindex, eventValue)
                                                                }}
                                                            />
                                                            <div className="col-span-4">
                                                                <Textbox
                                                                    maxLength={30}
                                                                    mandatory
                                                                    key={`dtljson_${index}_${subindex}_key`}
                                                                    autoFocus={!disabled}
                                                                    disabled={disabled || index !== 0}
                                                                    label="Key"
                                                                    value={subitem?.key}
                                                                    onBlur={(e) => {
                                                                        handleLeaveChangePropKey(index, subindex, e.target.value)
                                                                    }}
                                                                    onChange={(e) => {
                                                                        //handleChangePropKey(index, subindex, e.target.value)
                                                                    }}
                                                                />

                                                            </div>
                                                            <div className="col-span-5">
                                                                <ParmValueField
                                                                    typeData={subitem?.propType}
                                                                    disabled={disabled}
                                                                    currentValue={subitem.value}
                                                                    onUpdateValue={(rawValue) => {
                                                                        handleChangePropValue(index, subindex, rawValue)
                                                                    }
                                                                    }
                                                                />
                                                            </div>
                                                            <div className="col-span-1 h-full flex items-center justify-center align-middle">
                                                                <IconButton
                                                                    disabled={disabled || index !== 0}
                                                                    variant="outlined" size="xs" color="danger"
                                                                    onClick={(e) => {
                                                                        handleClickDeleteSubDetail(index, subindex)
                                                                    }}
                                                                >
                                                                    <CancelIcon />
                                                                </IconButton>
                                                            </div>
                                                        </div>
                                                    </Card>
                                                ))}
                                            </div>
                                            <div className="col-span-12 h-full flex items-center justify-center align-middle">
                                                <Button
                                                    disabled={disabled || index !== 0}
                                                    size="sm" color="primary" variant="outlined" startDecorator={<Add />}
                                                    onClick={(e) => {
                                                        handleClickAddSubDetail(index)
                                                    }}
                                                >
                                                    Tambah Properti
                                                </Button>
                                            </div>
                                        </div>
                                    ) : (
                                        <div>Tipe data belum dipilih</div>
                                    ) }
                                </div>
                                <div className="col-span-1 h-full flex items-center justify-center align-middle">
                                    <IconButton
                                        disabled={disabled}
                                        variant="outlined" size="sm" color="danger"
                                        onClick={(e) => {
                                            handleClickDeleteDetail(index)
                                        }}
                                    >
                                        <DeleteIcon />
                                    </IconButton>

                                </div>
                            </div>
                        </Card>
                    ))}
                </div>
                <div className="col-span-12">
                    <Button
                        size="sm" color="primary" variant="outlined" startDecorator={<Add />}
                        disabled={disabled}
                        onClick={handleClickAddDetail}
                    >
                        Tambah Record
                    </Button>
                </div>
            </TitleCard>

            <ResponsiveModal
                key="modalDelRecordJson"
                color={"danger"}
                open={showModalRecordDelete}
                title="Konfirmasi Hapus Record"
                desc={selectedRecord?.deleteMsg ? selectedRecord?.deleteMsg : 'Yakin hapus record ?'}
                yesAction={handleConfirmRecordDelete}
                noAction={() => setShowModalRecordDelete(false)}
            />
            <ResponsiveModal
                color={"danger"}
                open={showModalPropDelete}
                title="Konfirmasi Hapus Properti"
                desc="Penghapusan properti akan berdampak terhadap properti yang sama di record-record lain. Apakah anda yakin akan menghapus data properti ?"
                yesAction={handleConfirmPropDelete}
                noAction={() => setShowModalPropDelete(false)}
            />
            <ResponsiveModal
                color={"danger"}
                title="Konfirmasi Ubah"
                open={showModalPropChange}
                yesAction={handleConfirmChangePropKey}
                noAction={handleCancelChangePropKey}
                desc={selectedProp?.changeMsg ? selectedProp.changeMsg : 'Yakin ubah key properti ?'}
            />
        </>

    } else {
        return <Box>Error load value field : type data not recognized</Box>
    }
}

export default DynamicNestedParmValueField