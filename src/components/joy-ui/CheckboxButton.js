import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from '@mui/material/Checkbox';

function CheckboxButton({label, id, value, name, disabled, checked, className, onClick, padding}) {

    return(
        <>
            <FormControlLabel
                value={value}
                label={label}
                className={className}
                sx={{
                    '& .MuiTypography-root' : {
                        fontSize: "14px",
                        fontFamily: "Poppins",
                    },
                    margin: "0",
                }}

                control={
                    <Checkbox
                        id={id}
                        name={name}
                        checked={checked}
                        disabled={disabled}
                        color="info"
                        onClick={onClick}

                        sx={{
                            padding: padding ? padding : "auto",
                            '& .MuiSvgIcon-root': {
                                fontSize: "24px",
                            },
                        }}
                    />
                }
            />
        </>
    )
}

export default CheckboxButton