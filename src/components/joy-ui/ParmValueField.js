// Dynamic atomic input field based-on type data (non-collection)
import Intbox from "./Intbox";
import RadioButtonGroup from "./RadioButtonGroup";
import {convertAnyToBoolean, convertBoolToStr} from "../../utils/typeConverterUtil";
import CheckboxButton from "./CheckboxButton";
import FormLabel from "@mui/joy/FormLabel";
import {useEffect, useState} from "react";
import Textbox from "./Textbox";

/**
 * Conditional Input Field By TypeData For Parm Value
 * @param typeData tipe data yg diset oleh parent
 * @param disabled keadaan non-aktif yg diset oleh parent
 * @param currentValue nilai diset oleh parent
 * @param onUpdateValue fungsi untuk passing data kembali ke parent
 * @returns {JSX.Element}
 */
const ParmValueField = ({
                            typeData, disabled = true,
                            currentValue,
                            onUpdateValue,
}) => {
    // define internal value state for each type
    const [valStr, setValStr] = useState('')
    const [valBool, setValBool] = useState(false)
    const [valInt, setValInt] = useState(0)
    const [valDec, setValDec] = useState(0.00)
    const [recentVal, setRecentVal] = useState(null)


    // re-map nilai internal tiap perubahan tipe data
    useEffect(() => {
        if (!(typeData && typeData !== "")) {
            setRecentVal(null)
            setValStr('')
            setValBool(false)
            setValInt(0)
            setValDec(0.00)
        }
        if (currentValue) mapValueByTypeChange(currentValue, typeData)
    }, [typeData])

    // mapping nilai existing ke inputan
    useEffect(() => {
        mapValueByTypeChange(currentValue, typeData)
    }, [currentValue])

    const mapValueByTypeChange = (newVal, newType) => {
        if (newType && ['string','decimal'].includes(newType)) {
            let extractedStr
            if (typeof newVal === 'boolean') {
                extractedStr = newVal.toString()
            } else {
                extractedStr = newVal.value ? '' + newVal.value : '' + newVal
            }

            setValStr(extractedStr)
        } else if (newType && ['integer'].includes(newType)) {
            if (newVal === undefined || newVal === null) {
                return
            }

            let parsedInt = -1
            if (typeof newVal === "object") {//if (newVal?.value) { // if wrapped in obj
                typeof newVal?.value === "number" ? parsedInt = newVal.value : parsedInt = parseInt(newVal.value)
            } else {
                typeof newVal === "number" ? parsedInt = newVal : parsedInt = parseInt(newVal)
            }

            setValInt(parsedInt)
        } else if (newType && ['boolean'].includes(newType)) {
            const parsedBool = newVal?.value ?
                convertAnyToBoolean(newVal.value)
                : convertAnyToBoolean(newVal)
            setValBool(parsedBool)
        }
    }

    // return view template by type data
    if (['string', 'decimal'].includes(typeData)) {
        return <Textbox
            textarea={true}
            maxRows={2}
            disabled={disabled}
            id="txtParmValue"
            label="Nilai Parameter"
            tooltip="Isi nilai dengan teks"
            value={valStr}
            onChange={(e) => {
                const rawStr = e.target.value
                setValStr(rawStr)
                setRecentVal(rawStr)

                // emit again to parent
                if (onUpdateValue) {
                    onUpdateValue(rawStr)
                }
            }}
        />
    } else if (typeData === "integer") {
        return <Intbox
            label="Nilai Parameter"
            value={valInt}
            disabled={disabled}
            onChange={(e) => {
                const rawInt = parseInt(e.target.value)
                setValInt(rawInt)
                setRecentVal(rawInt)

                if (onUpdateValue) {
                    onUpdateValue(rawInt)
                }
            }}
        />
    } else if (typeData === "boolean") {
        return <RadioButtonGroup
            height="32px"
            span={3}
            label="Nilai Parameter"
            rgClassName={'-mt-[3.5px]'}
            mandatory
            disabled={disabled}
        >
            <CheckboxButton
                padding="0 5px 0 5px"
                checked={valBool}
                label={valBool ? "Aktif" : "Tidak Aktif"}
                disabled={disabled}
                onClick={(e) => {
                    const checked = !!e.target.checked
                    setValBool(checked)
                    setRecentVal(checked)

                    if (onUpdateValue) {
                        onUpdateValue(checked)
                    }
                }}
            />
        </RadioButtonGroup>
    }
    return <div>
        <FormLabel>Nilai Parameter</FormLabel>
        <p className="text-info">Silakan pilih tipe data untuk menampilkan field inputan</p>
    </div>;

}

export default ParmValueField