import Button from "@mui/joy/Button";
import RestartAltOutlinedIcon from '@mui/icons-material/RestartAltOutlined';
import SaveOutlinedIcon from '@mui/icons-material/SaveOutlined';
import React from "react";

const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};

export default function ButtonResetSave({reset, save, formMode, className, vertical=false, loadingState=false,disabledState=false,visible=true}) {

    return(
        <>{visible && (
            <div className={"grid grid-cols-2 pt-[10px] gap-x-2.5 " + className}>
                {
                    reset && (
                        <Button
                            disabled={loadingState}
                            className={vertical ? "col-span-2" : ""}
                            variant="outlined"
                            color="neutral"
                            startDecorator={<RestartAltOutlinedIcon className="w-4 h-4"/>}
                            onClick={reset}>
                            Reset
                        </Button>
                    )
                }

                {
                    save && (
                        <Button
                            className={vertical ? "col-span-2" : ""}
                            style={vertical ? { marginTop: "10px" } : {}}
                            disabled={!disabledState ? (formMode === FM.VIEW || loadingState) : disabledState}
                            loading={loadingState}
                            variant="solid"
                            startDecorator={<SaveOutlinedIcon className="w-4 h-4"/>}
                            onClick={save}>
                            Save
                        </Button>
                    )
                }
            </div>
        )}</>
    )
}