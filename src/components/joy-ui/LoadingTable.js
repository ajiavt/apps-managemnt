import React from "react";

export default function LoadingTable({span}) {
    return(
        <tr style={{ textAlign: "center", }}>
            <td colSpan={span}>
                <span className="loading loading-dots loading-lg text-primary"></span>
            </td>
        </tr>
    )
}