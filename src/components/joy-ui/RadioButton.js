import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";

function RadioButton({label, id, value, name, disabled, checked, className, onClick}) {

    return(
        <>
            <FormControlLabel
                value={value}
                label={label}
                className={className}
                sx={{
                    '& .MuiTypography-root' : {
                        fontSize: "14px",
                        fontFamily: "Poppins",
                    },
                    margin: "0",
                }}

                control={
                    <Radio
                        id={id}
                        name={name}
                        checked={checked}
                        disabled={disabled}
                        color="default"
                        onClick={onClick}

                        sx={{
                            '& .MuiSvgIcon-root': {
                                fontSize: "24px",
                            },
                        }}
                    />
                }
            />
        </>
    )

}

export default RadioButton