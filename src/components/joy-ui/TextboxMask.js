import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import FormHelperText from '@mui/joy/FormHelperText';
import Input from '@mui/joy/Input';
import Tooltip from '@mui/joy/Tooltip';
import { useState } from 'react';
import {QuestionMarkCircleIcon} from "@heroicons/react/24/outline";
import Alert from '@mui/joy/Alert';
import ExclamationTriangleIcon from "@heroicons/react/24/outline/ExclamationTriangleIcon";
import InputMask from 'react-input-mask';

const animateShowError = `
    @keyframes slide-down {
        from {height: 0}
        to {height: 25px}
    }
    
    .slide-down {
        animation-name: slide-down;
        animation-duration: 1s;      
    }
`;

function Textbox({id, name, label, width, mandatory, disabled, startWith, endWith, placeHolder, value, tooltip, error, readOnly, mask}) {

    const [showError, setShowError] = useState(true);

    return(
        <>
            {/*Inline CSS*/}
            <style>{animateShowError}</style>

            {
                tooltip ?
                    <div className="relative">
                        <div className="absolute right-0 z-50">
                            <Tooltip placement="left-start" title={tooltip} variant="solid">
                                <QuestionMarkCircleIcon color="rgba(0, 0, 0, 0.5)" className="inline-block h-[16px] w-[16px] mb-[5px]"/>
                            </Tooltip>
                        </div>
                    </div>
                    : null
            }

            <FormControl
                size="sm"
                color="neutral"
                id={id}
                required={mandatory}
                disabled={disabled}
                className="mb-[8px]"
                error={error}
                >
                <FormLabel>
                    <span className="text-[14px] font-Poppins">
                        {label}
                    </span>
                </FormLabel>

                <InputMask
                    mask={mask} // Gunakan prop mask
                    maskChar={null}
                    value={value}
                >
                    {(inputProps) => (
                        <Input
                            type="text"
                            variant="outlined"
                            className="-mt-[6.5px] z-50"
                            autoComplete="off"
                            autoCorrect="false"
                            spellCheck={false}
                            readOnly={readOnly}
                            name={name}
                            sx={
                                !error
                                    ? {
                                        maxWidth: width,
                                        fontSize: "16px",
                                        // BORDER COLOR
                                        '--Input-focusedInset': 'var(--any, )',
                                        '--Input-focusedThickness': '0.125rem',
                                        '--Input-focusedHighlight': 'rgba(87,13,248,.25)',
                                        '&::before': {
                                            transition: 'box-shadow .15s ease-in-out',
                                        },
                                        '&:focus-within': { borderColor: '#86b7fe' },
                                        // BORDER PROP
                                        "--Input-gap": "5px",
                                        "--Input-radius": "5px",
                                        "--Input-placeholderOpacity": 0.5,
                                    }
                                    : {
                                        maxWidth: width,
                                        fontSize: "16px",
                                    }
                            }
                            startDecorator={startWith}
                            endDecorator={endWith}
                            {...inputProps} // Spreader props dari InputMask
                        />
                    )}
                </InputMask>

                {error && (
                    <FormHelperText className={`${showError ? 'slide-down' : ''}`}>
                        <Alert
                            startDecorator={<ExclamationTriangleIcon className="w-5 h-5 pt-[3px]" />}
                            variant="soft"
                            color="danger"
                            className="min-w-full -mt-[12px] z-1"
                            size="sm"
                        >
                            <span className="pt-[3px]">{error}</span>
                        </Alert>
                    </FormHelperText>
                )}
            </FormControl>
        </>
    )
}

export default Textbox