import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Tooltip from "@mui/joy/Tooltip";
import {QuestionMarkCircleIcon} from "@heroicons/react/24/outline";
import FormHelperText from "@mui/joy/FormHelperText";
import Alert from "@mui/joy/Alert";
import ExclamationTriangleIcon from "@heroicons/react/24/outline/ExclamationTriangleIcon";
import {useState} from "react";
import RadioGroup from '@mui/material/RadioGroup';

const animateShowError = `
    @keyframes slide-down {
        from {height: 0}
        to {height: 25px}
    }
    
    .slide-down {
        animation-name: slide-down;
        animation-duration: 1s;      
    }
`;

function RadioButtonGroup({label, id, value, mandatory, disabled, error, tooltip, children, className, height, hidden, span, rgClassName, fcClassName='mb-[8px]'}) {

    const [showError, setShowError] = useState(true);

    return(
        <>
            <div className={hidden ? className + (span ? " col-span-" + span : "" ) + " hidden " : className + (span ? " col-span-" + span : "" )}>
                {/*Inline CSS*/}
                <style>{animateShowError}</style>

                {
                    tooltip ?
                        <div className="relative">
                            <div className="absolute right-0 z-10">
                                <Tooltip placement="left-start" title={tooltip} variant="solid">
                                    <QuestionMarkCircleIcon color="rgba(0, 0, 0, 0.5)" className="inline-block h-[16px] w-[16px] mb-[5px]"/>
                                </Tooltip>
                            </div>
                        </div>
                        : null
                }

                <FormControl
                    size="sm"
                    color="neutral"
                    id={id}
                    required={mandatory}
                    disabled={disabled}
                    className={`${fcClassName}`}
                    error={error}
                >
                    <FormLabel>
                        <span className="mb-[-5px] text-[14px] font-Poppins">
                            {label}
                        </span>
                    </FormLabel>

                    <RadioGroup
                        row
                        value={value}
                        aria-labelledby="demo-row-radio-buttons-group-label"
                        name="row-radio-buttons-group"
                        className={`shadow-sm -mt-[2.5px] ${rgClassName}`}
                        sx={{
                            height: height ? height : "auto",
                            border: "1px solid #d1d5db",
                            borderRadius: "5px",
                            zIndex: "10",
                            backgroundColor: "#f9fafb",
                        }}
                    >
                        {children}
                    </RadioGroup>

                    {error && (
                        <FormHelperText className={`${showError ? 'slide-down' : ''}`}>
                            <Alert
                                startDecorator={<ExclamationTriangleIcon className="w-5 h-5 pt-[3px]" />}
                                variant="soft"
                                color="danger"
                                className="min-w-full -mt-[12px] z-1"
                                size="sm"
                            >
                                <span className="pt-[3px]">{error}</span>
                            </Alert>
                        </FormHelperText>
                    )}
                </FormControl>
            </div>
        </>
    )
}

export default RadioButtonGroup