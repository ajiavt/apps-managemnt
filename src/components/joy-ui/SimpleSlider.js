import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Box from "@mui/joy/Box";

const height = "290px";
const width = "413px";
const padding = "10px";
const timeStay = 3000;
const speedSlide = 1000;

const customCss = `
    .slick-slide {
        margin-left: ${padding};
        margin-right: ${padding};
    }
    
    .slick-slide img {
        width: ${width} !important;
        height: ${height} !important;
    }
`;

export default function SimpleSlider({ className, imageProp, label, uploadedImages }) {

    const settingsHorizontal = {
        speed: speedSlide,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: timeStay,
        cssEase: "linear",
        autoplay: true,
        variableWidth: true,
        infinite: true,
        dots: false,
        swipe: false,
        draggable: false,
        arrows: false,
        pauseOnHover: false,
    };

    return (
        <Box className="absolute top-[21.5%] left-[0%] h-[54.6%] w-[100%]">
            <style>{customCss}</style>
            {
                uploadedImages?.length === 1 ?
                    <Slider {...settingsHorizontal} id="slider1" className="ml-[5%]">
                        {uploadedImages?.map((image, index) => (
                            <div key={index}>
                                <img alt='' src={image}/>
                            </div>
                        ))}
                    </Slider>
                    :
                    <Slider {...settingsHorizontal} id="slider1">
                        {uploadedImages?.map((image, index) => (
                            <div key={index}>
                                <img src={image}/>
                            </div>
                        ))}
                    </Slider>
            }
        </Box>
    );
}
