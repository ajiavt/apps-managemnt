import Button from "@mui/joy/Button";
import {DeleteForeverOutlined, EditNoteOutlined} from "@mui/icons-material";
import PostAddOutlinedIcon from '@mui/icons-material/PostAddOutlined';
import React from "react";

export default function ButtonCRUD({ add, edit, del, delLabel="Delete", delVariant="outlined", className, loadingState=false }) {
    return (
        <div className={`grid grid-cols-3 gap-x-2.5 -mb-3.5 ${className}`}>
            {add && (
                <Button
                    disabled={loadingState}
                    variant="soft"
                    color="success"
                    startDecorator={<PostAddOutlinedIcon className="w-4 h-4" />}
                    onClick={add}
                >
                    Add
                </Button>
            )}

            {edit && (
                <Button
                    disabled={loadingState}
                    variant="soft"
                    color="neutral"
                    startDecorator={<EditNoteOutlined className="w-4 h-4" />}
                    onClick={edit}
                >
                    Edit
                </Button>
            )}

            {del && (
                <Button
                    disabled={loadingState}
                    variant={delVariant}
                    color="danger"
                    startDecorator={<DeleteForeverOutlined className="w-4 h-4" />}
                    onClick={del}
                >
                    {delLabel}
                </Button>
            )}
        </div>
    );
}
