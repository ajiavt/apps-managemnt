import React, {useCallback, useEffect, useState} from "react";
import Card from "@mui/joy/Card";
import Selectbox from "./Selectbox";
import Textbox from "./Textbox";
import {IconButton} from "@mui/joy";
import CancelIcon from "@mui/icons-material/Close";
import Button from "@mui/joy/Button";
import {Add} from "@mui/icons-material";
import ParmValueField from "./ParmValueField";
import TitleCard from "../Cards/TitleCard";

const DynamicJsonField = ({ disabled = true, selectedData, selectedDataSetter }) => {
    const [jsonTypes] = useState({
        'Tipe Data Properti': [
            { id: 'string', name: 'String' },
            { id: 'integer', name: 'Integer' },
            { id: 'decimal', name: 'Decimal' },
            { id: 'boolean', name: 'Boolean' }
        ]
    })

    const [propItems, setPropItems] = useState([])
    const [cfgParmReffs, setCfgParmReffs] = useState([])
    const [modifiedSelectedData, setModifiedSelectedData] = useState({})

    // mapping data existing ke internal tiap selectedData diset (dari parent)
    useEffect(() => {
        if (selectedData == null) {
            return
        }

        let keyValues = {}
        if (selectedData?.parmValue) {
            if (typeof selectedData.parmValue === "string") {
                keyValues = JSON.parse(selectedData.parmValue)
            } else if (typeof selectedData.parmValue === "object") {
                keyValues = selectedData.parmValue
            }
        }

        let selectedParamRef = []
        if (selectedData?.cfgParmReffs && selectedData.cfgParmReffs.length > 0) {
            selectedParamRef = [...selectedData.cfgParmReffs]
        }
        setCfgParmReffs(selectedParamRef)

        // lookup map struktur json untuk keperluan setup tipe data prop items
        const parmRefMap = new Map()
        if (selectedParamRef.length > 0) {
            for (let eachRef of selectedParamRef) {
                parmRefMap.set(eachRef.key, { type: eachRef.typeData, key: eachRef.key, seq: eachRef?.seq, desc: eachRef.descr });
            }
        }

        let initialPropItems = []
        Object.entries(keyValues).forEach(kv => {
            const type = parmRefMap.has(kv[0]) ? parmRefMap.get(kv[0])['type'] : 'string'
            initialPropItems.push({ propType: type, key: kv[0], value: kv[1], isNew: false })
        });

        setPropItems(initialPropItems)

        const parmValueObj = convertPropItemsToParmValueObj(initialPropItems)
        setModifiedSelectedData({
            ...selectedData,
            cfgParmReffs: selectedParamRef,
            parmValue: parmValueObj
        })
    }, [selectedData])

    // emit selectedData baru tiap ada perubahan
    useEffect(() => {
        selectedDataSetter(modifiedSelectedData)
    }, [modifiedSelectedData])

    // sinkronkan penambahan prop (1 item) ke prop items
    const handleClickAddProp = useCallback((data) => {
        setPropItems([...propItems, { propType: '', key: '', value: '', isNew: true}])
    }, [propItems]);

    // sinkronkan penghapusan prop (1 item) dari prop items
    const handleClickDeleteProp = useCallback((index) => {
        const updatedPropItems = [...propItems]
        updatedPropItems.splice(index, 1)

        let updateParmRefs = []
        updatedPropItems.forEach((v, i) => {
            if (v?.propType) {
                const seqOneBased = i + 1
                updateParmRefs.push({ typeData: v.propType, key: v.key, descr: v.key, seq: seqOneBased})
            }
        })

        // UI affected asyncronously
        setCfgParmReffs(updateParmRefs)
        setPropItems(updatedPropItems)

        // update modified selected data
        const parmValueObj = convertPropItemsToParmValueObj(updatedPropItems)
        setModifiedSelectedData({
            ...modifiedSelectedData,
            cfgParmReffs: updateParmRefs,
            parmValue: parmValueObj
        })
    }, [propItems])

    // sinkronkan perubahan prop key ke prop items dan param ref change
    const handleChangePropType = useCallback((idx, value) => {
        const updatedPropItems = [...propItems]
        updatedPropItems[idx]['propType'] = value

        const updatedParmRefs = getUpdatedParmRefs(updatedPropItems)

        setPropItems(updatedPropItems)
        setCfgParmReffs(updatedParmRefs)

        // update modified selected data
        const parmValueObj = convertPropItemsToParmValueObj(updatedPropItems)
        setModifiedSelectedData({
            ...modifiedSelectedData,
            cfgParmReffs: updatedParmRefs,
            parmValue: parmValueObj
        })
    }, [propItems])

    // sinkronkan perubahan prop key ke prop items.
    const handleChangePropKey = useCallback((idx, value) => {
        const updatedPropItems = [...propItems]
        updatedPropItems[idx]['key'] = value

        const updatedParmRefs = getUpdatedParmRefs(updatedPropItems)

        setPropItems(updatedPropItems)
        setCfgParmReffs(updatedParmRefs)

        // update modified selected data
        const parmValueObj = convertPropItemsToParmValueObj(updatedPropItems)
        setModifiedSelectedData({
            ...modifiedSelectedData,
            cfgParmReffs: updatedParmRefs,
            parmValue: parmValueObj
        })
    }, [modifiedSelectedData, propItems])

    // sinkronkan perubahan prop value ke prop items.
    const handleChangePropValue = useCallback((idx, value) => {
        const updatedPropItems = [...propItems]
        updatedPropItems[idx]['value'] = value

        setPropItems(updatedPropItems)

        const parmValueObj = convertPropItemsToParmValueObj(updatedPropItems)
        setModifiedSelectedData({
            ...modifiedSelectedData,
            parmValue: parmValueObj
        })
    }, [propItems])

    const convertPropItemsToParmValueObj = (updatedPropItems) => {
        let parmValueObj = {}
        for (let eachItem of updatedPropItems) {
            parmValueObj[eachItem.key] = eachItem.value;
        }

        return parmValueObj
    }

    const getUpdatedParmRefs = (updatedPropItems) => {
        let updateParmRefs = []
        updatedPropItems.forEach((v, i) => {
            if (v?.propType) {
                const seqOneBased = i + 1
                updateParmRefs.push({ typeData: v.propType, key: v.key, descr: v.key, seq: seqOneBased})
            }
        })

        return updateParmRefs
    }

    // return view
    return <TitleCard>
        <div className="grid grid-cols-12 mb-2 gap-x-0.5 gap-y-0.5 h-[380px] overflow-y-auto">
            <div className="col-span-12">Key-values JSON ({propItems?.length} properti)</div>
            <div className="col-span-12">
                {propItems?.map((propitem, idx) => ( // nested loop for sub details json
                    <Card key={`dtljson_${idx}`} size="sm">
                        <div className="grid grid-cols-12 pb-1 pt-1 gap-x-0.5 gap-y-1">
                            <Selectbox
                                className="col-span-2"
                                disabled={disabled}
                                datas={jsonTypes}
                                inputValue={propitem?.propType}
                                setInputValue={(newState) => {}}
                                idKey="id"
                                nameKey="name"
                                label="Tipe Data"
                                showValueInOption={false}
                                onChange={(eventValue) => {
                                    handleChangePropType(idx, eventValue)
                                }}
                            />
                            <div className="col-span-4">
                                <Textbox
                                    maxLength={30}
                                    mandatory
                                    key={`dtljson_${idx}_key`}
                                    autoFocus={!disabled}
                                    disabled={disabled}
                                    label="Key"
                                    value={propitem?.key}
                                    onChange={(e) => {
                                        const eventValue = e.target.value
                                        handleChangePropKey(idx, eventValue)
                                    }}
                                />
                            </div>
                            <div className="col-span-5">
                                <ParmValueField
                                    disabled={disabled}
                                    typeData={propitem?.propType}
                                    currentValue={propitem}
                                    onUpdateValue={(rawValue) => {
                                        handleChangePropValue(idx, rawValue)
                                    }}
                                />
                            </div>
                            <div className="col-span-1 h-full flex items-center justify-center align-middle">
                                <IconButton
                                    disabled={disabled}
                                    variant="outlined" size="xs" color="danger"
                                    onClick={(e) => {
                                        handleClickDeleteProp(idx)
                                    }}
                                >
                                    <CancelIcon />
                                </IconButton>
                            </div>
                        </div>
                    </Card>
                ))}
            </div>
        </div>
        <div className="col-span-12 h-full flex items-center justify-center align-middle">
            <Button
                disabled={disabled}
                size="sm" color="primary" variant="outlined" startDecorator={<Add />}
                onClick={(e) => {
                    handleClickAddProp(e)
                }}
            >
                Tambah Properti
            </Button>
        </div>
    </TitleCard>
}

export default DynamicJsonField