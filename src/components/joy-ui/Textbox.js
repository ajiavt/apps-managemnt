import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import FormHelperText from '@mui/joy/FormHelperText';
import Input from '@mui/joy/Input';
import Tooltip from '@mui/joy/Tooltip';
import {useEffect, useState} from 'react';
import {QuestionMarkCircleIcon} from "@heroicons/react/24/outline";
import Alert from '@mui/joy/Alert';
import ExclamationTriangleIcon from "@heroicons/react/24/outline/ExclamationTriangleIcon";
import Textarea from '@mui/joy/Textarea';

const animateShowError = `
    @keyframes slide-down {
        from {height: 0}
        to {height: 25px}
    }
    
    .slide-down {
        animation-name: slide-down;
        animation-duration: 1s;      
    }
`;

function Textbox({id
        , name
        , label
        , width
        , mandatory = false
        , disabled
        , upperCase
        , maxLength
        , startWith
        , endWith
        , placeHolder
        , value: propValue
        , tooltip
        , error
        , readOnly
        , autoFocus
        , onChange
        , onBlurOrEnter
        , setOnBlurOrEnter
        , onBlur
        , hidden
        , height
        , textarea = false
        , className = ""
        , maxRows=3
        , minRows
        , span
        , inClassName}) {

    const [inputValue, setInputValue] = useState('');
    const [showError, setShowError] = useState(true);

    useEffect(() => {
        setInputValue(propValue || '');
    }, [propValue]);

    const handleOnChange = (event) => {
        let newValue = event.target.value;

        if(upperCase) {
            newValue = newValue.toUpperCase();
        }

        if(maxLength && newValue.length > maxLength) {
            newValue = newValue.slice(0, maxLength);
        }

        setInputValue(newValue);
        if (onChange) {
            const newEvent = {...event};
            newEvent.target.value = newValue;
            onChange(newEvent);
        }
    };

    const handleOnBlur = (event) => {
        if(onBlurOrEnter !== undefined) {
            let newValue = event.target.value;
            setOnBlurOrEnter(newValue);
        }

        if (onBlur) {
            onBlur(event)
        }
    };

    const handleOnEnter = (event) => {
        if (onBlurOrEnter !== undefined && event.keyCode === 13) {
            let newValue = event.target.value;
            setOnBlurOrEnter(newValue);
        }
    };

    const handleInputBlur = (event) => {
        if (onBlur) {
            const newEvent = {...event}
            newEvent.target.value = inputValue
            onBlur(newEvent)
        }
    }

    return(
        <>
            <div className={hidden ? "hidden" : className + (span ?` col-span-${span} ` : '')}>
                {/*Inline CSS*/}
                <style>{animateShowError}</style>

                {
                    tooltip ?
                        <div className="relative">
                            <div className="absolute right-0 z-10">
                                <Tooltip placement="left-start" title={tooltip} variant="solid">
                                    <QuestionMarkCircleIcon color="rgba(0, 0, 0, 0.5)" className="inline-block h-[16px] w-[16px] mb-[5px]"/>
                                </Tooltip>
                            </div>
                        </div>
                        : null
                }

                <FormControl
                    size="sm"
                    color="neutral"
                    id={id}
                    required={mandatory}
                    disabled={disabled}
                    className="mb-[8px]"
                    error={error}
                >
                    <FormLabel>
                        <span className="text-[14px] font-Poppins">
                            {label}
                        </span>
                    </FormLabel>

                    {
                        textarea ?
                            <Textarea
                                minRows={minRows ? minRows : maxRows}
                                maxRows={maxRows}
                                variant="outlined"
                                className={"-mt-[6.5px] z-20 " + inClassName}
                                autoComplete="off"
                                autoCorrect="false"
                                spellCheck={false}
                                readOnly={readOnly}
                                autoFocus={autoFocus}
                                name={name}
                                sx={!error ?
                                    {
                                        '--Textarea-minHeight': height,
                                        '--Textarea-focusedHighlight': 'rgba(87,13,248,.25)',
                                        '&::before': {transition: 'box-shadow .15s ease-in-out',},
                                        '&:focus-within': {borderColor: '#86b7fe',},
                                        maxWidth: width,
                                        fontSize: "16px",
                                    } : {
                                        maxWidth: width,
                                        fontSize: "16px",
                                    }}

                                startDecorator={startWith}
                                endDecorator={endWith}
                                // value={propValue?propValue:inputValue} // <!-- versi A
                                value={inputValue} // <!-- versi B : ambil nilai dari internal (hasil mapping via effect)
                                onChange={handleOnChange}
                                placeholder={placeHolder}
                            />
                            :
                            <Input
                                type="text"
                                variant="outlined"
                                className={"-mt-[6.5px] z-20 " + inClassName}
                                autoComplete="off"
                                autoCorrect="false"
                                spellCheck={false}
                                readOnly={readOnly}
                                autoFocus={autoFocus}
                                name={name}
                                sx={!error ?
                                    {
                                        '--Input-minHeight': height,
                                        '--Input-focusedHighlight': 'rgba(87,13,248,.25)',
                                        '&::before': {transition: 'box-shadow .15s ease-in-out',},
                                        '&:focus-within': {borderColor: '#86b7fe',},
                                        maxWidth: width,
                                        fontSize: "16px",
                                    } : {
                                        maxWidth: width,
                                        fontSize: "16px",
                                    }}

                                startDecorator={startWith}
                                endDecorator={endWith}
                                // value={propValue?propValue:inputValue} // <-- versi A : ambil nilai langsung dari prop
                                value={inputValue} // <!-- versi B : ambil nilai dari internal (hasil mapping via effect)
                                onChange={handleOnChange}
                                onBlur={handleOnBlur}
                                onKeyDown={handleOnEnter}
                                placeholder={placeHolder}
                            />
                    }

                    {error && (
                        <FormHelperText className={`${showError ? 'slide-down' : ''}`}>
                            <Alert
                                startDecorator={<ExclamationTriangleIcon className="w-5 h-5 pt-[3px]" />}
                                variant="soft"
                                color="danger"
                                className="min-w-full -mt-[12px] z-1"
                                size="sm"
                            >
                                <span className="pt-[3px]">{error}</span>
                            </Alert>
                        </FormHelperText>
                    )}
                </FormControl>
            </div>
        </>
    )
}

export default Textbox