import * as React from 'react';
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import Modal from '@mui/joy/Modal';
import ModalDialog from '@mui/joy/ModalDialog';
import Typography from '@mui/joy/Typography';
import Card from "@mui/joy/Card";

export default function PreviewModal({title, desc, open, keyData, isImage, isBase64, previewData, yesAction, noAction, color}) {

    const showBase64 = () => {

        if (isImage && previewData) {
            return <img src={previewData} alt={title} style={{width: `100%`, height: `100%` }} />
        }

        return (<div>letakkan jari di alat pemindai</div>);
    }

    return (
        <React.Fragment>
            <Modal open={open} onClose={noAction}>
                <ModalDialog
                    aria-labelledby="nested-modal-title"
                    aria-describedby="nested-modal-description"
                    sx={(theme) => ({
                        [theme.breakpoints.only('xs')]: {
                            top: 'unset',
                            bottom: 0,
                            left: 0,
                            right: 0,
                            borderRadius: 0,
                            transform: 'none',
                            maxWidth: 'unset',
                        },
                    })}
                >
                    <Typography id="nested-modal-title" level="h2">
                        {title} {keyData}
                    </Typography>
                    <Typography id="nested-modal-description" textColor="text.tertiary">
                        {desc}

                    </Typography>
                    <Card className="col-span-12" sx={{ padding: '0', }}>
                        <div
                            className="rounded-md"
                            style={{
                                    position: "relative",
                                    width: `350px`,
                                    height: `350px`,
                                    fontSize: `12px`,
                                    border: '1px solid grey'
                                }}
                        >
                            { showBase64() }
                            {/*<p>Letakkan jari di alat pemindai</p>*/}
                            {/*<img alt='captured fingerprint' src={previewData} style={{*/}
                            {/*    width: `100%`,*/}
                            {/*    height: `100%`*/}
                            {/*}}/>*/}
                        </div>
                    </Card>

                    {/*<div>*/}
                    {/*    {showBase64()}*/}
                    {/*</div>*/}
                    <Box
                        sx={{
                            mt: 1,
                            display: 'flex',
                            gap: 1,
                            flexDirection: { xs: 'column', sm: 'row-reverse' },
                        }}
                    >
                        <Button sx={{ width: "100px" }} variant={color === "primary" ? "solid" : "outlined"} color={color} onClick={yesAction}>
                            Ya
                        </Button>
                        <Button sx={{ width: "100px" }} variant="outlined" color ="neutral" onClick={noAction}>
                            Tidak
                        </Button>
                    </Box>
                </ ModalDialog>
            </ Modal>
        </ React.Fragment >
    );
}