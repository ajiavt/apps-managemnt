import React from "react";

export default function LoadingComponent({className}) {
    return(
        <div className={className}>
            <div className="absolute flex shadow-base-300 justify-center items-center h-full w-full ">
                <div className="flex-none">
                    <span className="loading loading-dots loading-lg text-primary"></span>
                </div>
            </div>
        </div>
    )
}