import * as React from 'react';
import Button from '@mui/joy/Button';
import { styled } from '@mui/joy';
import Tooltip from "@mui/joy/Tooltip";
import {QuestionMarkCircleIcon} from "@heroicons/react/24/outline";
import FormControl from "@mui/joy/FormControl";
import FormLabel from "@mui/joy/FormLabel";
import FormHelperText from "@mui/joy/FormHelperText";
import Alert from "@mui/joy/Alert";
import ExclamationTriangleIcon from "@heroicons/react/24/outline/ExclamationTriangleIcon";
import {useState} from "react";
import {showNotification} from "../../features/common/headerSlice";
import {useDispatch} from "react-redux";
import CloudUploadOutlinedIcon from '@mui/icons-material/CloudUploadOutlined';

const animateShowError = `
    @keyframes slide-down {
        from {height: 0}
        to {height: 25px}
    }
    
    .slide-down {
        animation-name: slide-down;
        animation-duration: 1s;      
    }
`;

const VisuallyHiddenInput = styled('input')`
  clip: rect(0 0 0 0);
  clip-path: inset(50%);
  height: 1px;
  overflow: hidden;
  position: absolute;
  bottom: 0;
  left: 0;
  white-space: nowrap;
  width: 1px;
`;

const style = document.createElement('style');
style.textContent = animateShowError;
document.head.append(style);

export default function UploadButton({label, mandatory, disabled, tooltip, hidden, error, className, onFileChange, fileName, setFileName, color="neutral", placeholder="Upload", labelMargin}) {

    const [showError, setShowError] = useState(true);
    const dispatch = useDispatch();

    const handleFileChange = (e) => {
        let file = e.target.files[0];
        if (file) {
            const validationMessage = validateFile(file);
            if (validationMessage === "") {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onloadend = () => {
                    const base64data = reader.result;
                    onFileChange(base64data, file);
                }
                setFileName(file.name);
            } else {
                dispatch(showNotification({ message: validationMessage, status: 0 }));
                setShowError(true);
            }
        }
    }

    const validateFile = (file) => {
        const allowedExtensions = ['jpg','jpeg','png'];
        const fileExtension = file.name.split('.').pop().toLowerCase();
        if (!allowedExtensions.includes(fileExtension)) {
            return 'Ekstensi file harus .jpg, .jpeg, .png';
        }

        const maxSize = 1 * 1024 * 1024; // 1MB dalam bytes.
        if (file.size > maxSize) {
            return 'Ukuran file tidak boleh melebihi 1MB';
        }

        return "";
    }

    return (
        <>
            <div className={hidden ? "hidden" : className}>
                <style>{animateShowError}</style>
                {tooltip && (
                    <div className="absolute right-[20px] pb-[10px] z-10">
                        <Tooltip placement="left-start" title={tooltip} variant="solid">
                        <span>
                            <QuestionMarkCircleIcon
                                color="rgba(0, 0, 0, 0.5)"
                                className="inline-block h-[16px] w-[16px] mb-[5px]"
                            />
                        </span>
                        </Tooltip>
                    </div>
                )}
                <FormControl
                    size="sm"
                    color="neutral"
                    required={mandatory}
                    disabled={disabled}
                    className="mb-[8px]"
                    error={error}
                >
                    <FormLabel>
                        <span className={labelMargin ? labelMargin + " text-[14px] font-Poppins" : "mb-[-8px] text-[14px] font-Poppins"}>{label}</span>
                    </FormLabel>
                    <Button
                        style={{ background: 'white', borderColor: error ? 'var(--joy-palette-danger-outlinedBorder, var(--joy-palette-danger-300, #F09898))' : null }}
                        className="z-10"
                        disabled={disabled}
                        component="label"
                        role={undefined}
                        tabIndex={-1}
                        variant="outlined"
                        color={color}
                        onChange={handleFileChange}
                        startDecorator={
                            <CloudUploadOutlinedIcon className="w-5 h-5"/>
                        }
                    >
                        <span style={{ whiteSpace: "pre-line", overflowWrap: "break-word", width: "90%", textAlign: "center" }}>{fileName || placeholder}</span>
                        {color !== 'danger' && <VisuallyHiddenInput type="file" />}
                    </Button>
                    {error && (
                        <FormHelperText className={`${showError ? 'slide-down' : ''}`}>
                            <Alert
                                startDecorator={<ExclamationTriangleIcon className="w-5 h-5 pt-[3px]" />}
                                variant="soft"
                                color="danger"
                                className="min-w-full -mt-[12px] z-1"
                                size="sm"
                            >
                                <span className="pt-[3px]">{error}</span>
                            </Alert>
                        </FormHelperText>
                    )}
                </FormControl>
            </div>
        </>
    );
}