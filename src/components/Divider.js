export default function Divider({className, horizontal,children}) {
    return(
        <div className={`divider ${horizontal ? 'divider-horizontal' : ''} ${className}`}>
            {children}
        </div>
    );
}