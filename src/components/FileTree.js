import SubFileTree from "./SubFileTree";

export default function FileTree({routes}) {
    return (
        <><ul className="menu menu-md">
            {routes.map((route, k) => (
                <li key={k}>
                    {
                        route.submenu ?
                            <details open>
                                <SubFileTree {...route} />
                            </details>
                        :
                        <summary>
                            {route.icon}
                            {route.name}
                            <input type="checkbox" className="checkbox checkbox-sm checkbox-primary"/>
                        </summary>
                    }
                </li>
            ))}
        </ul></>
    )
}