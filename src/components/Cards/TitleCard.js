import Subtitle from "../Typography/Subtitle"
import Typography from "@mui/joy/Typography";


  function TitleCard(
      {
          title,
          children,
          topMargin,
          TopSideButtons,
          subtitle,
          properties,
          className,
          span,
          isBordered = true,
          isLoading,
          footer
      }){
      return(
          <>
              <div className={"card w-full p-5 bg-base-100 shadow-md shadow-gray-400 overflow-x-auto "
                           + (topMargin || "mt-6" )
                           + " " + properties
                           + " " + className
                           + (span ?` col-span-${span} ` : '')
              }>
                  {/* Title for Card */}
                  <Subtitle styleClass={TopSideButtons ? "inline-block" : ""}>
                      {title}

                      {/* Top side button, show only if present */}
                      {
                          TopSideButtons && <div className="inline-block float-right">{TopSideButtons}</div>
                      }
                  </Subtitle>

                  <span className="text-large">
                    {subtitle}
                  </span>

                  {
                      title && isBordered?
                          <div className="my-2 w-full h-[1px] bg-black opacity-10"/>
                          :
                          null
                  }

                  {/** Card Body */}
                  <div className='h-full w-full base-100'>
                      {children}
                  </div>

                  {
                      footer && (
                          <Typography className="text-xs">
                              *) {footer}
                          </Typography>
                      )
                  }
              </div>
          </>
      )
  }


  export default TitleCard