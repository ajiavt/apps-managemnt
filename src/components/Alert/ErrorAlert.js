import React from "react";
import {XCircleIcon} from "@heroicons/react/24/outline";

function ErrorAlert({condition, errorMessage, styleClass, children}){
    if(condition) {
        return(
            <div className={`alert alert-error ${styleClass}`}>
                <XCircleIcon className="h-6 w-6"/>
                {errorMessage}
                
                {children}
            </div>
            )

    }  else {
        return(
            <div></div>
        )
    }
}

export default ErrorAlert