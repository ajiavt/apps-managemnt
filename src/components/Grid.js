export default function Grid({children, className='', smallGap}) {
    return(
        <div className={`grid grid-cols-12 ${smallGap ? `gap-x-2.5`: `gap-x-6`} ${className}`}>
            {children}
        </div>
    );
}