import { useState, } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";

function InputText({labelTitle, labelStyle, containerStyle, defaultValue, updateFormValue, updateType, isPassword, size}){

    const [value, setValue] = useState(defaultValue);
    const [ showPassword, setShowPassword ] = useState(false);
    const [ storedPassword, setStoredPassword ] = useState("");

    const handleClickShowPassword = () => {
        if(showPassword) {
            maskPassword();
        } else {
            unmaskPassword();
        }
        setShowPassword(!showPassword)
    };

    const handleInputChange = (val) => {
        if(showPassword) {
            updateInputValue(val);
        } else {
            updateMaskedValue(val);
        }
    }

    const updateInputValue = (val) => {
        setValue(val)
        setStoredPassword(val)
        updateFormValue({updateType, value : val})
    }

    const maskPassword = () => {
        setStoredPassword(value)
        const valueLength = value.length;
        setValue("\u25CF".repeat(valueLength))
    }

    const unmaskPassword = () => {
        setValue(storedPassword)
    }

    const updateMaskedValue = (val) => {
        let newValue, newStoredPassword;
        if(val.length > storedPassword.length) {
            const lastChar = val.slice(-1);
            newValue = value.concat("\u25CF")
            newStoredPassword = storedPassword.concat(lastChar);
        } else if(val.length < storedPassword.length) {
            newValue = value.slice(0,-1);
            newStoredPassword = storedPassword.slice(0,-1);
        }
        setStoredPassword(newStoredPassword);
        setValue(newValue);
        updateFormValue({updateType, value : newStoredPassword})
    }

    return(
        <div className={`form-control ${!size ? 'w-full' : ' '} ${containerStyle} `}>
            <label className="label">
                <span className={"label-text text-base-content " + labelStyle}>{labelTitle}</span>
            </label>
            <div className='flex flex-row border-solid border-2 border-base-950 rounded-lg'>
                {isPassword ? (
                    <div className='input-group input-bordered'>
                        <input type="text"
                               className={`input w-full ${size}`}
                               autoComplete="off"
                               value={value}
                               onChange={(e) => handleInputChange(e.target.value)}
                               name={labelTitle}

                        />
                        <button type='button' className="btn btn-square"
                                onClick={handleClickShowPassword}
                        >
                            {!showPassword ? <FontAwesomeIcon icon={faEye}/> : <FontAwesomeIcon icon={faEyeSlash}/>}
                        </button>
                    </div>
                ) : (
                    <input
                        type="text"
                        className={`input w-full ${size}`}
                        value={value}
                        onChange={(e) => updateInputValue(e.target.value)}
                        name={labelTitle}
                        autoComplete="off"
                  />
                )}
            </div>
        </div>
    )
}

export default InputText;