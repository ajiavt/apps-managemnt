import SubFileTree from "./SubFileTree";
import DocumentIcon from "@heroicons/react/24/outline/DocumentIcon";
import {FolderOpenIcon} from "@heroicons/react/24/outline";

export default function SubMenu({submenu, name, icon}) {
    return (
        <><summary><FolderOpenIcon className='h-5 w-5'/>{name}</summary>
            <ul >
                {submenu.map((m, k) => {
                    return (
                        <><li key={k}>
                            {
                                m.submenu ? <SubFileTree {...m}/>
                                :
                                <summary>
                                    <DocumentIcon className='h-5 w-5'/>
                                    {m.name}
                                    <input type="checkbox" className="checkbox checkbox-sm checkbox-primary"/>
                                </summary>
                            }
                        </li></>
                    )})}
            </ul>
        </>
    )
}
