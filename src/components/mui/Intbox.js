import {InputAdornment, TextField} from "@mui/material";

function Intbox({id, width, label, maxLength, allowNegative = false, adornment = ""}) {

    return(
        <TextField
            id={id}
            label={label}
            variant="outlined"
            size="small"
            type="number"
            autoComplete="off"
            margin="dense"

            onChange={(event) => {
                // Allow negative?
                if (!allowNegative) {
                    event.target.value = Math.max(0, parseInt(event.target.value));
                }

                // Maxlength
                if (event.target.value.toString().length > maxLength) {
                    event.target.value = event.target.value.toString().slice(0, maxLength);
                }
            }}

            // Biar labelnya selalu di atas
            InputLabelProps={{
                // shrink: true,
                sx: {
                    // color: "black",
                }
            }}

            // Styling the input
            inputProps={{
                style: {
                    // color: "blue"
                },
            }}

            // Styling the box
            style={{
                marginRight: "5px",
                marginBottom: "5px",
                width: width,
            }}

            // Adornment, contoh: kg, gram, gitu-gitu pokonya akhir akhiran. Bisa jadi bulan. misal 12 "bulan"
            InputProps={(adornment ? {
                endAdornment: <InputAdornment position="end">{adornment}</InputAdornment>,
            } : {})}
        />
    )

}

export default Intbox
