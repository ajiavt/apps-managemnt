import {InputAdornment, TextField} from "@mui/material";

function Textbox({id, width, label, maxLength, upperCase= false, email= false, adornment= ""}) {

    const ALPHA_NUMERIC_SPACE = /^[a-zA-Z0-9 ]+$/;

    return(
        <>
            <TextField
                id={id}
                label={label}
                variant="filled"
                size="small"
                type={email ? "email" : "text"}

                // ETC
                autoComplete="off"
                margin="dense"
                // Styling the box
                style={{
                    marginRight: "5px",
                    marginBottom: "5px",
                    width: "99%",
                    maxWidth: width,
                    // backgroundColor: "blue"
                }}

                // Biar labelnya selalu di atas
                InputLabelProps={{
                    shrink: true,
                    sx: {
                        // color: "black",
                        fontSize: "17px",
                    }
                }}

                // Styling the input
                inputProps={{
                    style: {
                        // color: "black",
                        textTransform: (upperCase ? "uppercase" : "")
                    },
                    maxLength: maxLength,
                }}

                // Adornment, contoh: kg, gram, gitu-gitu pokonya
                InputProps={(adornment ? {
                    endAdornment: <InputAdornment position="end">{adornment}</InputAdornment>,
                } : {})}

                // Allowed Character
                onKeyDown={(event) => {
                    if (!ALPHA_NUMERIC_SPACE.test(event.key) && !email) {
                        event.preventDefault();
                    }
                }}
            >
            </TextField>
        </>
    )

}

export default Textbox
