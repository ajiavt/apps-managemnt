import { NumericFormat } from 'react-number-format';
import { TextField } from "@mui/material";
import {useState} from "react";

function Decimalbox({
                        id, width, label, maxLength, prefix,
                        value, onBlur
                    }) {
    const [inputValue, setInputValue] = useState(0.00);
    const handleInputChange = (event) => {
        let newValue = parseFloat(event.target.value);
        setInputValue(newValue);
    }

    const handleInputBlur = (event) => {
        // if (inputValue == event.target.value)
        onBlur(inputValue)
    }

    return(
        <NumericFormat
            customInput={TextField}
            id={id}
            label={label}
            width={width}
            maxLength={maxLength}
            prefix={prefix}
            value={inputValue}
            onBlur={handleInputBlur}
            size="small"
            variant="standard"
            autoComplete="off"
            thousandSeparator
            style={{
                marginRight: "5px",
                marginBottom: "5px",
            }}
        />
    )

}

export default Decimalbox
