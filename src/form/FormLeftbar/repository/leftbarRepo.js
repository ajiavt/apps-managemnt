import { apiURL } from "../../../app/url"
import { apiClient } from "../../../app/axios";

const leftbarRepo = async () => {
    const ACCESS_TOKEN = localStorage.getItem("access_token")
    const TOKEN_TYPE = localStorage.getItem("token_type")

    try {
        const success = await apiClient.get(
            apiURL.GET_USER_DATA, {
                headers: {
                    'Authorization': `${TOKEN_TYPE} ${ACCESS_TOKEN}`,
                },
            }
        )

        return {response : success};
    }

    catch (error) {
        return {response: error};
    }
}

export default leftbarRepo;