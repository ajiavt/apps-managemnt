import Squares2X2Icon from '@heroicons/react/24/outline/Squares2X2Icon'
import HomeIcon from '@heroicons/react/24/outline/HomeIcon'
import { FolderIcon } from "@heroicons/react/24/outline";

const iconClasses = `h-4 w-4`

const GetRoutes = (menus) => {

    const pathSet = new Set();
    const homePath = '/hbam';
    pathSet.add(homePath);

    const additionalRoutes = [
        {
            path: homePath,
            icon: <HomeIcon className={iconClasses} />,
            name: 'Home',
        },
    ];

    for (let i = 0; i < menus.length; i++) {
        const menu = menus[i];
        const modifiedPath = `/app/menu-id=${menu.menuId}`;
        const icon = <FolderIcon className={iconClasses}/>;
        const name = menu.name;
        pathSet.add(modifiedPath);
        const route = {
            path: modifiedPath,
            icon: icon,
            name: name,
            submenu: []
        };

        if (menu.menuChild && menu.menuChild.length > 0) {
            for (let j = 0; j < menu.menuChild.length; j++) {
                const submenu = menu.menuChild[j];
                const subModifiedPath = `/app/menu-id=${submenu.menuId}`;
                const subIcon = <FolderIcon className={iconClasses}/>;
                const subName = submenu.name;
                pathSet.add(subModifiedPath);
                const subRoute = {
                    path: subModifiedPath,
                    icon: subIcon,
                    name: subName
                };

                route.submenu.push(subRoute);
            }
        }

        additionalRoutes.push(route);
    }

    if(menus.length > 0) {
        return { routes: additionalRoutes, pathSet: Array.from(pathSet) };
    }
    return additionalRoutes;
}

export default GetRoutes;