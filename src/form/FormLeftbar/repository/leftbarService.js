import leftbarRepo from "./leftbarRepo";

const leftbarService = async (data) => {
    const {response}  = await leftbarRepo(data);

    if(response.status === 200) {
        return {response: response?.data?.data.menus, status: 1};
    } else {
        if(!(response && response.response && response.response.data)) {
            return {response: "Gagal terhubung ke API Leftbar", status: 0};
        }

        let str = response.response.data['error_description'];

        if(str.toLowerCase() === "token is expired") {
            str = "sesi sudah habis";
        }

        let str2 = "";
        if(str) {
            str2 = str[0].toUpperCase() + str.slice(1) + "!";
        } else {
            str2 = "Koneksi API tidak tersambung!"
        }

        return {response: str2, status: 0};
    }
}

export default leftbarService;