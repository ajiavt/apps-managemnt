import ChevronDownIcon from  '@heroicons/react/24/outline/ChevronDownIcon'
import {useEffect, useState} from 'react'
import { Link, useLocation } from 'react-router-dom'
import {FolderOpenIcon} from "@heroicons/react/24/outline";
import DocumentIcon from "@heroicons/react/24/outline/DocumentIcon";

const iconClasses = `h-4 w-4`

function SubMenu({submenu, name, icon}){

    const location = useLocation();
    const [isExpanded, setIsExpanded] = useState(true);

    useEffect(() => {
        if(submenu.filter(m => m.path === location.pathname).length > 0){
            setIsExpanded(true)
        }
    }, [location.pathname, submenu])

    return (
        <>
        <div className='w-full py-[10px]' onClick={() => setIsExpanded(!isExpanded)}>
            {isExpanded ? (<FolderOpenIcon className={iconClasses}/>) : icon}
            {isExpanded ? <span className="font-semibold">{name}</span> : name}
            <ChevronDownIcon className={'w-3 h-3 mt-1 float-right delay-400 duration-500 transition-all ' + (isExpanded ? 'rotate-180' : '')}/>
        </div>

        <ul className={(isExpanded ? "pb-[5px]" : "hidden")}>
            {submenu.map((m, k) => {

                return(
                    <li key={k} className="mt-[2px]">{m.submenu ?
                        <SubMenu {...m}/>
                        :
                        <Link
                            to={m.path}
                            className={location.pathname == m.path
                                ? "font-semibold bg-primary/5 py-[10px]"
                                : "py-[10px]"}
                        >
                            <DocumentIcon className={iconClasses}/>
                            {m.name}
                            {location.pathname == m.path
                                ? (<span
                                    className="absolute inset-y-0 -left-[11px] h-[8px] w-[8px] rounded-full bg-primary/80 mt-[15px]"
                                    aria-hidden="true"/>)
                                : null
                            }
                        </Link>
                    }
                    </li>
                )
            })}
        </ul>
        </>
    )
}

export default SubMenu
