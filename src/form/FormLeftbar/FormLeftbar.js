import React, {useEffect, useState, useCallback, useMemo} from "react";
import {Link, NavLink, useLocation} from "react-router-dom";
import { useDispatch } from "react-redux";
import { XMarkIcon, FolderOpenIcon, FolderIcon } from "@heroicons/react/24/outline";
import SubMenu from "./SubMenu";
import appLogo from "../../assets/images/Olibs Hybrid/logo.png";
import leftbarService from "./repository/leftbarService";
import { showNotification } from "../../features/common/headerSlice";
import GetRoutes from "./repository/GetRoutes";
import {setPathSet} from "../../redux/pathSet/pathSetSlice";
import abstractImage from '../../assets/images/abstract.jpg';

const iconClasses = `h-4 w-4`

function FormLeftbar() {

    const location = useLocation();
    const dispatch = useDispatch();
    const [response, setResponse] = useState([]);
    const [errorMessage, setErrorMessage] = useState(null);

    const close = useCallback(() => {
        document.getElementById("left-sidebar-drawer").click();
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            const { response, status } = await leftbarService();
            if (status === 0) {
                setErrorMessage(response);
            } else {
                // Menyusun ulang array berdasarkan properti seq secara ascending
                const sortedResponse = response.slice().sort((a, b) => a.seq - b.seq);

                // Menyusun ulang menuChild dalam setiap objek menu
                const updatedResponse = sortedResponse.map(menu => {
                    if (menu.menuChild) {
                        // Menyusun ulang array menuChild berdasarkan properti seq secara ascending
                        const sortedMenuChild = menu.menuChild.slice().sort((a, b) => a.seq - b.seq);
                        return { ...menu, menuChild: sortedMenuChild };
                    }
                    return menu;
                });

                const { routes, pathSet } = GetRoutes(updatedResponse);
                setResponse(routes); // menyimpan routes di dalam state

                if (pathSet.length > 0) dispatch(setPathSet(pathSet)); // mendispatch action di sini
            }
        };

        fetchData();
    }, [dispatch]);

    useEffect(() => {
        if(errorMessage){
            dispatch(showNotification({message : errorMessage, status : 0}))
        }
    }, [errorMessage, dispatch]);

    const routes = useMemo(() => response , [response]);

    return (
        <div className="drawer-side shadow shadow-lg shadow-gray-400 z-50">
            <ul className="menu menu-xs pt-2 w-[275px] bg-base-100 text-base-content">
                <button className="btn btn-ghost bg-base-300 btn-circle z-50 top-0 right-0 mt-4 mr-2 absolute lg:hidden" onClick={close}>
                    <XMarkIcon className="inline-block h-5 w-5" />
                </button>

                <div className="flex justify-center items-center my-[5px]">
                    <img src={appLogo} className="h-[3rem]" alt="App Logo"/>
                </div>

                {routes.map((route, k) => (
                    <li key={k}
                        className="mb-[10px] shadow-md"
                        style={{
                            backgroundImage: `url(${abstractImage})`,
                            backgroundSize: '90%',
                            borderRadius: "5px",
                    }}>
                        {route.submenu ? (
                            <SubMenu {...route} />
                        ) : (
                            <NavLink
                                end to={route.path}
                                className={({ isActive }) => `${isActive 
                                    ? "font-semibold bg-primary/5 py-[10px]" 
                                    : "font-normal py-[10px]"}`}>

                                {route.icon || (location.pathname === route.path
                                    ? <FolderOpenIcon className={iconClasses} />
                                    : <FolderIcon className={iconClasses} />)}

                                {route.name}

                                {location.pathname === route.path
                                    ? <span className="absolute inset-y-0 left-0 w-[2.5px] bg-primary/80 " aria-hidden="true" />
                                    : null}
                            </NavLink>
                        )}
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default FormLeftbar;
