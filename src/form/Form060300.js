import TitleCard from "../components/Cards/TitleCard";
import { useDispatch } from "react-redux";
import React, { useEffect, useState, useCallback } from "react";
import { setPageTitle, showNotification } from "../features/common/headerSlice";
import TableForm060300 from "./_components/TableForm060300";
import { reportService } from "./_repository/reportService";
import Selectbox from "../components/joy-ui/Selectbox";
import { branchService } from "./_repository/branchService";
import userDataService from "./FormUserData/repository/userDataService";
import Button from "@mui/joy/Button";
import {PrintOutlined} from "@mui/icons-material";
import ExportPdfCabang060300 from "./_components/ExportPdfCabang060300";

const Form060300 = () => {

    const dispatch = useDispatch();
    const [errorMessage, setErrorMessage] = useState(null);
    const [loadingState, setLoadingState] = useState(false);
    const [inputValue, setInputValue] = useState({});
    const [reportResponse, setReportResponse] = useState({});
    const [branchResponse, setBranchResponse] = useState({});
    const [userDataResponse, setUserDataResponse] = useState({});

    const fetchData = useCallback(async (service, stateSetter, ...params) => {
        try {
            setLoadingState(true);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchUserData = useCallback(() => {
        fetchData(userDataService, setUserDataResponse, "GET_ALL");
    }, []);

    const fetchDataBranch = useCallback(() => {
        fetchData(branchService, setBranchResponse, "GET_ALL");
    }, []);

    const fetchDataReport = useCallback((branchId) => {
        fetchData(
            reportService,
            setReportResponse,
            "GET_RINCIAN_CABANG",
            branchId
        );
    }, []);

    useEffect(() => {
        if (errorMessage) {
            dispatch(showNotification({ message: errorMessage, status: 0 }));
        }
    }, [errorMessage]);

    useEffect(() => {
        fetchDataBranch();
        fetchUserData();
        dispatch(setPageTitle({ title: "Rincian Aktivitas Teller" }));
    }, []);

    useEffect(() => {
        if(userDataResponse?.cabang) {
            setInputValue(userDataResponse?.cabangValue);
            fetchDataReport(userDataResponse?.cabangValue);
        }
    }, [userDataResponse]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const listBranch = {
        "Daftar Cabang": branchResponse?.data
            ?.map((branch) => ({
                branchId: branch.branchId,
                branchNm: branch.branchNm,
            })),
    };

    const handleExportToPDF = () => {
        if(inputValue !== '' && reportResponse?.data) {
            ExportPdfCabang060300(reportResponse);
        } else {
            let message = "Data tidak tersedia.";
            dispatch(showNotification({ message, status: 2 }));
        }
    };

    return (
        <>
            <div className="grid lg:grid-cols-12 lg:gap-x-6">
                <div className="lg:col-span-12">
                    <div className="grid lg:grid-cols-12 gap-x-2.5">
                        <Selectbox
                            disabled={true}
                            inputValue={inputValue}
                            setInputValue={setInputValue}
                            className="col-span-4 mt-[5px] mb-[-10px]"
                            idKey="branchId"
                            nameKey="branchNm"
                            label="Cabang"
                            datas={listBranch}
                            error={errorMessage?.branchId}
                        />
                        <Button
                            className="col-span-2 h-[36.4px]"
                            style={{ marginTop: '24.5px' }}
                            variant="outlined"
                            color="primary"
                            startDecorator={<PrintOutlined className="w-4 h-4"/>}
                            onClick={handleExportToPDF}
                        >
                            Export to PDF
                        </Button>
                    </div>
                    <TitleCard className="overflow-x-auto max-h-[75vh] mt-[15px]">
                        <TableForm060300
                            isLoading={loadingState}
                            response={reportResponse}
                        />
                    </TitleCard>
                </div>
            </div>
        </>
    );
};

export default Form060300;
