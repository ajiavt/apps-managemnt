import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm010100 from "./_components/TableForm010100";
import Textbox from "../components/joy-ui/Textbox";
import Intbox from "../components/joy-ui/Intbox";
import {AddOutlined, DeleteForeverOutlined, EditNoteOutlined} from "@mui/icons-material";
import Button from '@mui/joy/Button';
import { userService } from "./_repository/userService";
import ResponsiveModal from "../components/joy-ui/Modal";
import Textarea from "../components/joy-ui/Textarea";
import CheckboxButton from "../components/joy-ui/CheckboxButton";
import RadioButtonGroup from "../components/joy-ui/RadioButtonGroup";
import Selectbox from "../components/joy-ui/Selectbox";
import {roleService} from "./_repository/roleService";
import {branchService} from "./_repository/branchService";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import {printerService} from "./_repository/printerService";
import {parmService} from "./_repository/parmService";
import Grid from "../components/Grid";

const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    userId: null,
    branchId: null,
    fullName: null,
    status: null,
    roleId: null,
    iaNbr: null,
    application: [],
};

export default function Form010100(){

    const dispatch = useDispatch();
    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [inputValue, setInputValue] = useState({});
    const [inputValueBranch, setInputValueBranch] = useState({});
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [selectedDataState, setSelectedDataState] = useState({...emptyData, application: [],});
    const [loadingState, setLoadingState] = useState(false)
    const [userResponse, setUserResponse] = useState({});
    const [userDetailResponse, setUserDetailResponse] = useState({});
    const [txtUserIdValue, setTxtUserIdValue] = useState('');
    const [roleResponse, setRoleResponse] = useState({});
    const [branchResponse, setBranchResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState({});
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);

    const fetchData = useCallback(async (service, stateSetter, ...params) => {
        try {
            setLoadingState(true);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchUser = useCallback(() => {
        fetchData(userService, setUserResponse, "GET_ALL");
    }, []);

    const fetchRole = useCallback(() => {
        fetchData(roleService, setRoleResponse, "GET_ALL");
    }, []);

    const fetchBranch = useCallback(() => {
        fetchData(branchService, setBranchResponse, "GET_ALL");
    }, []);

    const fetchDetailUser = useCallback((data) => {
        fetchData(userService, setUserDetailResponse, "GET_1_CORE", data);
    }, []);

    useEffect(() => {
        fetchUser();
        fetchRole();
        fetchBranch();
    }, [fetchUser, fetchRole, fetchBranch, refreshTable]);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Pemeliharaan User"}))
    }, []);

    useEffect(() => {
        setUserDetailResponse({});
        setTxtUserIdValue('');
        if(txtUserIdValue !== '') {
            const data = { userId: selectedDataState?.userId, branchId: selectedDataState?.branchId };
            fetchDetailUser(data)
        }
    }, [txtUserIdValue])

    useEffect(() => {
        if (Object.keys(userDetailResponse).length !== 0) {
            const data = userDetailResponse?.data;
            const branchId = data?.branchId;
            const fullName = data?.fullName;
            setSelectedDataState({...selectedDataState, branchId: branchId, fullName: fullName});
        } else {
            setSelectedDataState({...selectedDataState, branchId: '', fullName: ''});
        }
    }, [userDetailResponse])

    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.userId);
        setFormMode(FM.VIEW);
    }, []);

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setSelectedDataState({...selectedDataState});
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.userId !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.userId !== null) {
            setLoadingState(true);
            const { response: userResponse, status } = await userService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(userResponse);
                setErrorMessage(error);
            }
            else{
                if (userResponse.code === '00') {
                    dispatch(showNotification({message : userResponse?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const data = {
                userId: selectedDataState.userId,
                branchId: selectedDataState.branchId,
                fullName: selectedDataState.fullName,
                status: Number(selectedDataState.status),
                roleId: selectedDataState.roleId,
                iaNbr: selectedDataState.iaNbr,
                application: selectedDataState.application,
            };
            setLoadingState(true);
            const { response: userResponse, status } = await userService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(userResponse);
                setErrorMessage(error);
            }

            else{
                if (userResponse.code === '00') {
                    dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setFormMode(FM.VIEW);
        setSelectedRow(null);
        setErrorMessage({});
        setInputValue('');
        setInputValueBranch('');
        setTxtUserIdValue('');
        setUserDetailResponse('');
    }, []);

    const listRoles = {
        "Wewenang": roleResponse?.data?.map(item => ({roleId: item.id, name: item.name}))
    };

    const listBranch = {
        "Daftar Cabang": branchResponse?.data
            ?.map((branch) => ({
                branchId: branch.branchId,
                branchNm: branch.branchNm,
            })),
    };

    return(
        <>
            <Grid>
                <TitleCard className="max-h-[85vh] overflow-x-auto col-span-8">
                    <TableForm010100
                        handleRowClick={handleRowClick}
                        selectedRow={selectedRow}
                        refreshTable={refreshTable}
                        isLoading={loadingState}
                        userResponse={userResponse}
                    />
                </TitleCard>

                <div className="col-span-4 mt-6">
                    <ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick} loadingState={loadingState}/>

                    <TitleCard>
                        <Grid smallGap>
                            <RadioButtonGroup
                                className="col-span-6"
                                height="32px"
                                disabled={formMode === FM.VIEW}
                                error={errorMessage?.status}
                                label="Status"
                                mandatory
                            >
                                <CheckboxButton
                                    padding="0 0 0 5px"
                                    disabled={formMode === FM.VIEW}
                                    label="Aktif"
                                    checked={selectedDataState?.status === 1 ? true : false} // jika status adalah null, maka akan bernilai false
                                    onClick={(e) => {
                                        setSelectedDataState({...selectedDataState, status: e.target.checked ? 1 : 0});
                                    }}
                                />
                            </RadioButtonGroup>

                            <Textbox
                                className="col-span-6"
                                maxLength={5}
                                mandatory
                                key={renderKey}
                                autoFocus={formMode !== FM.VIEW}
                                disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                                label="User Id"
                                value={selectedDataState?.userId}
                                error={errorMessage?.userId}
                                onBlurOrEnter={txtUserIdValue}
                                setOnBlurOrEnter={setTxtUserIdValue}
                                onChange={(e) => setSelectedDataState({...selectedDataState, userId:e.target.value})}/>

                            <Textbox
                                className="col-span-12"
                                upperCase
                                mandatory
                                disabled={true}
                                label="Nama Lengkap"
                                name="name"
                                value={selectedDataState?.fullName}
                                error={errorMessage?.fullName}
                                onChange={(e) => setSelectedDataState({...selectedDataState, fullName:e.target.value})}/>

                            <Selectbox
                                className="col-span-12"
                                inputValue={inputValueBranch}
                                setInputValue={setInputValueBranch}
                                mandatory
                                disabled={true}
                                label="Cabang"
                                idKey="branchId"
                                nameKey="branchNm"
                                value={selectedDataState?.branchId}
                                datas={listBranch}
                                error={errorMessage?.branchId}
                                onChange={(child) => setSelectedDataState({...selectedDataState, branchId:child})}
                            />

                            <Selectbox
                                className="col-span-12"
                                inputValue={inputValue}
                                setInputValue={setInputValue}
                                mandatory
                                disabled={formMode === FM.VIEW}
                                label="Role Id"
                                idKey="roleId"
                                nameKey="name"
                                value={selectedDataState?.roleId}
                                datas={listRoles}
                                error={errorMessage?.roleId}
                                onChange={(child) => setSelectedDataState({...selectedDataState, roleId:child})}
                            />

                            <Textbox
                                className="col-span-12"
                                mandatory
                                disabled={formMode === FM.VIEW}
                                label="No. Rekening IA"
                                value={selectedDataState?.iaNbr}
                                error={errorMessage?.iaNbr}
                                onChange={(e) => setSelectedDataState({...selectedDataState, iaNbr:e.target.value})}/>

                            <RadioButtonGroup
                                className="col-span-12"
                                disabled={formMode === FM.VIEW}
                                error={errorMessage?.application}
                                label="Aplikasi"
                                mandatory
                            >
                                <div className="grid grid-cols-2 w-full">
                                    <CheckboxButton
                                        checked={selectedDataState.application.includes('FE_CUSTOMER')}
                                        disabled={formMode === FM.VIEW}
                                        value="FE_CUSTOMER"
                                        label="Customer"
                                        onClick={() => {
                                            const newApplication = [...selectedDataState.application];
                                            if (newApplication.includes('FE_CUSTOMER')) {
                                                const index = newApplication.indexOf('FE_CUSTOMER');
                                                newApplication.splice(index, 1);
                                            } else {
                                                newApplication.push('FE_CUSTOMER');
                                            }
                                            setSelectedDataState({...selectedDataState, application: newApplication});
                                        }}
                                    />

                                    <CheckboxButton
                                        checked={selectedDataState.application.includes('FE_TELLER')}
                                        disabled={formMode === FM.VIEW}
                                        value="FE_TELLER"
                                        label="Office"
                                        onClick={() => {
                                            const newApplication = [...selectedDataState.application];
                                            if (newApplication.includes('FE_TELLER')) {
                                                const index = newApplication.indexOf('FE_TELLER');
                                                newApplication.splice(index, 1);
                                            } else {
                                                newApplication.push('FE_TELLER');
                                            }
                                            setSelectedDataState({...selectedDataState, application: newApplication});
                                        }}
                                    />

                                    <CheckboxButton
                                        checked={selectedDataState.application.includes('FE_BE')}
                                        disabled={formMode === FM.VIEW}
                                        value="FE_BE"
                                        label="Apps Management"
                                        onClick={() => {
                                            const newApplication = [...selectedDataState.application];
                                            if (newApplication.includes('FE_BE')) {
                                                const index = newApplication.indexOf('FE_BE');
                                                newApplication.splice(index, 1);
                                            } else {
                                                newApplication.push('FE_BE');
                                            }
                                            setSelectedDataState({...selectedDataState, application: newApplication});
                                        }}
                                    />
                                </div>
                            </RadioButtonGroup>
                        </Grid>

                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode} loadingState={loadingState}/>
                    </TitleCard>
                </div>
            </Grid>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}