import React, { useEffect, useState, useCallback, useMemo } from 'react';
import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm051200 from "./_components/TableForm051200";
import Textbox from "../components/joy-ui/Textbox";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import { printerService } from "./_repository/printerService";
import { parmService } from "./_repository/parmService";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import Grid from "../components/Grid";
import RadioButtonGroup from "../components/joy-ui/RadioButtonGroup";
import CheckboxButton from "../components/joy-ui/CheckboxButton";
import Intbox from "../components/joy-ui/Intbox";
import Box from "@mui/joy/Box";
import Selectbox from "../components/joy-ui/Selectbox";
import AccordionGroup from '@mui/joy/AccordionGroup';
import Accordion from '@mui/joy/Accordion';
import AccordionDetails, {accordionDetailsClasses,} from '@mui/joy/AccordionDetails';
import AccordionSummary, {accordionSummaryClasses,} from '@mui/joy/AccordionSummary';
import {BlurOnOutlined, InfoOutlined} from "@mui/icons-material";
import RestartAltOutlinedIcon from "@mui/icons-material/RestartAltOutlined";
import Button from "@mui/joy/Button";
import SystemUpdateAltOutlinedIcon from '@mui/icons-material/SystemUpdateAltOutlined';
import {productService} from "./_repository/productService";
import HeightOutlinedIcon from "@mui/icons-material/HeightOutlined";
import FormatSizeOutlinedIcon from "@mui/icons-material/FormatSizeOutlined";

const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    details: [],
    grpId: 0,
    prodId: "",
    typePrinter: "",
    description: "",
    width: 0,
    height: 0,
    fontSize: 0,
    status: false,
};

const defaultDetail = (field) => {
    return {
        field: field,
        status: false,
        positionX: 0,
        positionY: 0,
        fieldValue: "",
        valueWidth: 0,
        label: "",
        labelWidth: 0,
        maxLength: 0,
    };
};

const magnifier = 1.6;

const Form051200 = () => {

    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [showModalUpdatePreview, setShowModalUpdatePreview] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [originalPreviewState, setOriginalPreviewState] = useState(emptyData);
    const [printerResponse, setPrinterResponse] = useState({});
    const [fieldPrint, setFieldPrint] = useState({});
    const [dpiScreen, setDpiScreen] = useState(0);
    const [typePrinter, setTypePrinter] = useState({});
    const [grupEform, setGrupEform] = useState({});
    const [productEform, setProductEform] = useState({});
    const [productName, setProductName] = useState({});
    const [updatePreviewReponse, setUpdatePreviewReponse] = useState(emptyData);
    const [inputValue, setInputValue] = useState({});
    const [inputValue2, setInputValue2] = useState({});
    const [inputValue3, setInputValue3] = useState({});
    const [positionY, setPositionY] = useState({});
    const [saveDisabledState, setSaveDisabledState] = useState(false);
    const [errorMessage, setErrorMessage] = useState(null);

    const dispatch = useDispatch();

    const fetchData = useCallback(async (service, stateSetter, ...params) => {
        try {
            setLoadingState(true);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchPrinter = useCallback(() => {
        fetchData(printerService, setPrinterResponse, "GET_ALL");
    }, []);

    const fetchFieldPrint = useCallback(() => {
        fetchData(parmService, setFieldPrint, "GET_1", "FIELDPRINT");
    }, []);

    const fetchDpiScreen = useCallback(() => {
        fetchData(parmService, setDpiScreen, "GET_1", "DPISCREEN");
    }, []);

    const fetchJenisPrinter = useCallback(() => {
        fetchData(parmService, setTypePrinter, "GET_1", "TYPEPRINTER");
    }, []);

    const fetchGrupEform = useCallback(() => {
        fetchData(parmService, setGrupEform, "GET_1", "EFORMGRPAPPRV");
    }, []);

    const fetchProductEform = useCallback(() => {
        fetchData(parmService, setProductEform, "GET_1", "EFORMPRD");
    }, []);

    const fetchProductName = useCallback((prodId) => {
        fetchData(productService, setProductName, "GET_1", prodId);
    }, []);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Konfigurasi Header Printer"}))
        fetchPrinter();
        fetchFieldPrint();
        fetchDpiScreen();
        fetchJenisPrinter();
        fetchGrupEform();
        fetchProductEform();
        setSaveDisabledState(true);
    }, []);

    useEffect(() => {
        fetchPrinter();
    }, [refreshTable]);

    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.grpId+data.prodId+data.typePrinter);
        setFormMode(FM.VIEW);
    }, []);

    const handleAddClick = useCallback(() => {
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setOriginalPreviewState(emptyData);
        setUpdatePreviewReponse(emptyData);
        setInputValue({});
        setInputValue2({});
        setInputValue3({});
        setErrorMessage(null);
        setSaveDisabledState(true);
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.grpId !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.grpId !== null) {
            setLoadingState(true);
            const { response, status } = await printerService("DELETE", selectedDataState);
            if (status === 0) {
                setErrorMessage(handleError(response));
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const data = {
                details: selectedDataState.details,
                prodId: selectedDataState.prodId,
                typePrinter: selectedDataState.typePrinter,
                description: selectedDataState.description,
                status: selectedDataState.status,
                grpId: parseInt(selectedDataState.grpId),
                width: parseInt(selectedDataState.width),
                height: parseInt(selectedDataState.height),
                fontSize: parseInt(selectedDataState.fontSize),
            };
            setLoadingState(true);
            const { response, status } = await printerService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }

            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);
                }
                resetFormData();
            }
            setShowModalUpdatePreview(false);
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            const responseDataError = response?.data?.error;

            if (responseDataError.hasOwnProperty('details')) {
                dispatch(showNotification({ message: "Daftar Kolom masih ada yang kosong", status: 0 }));
            }

            return responseDataError;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setLoadingState(false)
        setSelectedDataState(emptyData);
        setShowModalSave(false);
        setShowModalDelete(false);
        setShowModalUpdatePreview(false);
        setOriginalDataState(emptyData);
        setOriginalPreviewState(emptyData);
        setUpdatePreviewReponse(emptyData);
        setInputValue({});
        setInputValue2({});
        setInputValue3({});
        setPositionY({});
        setSaveDisabledState(false);
        setErrorMessage(null);
    }, []);

    const combineParamAndResponse = useMemo(() => {
        return (
            (fieldPrint?.data?.map((field, index) => {
                const detail = selectedDataState?.details?.find((detail) => detail.field === field);

                // Set default values if detail is not found
                const defaultValue = {
                    fieldValue: "",
                    label: "",
                    status: false,
                    labelWidth: 0,
                    valueWidth: 0,
                    maxLength: 0,
                    positionX: 0,
                    positionY: 0
                };

                return {
                    field,
                    ...((detail && {
                        fieldValue: detail.fieldValue,
                        label: detail.label,
                        status: detail.status,
                        labelWidth: detail.labelWidth,
                        valueWidth: detail.valueWidth,
                        maxLength: detail.maxLength,
                        positionX: detail.positionX,
                        positionY: detail.positionY
                    }) || defaultValue)
                };
            }) || [])
                .sort((a, b) => {
                    // Urutkan berdasarkan positionX secara ascending
                    if (a.positionX !== b.positionX) {
                        return a.positionX - b.positionX;
                    }
                    // Jika positionX sama, urutkan berdasarkan positionY secara ascending
                    return a.positionY - b.positionY;
                })
        );
    });

    useEffect(() => {
        if(selectedDataState?.details?.length > 0) {
            const details = selectedDataState?.details?.map(detail => ({
                ...detail,
                maxLength: Math.round((detail.valueWidth * dpiScreen.data / 25.4) / selectedDataState.fontSize)
            }));

            setSelectedDataState(prevState => ({
                ...prevState,
                details: details
            }));

            setSaveDisabledState(true);
        }
    }, [selectedDataState?.fontSize, selectedDataState?.height, selectedDataState?.width])

    const handleDetailChange = useCallback((fieldId, fieldName, value) => {
        setSelectedDataState((prevState) => {
            const initialDetails = { field: fieldId, [fieldName]: value };
            const initialDetailsArray = prevState?.details || [];
            const detailIndex = initialDetailsArray.findIndex((detail) => detail.field === fieldId);

            const updatedDetails = detailIndex !== -1
                ? initialDetailsArray.map((detail, index) =>
                    index === detailIndex ? { ...detail, [fieldName]: value } : detail
                )
                : [...initialDetailsArray, initialDetails];

            updatedDetails.forEach((detail) => {
                if (detail.field === fieldId) {
                    detail.maxLength = Math.round((detail.valueWidth * dpiScreen.data / 25.4) / selectedDataState.fontSize);
                }
            });

            setSaveDisabledState(true);

            return {
                ...prevState,
                details: updatedDetails,
            };
        });
    }, [selectedDataState]);

    const listTypePrinter = {
        "Tipe Printer": typePrinter?.data?.map((item, index) => ({ id: item, name: '' }))
    };

    let fieldWrapCount = {};

    function wordWrapContent(content, maxLength, fieldName) {
        if (content.length > maxLength) {
            const words = content.split(' ');
            const wrappedContent = [];
            let currentLine = '';
            let count = 1;

            words.forEach((word, index) => {
                if (currentLine.length + word.length <= maxLength) {
                    currentLine += word + ' ';
                } else {
                    wrappedContent.push(<React.Fragment key={index}>{currentLine.trim()}<br /></React.Fragment>);
                    count++;
                    currentLine = word + ' ';
                }

                if (index === words.length - 1) {
                    wrappedContent.push(<React.Fragment key={index + 1}>{currentLine.trim()}</React.Fragment>);
                }
            });

            fieldWrapCount[fieldName] = count;

            return wrappedContent;
        }

        return content;
    }

    function cleanObject(obj) {
            for (let key in obj) {
                 if (typeof obj[key] === 'string' && obj[key].trim() === '' || obj[key] === null || obj[key] === undefined) {
                    return false; // Kembalikan false jika ada properti yang kosong/null
                }
            }
            return true; // Kembalikan true jika semua properti tidak kosong/null
    }

    const handleUpdatePreview = useCallback(async () => {
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (selectedDataState.grpId !== null) {
            setLoadingState(true);
            selectedDataState.details.sort((a, b) => a.positionX - b.positionX);
            selectedDataState.details = selectedDataState.details.filter(detail => cleanObject(detail));
            const { response, status } = await printerService("GET_UPDATE_PREVIEW", selectedDataState);
            if (status === 0) {
                setErrorMessage(handleError(response));
            }
            else{
                if (response.code === '00') {
                    setOriginalPreviewState({...updatePreviewReponse});
                    setUpdatePreviewReponse(response.data);
                    setSaveDisabledState(false);
                    setShowModalUpdatePreview(true);
                }
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    });

    useEffect(() => {
        const updateDetails = updatePreviewReponse?.details || [];
        const selectedDetails = selectedDataState?.details || [];

        combineParamAndResponse?.forEach((item, index) => {

            const field = item.field;
            const detailIndex = updateDetails.findIndex((detail) => detail.field === field);
            const detail = detailIndex !== -1 ? updateDetails[detailIndex] : selectedDetails.find((detail) => detail.field === field) || {};
            const currentDetail = { ...defaultDetail(field), ...detail };
            const newPosition = `${currentDetail.positionY * magnifier}mm`;

            setPositionY((prevPositionY) => ({
                ...prevPositionY,
                [item.field]: newPosition,
            }));
        });
    }, [updatePreviewReponse, selectedDataState]);

    const listJenisPrinter = {
        "Jenis Printer": typePrinter?.data?.map((item, index) => ({ id: item, name: '' }))
    };

    const listGrupEform = {
        "Grup": grupEform?.data?.map((item, index) => ({ id: item.id, name: item.desc }))
    };

    useEffect(() => {
        if (Object.keys(productEform).length !== 0) {
            fetchProductName(productEform?.data?.prdSvgId);
        }
    }, [productEform]);

    const [listProdukEform, setListProdukEform] = useState({
        "Produk": [
            {
                "id": productEform?.data?.prdSvgId,
                "name": "",
            }
        ]
    });

    useEffect(() => {
        setListProdukEform({
            "Produk": [
                {
                    "id": productEform?.data?.prdSvgId,
                    "name": productName?.data?.prodNm || "",
                }
            ]
        });
    }, [productEform, productName]);

    return(
        <>
            <Grid>
                <TitleCard className="col-span-5 h-[213px]">
                    <TableForm051200
                        handleRowClick={handleRowClick}
                        selectedRow={selectedRow}
                        refreshTable={refreshTable}
                        isLoading={loadingState}
                        response={printerResponse}
                    />
                </TitleCard>

                <div className="col-span-7 mt-6">
                    <ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick}/>
                    <TitleCard className={'col-span-6 h-fit'}>
                        <Grid smallGap>
                            <RadioButtonGroup
                                className="col-span-3"
                                height="32px"
                                label="Status"
                                rgClassName={'-mt-[3.5px]'}
                                mandatory
                                disabled={formMode === FM.VIEW}
                            >
                                <CheckboxButton
                                    padding="0 5px 0 5px"
                                    checked={selectedDataState?.status || false}
                                    label="Aktif"
                                    disabled={formMode === FM.VIEW}
                                    onClick={() => {
                                        setSelectedDataState({...selectedDataState, status: !selectedDataState?.status});
                                    }}
                                />
                            </RadioButtonGroup>

                            <Selectbox
                                className="col-span-3"
                                inputValue={inputValue}
                                setInputValue={setInputValue}
                                mandatory
                                disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                                label="Tipe Printer"
                                idKey="id"
                                nameKey=""
                                value={selectedDataState?.typePrinter}
                                datas={listTypePrinter}
                                error={errorMessage?.typePrinter}
                                onChange={(child) => setSelectedDataState({...selectedDataState, typePrinter:child})}
                            />

                            <Selectbox
                                inputValue={inputValue3}
                                setInputValue={setInputValue3}
                                className="col-span-6"
                                mandatory
                                disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                                label="ID Produk"
                                idKey="id"
                                nameKey="name"
                                value={selectedDataState?.prodId}
                                datas={listProdukEform}
                                error={errorMessage?.prodId}
                                onChange={(child) => setSelectedDataState({...selectedDataState, prodId:child})}
                            />

                            <Selectbox
                                inputValue={inputValue2}
                                setInputValue={setInputValue2}
                                className="col-span-6"
                                mandatory
                                disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                                label="ID Grup"
                                idKey="id"
                                nameKey="name"
                                value={selectedDataState?.grpId}
                                datas={listGrupEform}
                                error={errorMessage?.grpId}
                                onChange={(child) => setSelectedDataState({...selectedDataState, grpId:child})}
                            />

                            <Textbox
                                key={renderKey}
                                autoFocus={formMode !== FM.VIEW}
                                maxLength={50}
                                className="col-span-6"
                                value={selectedDataState?.description}
                                label="Deskripsi"
                                error={errorMessage?.description}
                                disabled={formMode === FM.VIEW}
                                onChange={(e) => setSelectedDataState({...selectedDataState, description:e.target.value})}
                            />
                        </Grid>
                    </TitleCard>
                </div>

                <div className="col-span-2">
                    <TitleCard>
                        <Grid smallGap>
                            <Intbox
                                startWith={<BlurOnOutlined/>}
                                className="col-span-12"
                                tooltip={"DPI adalah Dots Per Inch (satuan layar) yang digunakan untuk perhitungan rumus jumlah baris dan maksimal karakter."}
                                value={dpiScreen?.data}
                                label="DPI"
                                disabled={true}
                            />

                            <Intbox
                                startWith={<HeightOutlinedIcon className="rotate-90"/>}
                                endWith={"mm"}
                                className="col-span-12 "
                                inStyle={{ color: '#2094f3' }}
                                value={selectedDataState?.width}
                                label="Lebar Buku"
                                error={errorMessage?.width}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, width: parseInt(e.target.value)})}
                            />

                            <Intbox
                                startWith={<HeightOutlinedIcon/>}
                                endWith={"mm"}
                                className="col-span-12"
                                inStyle={{ color: '#ff5724' }}
                                value={selectedDataState?.height}
                                label="Tinggi Buku"
                                error={errorMessage?.height}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, height: parseInt(e.target.value)})}
                            />

                            <Intbox
                                startWith={<FormatSizeOutlinedIcon/>}
                                endWith={"px"}
                                className="col-span-12"
                                value={selectedDataState?.fontSize}
                                label="Ukuran Huruf"
                                error={errorMessage?.fontSize}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, fontSize: parseInt(e.target.value)})}
                            />
                        </Grid>
                    </TitleCard>

                    <div className="mt-[14px]">
                        <Button
                            disabled={formMode === FM.VIEW}
                            className={"w-full"}
                            variant={saveDisabledState ? "solid" : "soft"}
                            color="primary"
                            startDecorator={<SystemUpdateAltOutlinedIcon className="w-4 h-4"/>}
                            onClick={() => handleUpdatePreview()}
                        >
                            Update Preview
                        </Button>
                    </div>

                    <ButtonResetSave
                        vertical
                        reset={resetFormData}
                        save={handleSaveClick}
                        formMode={formMode}
                        disabledState={saveDisabledState}
                    />
                </div>

                <Grid className="col-span-10 mb-6">
                    <TitleCard className="col-span-12" title="Daftar Kolom">
                        <Grid>
                            <div className="grid grid-cols-89 col-span-12 gap-x-2.5">
                                {combineParamAndResponse?.map((item, index) => {
                                    const detailIndex = selectedDataState.details.findIndex((detail) => detail.field === item.field);
                                    const detail = detailIndex !== -1 ? selectedDataState.details[detailIndex] : {};
                                    const currentDetail = { ...defaultDetail(item.field), ...detail };

                                    return (
                                        <React.Fragment key={item.field}>
                                            <Textbox
                                                className="col-span-12"
                                                label={index === 0 ? "Kolom" : null}
                                                value={currentDetail.field}
                                                disabled={true}
                                                onChange={(e) => handleDetailChange(item?.field, 'field', e.target.value)}
                                            />

                                            <RadioButtonGroup
                                                height="32px"
                                                className="col-span-8"
                                                label={index === 0 ? "Status" : null}
                                                disabled={formMode === FM.VIEW}
                                                fcClassName={index === 0 ? "" : "mt-[-4px]"}
                                            >
                                                <CheckboxButton
                                                    padding="0 0 0 5px"
                                                    checked={currentDetail.status}
                                                    label="Aktif"
                                                    disabled={formMode === FM.VIEW}
                                                    onClick={(e) => handleDetailChange(item?.field, 'status', !currentDetail?.status)}
                                                />
                                            </RadioButtonGroup>

                                            <Intbox
                                                className="col-span-7"
                                                label={index === 0 ? "X" : null}
                                                value={currentDetail.positionX}
                                                disabled={formMode === FM.VIEW}
                                                onChange={(e) => handleDetailChange(item?.field, 'positionX', parseInt(e.target.value))}
                                            />

                                            <Intbox
                                                className="col-span-7"
                                                label={index === 0 ? "Y" : null}
                                                value={currentDetail.positionY}
                                                disabled={formMode === FM.VIEW}
                                                onChange={(e) => handleDetailChange(item?.field, 'positionY', parseInt(e.target.value))}
                                            />

                                            <Textbox
                                                className="col-span-14"
                                                label={index === 0 ? "Label" : null}
                                                value={currentDetail.label}
                                                disabled={formMode === FM.VIEW}
                                                onChange={(e) => handleDetailChange(item?.field, 'label', e.target.value)}
                                            />

                                            <Intbox
                                                className="col-span-8"
                                                label={index === 0 ? "P. Label" : null}
                                                value={currentDetail.labelWidth}
                                                disabled={formMode === FM.VIEW}
                                                onChange={(e) => handleDetailChange(item?.field, 'labelWidth', parseInt(e.target.value))}
                                            />

                                            <Textbox
                                                className="col-span-25"
                                                label={index === 0 ? "Nilai" : null}
                                                value={currentDetail.fieldValue}
                                                disabled={formMode === FM.VIEW}
                                                onChange={(e) => handleDetailChange(item?.field, 'fieldValue', e.target.value)}
                                            />

                                            <Intbox
                                                className="col-span-8"
                                                label={index === 0 ? "P. Nilai" : null}
                                                value={currentDetail.valueWidth}
                                                disabled={formMode === FM.VIEW}
                                                onChange={(e) => handleDetailChange(item?.field, 'valueWidth', parseInt(e.target.value))}
                                            />

                                            {/*== DEV MODE ==*/}
                                            {/*<Intbox*/}
                                            {/*    className="col-span-8"*/}
                                            {/*    label={index === 0 ? "Max. Nilai" : null}*/}
                                            {/*    value={currentDetail.maxLength}*/}
                                            {/*    disabled={true}*/}
                                            {/*    onChange={(e) => handleDetailChange(item?.field, 'maxLength', parseInt(e.target.value))}*/}
                                            {/*/>*/}
                                        </React.Fragment>
                                    );
                                })}
                            </div>

                            <AccordionGroup
                                className="col-span-12"
                                variant="outlined"
                                transition="0.2s"
                                sx={{
                                    opacity: '80%',
                                    fontSize: '14px',
                                    borderRadius: 'lg',
                                    [`& .${accordionSummaryClasses.button}:hover`]: {
                                        bgcolor: 'transparent',
                                    },
                                    [`& .${accordionDetailsClasses.content}`]: {
                                        boxShadow: (theme) => `inset 0 1px ${theme.vars.palette.divider}`,
                                        [`&.${accordionDetailsClasses.expanded}`]: {
                                            paddingBlock: '0.75rem',
                                        },
                                    },
                                }}
                            >
                                <Accordion>
                                    <AccordionSummary>
                                <span>
                                    <InfoOutlined sx={{width: '17px', paddingRight: '5px'}}/>Informasi Detail
                                </span>
                                    </AccordionSummary>
                                    <AccordionDetails variant="soft">
                                        <table>
                                            <tr>
                                                <td>- P. Label</td>
                                                <td>:</td>
                                                <td>Panjangnya kotak kolom Label dalam satuan milimeter (mm)</td>
                                            </tr>
                                            <tr>
                                                <td>- P. Nilai</td>
                                                <td>:</td>
                                                <td>Panjangnya kotak kolom Nilai dalam satuan milimeter (mm)</td>
                                            </tr>
                                            <tr>
                                                <td>- Max. Nilai</td>
                                                <td>:</td>
                                                <td>Maksimal isi nilai yang dapat diketik</td>
                                            </tr>
                                        </table>
                                    </AccordionDetails>
                                </Accordion>
                            </AccordionGroup>
                        </Grid>
                    </TitleCard>

                    <TitleCard className="col-span-12"
                               title="Preview"
                               isBordered={false}
                    >
                        <Box style={{
                            position: "relative",
                            width: `${selectedDataState.width * magnifier}mm`,
                            height: `${selectedDataState.height * magnifier}mm`,
                            // fontSize: `${((selectedDataState.fontSize / dpiScreen.data) * 25.4) * magnifier}mm`,
                            fontSize: `${selectedDataState.fontSize * magnifier}px`,
                            borderTop: '2px dashed #9ca3af',
                            borderLeft: '2px dashed #9ca3af',
                            borderBottom: '2px dashed #2094f3',
                            borderRight: '2px dashed #ff5724',
                            marginRight: '5px',
                        }}
                        >
                            {combineParamAndResponse?.map((item, index) => {
                                const detailIndex = selectedDataState.details.findIndex((detail) => detail.field === item.field);
                                const detail = detailIndex !== -1 ? selectedDataState.details[detailIndex] : {};

                                if (detailIndex === -1) {
                                    return null;
                                }

                                const currentDetail = { ...defaultDetail(item.field), ...detail };
                                if (!currentDetail?.status) {
                                    return null;
                                }

                                const fieldValueFormatted = wordWrapContent(currentDetail.fieldValue, currentDetail.maxLength, item.field);

                                return (
                                    <div key={item.field}
                                         style={{
                                             fontFamily: "CourierPrime",
                                             position: "absolute",
                                             top: positionY[item.field],
                                             left: `${currentDetail.positionX * magnifier}mm`,
                                         }}
                                    >
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td style={{
                                                    verticalAlign: `top`,
                                                    width: `${currentDetail.labelWidth * magnifier}mm`,
                                                }}>
                                                    {currentDetail.label}
                                                </td>

                                                <td style={{
                                                    verticalAlign: `top`,
                                                }}>
                                                    :
                                                </td>

                                                <td style={{
                                                    verticalAlign: `top`,
                                                    width: `${currentDetail.valueWidth * magnifier}mm`,
                                                }}>
                                                    {fieldValueFormatted}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                );
                            })}

                        </Box>
                        <AccordionGroup
                            variant="outlined"
                            transition="0.2s"
                            sx={{
                                marginTop: '5px',
                                opacity: '80%',
                                fontSize: '14px',
                                borderRadius: 'lg',
                                [`& .${accordionSummaryClasses.button}:hover`]: {
                                    bgcolor: 'transparent',
                                },
                                [`& .${accordionDetailsClasses.content}`]: {
                                    boxShadow: (theme) => `inset 0 1px ${theme.vars.palette.divider}`,
                                    [`&.${accordionDetailsClasses.expanded}`]: {
                                        paddingBlock: '0.75rem',
                                    },
                                },
                            }}
                        >
                            <Accordion>
                                <AccordionSummary>
                                <span>
                                    <InfoOutlined sx={{width: '17px', paddingRight: '5px'}}/>Informasi Detail
                                </span>
                                </AccordionSummary>
                                <AccordionDetails variant="soft">
                                    <table>
                                        <tr>
                                            <td>- Tekan "Update Preview" terlebih dahulu sebelum melakukan Save.</td>
                                        </tr>
                                        <tr>
                                            <td>- Tampilan pada preview bukan tampilan sebenarnya, silahkan lakukan print test pada aplikasi front end.</td>
                                        </tr>
                                        <tr>
                                            <td>- Tampilan pada preview ditingkatkan {magnifier}x dari ukuran aslinya, untuk meningkatkan keterbacaan.</td>
                                        </tr>
                                        <tr>
                                            <td>- <span className="text-[#2094f3] font-semibold">Garis Biru</span> menggambarkan lebar buku/kertas.</td>
                                        </tr>
                                        <tr>
                                            <td>- <span className="text-[#ff5724] font-semibold">Garis Merah</span> menggambarkan tinggi buku/kertas.</td>
                                        </tr>
                                    </table>
                                </AccordionDetails>
                            </Accordion>
                        </AccordionGroup>
                    </TitleCard>
                </Grid>
            </Grid>

            <ResponsiveModal
                color={"primary"}
                open={showModalUpdatePreview}
                title="Konfirmasi"
                desc="Anda telah melakukan Update Preview, lanjut lakukan Save?"
                yesAction={handleConfirmSave}
                noAction={() => {
                    setShowModalUpdatePreview(false);
                    dispatch(showNotification({message : "Preview berhasil diperbarui", status : 2}));
                }}
            />

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
};

export default Form051200;
