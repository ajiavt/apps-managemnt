import LandingIntro from "../../features/user/LandingIntro";
import InputText from "../../components/Input/InputText";
import loginService from "./repository/loginService";
import ErrorAlert from "../../components/Alert/ErrorAlert";
import setLocalStorage from "../../utils/localStorages/setLocalStorage";
import React , { useState } from "react";

function FormLogin() {
    const INITIAL_LOGIN_OBJ = {
        grant_type: "password",
        username: "",
        password: ""
    }

    const [loading, setLoading] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")
    const [loginObj, setLoginObj] = useState(INITIAL_LOGIN_OBJ)

    const submitForm = async (e) => {
        e.preventDefault()
        setErrorMessage("")

        if (loginObj.username.trim() === "") return setErrorMessage("Username tidak boleh kosong!")
        if (loginObj.password.trim() === "") return setErrorMessage("Password tidak boleh kosong!")
        else {
            setLoading(true)
            const {response, status} = await loginService(loginObj)
            if(status === 0) {
                setErrorMessage(response)
            } else {
                // INSERT TOKEN TO LOCAL STORAGE, bisa juga ke cookies/session storage
                setLocalStorage(response);
                window.location.href = '/hbam'
            }
            setLoading(false)
        }
    }

    const updateFormValue = ({updateType, value}) => {
        setErrorMessage("")
        setLoginObj({...loginObj, [updateType]: value})
    }

    return (
        <div className="min-h-screen bg-base-200 flex items-center">
            <div className="card mx-auto w-full max-w-5xl shadow-xl">
                <div className="grid md:grid-cols-2 grid-cols-1 bg-base-100 rounded-xl md:-mt-0">
                    <LandingIntro backgroundColor="bg-base-200"/>
                    <div className='md:py-24 md:px-24 py-10 px-10'>
                        <h2 className='text-2xl font-semibold text-center'>Halaman Login</h2>
                        <form onSubmit={(e) => submitForm(e)}>
                            <div className="mb-4">
                                <InputText defaultValue={loginObj.username}
                                           updateType="username"
                                           containerStyle="mt-4"
                                           labelTitle="Username"
                                           updateFormValue={updateFormValue}/>

                                <InputText isPassword
                                           defaultValue={loginObj.password}
                                           updateType="password"
                                           containerStyle="mt-4"
                                           labelTitle="Password"
                                           updateFormValue={updateFormValue}/>
                            </div>

                            <button type="submit" className="btn mt-2 w-full btn-primary">
                                <span className={(loading ? " loading loading-spinner" : "")}></span>
                                Masuk
                            </button>
                            <ErrorAlert condition={errorMessage} styleClass="mt-8">{errorMessage}</ErrorAlert>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FormLogin