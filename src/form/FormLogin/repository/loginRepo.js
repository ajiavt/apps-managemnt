import {apiClient} from "../../../app/axios";
import {apiURL} from "../../../app/url";

const loginRepo = async (data) => {
    try {
        const success = await apiClient.post(
            apiURL.GET_TOKEN,
            data
        )

        return {response : success};
    }

    catch (error) {
        return {response: error};
    }
}

export default loginRepo;