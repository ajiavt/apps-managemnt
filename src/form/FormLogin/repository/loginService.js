import loginRepo from "./loginRepo";

const loginService = async (data) => {
    const {response}  = await loginRepo(data);

    if(response.status === 200) {
        return {response: response?.data, status: 1};
    } else {
        let str2 = "";
        try {
            const str = response.response?.data['error_description'];
            if(str) {
                str2 = str[0].toUpperCase() + str.slice(1) + "!";
            } else {
                str2 = "Koneksi API tidak tersambung!"
            }
        } catch(error) {
            str2 = "Koneksi API tidak tersambung! " + error;
        } finally {
            return {response: str2, status: 0};
        }
        
    }
}

export default loginService;