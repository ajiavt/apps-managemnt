import React, {useEffect, useState} from 'react'
import { useDispatch } from 'react-redux'
import {setPageTitle, showNotification} from '../../features/common/headerSlice'

function FormHome(){

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setPageTitle({ title : "Home"}))
    }, [])

    return(
        <>
            <div className="h-full w-full flex justify-center items-center text-lg opacity-20">
                Selamat datang di aplikasi Apps Management Hybrid Branch
            </div>
        </>
    )
}

export default FormHome