import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, {useEffect, useState, useCallback} from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm050100 from "./_components/TableForm050100";
import Textbox from "../components/joy-ui/Textbox";
import { parmService } from "./_repository/parmService";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";

import Box from "@mui/joy/Box";
import Selectbox from "../components/joy-ui/Selectbox";
import FormLabel from "@mui/joy/FormLabel";
import DynamicJsonField from "../components/joy-ui/DynamicJsonField";
import ParmValueField from "../components/joy-ui/ParmValueField";
import DynamicNestedParmValueField from "../components/joy-ui/DynamicNestedParmValueField";
import Button from "@mui/joy/Button";
import RestartAltOutlinedIcon from "@mui/icons-material/RestartAltOutlined";

// FM = Form Modes
const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    parmId: null,
    parmNm: null,
    parmValue: null,
    typeData: null,
    isEdit: false,
    cfgParmReffs: []
};

export default function Form050100(){
    const [errMsg, setErrMsg] = useState({});
    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);

    const [formLoading, setFormLoading] = useState(false)
    const [parmTypesData] = useState({
        'Daftar Tipe Data': [
            {
                typeData: 'string',
                typeDataNm: 'String'
            },
            {
                typeData: 'integer',
                typeDataNm: 'Integer'
            },
            {
                typeData: 'decimal',
                typeDataNm: 'Decimal'
            },
            {
                typeData: 'boolean',
                typeDataNm: 'Boolean'
            },
            {
                typeData: 'json',
                typeDataNm: 'JSON'
            },
            {
                typeData: 'array_string',
                typeDataNm: 'Array Of String'
            },
            {
                typeData: 'array_integer',
                typeDataNm: 'Array Of Integer'
            },
            {
                typeData: 'array_decimal',
                typeDataNm: 'Array Of Decimal'
            },
            {
                typeData: 'array_json',
                typeDataNm: 'Array Of JSON'
            },
        ]
    })
    const [selectedParmType, setSelectedParmType] = useState('')


    const [recentSelectedData, setRecentSelectedData] = useState(null); // data dari row
    const [detailedSelectedData, setDetailedSelectedData] = useState(null)
    const [selectedDataState, setSelectedDataState] = useState(emptyData); // modifiable form data
    const [originalDataState, setOriginalDataState] = useState(emptyData); // dimapping dari selectedDataState, saat edit dan reset (dikongkan)

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    const [filterKeyword, setFilterKeyword] = useState('')
    const [filteredResult, setFilteredResult] = useState([])
    const [filteredCount, setFilteredCount] = useState(0)

    // parm value in array-object format
    const [items, setItems] = useState([])
    const [parmRefs, setParmRefs] = useState([])

    const fetchData = useCallback(async () => {
        try{
            setLoadingState(true);

            await new Promise(resolve => setTimeout(resolve, 500));
            const { response, status } = await parmService("GET_ALL");

            if (status === 0) {
                setErrorMessage(response)
            } else {
                setResponse(response)
            }
        } catch (error){
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchDetailedParam = async () => {
        try {
            setFormLoading(true)

            // call API detailed param by id
            const { response, status } = await parmService("GET_BY_ID", { parmId: selectedDataState.parmId});
            if (status === 0) {
                const errMsg = response?.data?.message ? response.data.message : `${response.status} - ${response.statusText}`
                setErrorMessage(errMsg)
            } else {
                // update current selected data
                let parmRefs = response?.data?.cfgParmReffs ? response.data.cfgParmReffs : []
                let updatedDataState = {...selectedDataState, cfgParmReffs: parmRefs}

                setSelectedDataState(updatedDataState)
                setDetailedSelectedData(updatedDataState)

            }
        } catch (error) {
            setErrorMessage("Gagal ambil data detail parameter");
        } finally {
            setFormLoading(false)
        }
    }

    useEffect(() => {
        dispatch(setPageTitle({ title : "Pemeliharaan Parameter"}))
    }, [])

    useEffect(() => {
        fetchData();
    }, [fetchData, refreshTable]);

    useEffect(() => {
        if (errorMessage) {
            // TODO tambah handling jika error tanpa field error, data
            dispatch(showNotification({ message: errorMessage, status: 0 }));
        }
    }, [errorMessage]);

    useEffect(() => {
        if (['json', 'array_json'].includes(selectedDataState.typeData)) {
            fetchDetailedParam()
        }
    }, [recentSelectedData])

    useEffect(() => {
        // convert items --to--> array values --to--> JSON string
        let arrValues = [];
        if (selectedParmType === 'array_json') {
            for (let item of items) {
                if (item?.subDetails) {
                    let valueObj = {}
                    for (let eachProp of item.subDetails) {
                        valueObj[eachProp.key] = eachProp.value
                    }

                    arrValues.push(valueObj)
                } else {
                    arrValues = []
                }
            }
        } else {
            for (let x of items) {
                arrValues.push(x.value)
            }
        }

        const parmValueJson = JSON.stringify(arrValues, null, 2);
        setSelectedDataState({ ...selectedDataState, parmValue: parmValueJson })
    }, [items])

    useEffect(() => {
        setSelectedDataState({...selectedDataState, cfgParmReffs: parmRefs})
    }, [parmRefs])

    useEffect(() => {
        if (response && response.data && Array.isArray(response.data)) {
            setFilteredResult([...response.data])
        }
    }, [response])

    useEffect(() => {
        triggerSearch(filterKeyword)
    }, [filterKeyword])

    {/** START - EVENTS ON BUTTON*/}
    const handleRowClick = useCallback((data) => {
        let jsonParmValue
        if (Array.isArray(data?.parmValue) && data?.parmValue.length === 1) { // note: hack jika array berisi 1 data ditampilkan sebagai string/integer
            jsonParmValue = [...data.parmValue]
        } else {
            try {
                jsonParmValue = JSON.parse(data?.parmValue);
            } catch (error) {
                jsonParmValue = data?.parmValue;
            }
        }
        data.parmValue = JSON.stringify(jsonParmValue);

        setSelectedDataState(data);
        setSelectedRow(data.parmId);

        setSelectedParmType(data?.typeData)
        setRecentSelectedData(data)
        setDetailedSelectedData(null)

        setFormMode(FM.VIEW);
    }, []);

    const triggerSearch = async (keyword) => {
        const responseData = response?.data ? response.data : []
        if (keyword !== null && keyword.length > 3) {
            let searchResult = await doLocalSearch(responseData, keyword);

            setFilteredResult(searchResult)
            setFilteredCount(searchResult.length)
        } else {
            setFilteredResult(responseData)
            setFilteredCount(responseData.length)

            return false
        }

        return true
    }


    const handleResetSearch = (e) => {
        setFilterKeyword(null)
    }

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setRecentSelectedData(emptyData)
        setDetailedSelectedData(emptyData)
        setSelectedParmType('')

        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.parmId !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.parmId !== null) {
            setLoadingState(true);
            const { response, status } = await parmService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrMsg(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        // Cek jika tidak ada data yang berubah
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = async () => { // useCallback(async () => {
        if (formMode !== FM.VIEW) {
            let jsonParmValue;
            try {
                jsonParmValue = JSON.parse(selectedDataState.parmValue);
            } catch (error) {
                jsonParmValue = selectedDataState.parmValue;
            }

            const data = {
                parmId: selectedDataState.parmId,
                parmNm: selectedDataState.parmNm,
                parmValue: jsonParmValue,
                typeData: selectedDataState.typeData,
                cfgParmReffs: selectedDataState.cfgParmReffs ? selectedDataState.cfgParmReffs : []
            };

            try {
                setFormLoading(true);

                await new Promise(resolve => setTimeout(resolve, 1000));
                const { response, status } = await parmService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
                if (status === 0) {
                    let error = handleError(response); // extract error jika ada
                    setErrMsg(error);
                } else {
                    if (response.code === '00') {
                        dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                        setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                    }
                    resetFormData();
                }
            } catch (error) {
                dispatch(showNotification({message : `Error lainnya saat simpan data!`, status : 0}));
            } finally {
                setShowModalSave(false);
                setFormLoading(false);
            }
        }
    } // }, [formMode, selectedDataState]);

    /** END - EVENTS ON BUTTON*/


    // handleOnUpdateValue()

    const handleError = (response) => {
        if (response?.data?.code === "98") {
            dispatch(showNotification({ message: response?.data?.message, status: 0 }));
            return response?.data?.error;
        } else if (["99", "93", "94"].includes(response?.data?.code)) {
            dispatch(showNotification({ message: response?.data?.message, status: 0 }));
        } else {
            return response;
        }
    }

    const doLocalSearch = async (arrayData, keyword) => {
        const searchTerm = keyword.toLowerCase()

        return arrayData.filter(value => {
            return value.parmId.toLowerCase().match(new RegExp(searchTerm, 'g')) ||
                value.parmNm.toLowerCase().match(new RegExp(searchTerm, 'g'))
        })
    }

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrMsg({});

        setRecentSelectedData(emptyData)
        setDetailedSelectedData(emptyData)
        setSelectedParmType('')
    }, []);

    return(
        <>
            <div className="grid lg:grid-cols-12 lg:gap-x-6">
                <div className="lg:col-span-5">
                    <div className="grid lg:grid-cols-12 mt-4 gap-x-2.5">
                        <Textbox
                            className="col-span-9"
                            placeHolder="Ketik id atau nama parameter"
                            value={filterKeyword}
                            onChange={(e) => {
                                const rawKeyword = e.target.value
                                setFilterKeyword(rawKeyword)
                                // handleChangeSearch
                            }}
                        />
                        <Button
                            className="col-span-3"
                            variant="outlined"
                            color="neutral"
                            size="sm"
                            startDecorator={<RestartAltOutlinedIcon className="w-4 h-4" />}
                            onClick={handleResetSearch}
                        >
                            Reset
                        </Button>
                    </div>
                    {/*<TitleCard className="max-h-[85vh] overflow-x-auto lg:col-span-5">*/}
                    <TitleCard className="max-h-[85vh] overflow-x-auto lg:col-span-12">
                        <TableForm050100
                            handleRowClick={handleRowClick}
                            selectedRow={selectedRow}
                            refreshTable={refreshTable}
                            isLoading={loadingState}
                            filteredResult={filteredResult}
                            // response={response}
                        />
                    </TitleCard>
                </div>

                <div className="lg:col-span-7 mt-6">
                    <ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick} />
                    <TitleCard>
                        <div className="grid md:grid-cols-12 gap-x-2.5">
                            <Textbox
                                mandatory
                                upperCase
                                key={renderKey}
                                autoFocus={formMode !== FM.VIEW}
                                className="col-span-5"
                                disabled={formMode === FM.VIEW}
                                id="txtParmId"
                                label="Parm Id"
                                name="parmId"
                                value={selectedDataState?.parmId}
                                error={errMsg?.parmId}
                                onChange={(e) => setSelectedDataState({...selectedDataState, parmId:e.target.value})}/>

                            <Textbox
                                mandatory
                                autoFocus={formMode !== FM.VIEW}
                                className="col-span-7"
                                disabled={formMode === FM.VIEW}
                                id="txtParmNm"
                                label="Parm Name"
                                name="parmNm"
                                value={selectedDataState?.parmNm}
                                error={errMsg?.parmNm}
                                onChange={(e) => setSelectedDataState({...selectedDataState, parmNm:e.target.value})}/>
                        </div>

                        <div className="grid grid-cols-12 gap-x-1 gap-y-2">
                            <Selectbox
                                datas={parmTypesData}
                                inputValue={selectedParmType}
                                setInputValue={setSelectedParmType}
                                disabled={formMode !== FM.ADD}
                                className="col-span-6 mt-[5px] mb-[-10px]"
                                idKey="typeData"
                                nameKey="typeDataNm"
                                label="Tipe Data"
                                showValueInOption={false}
                                onChange={(eventValue) => {
                                    setSelectedDataState({
                                        ...selectedDataState,
                                        parmValue: '',
                                        typeData: eventValue
                                    })
                                }}
                            />
                            <span className="col-span-6"></span>
                            <div className="col-span-12">
                            {
                                ['string', 'integer', 'decimal', 'boolean'].includes(selectedParmType) ? ( // SKENARIO 1 ✅ : FORM <<---1to1--->> ParmValueField
                                    <ParmValueField
                                        disabled={formMode === FM.VIEW}
                                        typeData={selectedParmType}
                                        currentValue={recentSelectedData?.parmValue}
                                        onUpdateValue={(rawValue) => {
                                            // handle update value, parmValue (preview textarea) hanya dapat menerima nilai string & int
                                            let parmValueStr = '' + rawValue
                                            setSelectedDataState({...selectedDataState, parmValue: parmValueStr})
                                        }}
                                    />
                                ) : ['array_string', 'array_integer', 'array_boolean', 'array_json'].includes(selectedParmType) ? ( // SKENARIO 2 : FORM <<---1to1--->> DynamicNestedParmValueField <<---1toN--->> ParmValueField
                                    <Box
                                        sx={{
                                            height: 500,
                                            width: '100%',
                                            '& .actions': {
                                                color: 'text.secondary',
                                            },
                                            '& .textPrimary': {
                                                color: 'text.primary',
                                            },
                                        }}
                                    >
                                        <DynamicNestedParmValueField
                                            typeData={selectedParmType}
                                            disabled={formMode === FM.VIEW}
                                            parmValue={recentSelectedData?.parmValue}
                                            detailedSelectedData={detailedSelectedData}
                                            items={items}
                                            setItems={setItems}
                                            setParmRefs={setParmRefs}
                                        />
                                    </Box>

                                ) : ['json'].includes(selectedParmType) ? (  // SKENARIO 3 : FORM <<---1to1--->> DynamicJsonField <<---1toN--->> ParmValueField
                                    <div>
                                        <FormLabel className="col-span-12 mt-2">Nilai Parameter JSON</FormLabel>
                                        <DynamicJsonField
                                            typeData={selectedParmType}
                                            disabled={formMode === FM.VIEW}
                                            selectedData={detailedSelectedData}
                                            selectedDataSetter={(setterValue) => {
                                                setSelectedDataState({
                                                    ...selectedDataState,
                                                    parmValue: JSON.stringify(setterValue.parmValue, null, 2),
                                                    cfgParmReffs: setterValue.cfgParmReffs
                                                })
                                            }}
                                        />
                                    </div>
                                ) : (
                                    <div><p className="text-info">Silakan pilih tipe data terlebih dahulu untuk menampilkan area inputan nilai</p></div>
                                )
                            }
                            </div>
                        </div>
                        <hr />

                        <div className="col-span-12 mt-2">
                            <Textbox
                                textarea={true}
                                mandatory
                                minRows={3}
                                maxRows={5}
                                disabled={false}
                                id="txtParmValue"
                                label="Preview Parm Value"
                                placeHolder="Preview Value JSON"
                                tooltip="Parm Value menggunakan value berbentuk JSON."
                                value={selectedDataState?.parmValue}
                                error={errMsg?.parmValue}
                            />
                        </div>

                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode} loadingState={formLoading}/>
                    </TitleCard>

                </div>
            </div>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
                loadingState={formLoading}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}