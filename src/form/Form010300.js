import TitleCard from "../components/Cards/TitleCard";
import { useDispatch } from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import { setPageTitle, showNotification } from "../features/common/headerSlice";
import Selectbox from "../components/joy-ui/Selectbox";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import { roleService } from "./_repository/roleService";
import Grid from "../components/Grid";
import DataGridModule from "./_components/DataGridModule";
import {parmService} from "./_repository/parmService";
import Textbox from "../components/joy-ui/Textbox";
import {branchService} from "./_repository/branchService";
import {userService} from "./_repository/userService";

const Form010300 = () => {

    const [errorMessage, setErrorMessage] = useState(null);
    const [showModalSave, setShowModalSave] = useState(false);
    const [selectedDataState, setSelectedDataState] = useState({});
    const [originalDataState, setOriginalDataState] = useState({});
    const [roleResponse, setRoleResponse] = useState({});
    const [branchResponse, setBranchResponse] = useState({});
    const [userResponse, setUserResponse] = useState({});
    const [moduleResponse, setModuleResponse] = useState({});
    const [dataGrid, setDataGrid] = useState([]);
    const [columnGrid, setColumnGrid] = useState([]);
    const [inputValRole, setInputValRole] = useState({});
    const [inputValBranch, setInputValBranch] = useState({});
    const [selection, setSelection] = useState([]);
    const [loadingState, setLoadingState] = useState(false)
    const [txtUserIdValue, setTxtUserIdValue] = useState('');

    const dispatch = useDispatch();

    const fetchData = useCallback(async (service, stateSetter, ...params) => {
        setLoadingState(true);
        try {
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchRoles = useCallback(() => {
        fetchData(roleService, setRoleResponse, "GET_ALL");
    }, []);

    const fetchBranch = useCallback(() => {
        fetchData(branchService, setBranchResponse, "GET_ALL");
    }, []);

    const fetchDataGrid = useCallback((data) => {
        const pk = {id: data};
        fetchData(roleService, setModuleResponse, "GET_1_MODULE", pk);
    }, []);

    const fetchDataSelection = useCallback((data) => {
        fetchData(userService, setUserResponse, "GET_1_MODULE_BY_BRANCH", data);
    }, []);

    useEffect(() => {
        dispatch(setPageTitle({ title: "Generate User Akses Modul Aplikasi Nasabah" }));
        fetchRoles();
        fetchBranch();

        const columns = [
            { field: 'id', headerName: 'ID', width: 70 },
            { field: 'desc', headerName: 'Deskripsi', width: 400 },
        ];

        setColumnGrid(columns);
    }, []);

    const handleSaveClick = useCallback(() => {
        let newData  = {
            listGroupId: selection,
            roleId: selectedDataState.roleId,
            userId: selectedDataState.userId,
            branchId: selectedDataState.branchId,
        };

        setSelectedDataState(newData);

        if (JSON.stringify(originalDataState) === JSON.stringify(newData)) {
            dispatch(showNotification({ message: "Tidak ada data yang berubah!", status: 2 }));
            return;
        }

        setShowModalSave(true);
    }, [originalDataState, selectedDataState, selection]);

    const handleConfirmSave = useCallback(async () => {
        setLoadingState(true);
        const { response, status } = await userService("GENERATE_MODULE", selectedDataState);
        if (status === 0) {
            let error = handleError(response);
            setErrorMessage(error);
        } else {
            if (response.code === '00') {
                dispatch(showNotification({ message: response?.message, status: 1 }));
            }
            resetFormData();
        }
        setShowModalSave(false);
        setLoadingState(false);
    }, [selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    useEffect(() => {
        if(Object.keys(moduleResponse).length !== 0) {
            const newData = moduleResponse.data.modules.map(module => ({
                desc: module.grpNm,
                id: module.grpId
            }));

            setDataGrid(newData);
        }
    }, [moduleResponse]);

    const resetFormData = useCallback(() => {
        setErrorMessage(null);
        setShowModalSave(false);
        setSelectedDataState({});
        setOriginalDataState({});
        setUserResponse({});
        setModuleResponse({});
        setDataGrid([]);
        setInputValRole({});
        setInputValBranch({});
        setSelection([]);
        setLoadingState(false)
        setTxtUserIdValue('');
    }, []);

    const handleSelectboxBranch = (child) => {
        // jika ada perubahan di selectbox branch maka ...
        setSelectedDataState({...selectedDataState, branchId: child});
    };

    const listRoles = {
        "Daftar Wewenang": roleResponse?.data
    };

    const listBranch = {
        "Daftar Cabang": branchResponse?.data
            ?.map((branch) => ({
                branchId: branch.branchId,
                branchNm: branch.branchNm,
            })),
    };

    useEffect(() => {
        if(txtUserIdValue === '') {
            // Jika setiap Branch diganti, maka reset ulang dulu intinya sih gitu
            setUserResponse({});
            setTxtUserIdValue('');
            setModuleResponse([]);
            setDataGrid([]);
            setSelectedDataState({...selectedDataState, userId: ''});
        }
    }, [inputValBranch]);

    useEffect(() => {
        setUserResponse({});
        setTxtUserIdValue('');
        setModuleResponse([]);
        setDataGrid([]);

        if(txtUserIdValue !== '') {
            const data = { userId: selectedDataState?.userId, branchId: selectedDataState?.branchId };
            fetchDataSelection(data)
        }
    }, [txtUserIdValue]);

    useEffect(() => {
        if (Object.keys(userResponse).length !== 0) {
            const data = userResponse?.data;
            const roleId = data?.roleId;

            if(roleId !== '') {
                fetchDataGrid(roleId);
                if(errorMessage == null) {
                    const dataState = {
                        listGroupId: data?.modules?.map(module => module.grpId),
                        roleId: roleId,
                        userId: data?.userId,
                        branchId: data?.branchId,
                    };
                    setOriginalDataState(dataState);
                    setSelectedDataState(dataState);
                }
            }
        }
    }, [userResponse])

    return (
        <>
            <Grid>
                <Grid className="col-span-4 h-fit">
                    <TitleCard className="col-span-12">
                        <Grid smallGap>
                            <Selectbox
                                className="col-span-12"
                                inputValue={inputValBranch}
                                setInputValue={setInputValBranch}
                                mandatory
                                label="Cabang"
                                idKey="branchId"
                                nameKey="branchNm"
                                value={selectedDataState?.branchId}
                                datas={listBranch}
                                error={errorMessage?.branchId}
                                onChange={handleSelectboxBranch}
                            />

                            <Textbox
                                className="col-span-4"
                                tooltip="Tekan enter atau tab untuk melakukan proses."
                                maxLength={5}
                                mandatory
                                label="User Id"
                                value={selectedDataState?.userId}
                                error={errorMessage?.userId}
                                onBlurOrEnter={txtUserIdValue}
                                setOnBlurOrEnter={setTxtUserIdValue}
                                disabled={!inputValBranch}
                                onChange={(e) => setSelectedDataState({...selectedDataState, userId:e.target.value})}
                            />

                            <Textbox
                                className="col-span-8"
                                maxLength={5}
                                mandatory
                                disabled={true}
                                label="Nama Lengkap"
                                value={userResponse?.data?.fullName || ''}
                                error={errorMessage?.userId}
                            />

                            <Selectbox
                                className="col-span-12"
                                inputValue={inputValRole}
                                setInputValue={setInputValRole}
                                value={userResponse?.data?.roleId || ''}
                                idKey="id"
                                nameKey="name"
                                label="Wewenang"
                                disabled={true}
                                datas={listRoles}
                                error={errorMessage?.id}
                            />
                        </Grid>
                    </TitleCard>

                    <div className="col-span-12 mt-[10px]">
                        <ButtonResetSave
                            reset={resetFormData}
                            save={handleSaveClick}
                            loadingState={loadingState}
                        />
                    </div>
                </Grid>

                <TitleCard className="h-[85vh] overflow-x-auto col-span-8">
                    <DataGridModule
                        loadingState={loadingState}
                        selection={selection}
                        setSelection={setSelection}
                        data={dataGrid}
                        columns={columnGrid}
                        selectedData={userResponse?.data?.modules}
                    />
                </TitleCard>
            </Grid>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />
        </>
    );
};

export default Form010300;