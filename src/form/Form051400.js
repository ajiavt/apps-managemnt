import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import ResponsiveModal from "../components/joy-ui/Modal";
import Grid from "../components/Grid";
import Button from "@mui/joy/Button";
import {DeleteForeverOutlined} from "@mui/icons-material";
import Alert from "@mui/joy/Alert";
import {cacheService} from "./_repository/cacheService";

const emptyChecklist = {
    anyChecked: false,
    checkedIds: []
}

export default function Form051400(){

    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [loadingState, setLoadingState] = useState(false)
    const [showModalDelete, setShowModalDelete] = useState(false);

    const [selectedChecklist, setSelectedChecklist] = useState(emptyChecklist)
    const [showModalClear, setShowModalClear] = useState(false);

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Cache Sistem"}))
    }, []);

    {/** START - EVENTS ON BUTTON*/}

    const handleClickClear = useCallback(() => {
        setSelectedChecklist({...selectedChecklist, anyChecked: true, checkedIds: ['ALL']})
        setShowModalClear(true);
    }, [selectedChecklist]);

    const handleConfirmClear = useCallback(async () => {
        if (selectedChecklist.anyChecked) {
            setLoadingState(true);
            const { response, status } = await cacheService("CLEAR", selectedChecklist);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalClear(false);
        }
    }, [selectedChecklist]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedRow(null);
        setErrorMessage(null);
        setSelectedChecklist(emptyChecklist)
    }, []);

    return(
        <>
            <Grid>

                <div className="col-span-6 mt-6">
                    <TitleCard>
                        <Alert color="warning" variant="outlined">
                            <p>Membersihkan tampungan cache artinya mengosongkan tampungan salinan data terkait kebutuhan quick retrieval. <br />Cache akan terisi kembali secara incremental sepanjang penggunaan sistem</p>
                        </Alert>

                        {/*<div className="-mb-3.5">*/}
                            <Button
                                disabled={loadingState}
                                className="w-full"
                                variant={"soft"}
                                color="danger"
                                size="lg"
                                startDecorator={<DeleteForeverOutlined className="w-4 h-4" />}
                                onClick={handleClickClear}
                            >
                                Bersihkan Cache
                            </Button>
                        {/*</div>*/}
                    </TitleCard>
                </div>
            </Grid>

            <ResponsiveModal
                color={"danger"}
                open={showModalClear}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin membersihkan cache ?"
                yesAction={handleConfirmClear}
                noAction={() => setShowModalClear(false)}
            />
        </>
    )
}