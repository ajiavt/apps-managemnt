import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch } from "react-redux";
import { setPageTitle, showNotification } from "../features/common/headerSlice";
import { bannerService } from "./_repository/bannerService";
import TitleCard from "../components/Cards/TitleCard";
import TableForm050900 from "./_components/TableForm050900";
import Textbox from "../components/joy-ui/Textbox";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import UploadButton from "../components/joy-ui/UploadButton";
import Card from '@mui/joy/Card';
import Box from "@mui/joy/Box";
import CheckboxButton from "../components/joy-ui/CheckboxButton";
import RadioButtonGroup from "../components/joy-ui/RadioButtonGroup";
import Typography from "@mui/joy/Typography";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import bannerLayout from "../assets/images/banner-layout.png";
import LoadingComponent from "../components/joy-ui/LoadingComponent";
import Intbox from "../components/joy-ui/Intbox";
import AccordionUploadGambar from "./_components/AccordionUploadGambar";

// FM = Form Modes
const FM = { VIEW: 'view', EDIT: 'edit', ADD: 'add' };
const emptyData = { id: null, title: null, description: null, seq: null, images: null, status: null, };

export default function Form050900() {
    // State variables
    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);
    const [uploadImage, setUploadImage] = useState(null);
    const [imageFileName, setImageFileName] = useState("");

    const dispatch = useDispatch();

    // Fetch data
    const fetchData = useCallback(async () => {
        setLoadingState(true);
        try {
            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await bannerService("GET_ALL");
            status === 0 ? setErrorMessage(response) : setResponse(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    useEffect(() => { fetchData(); }, [fetchData, refreshTable]);

    // Set page title
    useEffect(() => {
        dispatch(setPageTitle({ title: "Pemeliharaan Banner" }))
    }, []);

    // Handle row click
    const handleRowClick = useCallback(async (data) => {
        setLoadingState(true);

        // Check if data has already been loaded
        if (data.images) {
            setLoadingState(false);
            setFormMode(FM.VIEW);
            setSelectedDataState(data);
            setSelectedRow(data.id);
            setUploadImage("data:image/jpeg;base64," + data.images);
            return;
        }

        try {
            const { response, status } = await bannerService("GET_1", data);

            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
                setLoadingState(false);
                return;
            }

            data.images = response.data.images;
            setUploadImage("data:image/jpeg;base64," + data.images);
            setSelectedDataState(data);
            setSelectedRow(data.id);
            setFormMode(FM.VIEW);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, [setUploadImage]);

    // Handle add click
    const handleAddClick = useCallback(() => {
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setErrorMessage(null);
        setImageFileName("");
        setUploadImage(null);
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
    }, []);

    // Handle edit click
    const handleEditClick = useCallback(() => {
        if (selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({ ...selectedDataState });
        } else {
            dispatch(showNotification({ message: "Tidak ada data yang dipilih!", status: 2 }));
        }
    }, [selectedDataState, formMode]);

    // Handle delete click
    const handleDeleteClick = useCallback(() => {
        selectedDataState.id !== null ? setShowModalDelete(true) : dispatch(showNotification({ message: "Tidak ada data yang dipilih!", status: 2 }));
    }, [selectedDataState]);

    // Handle confirm delete
    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.id !== null) {
            setLoadingState(true);
            const { response, status } = await bannerService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }
            else {
                if (response.code === '00') {
                    dispatch(showNotification({ message: "Data berhasil dihapus!", status: 1 }));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    // Handle save click
    const handleSaveClick = useCallback(() => {
        if (formMode === FM.ADD && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({ message: "Data yang ditambahkan masih kosong", status: 2 }));
            return;
        }

        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({ message: "Tidak ada data yang berubah!", status: 2 }));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    // Handle confirm save
    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const imagesWithoutPrefix = selectedDataState?.images?.replace(/^data:image\/[a-zA-Z]+;base64,/, '');

            const data = {
                id: selectedDataState.id,
                status: selectedDataState.status,
                seq: parseInt(selectedDataState.seq),
                description: selectedDataState.description,
                images: imagesWithoutPrefix,
            };

            setLoadingState(true);
            const { response, status } = await bannerService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }
            else {
                if (response.code === '00') {
                    dispatch(showNotification({ message: `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status: 1 }));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrorMessage(null);
        setImageFileName("");
        setUploadImage(null);
    }, []);

    return (
        <>
            <div className="grid grid-cols-12 gap-x-6 overflow-y-hidden">
                <div className="grid grid-cols-12 col-span-12 gap-x-6">
                    <div className="grid grid-cols-12 col-span-4">
                        <TitleCard className="overflow-x-auto col-span-12 h-[305px]">
                            <TableForm050900
                                handleRowClick={handleRowClick}
                                selectedRow={selectedRow}
                                refreshTable={refreshTable}
                                isLoading={loadingState}
                                response={response}
                            />
                        </TitleCard>
                    </div>

                    <div className="grid grid-cols-12 col-span-6 mt-[25px]">
                        <div className="col-span-12">
                            <ButtonCRUD
                                add={handleAddClick}
                                edit={handleEditClick}
                                del={handleDeleteClick}
                                loadingState={loadingState}
                            />
                        </div>

                        <TitleCard className="col-span-12">
                            <div className="grid grid-cols-12 gap-x-2.5">
                                <RadioButtonGroup
                                    height="32px"
                                    className="col-span-3"
                                    disabled={formMode === FM.VIEW}
                                    error={errorMessage?.status}
                                    label="Status Banner"
                                >
                                    <CheckboxButton
                                        padding="0 0 0 5px"
                                        disabled={formMode === FM.VIEW}
                                        value="FE_TELLER"
                                        label="Aktif"
                                        checked={selectedDataState?.status || false} // jika status adalah null, maka akan bernilai false
                                        onClick={() => {
                                            setSelectedDataState({...selectedDataState, status: !selectedDataState?.status});
                                        }}
                                    />
                                </RadioButtonGroup>

                                <Textbox
                                    key={renderKey}
                                    className="col-span-9"
                                    autoFocus={formMode !== FM.VIEW}
                                    maxLength={50}
                                    mandatory
                                    disabled={formMode === FM.VIEW}
                                    label="Deskripsi"
                                    value={selectedDataState?.description}
                                    error={errorMessage?.description}
                                    onChange={(e) => setSelectedDataState({ ...selectedDataState, description: e.target.value })} />
                            </div>

                            <div className="grid grid-cols-12 gap-x-2.5">
                                <Intbox
                                    inClassName="h-[35px]"
                                    mandatory
                                    className="col-span-3"
                                    maxLength={2}
                                    disabled={formMode === FM.VIEW}
                                    label="Urutan Tampil"
                                    value={selectedDataState?.seq}
                                    error={errorMessage?.seq}
                                    onChange={(e) => setSelectedDataState({...selectedDataState, seq:e.target.value})}
                                />

                                <UploadButton
                                    error={errorMessage?.images}
                                    mandatory
                                    labelMargin={"mt-[-2px] mb-[-11px]"}
                                    className="col-span-4 mt-[-4px]"
                                    disabled={formMode === FM.VIEW}
                                    label="Gambar Banner"
                                    fileName={imageFileName}
                                    setFileName={setImageFileName}
                                    onFileChange={(objectUrl, file) => {
                                        const reader = new FileReader();
                                        reader.readAsDataURL(file);
                                        reader.onloadend = () => {
                                            const base64data = reader.result;
                                            setUploadImage(base64data);

                                            setSelectedDataState(prevState => ({
                                                ...prevState,
                                                images: base64data,
                                            }));
                                        }
                                    }}
                                />
                            </div>

                            <AccordionUploadGambar/>
                        </TitleCard>

                        <div className="col-span-12 mt-[10px]">
                            <ButtonResetSave
                                reset={resetFormData}
                                save={handleSaveClick}
                                formMode={formMode}
                                loadingState={loadingState}
                            />
                        </div>
                    </div>

                    <div className="grid grid-cols-12 col-span-12 gap-x-6 mb-[25px]">
                        <TitleCard className="col-span-10" title="Preview" isBordered={false}>
                            <Card className="col-span-12" sx={{ padding: '0', border: '2px dashed #9ca3af' }}>
                                <div className="rounded-md">
                                    <img alt='' src={bannerLayout} style={{ position: 'relative', zIndex: 1 }} />
                                    {uploadImage !== null && (
                                        <img
                                            alt=''
                                            src={uploadImage}
                                            style={{
                                                position: 'absolute',
                                                top: 0,
                                                left: 0,
                                                zIndex: 0,
                                                objectFit: 'cover',
                                                width: '100%',
                                                height: '100%',
                                            }}
                                        />
                                    )}

                                    <Box className="absolute z-10 left-[1%] top-[78%] w-[55.9%] text-white rounded-md overflow-y-auto">
                                        <div className="p-5">
                                            <Typography style={{ whiteSpace: 'pre-line' }}>
                                                    <span className={"font-MontserratBold text-[120%] " + (uploadImage !== null ? "text-white" : "")}>
                                                        {selectedDataState?.description
                                                            ||
                                                            "Lorem Ipsum is simply dummy text of the printing."
                                                        }
                                                    </span>
                                            </Typography>
                                        </div>
                                    </Box>
                                </div>
                            </Card>
                        </TitleCard>
                    </div>
                </div>
            </div>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}