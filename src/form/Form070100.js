import TitleCard from "../components/Cards/TitleCard";
import {useDispatch, useSelector} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import { setPageTitle, showNotification } from "../features/common/headerSlice";
import Selectbox from "../components/joy-ui/Selectbox";
import ResponsiveModal from "../components/joy-ui/Modal";
import Grid from "../components/Grid";
import DataGridModule from "./_components/DataGridModule";
import {branchService} from "./_repository/branchService";
import {approvalService} from "./_repository/approvalService";
import {parmService} from "./_repository/parmService";
import Button from "@mui/joy/Button";
import UnpublishedOutlinedIcon from '@mui/icons-material/UnpublishedOutlined';
import {Chip} from "@mui/joy";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import ModalDetail070100 from "./_components/ModalDetail070100";
import Textarea from "../components/joy-ui/Textarea";
import userDataService from "./FormUserData/repository/userDataService";

const STS = {1: 'Approved', 2: 'Returned', 9: 'Rejected', 0: 'Ready' };
const MENU_ID = {CABANG_LOGIN: '070100', PER_CABANG: '070200'};

const Form010300 = () => {

    const [errorMessage, setErrorMessage] = useState(null);
    const [loadingState, setLoadingState] = useState(false)
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDetail, setShowModalDetail] = useState(false);
    const [selectedDataState, setSelectedDataState] = useState({});
    const [modalDataState, setModalDataState] = useState({});
    const [dataGrid, setDataGrid] = useState([]);
    const [columnGrid, setColumnGrid] = useState([]);
    const [branchResponse, setBranchResponse] = useState({});
    const [userDataResponse, setUserDataResponse] = useState({});
    const [detailResponse, setDetailResponse] = useState({});
    const [statusAksi, setStatusAksi] = useState({});
    const [listDataApproval, setListDataApproval] = useState({});
    const [inputValBranch, setInputValBranch] = useState({});

    const [menuId, setMenuId] = useState(null);
    const {activePath} = useSelector(state => state.pathSet);
    const dispatch = useDispatch();

    const fetchData = useCallback(async (service, stateSetter, isLoading, ...params) => {
        if(isLoading){
            setLoadingState(true);
        }

        try {
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            if(isLoading) {
                setLoadingState(false);
            }
        }
    }, []);

    const fetchBranch = useCallback(() => {
        fetchData(branchService, setBranchResponse, true, "GET_ALL");
    }, []);

    const fetchStatusAksi = useCallback(() => {
        fetchData(parmService, setStatusAksi, true, "GET_1", "APPRVTELBRANCH");
    }, []);

    const fetchListDataApproval = useCallback((data) => {
        let action = "";
        if(menuId === MENU_ID.CABANG_LOGIN) {
            action = "GET_1_BY_BRANCH_USER";
        }

        if(menuId === MENU_ID.PER_CABANG) {
            action = "GET_1_BY_BRANCH";
        }
        fetchData(approvalService, setListDataApproval, true, action, data);
    }, [menuId]);

    const fetchDetailApproval = useCallback((data) => {
        fetchData(approvalService, setDetailResponse, false, "GET_1_DETAIL", data);
    }, []);

    const fetchUserData = useCallback(() => {
        fetchData(userDataService, setUserDataResponse, false, "GET_ALL");
    }, []);

    const listPathForm = [
        {menuId: MENU_ID.CABANG_LOGIN, path: "/app/menu-id=070100", title: ""},
        {menuId: MENU_ID.PER_CABANG, path: "/app/menu-id=070200", title: "Per Cabang"},
    ];

    useEffect(() => {
        if(userDataResponse?.cabang && menuId === MENU_ID.CABANG_LOGIN) {
            setInputValBranch(userDataResponse?.cabangValue);
            handleSelectboxBranch(userDataResponse?.cabangValue);
        }
    }, [userDataResponse]);

    useEffect(() => {
        resetFormData();
        const currentRoute = listPathForm.find(item => item.path === activePath);
        const currentTitle = currentRoute ? currentRoute.title : activePath;
        const menuId = currentRoute ? currentRoute.menuId : null;
        setMenuId(menuId);

        dispatch(setPageTitle({ title : ('Approval Transaksi ' + currentTitle) }));
        if (menuId) {
            fetchBranch();
            fetchStatusAksi();
            fetchUserData();
        }
    }, [activePath]);

    useEffect(() => {
        const columns = [
            {
                width: 40,
                field: "no",
                headerName: "No",
                filterable: false,
                sortable: false,
                align: 'center',
                headerAlign: 'center',
                renderCell: (params) => params.api.getAllRowIds().indexOf(params.id)+1,
            },
            {
                field: 'action',
                headerName: 'Aksi',
                width: 100,
                align: 'center',
                headerAlign: 'center',
                filterable: false,
                sortable: false,
                renderCell: (params) =>
                {
                    const stsValue = params?.row?.sts;
                    const showAksi = statusAksi?.data || [];

                    const handleAksiWithParams = () => {
                        handleAksi(params?.row);
                    };

                    if (showAksi.includes(stsValue)) {
                        return (
                            <Button
                                startDecorator={<UnpublishedOutlinedIcon/>}
                                variant="outlined"
                                color="danger"
                                sx={{ transform: 'scale(0.8)' }}
                                onClick={handleAksiWithParams}
                            >
                                Reject
                            </Button>
                        );
                    } else {
                        return null;
                    }
                }
            },
            {
                field: 'sts',
                headerName: 'Status',
                width: 125,
                align: 'center',
                headerAlign: 'center',
                renderCell: (prop) => {
                    const value = prop.value;
                    const name = STS[value] || value;

                    let color = "neutral";
                    if(value === 1) {
                        color = "primary"
                    } else if(value === 9) {
                        color = "danger"
                    }

                    return (
                        <Chip size="sm" variant="soft" color={color}>
                            {value}-{name}
                        </Chip>
                    );
                },
            },
            { field: 'reason', headerName: 'Alasan', width: 410, },
            { field: 'createdAt', headerName: 'Tgl. Request', width: 165, },
            { field: 'createdBy', headerName: 'User Request', width: 125, align: 'center', headerAlign: 'center', },
            { field: 'grpId', headerName: 'ID Grup', width: 125, align: 'center', headerAlign: 'center', },
            { field: 'deviceId', headerName: 'Device ID', width: 155, },
            { field: 'deviceName', headerName: 'Device Name', width: 125, align: 'center', headerAlign: 'center', },
            { field: 'authUser', headerName: 'User Approval', width: 125, align: 'center', headerAlign: 'center', },
        ];

        setColumnGrid(columns);
    }, [statusAksi]);

    const handleConfirmSave = useCallback(async () => {
        const { response, status } = await approvalService("UPDATE_STATUS_POST", modalDataState);
        if (status === 0) {
            let error = handleError(response);
            setErrorMessage(error);
        }

        else{
            if (response.code === '00') {
                dispatch(showNotification({message : response.message, status : 1}));
                resetFormData();
                if(menuId === MENU_ID.CABANG_LOGIN) {
                    setSelectedDataState({...selectedDataState, branchId: inputValBranch});
                    fetchListDataApproval(selectedDataState);
                }
                fetchStatusAksi();
            }
        }
    }, [modalDataState]);

    const handleConfirmReject = useCallback(() => {
        setShowModalSave(true);
    });

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setErrorMessage(null);
        setLoadingState(false)
        setShowModalSave(false);
        setShowModalDetail(false);
        setSelectedDataState({});
        setModalDataState({});
        setDataGrid([]);
        setColumnGrid([]);
        setDetailResponse({});
        setStatusAksi({});
        setListDataApproval({});
    }, []);

    const handleSelectboxBranch = (child) => {
        // jika ada perubahan di selectbox branch maka ...
        setSelectedDataState({...selectedDataState, branchId: child});
    };

    const handleAksi = (listItem) => {
        setDetailResponse({});
        setModalDataState({...modalDataState, id: listItem?.id, status: 9})
        fetchDetailApproval(listItem);
        setShowModalDetail(true);
    };

    useEffect(() => {
        if(selectedDataState?.branchId !== undefined) {
            const data = {branchId: selectedDataState?.branchId};
            fetchListDataApproval(data);
        }
    }, [selectedDataState?.branchId])

    function formatCreatedAt(dateString) {
        const date = new Date(dateString);
        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const year = date.getFullYear();
        const hours = String(date.getHours()).padStart(2, '0');
        const minutes = String(date.getMinutes()).padStart(2, '0');
        const seconds = String(date.getSeconds()).padStart(2, '0');
        const milliseconds = String(date.getMilliseconds()).padStart(3, '0');
        // return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}.${milliseconds}`;
        return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
    }

    useEffect(() => {
        if (listDataApproval && Object.keys(listDataApproval).length !== 0) {
            const newDataApproval = { ...listDataApproval };
            if (newDataApproval && newDataApproval.data) {
                newDataApproval.data.forEach(item => {
                    item.createdAt = formatCreatedAt(item.createdAt);
                });
            }
            setDataGrid(newDataApproval?.data);
        }
    }, [listDataApproval]);

    const listBranch = {
        "Daftar Cabang": branchResponse?.data
            ?.map((branch) => ({
                branchId: branch.branchId,
                branchNm: branch.branchNm,
            })),
    };

    return (
        <>
            <Grid>
                <Grid smallGap className="col-span-6">
                    <Selectbox
                        disabled={menuId === MENU_ID.CABANG_LOGIN}
                        className="mt-[5px] mb-[-10px] col-span-8"
                        inputValue={inputValBranch}
                        setInputValue={setInputValBranch}
                        mandatory
                        label="Cabang"
                        idKey="branchId"
                        nameKey="branchNm"
                        value={selectedDataState?.branchId}
                        datas={listBranch}
                        error={errorMessage?.branchId}
                        onChange={handleSelectboxBranch}
                    />

                    <div className="mt-[15px] mb-[-5px] col-span-4">
                        <ButtonResetSave reset={resetFormData} className="w-full" visible={menuId === MENU_ID.PER_CABANG}/>
                    </div>

                    {/*Jika perlu pakai button Cari*/}
                    {/*<div className="mt-[25px] mb-[-5px] col-span-3">*/}
                    {/*    <Button*/}
                    {/*        className="w-full"*/}
                    {/*        disabled={loadingState}*/}
                    {/*        variant="outlined"*/}
                    {/*        color="primary"*/}
                    {/*        startDecorator={<SearchOutlined className="w-4 h-4" />}*/}
                    {/*        // onClick={add}*/}
                    {/*    >*/}
                    {/*        Cari*/}
                    {/*    </Button>*/}
                    {/*</div>*/}
                </Grid>

                <TitleCard className="overflow-x-auto h-[80vh] mt-[15px] p-0 col-span-12">
                    <DataGridModule
                        disableExport={false}
                        loadingState={loadingState}
                        data={dataGrid}
                        columns={columnGrid}
                        disableQuickFilter={false}
                        disableColumnFilter={false}
                        disableColumnSelector={false}
                        disableColumnMenu={false}
                        density="standard"
                        pageSize={[25, 50, 100]}
                    />
                </TitleCard>
            </Grid>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                yesAction={handleConfirmSave}
                yesLabel="Submit"
                noAction={() => setShowModalSave(false)}
                noLabel="Tutup"
                enableCenteredButton={true}
            >
                <Textarea label={"Alasan"}
                          value=""
                          minRows={4}
                          maxRows={4}
                          autoFocus={true}
                          onChange={(e) => setModalDataState({...modalDataState, reason:e.target.value})}
                />
            </ResponsiveModal>

            <ResponsiveModal
                width={"100vh"}
                color={"danger"}
                title="Detail Data"
                open={showModalDetail}
                yesAction={handleConfirmReject}
                yesLabel="Reject"
                noAction={() => setShowModalDetail(false)}
                noLabel="Tutup"
                enableCenteredButton={true}
            >
                {detailResponse?.data !== undefined ? (
                    <ModalDetail070100 data={detailResponse?.data} grpNm={"Setoran Tunai"}/>
                ) : (
                    <div className="h-full w-full flex justify-center items-center text-md opacity-20">
                        <span className={"loading loading-spinner"}/>
                    </div>
                )}
            </ResponsiveModal>
        </>
    );
};

export default Form010300;