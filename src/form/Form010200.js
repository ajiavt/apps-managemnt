import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm010200 from "./_components/TableForm010200";
import Textbox from "../components/joy-ui/Textbox";
import { authService } from "./_repository/authService";
import ResponsiveModal from "../components/joy-ui/Modal";
import Grid from "../components/Grid";
import Button from "@mui/joy/Button";
import {DeleteForeverOutlined} from "@mui/icons-material";

// FM = Form Modes
const emptyData = {
    accessToken: null,
    userId: null,
    branchId: null,
    branchNm: null,
    fullName: null,
    roleNm: null,
    ipAddr: null,
    deviceId: null,
    application: null,
    startedAt: null,
};

export default function Form010200(){

    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalDelete, setShowModalDelete] = useState(false);

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    const fetchData = useCallback(async () => {
        try{
            setLoadingState(true);
            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await authService("GET_ALL_SESSION");

            if (status === 0)
                setErrorMessage(response);
            else
                setResponse(response);

        } catch (error){
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData, refreshTable]);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Aktif User"}))
    }, []);

    {/** START - EVENTS ON BUTTON*/}
    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.accessToken);
    }, []);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.accessToken !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.accessToken !== null) {
            setLoadingState(true);
            const { response, status } = await authService("DELETE_SESSION", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setErrorMessage(null);
    }, []);

    const formatDate = (dateString) => {
        if (!dateString) {
            return "";
        }

        const options = {
            day: "2-digit",
            month: "2-digit",
            year: "numeric",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit",
        };

        const formattedDate = new Date(dateString).toLocaleDateString("id-ID", options);
        return formattedDate;
    };

    return(
        <>
            <Grid>
                <TitleCard className="max-h-[85vh] overflow-x-auto col-span-8">
                    <TableForm010200
                        handleRowClick={handleRowClick}
                        selectedRow={selectedRow}
                        refreshTable={refreshTable}
                        isLoading={loadingState}
                        response={response}
                    />
                </TitleCard>

                <div className="col-span-4 mt-6">
                    <div className="-mb-3.5">
                        <Button
                            disabled={loadingState}
                            className="w-full"
                            variant={"soft"}
                            color="danger"
                            startDecorator={<DeleteForeverOutlined className="w-4 h-4" />}
                            onClick={handleDeleteClick}
                        >
                            Akhiri Sesi
                        </Button>
                    </div>

                    <TitleCard>
                        <Grid smallGap>
                            <Textbox
                                className="col-span-6"
                                label="User Id"
                                readOnly={true}
                                value={selectedDataState?.userId}
                                error={errorMessage?.userId}
                                onChange={(e) => setSelectedDataState({...selectedDataState, userId:e.target.value})}/>

                            <Textbox
                                className="col-span-6"
                                readOnly={true}
                                label="Kode Cabang"
                                value={selectedDataState?.branchId}
                                error={errorMessage?.branchId}
                                onChange={(e) => setSelectedDataState({...selectedDataState, branchId:e.target.value})}/>

                            <Textbox
                                className="col-span-12"
                                readOnly={true}
                                label="Nama Cabang"
                                value={selectedDataState?.branchNm}
                                error={errorMessage?.branchNm}
                                onChange={(e) => setSelectedDataState({...selectedDataState, branchNm:e.target.value})}/>

                            <Textbox
                                className="col-span-12"
                                readOnly={true}
                                label="Nama Lengkap"
                                value={selectedDataState?.fullName}
                                error={errorMessage?.fullName}
                                onChange={(e) => setSelectedDataState({...selectedDataState, fullName:e.target.value})}/>

                            <Textbox
                                className="col-span-12"
                                readOnly={true}
                                label="Nama Wewenang"
                                value={selectedDataState?.roleNm}
                                error={errorMessage?.roleNm}
                                onChange={(e) => setSelectedDataState({...selectedDataState, roleNm:e.target.value})}/>

                            <Textbox
                                className="col-span-12"
                                readOnly={true}
                                label="Aplikasi"
                                value={selectedDataState?.application}
                                error={errorMessage?.application}
                                onChange={(e) => setSelectedDataState({...selectedDataState, application:e.target.value})}/>

                            <Textbox
                                className="col-span-12"
                                readOnly={true}
                                label="Dimulai Pada"
                                value={formatDate(selectedDataState?.startedAt)}
                                error={errorMessage?.startedAt}
                                onChange={(e) => setSelectedDataState({...selectedDataState, startedAt:e.target.value})}/>

                            <Textbox
                                className="col-span-12"
                                readOnly={true}
                                label="Device Id"
                                value={selectedDataState?.deviceId}
                                error={errorMessage?.deviceId}
                                onChange={(e) => setSelectedDataState({...selectedDataState, deviceId:e.target.value})}/>
                        </Grid>
                    </TitleCard>
                </div>
            </Grid>

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin mengakhiri sesi?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}