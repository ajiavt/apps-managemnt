import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm050200 from "./_components/TableForm050200";
import Textbox from "../components/joy-ui/Textbox";
import Intbox from "../components/joy-ui/Intbox";
import { deviceService } from "./_repository/deviceService";
import ResponsiveModal from "../components/joy-ui/Modal";
import Textarea from "../components/joy-ui/Textarea";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";

// FM = Form Modes
const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    deviceIdParent: null,
    deviceId: null,
    name: null,
    branchId: null,
    seq: null,
    userLogin: null,
    userRcv: null,
    status: null,
    descr: null,
};

export default function Form050200(){

    const [errMsg, setErrMsg] = useState({});
    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    const fetchData = useCallback(async () => {
        try{
            setLoadingState(true);

            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await deviceService("GET_ALL");

            if (status === 0)
                setErrorMessage(response);
            else
                setResponse(response);

        } catch (error){
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData, refreshTable]);

    useEffect(() => {
        if (errorMessage) {
            dispatch(showNotification({ message: errorMessage, status: 0 }));
        }
    }, [errorMessage]);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Pemeliharaan Perangkat"}))
    }, []);

    {/** START - EVENTS ON BUTTON*/}
    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.deviceId);
        setFormMode(FM.VIEW);
    }, []);

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setSelectedDataState({...selectedDataState});
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.deviceId !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.deviceId !== null) {
            setLoadingState(true);
            const { response, status } = await deviceService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrMsg(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        // Cek jika tidak ada data yang berubah
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const data = {
                deviceId: selectedDataState.deviceId,
                deviceIdParent: selectedDataState.deviceIdParent,
                name: selectedDataState.name,
                branchId: selectedDataState.branchId,
                seq: Number(selectedDataState.seq),
                userLogin: selectedDataState.userLogin,
                userRcv: selectedDataState.userRcv,
                status: Number(selectedDataState.status),
                descr: selectedDataState.descr,
            };
            setLoadingState(true);
            const { response, status } = await deviceService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrMsg(error);
            }

            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);
    {/** END - EVENTS ON BUTTON*/}

    const handleError = (response) => {
        if (response?.data?.code === "98") {
            return response?.data?.error;
        } else if (["99", "93", "94"].includes(response?.data?.code)) {
            dispatch(showNotification({ message: response?.data?.message, status: 0 }));
        } else {
            return response;
        }
    }

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrMsg({});
    }, []);

    return(
        <>
            <div className="grid lg:grid-cols-12 lg:gap-x-6">
                <TitleCard className="max-h-[85vh] overflow-x-auto lg:col-span-8">
                    <TableForm050200
                        handleRowClick={handleRowClick}
                        selectedRow={selectedRow}
                        refreshTable={refreshTable}
                        isLoading={loadingState}
                        response={response}
                    />
                </TitleCard>

                <div className="lg:col-span-4 mt-6">
                    <ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick} loadingState={loadingState}/>
                    <TitleCard>
                        <div className="grid grid-cols-2 gap-x-2.5">
                            <Textbox
                                upperCase
                                key={renderKey}
                                autoFocus={formMode !== FM.VIEW}
                                className="mb-[11px]"
                                disabled={formMode === FM.VIEW}
                                id="txtDeviceName"
                                label="Nama Device"
                                name="name"
                                value={selectedDataState?.name}
                                error={errMsg?.name}
                                onChange={(e) => setSelectedDataState({...selectedDataState, name:e.target.value})}/>

                            <Textbox
                                maxLength={3}
                                disabled={formMode === FM.VIEW}
                                id="txtKodeCabang"
                                label="Kode Cabang"
                                value={selectedDataState?.branchId}
                                error={errMsg?.branchId}
                                onChange={(e) => setSelectedDataState({...selectedDataState, branchId:e.target.value})}/>
                        </div>

                        <Textarea
                            minRows={2}
                            upperCase
                            className="mb-[11px]"
                            disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                            id="txtDeviceId"
                            label="Serial Number"
                            value={selectedDataState?.deviceId}
                            error={errMsg?.deviceId}
                            onChange={(e) => setSelectedDataState({...selectedDataState, deviceId:e.target.value})}/>

                        <Textarea
                            minRows={2}
                            maxLength={40}
                            upperCase
                            disabled={formMode === FM.VIEW}
                            id="txtDeviceIdParent"
                            label="Serial Number Parent"
                            value={selectedDataState?.deviceIdParent}
                            error={errMsg?.deviceIdParent}
                            onChange={(e) => setSelectedDataState({...selectedDataState, deviceIdParent:e.target.value})}/>

                        <Textarea
                            minRows={2}
                            upperCase
                            disabled={formMode === FM.VIEW}
                            id="txtDeskripsi"
                            label="Deskripsi"
                            value={selectedDataState?.descr}
                            error={errMsg?.descr}
                            onChange={(e) => setSelectedDataState({...selectedDataState, descr:e.target.value})}/>

                        <div className="grid grid-cols-2 gap-x-2.5">
                            <Textbox
                                tooltip="User Login digenerate otomatis oleh sistem."
                                upperCase
                                maxLength={5}
                                disabled={true}
                                hidden={formMode !== FM.VIEW}
                                id="txtUserLogin"
                                label="User Login"
                                value={selectedDataState?.userLogin}
                                error={errMsg?.userLogin}
                                onChange={(e) => setSelectedDataState({...selectedDataState, userLogin:e.target.value})}/>

                            <Textbox
                                tooltip="User Rcv digenerate otomatis oleh sistem."
                                upperCase
                                maxLength={5}
                                disabled={true}
                                hidden={formMode !== FM.VIEW}
                                id="txtUserRcv"
                                label="User Rcv"
                                value={selectedDataState?.userRcv}
                                error={errMsg?.userRcv}
                                onChange={(e) => setSelectedDataState({...selectedDataState, userRcv:e.target.value})}/>
                        </div>

                        <div className="grid grid-cols-2 gap-x-2.5">
                            <Intbox
                                tooltip="Status=1 (perangkat sedang aktif)."
                                disabled={true}
                                hidden={formMode !== FM.VIEW}
                                id="txtStatus"
                                label="Status"
                                name="status"
                                value={selectedDataState?.status}
                                error={errMsg?.status}
                                onChange={(e) => setSelectedDataState({...selectedDataState, status:e.target.value})}/>

                            <Intbox
                                tooltip="Sequence digenerate otomatis oleh sistem."
                                maxLength={2}
                                disabled={true}
                                hidden={formMode !== FM.VIEW}
                                id="txtSeq"
                                label="Sequence"
                                name="seq"
                                value={selectedDataState?.seq}
                                error={errMsg?.seq}
                                onChange={(e) => setSelectedDataState({...selectedDataState, seq:e.target.value})}/>
                        </div>

                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode} loadingState={loadingState}/>
                    </TitleCard>
                </div>
            </div>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}