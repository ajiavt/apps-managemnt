import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const bannerService = async (action, data) => {

    const accessToken = localStorage.getItem("access_token");
    const tokenType = localStorage.getItem("token_type");
    const api = apiURL.BANNER_API;
    const primaryKey = data?.id;

    try {
        let response;
        switch(action) {
            case "GET_1":
                response = await apiClient.get(
                    `${api}/${primaryKey}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );

                break;

            case "GET_ALL":
                response = await apiClient.get(
                    `${api}/?start=0&limit=1`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );

                break;

            case "INSERT":
                response = await apiClient.post(
                    api,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "DELETE":
                response = await apiClient.delete(
                    `${api}/${primaryKey}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "UPDATE":
                response = await apiClient.put(
                    api,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}