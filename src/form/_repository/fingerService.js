import {apiClient, apiClientAgent} from "../../app/axios";
import {apiURL} from "../../app/url";

export const fingerService = async (action, data) => {
    const accessToken = localStorage.getItem("access_token");
    const tokenType = localStorage.getItem("token_type");
    const api = apiURL.USER_FINGER_API;
    const apiAgent = '/v1/finger';
    const primaryKey = data?.id;

    try {
        let response;
        switch(action) {
            case "CAPTURE":
                response = await apiClientAgent.post(
                    apiAgent + '/capture/image',
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_ALL":
                response = await apiClient.get(
                    `${api}/?start=0&limit=1000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;
            case "GET_ALL_BY_BRANCH_ID":
                response = await apiClient.get(
                    `${api}/branch/${data.branchId}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;
            case "GET_BY_ID":
                response = await apiClient.get(
                    `${api}/${data}?loadAccess=true&loadMenu=true`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );

                //return { get1: response.data, status: 1 };
                break;

            case "CREATE":
                response = await apiClient.post(
                    api,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "DELETE":
                response = await api.delete(
                    `${api}/${primaryKey}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        if (!(error && error.response)) {
            return {
                status: 0,
                response: {
                    code: error.code,
                    message: error.message,
                    status: 'FAILED'
                }
            }
        }

        return { response: error?.response, status: 0 };
    }
}