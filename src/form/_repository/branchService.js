import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const branchService = async (
    action,
    branchId,
    accessToken = localStorage.getItem("access_token"),
    tokenType=localStorage.getItem("token_type")) =>
{
    const api = apiURL.BRANCH_API;

    try {
        let response;
        switch(action) {
            case "GET_ALL":
                response = await apiClient.get(
                    `${api}/?start=0&limit=1000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_1":
                response = await apiClient.get(
                    `${api}/${branchId}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}
