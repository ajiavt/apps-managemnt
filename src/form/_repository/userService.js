import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const userService = async (action, data) => {

    const accessToken = localStorage.getItem("access_token");
    const tokenType = localStorage.getItem("token_type");
    const api = apiURL.USER_API;
    const primaryKey = data?.userId;
    const branchId = data?.branchId;

    try {
        let response;
        switch(action) {
            case "GET_1_CORE":
                response = await apiClient.get(
                    `${api}/core/banking/${primaryKey}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_1_MODULE":
                response = await apiClient.get(
                    `${api}/${primaryKey}?loadModule=true`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_1_MODULE_BY_BRANCH":
                response = await apiClient.get(
                    `${api}/${primaryKey}/branch/${branchId}?loadModule=true`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GENERATE_MODULE":
                response = await apiClient.post(
                    `${api}/generate/module`,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "GET_ALL":
                response = await apiClient.get(
                    `${api}/?start=0&limit=1000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "INSERT":
                response = await apiClient.post(
                    api,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "DELETE":
                response = await apiClient.delete(
                    `${api}/${primaryKey}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "UPDATE":
                response = await apiClient.put(
                    api,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}