import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const roleService = async (action, data) => {

    const accessToken = localStorage.getItem("access_token");
    const tokenType = localStorage.getItem("token_type");
    const api = apiURL.ROLE_API;
    const primaryKey = data?.id;

    try {
        let response;
        switch(action) {
            case "GET_1_MODULE":
                response = await apiClient.get(
                    `${api}/${primaryKey}?loadModule=true`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GENERATE_MODULE":
                response = await apiClient.post(
                    `${api}/generate/module`,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "GENERATE_ACCESS":
                response = await apiClient.post(
                    `${api}/generate/access`,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "GENERATE_MENUS":
                response = await apiClient.post(
                    `${api}/generate/menus`,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "GET_1":
                response = await apiClient.get(
                    `${api}/${data}?loadAccess=true&loadMenu=true`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );

                return { get1: response.data, status: 1 };
                break;

            case "GET_ALL":
                response = await apiClient.get(
                    `${api}/?start=0&limit=1000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "INSERT":
                response = await apiClient.post(
                    api,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "DELETE":
                response = await apiClient.delete(
                    `${api}/${primaryKey}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "UPDATE":
                response = await apiClient.put(
                    api,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }
        
        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}