import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const addrService = async (menuId) => {

    const accessToken = localStorage.getItem("access_token");
    const tokenType = localStorage.getItem("token_type");

    // {menuId: "050300", path: "/app/menu-id=050300", title: "Negara"},
    // {menuId: "050400", path: "/app/menu-id=050400", title: "Provinsi"},
    // {menuId: "050500", path: "/app/menu-id=050500", title: "Kota/Kabupaten"},
    // {menuId: "050600", path: "/app/menu-id=050600", title: "Kecamatan"},
    // {menuId: "050700", path: "/app/menu-id=050700", title: "Kelurahan/Desa"},

    try {
        let response;
        switch(menuId) {
            case "050300":
                response = await apiClient.get(
                    `${apiURL.GET_COUNTRY}/?start=0&limit=10000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "050400":
                response = await apiClient.get(
                    `${apiURL.GET_PROV}/?start=0&limit=10000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "050500":
                response = await apiClient.get(
                    `${apiURL.GET_CITY}/?start=0&limit=10000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "050600":
                response = await apiClient.get(
                    `${apiURL.GET_KEC}/?start=0&limit=10000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            case "050700":
                response = await apiClient.get(
                    `${apiURL.GET_KEL}/?start=0&limit=10000`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    }
                );
                break;

            default:
                response = `Unknown action: ${menuId}`;
        }
        
        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}