import {apiURL} from "../../app/url";
import {apiClient} from "../../app/axios";

export const cacheService = async (action, data) => {

    const accessToken = localStorage.getItem("access_token");
    const tokenType = localStorage.getItem("token_type");
    const api = apiURL.CACHE_API;

    try {
        let response;
        switch(action) {
            case "CLEAR":
                response = await apiClient.post(
                    `${api}/clear`,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;
            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}