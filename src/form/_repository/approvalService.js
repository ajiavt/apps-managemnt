import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const approvalService = async (
    action,
    data,
    accessToken = localStorage.getItem("access_token"),
    tokenType=localStorage.getItem("token_type")) =>
{
    const api = apiURL.APPROVAL_API;
    const apiBE = apiURL.APPROVAL_BE_API;

    try {
        let response;
        switch(action) {
            case "UPDATE_STATUS_POST":
                response = await apiClient.post(
                    `${apiBE}/approval`,
                    data,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_1_BY_BRANCH":
                response = await apiClient.get(
                    `${apiBE}/approval/${data?.branchId}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_1_BY_BRANCH_USER":
                response = await apiClient.get(
                    `${apiBE}/user/approval/${data?.branchId}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_1_DETAIL":
                response = await apiClient.get(
                    `${api}/${data?.id}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "UPDATE_STATUS":
                response = await apiClient.delete(
                    `${api}/${data?.id}?status=${data?.status}&reason=${data?.reason}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}
