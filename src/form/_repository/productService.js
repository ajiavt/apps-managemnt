import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const productService = async (
    action,
    prodId,
    accessToken = localStorage.getItem("access_token"),
    tokenType=localStorage.getItem("token_type")) =>
{
    const api = apiURL.PRODUCT_API;

    try {
        let response;
        switch(action) {
            case "GET_1":
                response = await apiClient.get(
                    `${api}/02/${prodId}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}
