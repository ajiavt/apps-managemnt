import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const authService = async (
    action,
    data,
    accessToken = localStorage.getItem("access_token"),
    tokenType=localStorage.getItem("token_type")) =>
{

    const api = apiURL.AUTH_API;

    try {
        let response;
        switch(action) {
            case "GET_ALL_SESSION":
                response = await apiClient.get(
                    `${api}/session`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );

                break;

            case "LOGOUT_TELLER":
                response = await apiClient.post(
                    `${api}/logout/teller`,
                    null,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "DELETE_SESSION":
                response = await apiClient.delete(
                    `${api}/session?accessToken=${data.accessToken}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}
