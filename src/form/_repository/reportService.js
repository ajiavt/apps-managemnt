import {apiClient} from "../../app/axios";
import {apiURL} from "../../app/url";

export const reportService = async (
    action,
    userId,
    accessToken = localStorage.getItem("access_token"),
    tokenType=localStorage.getItem("token_type")) =>
{
    const api = apiURL.REPORT_API;

    try {
        let response;
        switch(action) {
            case "GET_RINCIAN_CABANG":
                response = await apiClient.get(
                    `${api}/detail/trx/branchId/${userId}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_RINCIAN_ALL_TELLER":
                response = await apiClient.get(
                    `${api}/detail/trx/userTeller`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            case "GET_RINCIAN_PER_TELLER":
                response = await apiClient.get(
                    `${api}/detail/trx/userTeller/${userId}`,
                    {
                        headers: {
                            'Authorization': `${tokenType} ${accessToken}`,
                        },
                    },
                );
                break;

            default:
                response = `Unknown action: ${action}`;
        }

        return { response: response.data, status: 1 };
    } catch (error) {
        return { response: error?.response, status: 0 };
    }
}
