import TitleCard from "../components/Cards/TitleCard";
import { useDispatch } from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import { setPageTitle, showNotification } from "../features/common/headerSlice";
import Selectbox from "../components/joy-ui/Selectbox";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import { roleService } from "./_repository/roleService";
import Grid from "../components/Grid";
import DataGridModule from "./_components/DataGridModule";
import {parmService} from "./_repository/parmService";

const Form020400 = () => {

    const [errorMessage, setErrorMessage] = useState(null);
    const [showModalSave, setShowModalSave] = useState(false);
    const [selectedDataState, setSelectedDataState] = useState({});
    const [originalDataState, setOriginalDataState] = useState({});
    const [roleResponse, setRoleResponse] = useState({});
    const [moduleResponse, setModuleResponse] = useState({});
    const [csResponse, setCsResponse] = useState({});
    const [tellerResponse, setTellerResponse] = useState({});
    const [dataGrid, setDataGrid] = useState([]);
    const [columnGrid, setColumnGrid] = useState([]);
    const [inputValue, setInputValue] = useState({});
    const [selection, setSelection] = useState([]);
    const [loadingState, setLoadingState] = useState(false)

    const dispatch = useDispatch();

    const fetchData = useCallback(async (service, stateSetter, ...params) => {
        setLoadingState(true);
        try {
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchRoles = useCallback(() => {
        fetchData(roleService, setRoleResponse, "GET_ALL");
    }, []);

    const fetchDataCS = useCallback(() => {
        fetchData(parmService, setCsResponse, "GET_1", "EFORMGRPAPPRV");
    }, []);

    const fetchDataTeller = useCallback(() => {
        fetchData(parmService, setTellerResponse, "GET_1", "TXGRPAPPRV");
    }, []);

    const fetchModule = useCallback((data) => {
        fetchData(roleService, setModuleResponse, "GET_1_MODULE", data);
    }, []);

    useEffect(() => {
        dispatch(setPageTitle({ title: "Generate Akses Modul Aplikasi Nasabah" }));
        fetchRoles();
    }, []);

    const handleSaveClick = useCallback(() => {
        let newData = {};
        newData = {
            listGroupId: selection,
            roleId: selectedDataState.roleId,
        };

        setSelectedDataState(newData);

        if (JSON.stringify(originalDataState) === JSON.stringify(newData)) {
            dispatch(showNotification({ message: "Tidak ada data yang berubah!", status: 2 }));
            return;
        }

        setShowModalSave(true);
    }, [originalDataState, selectedDataState, selection]);

    const handleConfirmSave = useCallback(async () => {
        setLoadingState(true);
        const { response, status } = await roleService("GENERATE_MODULE", selectedDataState);
        if (status === 0) {
            let error = handleError(response);
            setErrorMessage(error);
        } else {
            if (response.code === '00') {
                dispatch(showNotification({ message: response?.message, status: 1 }));
            }
            resetFormData();
        }
        setShowModalSave(false);
        setLoadingState(false);
    }, [selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const handleSelectboxWewenang = useCallback( (value) => {
        setOriginalDataState({});
        setSelectedDataState({});

        if(value !== '') {
            if (value === '05') { // 05 - CS
                setTellerResponse([]);
                fetchDataCS();
            } else if(value === '06') { // 06 - Teller
                setCsResponse([]);
                fetchDataTeller();
            }

            if(errorMessage == null) {
                const data = {id: value};
                fetchModule(data);
            }
        }
    });

    useEffect(() => {
        setColumnGrid([]);
        setDataGrid([]);

        const columns = [
            { field: 'id', headerName: 'ID', width: 70 },
            { field: 'desc', headerName: 'Deskripsi', width: 400 },
        ];

        setColumnGrid(columns);
        if(csResponse?.data?.length > 0) {
            setDataGrid(csResponse?.data);
        }

        if(tellerResponse?.data?.length > 0) {
            setDataGrid(tellerResponse?.data);
        }

    }, [csResponse, tellerResponse]);

    useEffect(() => {
        if (Object.keys(moduleResponse).length !== 0) {
            const dataState = {
                listGroupId: moduleResponse?.data?.modules?.map(module => module.grpId),
                roleId: moduleResponse?.data?.id
            };
            setOriginalDataState(dataState);
            setSelectedDataState(dataState);
        }
    }, [moduleResponse]);

    const resetFormData = useCallback(() => {
        setSelectedDataState({});
        setOriginalDataState({});
        setErrorMessage(null);
        setInputValue('');
        setModuleResponse({});
        setCsResponse({});
        setTellerResponse({});
        setDataGrid([]);
        setColumnGrid([]);
        setSelection([]);
    }, []);

    const listRoles = {
        "Daftar Wewenang": roleResponse?.data?.filter(item => item.id === "05" || item.id === "06")
    };

    return (
        <>
            <Grid>
                <div className="col-span-8 ">
                    <Grid smallGap>
                        <Selectbox
                            inputValue={inputValue}
                            setInputValue={setInputValue}
                            className="mt-[5px] mb-[-10px] col-span-6"
                            idKey="id"
                            nameKey="name"
                            label="Wewenang"
                            datas={listRoles}
                            error={errorMessage?.id}
                            onChange={(value) =>
                                handleSelectboxWewenang(value)
                            }
                        />
                        <ButtonResetSave
                            className="col-span-6 h-[36.4px] mt-[15px]"
                            reset={resetFormData}
                            save={handleSaveClick}
                            loadingState={loadingState}
                        />
                    </Grid>

                        <TitleCard className="overflow-x-auto w-[80%] h-[75vh] mt-[15px] p-0">
                        <DataGridModule
                            loadingState={loadingState}
                            selection={selection}
                            setSelection={setSelection}
                            data={dataGrid}
                            columns={columnGrid}
                            selectedData={moduleResponse?.data?.modules}
                        />
                    </TitleCard>
                </div>
            </Grid>
            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />
        </>
    );
};

export default Form020400;