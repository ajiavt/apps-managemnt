import TitleCard from "../components/Cards/TitleCard";
import { useDispatch } from "react-redux";
import React, { useEffect, useState, useCallback } from "react";
import { setPageTitle, showNotification } from "../features/common/headerSlice";
import TableForm060200 from "./_components/TableForm060200";
import { reportService } from "./_repository/reportService";
import Selectbox from "../components/joy-ui/Selectbox";
import { userService } from "./_repository/userService";
import Button from "@mui/joy/Button";
import ExportPdf060200 from "./_components/ExportPdf060200";
import ExportPdfAll060200 from "./_components/ExportPdfAll060200";
import RestartAltOutlinedIcon from "@mui/icons-material/RestartAltOutlined";
import {PrintOutlined} from "@mui/icons-material";
import CheckboxButton from "../components/joy-ui/CheckboxButton";
import RadioButtonGroup from "../components/joy-ui/RadioButtonGroup";

const Form060200 = () => {

    const dispatch = useDispatch();
    const [errorMessage, setErrorMessage] = useState(null);
    const [loadingState, setLoadingState] = useState(false);
    const [inputValue, setInputValue] = useState({});
    const [reportResponse, setReportResponse] = useState({});
    const [userTellerResponse, setUserTellerResponse] = useState({});
    const [checkedAll, setChecked] = useState(false);

    const resetFormData = useCallback(() => {
        setLoadingState(false);
        setErrorMessage(null);
        setReportResponse(null);
        setChecked(false);
    }, []);

    const fetchData = useCallback(async (service, stateSetter, ...params) => {
        try {
            setLoadingState(true);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchDataUserTeller = useCallback(() => {
        fetchData(userService, setUserTellerResponse, "GET_ALL");
    }, []);

    const fetchDataReport = useCallback((userId) => {
        const reportType = checkedAll ? "GET_RINCIAN_ALL_TELLER" : "GET_RINCIAN_PER_TELLER";
        fetchData(
            reportService,
            setReportResponse,
            reportType,
            userId
        );
    }, [checkedAll]);

    useEffect(() => {
        if (errorMessage) {
            dispatch(showNotification({ message: errorMessage, status: 0 }));
        }
    }, [errorMessage]);

    useEffect(() => {
        fetchDataUserTeller();
        dispatch(setPageTitle({ title: "Rincian Aktivitas per Teller" }));
    }, []);

    useEffect(() => {
        if (checkedAll) {
            setInputValue('ALL');
            setReportResponse(null);
            fetchDataReport(null);
        }
    }, [checkedAll]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const handleSelectbox = useCallback(async (userId) => {
        resetFormData();
        setChecked(false);
        setErrorMessage(null);
        fetchDataReport(userId);
    }, []);

    const handleCheckboxChange = () => {
        setChecked(!checkedAll);
    };

    const listUserTeller = {
        "Daftar User": checkedAll
            ? [
                { userId: "ALL", fullName: "SEMUA TELLER" },
                ...userTellerResponse?.data
                    ?.filter((user) => user.roleId === "06")
                    .map((user) => ({
                        userId: user.userId,
                        fullName: user.fullName,
                    })),

            ]
            : userTellerResponse?.data
                ?.filter((user) => user.roleId === "06")
                .map((user) => ({
                    userId: user.userId,
                    fullName: user.fullName,
                })),
    };

    const handleExportToPDF = () => {
        if(inputValue !== '' && reportResponse?.data) {
            if(checkedAll) {
                ExportPdfAll060200(reportResponse, checkedAll);
            } else {
                ExportPdf060200(reportResponse);
            }
        } else {
            let message = "Data tidak tersedia.";
            dispatch(showNotification({ message, status: 2 }));
        }
    };

    return (
        <>
            <div className="grid lg:grid-cols-12 lg:gap-x-6">
                <div className="lg:col-span-12">
                    <div className="grid lg:grid-cols-12 gap-x-2.5">
                        <Selectbox
                            disabled={checkedAll}
                            inputValue={inputValue}
                            setInputValue={setInputValue}
                            className="col-span-4 mt-[5px] mb-[-10px]"
                            idKey="userId"
                            nameKey="fullName"
                            label="User Teller"
                            datas={listUserTeller}
                            error={errorMessage?.userId}
                            onChange={handleSelectbox}
                        />

                        <RadioButtonGroup
                            height="36.4px"
                            className="mt-[5px] mb-[-8px]"
                            label="Semua Teller"
                        >
                            <CheckboxButton
                                padding="0 0 0 5px"
                                value="YA"
                                label="Ya"
                                checked={checkedAll}
                                onClick={() => {
                                    handleCheckboxChange();
                                }}
                            />
                        </RadioButtonGroup>

                        <Button
                            className="col-span-2 h-[36.4px]"
                            style={{ marginTop: '24.5px' }}
                            variant="outlined"
                            color="neutral"
                            startDecorator={<RestartAltOutlinedIcon className="w-4 h-4" />}
                            onClick={() => {
                                resetFormData();
                                setInputValue(null);
                            }}
                        >
                            Reset
                        </Button>
                        <Button
                            className="col-span-2 h-[36.4px]"
                            style={{ marginTop: '24.5px' }}
                            variant="outlined"
                            color="primary"
                            startDecorator={<PrintOutlined className="w-4 h-4"/>}
                            onClick={handleExportToPDF}
                        >
                            Export to PDF
                        </Button>
                    </div>
                    <TitleCard className="overflow-x-auto max-h-[75vh] mt-[15px]">
                        <TableForm060200
                            checkedAll={checkedAll}
                            isLoading={loadingState}
                            response={reportResponse}
                        />
                    </TitleCard>
                </div>
            </div>
        </>
    );
};

export default Form060200;
