import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm020100 from "./_components/TableForm020100";
import Textbox from "../components/joy-ui/Textbox";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import { roleService } from "./_repository/roleService";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import Grid from "../components/Grid";

const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    id: null,
    name: null,
};

export default function Form020100(){

    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    const fetchData = useCallback(async () => {
        try{
            setLoadingState(true);

            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await roleService("GET_ALL");

            if (status === 0)
                setErrorMessage(response);
            else
                setResponse(response);

        } catch (error){
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData, refreshTable]);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Pemeliharaan Wewenang"}))
    }, []);

    {/** START - EVENTS ON BUTTON*/}
    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.id);
        setFormMode(FM.VIEW);
    }, []);

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setSelectedDataState({...selectedDataState});
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.id !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.id !== null) {
            setLoadingState(true);
            const { response, status } = await roleService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        // Cek jika tidak ada data yang berubah
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const data = {
                id: selectedDataState.id,
                name: selectedDataState.name,
            };
            setLoadingState(true);
            const { response, status } = await roleService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }

            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);
    {/** END - EVENTS ON BUTTON*/}

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrorMessage( null);
    }, []);

    return(
        <>
            <Grid>
                <TitleCard className="max-h-[85vh] overflow-x-auto col-span-5">
                    <TableForm020100
                        handleRowClick={handleRowClick}
                        selectedRow={selectedRow}
                        refreshTable={refreshTable}
                        isLoading={loadingState}
                        response={response}
                    />
                </TitleCard>

                <div className="col-span-4 mt-6">
                    <ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick} loadingState={loadingState} />

                    <TitleCard>
                        <Textbox
                            maxLength={2}
                            mandatory
                            key={renderKey}
                            autoFocus={formMode !== FM.VIEW}
                            disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                            id="txtId"
                            label="Role Id"
                            value={selectedDataState?.id}
                            error={errorMessage?.id}
                            onChange={(e) => setSelectedDataState({...selectedDataState, id:e.target.value})}/>

                        <Textbox
                            upperCase
                            mandatory
                            disabled={formMode === FM.VIEW}
                            id="txtNama"
                            label="Nama Role"
                            name="name"
                            value={selectedDataState?.name}
                            error={errorMessage?.name}
                            onChange={(e) => setSelectedDataState({...selectedDataState, name:e.target.value})}/>

                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode} loadingState={loadingState}/>
                    </TitleCard>
                </div>
            </Grid>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}