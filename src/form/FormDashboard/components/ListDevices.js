import TitleCard from "../../../components/Cards/TitleCard"
import {useState} from "react";

let listData = [
    {deviceName : "-", countTx : "-", totalTx : "-"},
]

const formatToRupiah = (number) => {
    return new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR'
    }).format(number);
};

export default function ListDevices({data}){

    let totalData = 0;
    let totalCountTx = 0;
    let totalTotalTx = 0;

    if(data) {
        const userSourceData = {};

        data.forEach(entry => {
            const deviceNames = Object.keys(entry).filter(key => key !== 'name');
            deviceNames.forEach(deviceName => {
                const countTx = entry[deviceName] !== 0 ? 1 : 0;
                if (!userSourceData[deviceName]) {
                    userSourceData[deviceName] = { deviceName, countTx, totalTx: entry[deviceName] };
                } else {
                    userSourceData[deviceName].countTx += countTx;
                    userSourceData[deviceName].totalTx = (parseInt(userSourceData[deviceName].totalTx) + entry[deviceName]).toString();
                }
            });
        });

        const finalData = Object.values(userSourceData);
        listData = Object.values(finalData)

        // SORT
        listData.sort(function(a, b) {
            return b.countTx - a.countTx; // Urutkan secara descending
        });
    }

    return(
        <TitleCard title={"Daftar Perangkat Aktif"} subtitle={"(harian)"}>
            {/** Table Data */}
            {
                listData.map((u, k) => {
                    totalData = totalData + 1;
                })
            }
            <div className={"overflow-x-auto " + ( totalData > 5 ? "lg:h-[275px] lg:-mb-96" : "")}>
                {/*<div className={"overflow-x-auto bg-red-900"}>*/}
                <table className="table-fixed w-full">
                    <thead className="text-xs">
                    <tr>
                        <th className="w-[45px]"></th>
                        <th className="text-gray-400 normal-case text-left">Nama Perangkat</th>
                        <th className="text-gray-400 w-[75px] normal-case text-center">Transaksi Dilakukan</th>
                        <th className="text-gray-400 normal-case text-right pr-2">Akumulasi Transaksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        listData.map((u, k) => {
                            totalCountTx += u.countTx;
                            totalTotalTx += parseInt(u.totalTx);

                            return(
                                <tr key={k} className="odd:bg-accent/20">
                                    <th className="py-2">{k+1}</th>
                                    <td>{u.deviceName}</td>
                                    <td className="text-center">
                                        {
                                            u.countTx
                                        }
                                        x
                                    </td>
                                    <td className="pr-2 text-right">{parseInt(u.totalTx).toLocaleString()}</td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                    <tfoot className="sticky bottom-0">
                    <tr className="bg-gray-300 font-semibold">
                        <th className=""></th>
                        <td className="py-2 text-left">Total</td>
                        <td className="text-center">{totalCountTx}x</td>
                        <td className="pr-2 text-right">{totalTotalTx.toLocaleString()}</td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </TitleCard>
    )
}