import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import TitleCard from '../../../components/Cards/TitleCard';

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

function BarChart({title, data, barFill, borderColor, properties}) {
    const labels = [];
    const values = [];

    let dataLen = 0;
    dataLen = data?.component?.length > 0 ? data.component.length : dataLen;
    for(let i = 0; i < dataLen; i++) {
        labels.push(data?.component[i]?.timeRange);
        values.push(data?.component[i]?.dataTransaction);
    }

    const barOptions = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            }
        },
        scales: {
            y: {
                beginAtZero: true,
            },
        },
    };

    const barDatas = {
        labels,
        datasets: [
            {
                label: 'Transaksi',
                data: values,
                backgroundColor: barFill,
                borderColor: borderColor,
                borderWidth: 1,
                barPercentage: 0.5,
            },
        ],
    };

    return(
        <>
            <TitleCard title={title} subtitle={data?.totalTransaction + ' jumlah transaksi '} properties={properties}>
                <Bar options={barOptions} data={barDatas} />
            </TitleCard>
        </>
    )
}

export default BarChart