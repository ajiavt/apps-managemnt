import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import React, { useState } from 'react';
import TitleCard from '../../../components/Cards/TitleCard';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend
);

function getNextContrastingColor() {
  const red = Math.floor(Math.random() * 256);
  const green = Math.floor(Math.random() * 256);
  const blue = Math.floor(Math.random() * 256);

  return `rgba(${red}, ${green}, ${blue}, 0.8)`;
}

function LineChart({title, data}){
  // HIGHEST AMOUNT
  let highestDeviceAmount = null;
  let highestValue = -Infinity;

  data?.forEach(item => {
    for (const device in item) {
      if (device !== 'name') {
        if (item[device] > highestValue) {
          highestValue = item[device];
          highestDeviceAmount = device;
        }
      }
    }
  });

  // DATASETS
  const dataSets = [];
  if(data && data.length > 0) { // Pengecekan data tidak kosong
    for (const propertyName in data[0]) {
      if (propertyName !== 'name') {
        const label = propertyName;
        const dataPoints = data?.map(item => item[propertyName]);

        const backgroundColor = getNextContrastingColor();

        const dataSet = {
          label: label,
          data: dataPoints,
          backgroundColor: backgroundColor,
          borderColor: backgroundColor,
          yAxisID: 'y',
        };

        dataSets.push(dataSet);
      }
    }
  }

  // LIST NAMA PC
  const listDevice = dataSets?.map(item => item.label);

  const options = {
    responsive: true,
    scales: {
      y: {
        stacked: false,
        display: true,
        beginAtZero: true,
        type: 'linear',
        position: 'left',
        grid: {
          drawOnChartArea: true,
        },
      }
    },
    plugins: {
      filler: {
        propagate: false
      },
      'samples-filler-analyser': {
        target: 'chart-analyser'
      }
    },
    interaction: {
      intersect: false,
    },
    elements: {
      line: {
        tension: '0.2',
      },
    }
  };

  const labels = data?.map(item => item.name);
  const chartDatas = {
    labels: labels,
    datasets: dataSets
  };

  const [chartDataSets, setChartDataSets] = useState([]);
  const [backgroundColor, setBackgroundColor] = useState([]);

  const handleRecolor = () => {
    const updatedDataSets = [];
    const updatedBackgroundColors = [];

    // Perbarui dataSets dan warna latar belakang sesuai kebutuhan (misalnya, dengan memanggil getNextContrastingColor() untuk setiap dataset baru)

    setChartDataSets(updatedDataSets);
    setBackgroundColor(updatedBackgroundColors);
  };

  return(
      <>
        <TitleCard title={title}>
          <button className="btn btn-outline btn-xs md:btn-md right-6 absolute" onClick={handleRecolor}>Recolor</button>
          <div className="pt-6">
            <Line id={"lineChart"} data={chartDatas} options={options} type='line'/>
          </div>
        </TitleCard>
      </>
  )
}

export default LineChart