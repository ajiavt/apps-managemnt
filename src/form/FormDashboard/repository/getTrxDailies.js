const getTrxDailies = (trxData) => {
    let txJamPertama, txJamTerakhir;
    if(trxData?.length === 1) {
        txJamPertama = Math.min(parseInt(trxData[0]["txTime"].slice(0, 2)),8);
        txJamTerakhir = parseInt(trxData[0]["txTime"].slice(0, 2));
    } else if(trxData?.length > 1) {
        txJamPertama = Math.min(parseInt(trxData[0]["txTime"].slice(0, 2)), 8);
        txJamTerakhir = parseInt(trxData[trxData?.length - 1]["txTime"].slice(0, 2));
    } else {
        txJamPertama = 0;
        txJamTerakhir = 0;
    }

    let newTrxData = {};
    for(let i=txJamPertama; i<=txJamTerakhir; i++){
        newTrxData[i] = {};
    }

    for(let element of trxData) {
        let deviceName = element["deviceName"];
        let txAmount = element["txAmount"];
        let txTime = element["txTime"];
        let txTimeNum = parseInt(txTime.slice(0, 2));
        if(txAmount === null || txAmount ==="" || !txAmount) txAmount = 0;
        newTrxData[txTimeNum][deviceName] = parseFloat(txAmount);
    }

    const accumulation = true;
    if(accumulation) {
        for(let element of trxData) {
            let deviceName = element["deviceName"];
            for(let i=txJamPertama; i<=txJamTerakhir; i++) {
                if(!(deviceName in newTrxData[i])){
                    newTrxData[i][deviceName] = 0;
                } else {
                    newTrxData[i][deviceName] = parseFloat(newTrxData[i][deviceName]);
                }
            }
        }
    }

    const resultTrxData = changeObjectToArray(newTrxData, txJamPertama, txJamTerakhir);
    return resultTrxData;
}

const changeObjectToArray = (trxObj, startHour, endHour) => {
    let trxDataArray = [];
    for(let i=startHour; i<= endHour; i++){
        let name = i.toLocaleString(undefined, {minimumIntegerDigits: 2}) + ":00-" + (i+1).toLocaleString(undefined, {minimumIntegerDigits: 2}) +":00";
        let newElementObject = {'name': name};
        Object.entries(trxObj[i]).forEach(([deviceName,value]) => {
            newElementObject[deviceName]= trxObj[i][deviceName];
        });
        trxDataArray.push(newElementObject);
    }
    return trxDataArray;
}

export default getTrxDailies;