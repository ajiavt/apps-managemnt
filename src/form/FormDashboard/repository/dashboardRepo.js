import { apiClient } from "../../../app/axios"
import { apiURL } from "../../../app/url"

const dashboardRepo = async () => {

    let ACCESS_TOKEN = localStorage.getItem("access_token")
    let TOKEN_TYPE = localStorage.getItem("token_type")

    try {
        const success = await apiClient.get(
            apiURL.GET_DASHBOARD_DATA, {
                headers: {
                    'Authorization': `${TOKEN_TYPE} ${ACCESS_TOKEN}`,
                },
            }
        )

        return {response : success};
    }

    catch (error) {
        return {response: error};
    }
}

export default dashboardRepo;