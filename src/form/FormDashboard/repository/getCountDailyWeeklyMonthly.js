const getCountDailyWeeklyMonthly = (dashboardData) => {
    const dataKeys = Object.keys(dashboardData);
    let intTotTx = 0;

    let tempDataObject = {};
    let newDataObject = {};
    let finalDataObject = {};
    var arrComponent = [];

    for(let parentKey of dataKeys) {
        intTotTx = 0;
        arrComponent = [];

        if(dashboardData[parentKey]){
            Object.entries(dashboardData[parentKey]).forEach(([key,value]) => {
                if(key === 'countDaily') {
                    key = hourFormatter(key);
                }

                if(key === 'countWeekly') {
                    key = weeklyFormatter(key);
                }

                if(key === 'countMonthly') {
                    key = monthlyFormatter(key);
                }

                tempDataObject = {
                    "timeRange": key, "dataTransaction": value
                };

                intTotTx += value;
                arrComponent.push(tempDataObject);
            });
        }

        finalDataObject = {'component': arrComponent, 'totalTransaction': intTotTx}
        newDataObject[parentKey] = finalDataObject;
    }

    return newDataObject;
}

const hourFormatter = (key) => {
    return key.slice(0,2) + "-" + key.slice(3,5);
}

const weeklyFormatter = (key) => {
    return key.slice(0,3);
}

const monthlyFormatter = (key) => {
    return "w" + key.slice(-1);
}

export default getCountDailyWeeklyMonthly;