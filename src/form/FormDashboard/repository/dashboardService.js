import getCountDailyWeeklyMonthly from "./getCountDailyWeeklyMonthly";
import dashboardRepo from "./dashboardRepo";
import getTrxDailies from "./getTrxDailies";

const dashboardService = async () => {
    const {response}  = await dashboardRepo();

    if(response.status === 200) {
        const tempData = response.data.data;
        let tempTrxDailies = tempData.trxDailies;
        delete tempData.trxDailies;

        let newResponse = getCountDailyWeeklyMonthly(tempData);
        let trxDailies = getTrxDailies(tempTrxDailies);
        newResponse['trxDailies'] = trxDailies;

        return {response: newResponse, status: 1};
    } else {
        if(!(response && response.response && response.response.data)) {
            return {response: "Gagal terhubung ke API Dashboard", status: 0};
        }

        const str = response.response.data['error_description'];
        let str2 = "";
        if(str) {
            str2 = str[0].toUpperCase() + str.slice(1) + "!";
        } else {
            str2 = "Koneksi API tidak tersambung!"
        }

        return {response: str2, status: 0};
    }
}

export default dashboardService;