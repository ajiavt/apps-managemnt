import React, {useEffect, useState} from 'react'
import { useDispatch } from 'react-redux'
import {setPageTitle, showNotification} from '../../features/common/headerSlice'
import BarChart from "./components/BarChart";
import LineChart from "./components/LineChart";
import dashboardService from "./repository/dashboardService";
import ListDevices from "./components/ListDevices";

function FormDashboard(){

    const dispatch = useDispatch()
    const [response, setResponse] = useState({})
    const [errorMessage, setErrorMessage] = useState()

    const fetchData = async () => {
        const {response, status} = await dashboardService()
        if(status === 0) {
            setErrorMessage(response)
        } else {
            setResponse(response)
        }
    }

    useEffect(() => {
        dispatch(setPageTitle({ title : "Dashboard"}))
        fetchData()
    }, [])

    useEffect(() => {
        if(errorMessage) {
            dispatch(showNotification({message : errorMessage, status : 0}))
        }
    }, [errorMessage])

    return(
        <>
            <div className="grid md:grid-cols-2 grid-cols-1 gap-x-5">
                <BarChart title='Transaksi Harian' data={response['countDaily']} barFill='rgba(255, 99, 132, 0.2)' borderColor='rgb(255, 99, 132)'/>
                <BarChart title='Transaksi Mingguan' data={response['countWeekly']} barFill='rgba(54, 162, 235, 0.2)' borderColor='rgb(54, 162, 235)'/>
                <BarChart title='Transaksi Bulanan' data={response['countMonthly']} barFill='rgba(153, 102, 255, 0.2)' borderColor='rgb(153, 102, 255)'/>
                <ListDevices data={response['trxDailies']}/>
            </div>

            <div className="grid grid-cols-1 gap-x-5">
                <LineChart title='Jumlah Transaksi Per Perangkat (harian)' data={response['trxDailies']}/>
            </div>
        </>
    )
}

export default FormDashboard