import TitleCard from "../components/Cards/TitleCard";
import {useSelector, useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableFormAddress from "./_components/TableFormAddress";
import { addrService } from "./_repository/addrService";

export default function FormAddress(){

    const {activePath} = useSelector(state => state.pathSet);
    const [errMsg, setErrMsg] = useState({});
    const [loadingState, setLoadingState] = useState(false)
    const [menuId, setMenuId] = useState(null); // deklarasi di awal komponen

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    const fetchData = useCallback(async (menuId) => {
        try{
            setLoadingState(true);

            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await addrService(menuId);

            if (status === 0)
                setErrorMessage(response);
            else
                setResponse(response);

        } catch (error){
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    useEffect(() => {
        if (errorMessage) {
            dispatch(showNotification({ message: errorMessage, status: 0 }));
        }
    }, [errorMessage]);

    useEffect(() => {
        const currentRoute = listPathAddr.find(item => item.path === activePath);
        const currentTitle = currentRoute ? currentRoute.title : activePath; // jika tidak ada judul yang cocok, tampilkan activePath sebagai judul
        const menuId = currentRoute ? currentRoute.menuId : null; // mengatur menuId
        setMenuId(menuId);
        dispatch(setPageTitle({ title : ('Pemeliharaan ' + currentTitle) }));
        if (menuId) {
            fetchData(menuId);
        }
    }, [activePath]);

    const listPathAddr = [
        {menuId: "050300", path: "/app/menu-id=050300", title: "Negara"},
        {menuId: "050400", path: "/app/menu-id=050400", title: "Provinsi"},
        {menuId: "050500", path: "/app/menu-id=050500", title: "Kota/Kabupaten"},
        {menuId: "050600", path: "/app/menu-id=050600", title: "Kecamatan"},
        {menuId: "050700", path: "/app/menu-id=050700", title: "Kelurahan/Desa"},
    ];

    const handleError = (response) => {
        // 98 = Error pada inputan
        if(response?.data?.code === "98") {
            return response?.data?.error;
        }

        else if (response?.data?.code === "99" || response?.data?.code === "93" || response?.data?.code === "94") {
            dispatch(showNotification({message : response?.data?.message, status : 0}))
        }

        else {
            return response;
        }
    }

    return(
        <>
            <div className="grid md:grid-cols-7 md:gap-x-6 grid-rows-2">
                <TitleCard className="lg:h-[85vh] overflow-x-auto md:col-span-3 row-span-2">
                    <TableFormAddress
                        isLoading={loadingState}
                        response={response}
                    />
                </TitleCard>
            </div>
        </>
    )
}