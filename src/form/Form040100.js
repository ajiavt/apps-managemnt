import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm040100 from "./_components/TableForm040100";
import Textbox from "../components/joy-ui/Textbox";
import { pathApiService } from "./_repository/pathApiService";
import ResponsiveModal from "../components/joy-ui/Modal";
import Textarea from "../components/joy-ui/Textarea";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";

// FM = Form Modes
const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    code: null,
    path: null,
    desc: null,
};

export default function Form040100(){

    const [errMsg, setErrMsg] = useState({});
    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    const fetchData = useCallback(async () => {
        try{
            setLoadingState(true);

            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await pathApiService("GET_ALL");

            if (status === 0)
                setErrorMessage(response);
            else
                setResponse(response);

        } catch (error){
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData, refreshTable]);

    useEffect(() => {
        if (errorMessage) {
            dispatch(showNotification({ message: errorMessage, status: 0 }));
        }
    }, [errorMessage]);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Pemeliharaan Path API"}))
    }, []);

    useEffect(() => {
    }, [selectedDataState]);

    {/** START - EVENTS ON BUTTON*/}
    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.code);
        setFormMode(FM.VIEW);
    }, []);

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setSelectedDataState({...selectedDataState});
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.code !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.code !== null) {
            setLoadingState(true);
            const { response, status } = await pathApiService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrMsg(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        // Cek jika tidak ada data yang berubah
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const data = {
                code: selectedDataState.code,
                path: selectedDataState.path,
                desc: selectedDataState.desc,
            };
            setLoadingState(true);
            const { response, status } = await pathApiService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrMsg(error);
            }

            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);
    {/** END - EVENTS ON BUTTON*/}

    const handleError = (response) => {
        // 98 = Error pada inputan
        if(response?.data?.code === "98") {
            return response?.data?.error;
        }

        else if (response?.data?.code === "99" || response?.data?.code === "93" || response?.data?.code === "94") {
            dispatch(showNotification({message : response?.data?.message, status : 0}))
        }

        else {
            return response;
        }
    }

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrMsg({});
    }, []);

    return(
        <>
            <div className="grid lg:grid-cols-12 lg:gap-x-6">
                <TitleCard className="max-h-[85vh] overflow-x-auto lg:col-span-8">
                    <TableForm040100
                        handleRowClick={handleRowClick}
                        selectedRow={selectedRow}
                        refreshTable={refreshTable}
                        isLoading={loadingState}
                        response={response}
                    />
                </TitleCard>

                <div className="lg:col-span-4 mt-6">
                    <ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick} loadingState={loadingState}/>
                    <TitleCard>
                        <Textbox
                            upperCase
                            maxLength={6}
                            mandatory
                            key={renderKey}
                            autoFocus={formMode !== FM.VIEW}
                            disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                            id="txtCode"
                            label="Kode API"
                            name="code"
                            value={selectedDataState?.code}
                            error={errMsg?.code}
                            onChange={(e) => setSelectedDataState({...selectedDataState, code:e.target.value})}/>

                        <Textarea
                            minRows={2}
                            maxRows={13}
                            mandatory
                            disabled={formMode === FM.VIEW}
                            id="txtPath"
                            label="Path API"
                            name="path"
                            value={selectedDataState?.path}
                            error={errMsg?.path}
                            onChange={(e) => setSelectedDataState({...selectedDataState, path:e.target.value})}/>

                        <Textarea
                            minRows={2}
                            maxRows={5}
                            mandatory
                            disabled={formMode === FM.VIEW}
                            id="txtDesc"
                            label="Deskripsi"
                            name="desc"
                            value={selectedDataState?.desc}
                            error={errMsg?.desc}
                            onChange={(e) => setSelectedDataState({...selectedDataState, desc:e.target.value})}/>

                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode} loadingState={loadingState}/>
                    </TitleCard>
                </div>
            </div>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}