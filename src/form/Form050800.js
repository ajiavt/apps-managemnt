import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch } from "react-redux";
import { setPageTitle, showNotification } from "../features/common/headerSlice";
import { infoBankService } from "./_repository/infoBankService";
import {AddOutlined, InfoOutlined} from "@mui/icons-material";
import { TrashIcon } from "@heroicons/react/24/outline";
import TitleCard from "../components/Cards/TitleCard";
import TableForm050800 from "./_components/TableForm050800";
import Textbox from "../components/joy-ui/Textbox";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import UploadButton from "../components/joy-ui/UploadButton";
import Card from '@mui/joy/Card';
import Box from "@mui/joy/Box";
import CheckboxButton from "../components/joy-ui/CheckboxButton";
import RadioButtonGroup from "../components/joy-ui/RadioButtonGroup";
import SimpleSlider from "../components/joy-ui/SimpleSlider";
import Typography from "@mui/joy/Typography";
import Button from "@mui/joy/Button";
import Textarea from "../components/joy-ui/Textarea";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import FormLabel from "@mui/joy/FormLabel";
import FormControl from "@mui/joy/FormControl";
import adsLayout from "../assets/images/ads-layout.png";
import Selectbox from "../components/joy-ui/Selectbox";
import ColorPicker from "../components/ColorPicker";
import AccordionUploadGambar from "./_components/AccordionUploadGambar";

// FM = Form Modes
const FM = { VIEW: 'view', EDIT: 'edit', ADD: 'add' };
const emptyData = { id: null, title: null, descr: null, details: null, jenisBG: null, status: null, };
const listJenisBG = {
    "Jenis Background": [
        {"id": "1", "name": "Gambar"},
        // {"id": "2", "name": "Color Picker"},
    ]
};

export default function Form050800() {
    // State variables
    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [progress, setProgress] = useState(0);
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);
    const [uploadButtons, setUploadButtons] = useState([0]);
    const [uploadBackground, setUploadBackground] = useState(null);
    const [uploadGambarUtama, setUploadGambarUtama] = useState([]);
    const [isHovered, setIsHovered] = useState(false);
    const [fileNames, setFileNames] = useState([]);
    const [backgroundFileName, setBackgroundFileName] = useState("");
    const [backgroundColor, setBackgroundColor] = useState("#FFFFFF");
    const [inputValue, setInputValue] = useState({});

    const dispatch = useDispatch();

    // Fetch data
    const fetchData = useCallback(async () => {
        setLoadingState(true);
        try {
            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await infoBankService("GET_ALL");
            status === 0 ? setErrorMessage(response) : setResponse(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    useEffect(() => { fetchData(); }, [fetchData, refreshTable]);

    // Set page title
    useEffect(() => {
        dispatch(setPageTitle({ title: "Pemeliharaan Informasi Bank" }))
    }, []);

    // Handle row click
    const handleRowClick = useCallback(async (data) => {
        setProgress(0);
        setLoadingState(true);

        // Check if data has already been loaded
        if (data.details) {
            setLoadingState(false);
            setFormMode(FM.VIEW);
            setSelectedDataState(data);
            setSelectedRow(data.id);

            setUploadBackground("data:image/jpeg;base64," + data.background);

            const formattedImages = data.mainImage?.map(image => "data:image/jpeg;base64," + image);
            setUploadGambarUtama(formattedImages);

            const newUploadButtons = Array.from({length: data.mainImage?.length}, (_, index) => index);
            setUploadButtons(newUploadButtons);
            return;
        }

        try {
            const { get1, status } = await infoBankService("GET_1", data);

            if (status === 0) {
                let error = handleError(get1);
                setErrorMessage(error);
                setLoadingState(false);
                return;
            }

            data.details = get1?.data?.details;

            const descObj = get1?.data?.details.find(detail => detail.typeContent === "DESC");
            if (descObj) {
                const formattedDescr = descObj?.content?.join('\n\n');
                data.descr = formattedDescr;
            }

            const backgroundObj = get1?.data?.details.find(detail => detail.typeContent === "BG");
            if (backgroundObj) {
                data.background = backgroundObj.content;
                setUploadBackground("data:image/jpeg;base64," + data.background);
                data.jenisBG = '1';
                data.blurBg = backgroundObj.isBlur;
            }

            const mainImageObj = get1?.data?.details.find(detail => detail.typeContent === "GU");
            if (mainImageObj) {
                data.mainImage = mainImageObj?.content;
                const formattedImages = data?.mainImage?.map(image => "data:image/jpeg;base64," + image);
                setUploadGambarUtama(formattedImages);

                const newUploadButtons = Array.from({length: formattedImages.length}, (_, index) => index);
                setUploadButtons(newUploadButtons);
            }

            setSelectedDataState(data);
            setSelectedRow(data.id);
            setFormMode(FM.VIEW);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setProgress(100);
            setLoadingState(false);
        }
    }, [setUploadButtons, setUploadGambarUtama]);

    // Handle add click
    const handleAddClick = useCallback(() => {
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setErrorMessage({});
        setFileNames([]);
        setBackgroundFileName("");
        setUploadBackground(null);
        setBackgroundColor("#FFFFFF");
        setUploadButtons([0]);
        setUploadGambarUtama([]);
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
    }, []);

    // Handle edit click
    const handleEditClick = useCallback(() => {
        if (selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({ ...selectedDataState });
        } else {
            dispatch(showNotification({ message: "Tidak ada data yang dipilih!", status: 2 }));
        }
    }, [selectedDataState, formMode]);

    // Handle delete click
    const handleDeleteClick = useCallback(() => {
        selectedDataState?.id !== null ? setShowModalDelete(true) : dispatch(showNotification({ message: "Tidak ada data yang dipilih!", status: 2 }));
    }, [selectedDataState]);

    // Handle confirm delete
    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState?.id !== null) {
            setLoadingState(true);
            const { response, status } = await infoBankService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }
            else {
                if (response.code === '00') {
                    dispatch(showNotification({ message: "Data berhasil dihapus!", status: 1 }));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    // Handle save click
    const handleSaveClick = useCallback(() => {
        // Cek jika tidak ada data yang berubah
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({ message: "Tidak ada data yang berubah!", status: 2 }));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    // Handle confirm save
    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const backgroundWithoutPrefix = selectedDataState?.background?.replace(/^data:image\/[a-zA-Z]+;base64,/, '');
            const mainImagesWithoutPrefix = selectedDataState?.mainImage?.map(image => (image ? image?.replace(/^data:image\/[a-zA-Z]+;base64,/, '') : null));
            const regex = /(?:\r\n|\r|\n){2,}/;
            const arrDescr = selectedDataState?.descr?.split(regex);

            const data = {
                id: selectedDataState?.id,
                title: selectedDataState?.title,
                status: selectedDataState?.status,
                details: [
                    {
                        idParent: selectedDataState?.id,
                        id: 1,
                        typeContent: "BG",
                        isBlur: selectedDataState?.blurBg,
                        content: backgroundWithoutPrefix,
                    },
                    {
                        idParent: selectedDataState?.id,
                        id: 2,
                        typeContent: "GU",
                        isBlur: false,
                        content: mainImagesWithoutPrefix,
                    },
                    {
                        idParent: selectedDataState?.id,
                        id: 3,
                        typeContent: "JK",
                        isBlur: false,
                        content: selectedDataState?.title
                    },
                    {
                        idParent: selectedDataState?.id,
                        id: 4,
                        typeContent: "DESC",
                        isBlur: false,
                        content: arrDescr,
                    }
                ]
            };

            setLoadingState(true);
            const { response, status } = await infoBankService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }
            else {
                if (response.code === '00') {
                    dispatch(showNotification({ message: `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status: 1 }));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    // Reset form data
    const resetFormData = useCallback(() => {
        setUploadButtons([0]);
        setUploadGambarUtama([]);
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrorMessage(null);
        setFileNames([]);
        setBackgroundFileName("");
        setUploadBackground(null);
        setBackgroundColor("#FFFFFF");
        setInputValue('');
    }, []);

    // Handle remove
    const handleRemove = (index) => {
        const newUploadButtons = [...uploadButtons];
        newUploadButtons.splice(index, 1);
        setUploadButtons(newUploadButtons);

        const newUploadGambarUtama = [...uploadGambarUtama];
        newUploadGambarUtama.splice(index, 1);
        setUploadGambarUtama(newUploadGambarUtama);

        const newFileNames = [...fileNames];
        newFileNames.splice(index, 1);
        setFileNames(newFileNames);
    };

    return (
        <>
            <div className="grid grid-cols-12 gap-x-6 overflow-y-hidden">
                <div className="grid grid-cols-12 col-span-12 gap-x-6">
                    <div className="grid grid-cols-12 col-span-2">
                        <TitleCard className="overflow-x-auto col-span-12 h-[493px]">
                            <TableForm050800
                                handleRowClick={handleRowClick}
                                selectedRow={selectedRow}
                                refreshTable={refreshTable}
                                isLoading={loadingState}
                                response={response}
                            />
                        </TitleCard>

                        <div className="col-span-12 mt-[10px]">
                            <ButtonResetSave
                                reset={resetFormData}
                                save={handleSaveClick}
                                formMode={formMode}
                                vertical={true}
                                loadingState={loadingState}
                            />
                        </div>
                    </div>

                    <div className="grid grid-cols-12 col-span-8 mt-[25px]">
                        <div className="col-span-12">
                            <ButtonCRUD
                                add={handleAddClick}
                                edit={handleEditClick}
                                del={handleDeleteClick}
                                loadingState={loadingState}
                            />
                        </div>

                        <TitleCard className="col-span-12">
                            <div className="grid grid-cols-12 gap-x-2.5">
                                <RadioButtonGroup
                                    height="32px"
                                    className="col-span-2"
                                    disabled={formMode === FM.VIEW}
                                    error={errorMessage?.status}
                                    label="Status Iklan"
                                >
                                    <CheckboxButton
                                        padding="0 0 0 5px"
                                        disabled={formMode === FM.VIEW}
                                        value="FE_TELLER"
                                        label="Aktif"
                                        checked={selectedDataState?.status || false} // jika status adalah null, maka akan bernilai false
                                        onClick={() => {
                                            setSelectedDataState({...selectedDataState, status: !selectedDataState?.status});
                                        }}
                                    />
                                </RadioButtonGroup>

                                <Textbox
                                    key={renderKey}
                                    maxLength={100}
                                    mandatory
                                    autoFocus={formMode !== FM.VIEW}
                                    className="col-span-10"
                                    disabled={formMode === FM.VIEW}
                                    label="Judul Iklan"
                                    name="title"
                                    value={selectedDataState?.title}
                                    error={errorMessage?.title}
                                    onChange={(e) => setSelectedDataState({ ...selectedDataState, title: e.target.value })} />
                            </div>

                            <Textarea
                                maxRows={10}
                                tooltip="Maksimal 1000 karakter!"
                                maxLength={1000}
                                readOnly={formMode === FM.VIEW}
                                label="Deskripsi"
                                value={selectedDataState?.descr}
                                error={errorMessage?.descr}
                                onChange={(e) => setSelectedDataState({ ...selectedDataState, descr: e.target.value })} />

                            <div className="grid grid-cols-12 gap-x-2.5">
                                <div className="col-span-3">
                                    <Selectbox
                                        inputValue={inputValue}
                                        setInputValue={setInputValue}
                                        mandatory
                                        key={renderKey}
                                        disabled={formMode === FM.VIEW}
                                        label="Jenis Background"
                                        value={selectedDataState?.jenisBG}
                                        datas={listJenisBG}
                                        idKey="id"
                                        nameKey="name"
                                        error={errorMessage?.jenisBG}
                                        onChange={(child) => {
                                            setSelectedDataState({ ...selectedDataState, jenisBG: child });
                                            setUploadBackground(null);
                                            setBackgroundColor("#FFFFFF");
                                        }}
                                    />
                                </div>

                                <UploadButton
                                    mandatory
                                    labelMargin={"mt-[-2px] mb-[-11px]"}
                                    className="col-span-3 mt-[-4px]"
                                    disabled={formMode === FM.VIEW}
                                    hidden={selectedDataState?.jenisBG !== '1'}
                                    label="Gambar Background"
                                    fileName={backgroundFileName}
                                    setFileName={setBackgroundFileName}
                                    onFileChange={(objectUrl, file) => {
                                        const reader = new FileReader();
                                        reader.readAsDataURL(file);
                                        reader.onloadend = () => {
                                            const base64data = reader.result;
                                            setUploadBackground(base64data);
                                            setBackgroundFileName(file.name);

                                            setSelectedDataState(prevState => ({
                                                ...prevState,
                                                background: base64data,
                                            }));
                                        };
                                    }}
                                />

                                <RadioButtonGroup
                                    height="36px"
                                    className="col-span-3"
                                    disabled={formMode === FM.VIEW}
                                    hidden={selectedDataState?.jenisBG !== '1'}
                                    error={errorMessage?.status}
                                    label="Blur Background"
                                >
                                    <CheckboxButton
                                        padding="0 0 0 5px"
                                        disabled={formMode === FM.VIEW}
                                        value="ya"
                                        label="Ya"
                                        checked={selectedDataState?.blurBg || false}
                                        onClick={() => {
                                            setSelectedDataState({...selectedDataState, blurBg: !selectedDataState?.blurBg});
                                        }}
                                    />
                                </RadioButtonGroup>

                                <ColorPicker
                                    className="col-span-3"
                                    hidden={selectedDataState?.jenisBG !== '2'}
                                    disabled={selectedDataState?.jenisBG !== '2'}
                                    label="Warna Background"
                                    onChange={(color) => setBackgroundColor(color)}
                                />
                            </div>

                            <div className="grid grid-cols-12 gap-x-2.5">
                                {uploadButtons.map((_, index) => (
                                    <div
                                        onMouseEnter={() => setIsHovered(index)}
                                        onMouseLeave={() => setIsHovered(null)}
                                        className="col-span-3"
                                        key={index}
                                    >
                                        {isHovered === index && index !== 0 && uploadGambarUtama[index] ? (
                                            <FormControl
                                                disabled={formMode === FM.VIEW}
                                                size="sm"
                                                color="neutral"
                                                className="mb-[8px]"
                                            >
                                                <FormLabel>
                                                    <span className="mb-[-5px] text-[14px] font-Poppins">
                                                        Gambar Utama {index + 1}
                                                    </span>
                                                </FormLabel>

                                                <Button
                                                    disabled={formMode === FM.VIEW}
                                                    startDecorator={<TrashIcon className='h-4 w-4' />}
                                                    className="w-full"
                                                    variant="outlined"
                                                    color="danger"
                                                    sx={{ height: "0", }}
                                                    onClick={() => handleRemove(index)}
                                                >
                                                    Remove
                                                </Button>
                                            </FormControl>
                                        ) : (
                                            <UploadButton
                                                labelMargin={index === 0 ? "" : "mt-[-2px] mb-[-5px]"}
                                                className={index === 0 ? "mt-[-2px]" : "mt-[2px]"}
                                                mandatory={index === 0 ? true : false}
                                                disabled={formMode === FM.VIEW}
                                                label={index === 0 ? "Gambar Utama" : `Gambar Utama ${index + 1}`}
                                                onFileChange={(objectUrl, file) => {
                                                    const reader = new FileReader();
                                                    reader.readAsDataURL(file);
                                                    reader.onloadend = () => {
                                                        const base64data = reader.result;
                                                        let newUploadGambarUtama = [...uploadGambarUtama];
                                                        newUploadGambarUtama[index] = base64data;
                                                        setUploadGambarUtama(newUploadGambarUtama);

                                                        setSelectedDataState(prevState => ({
                                                            ...prevState,
                                                            mainImage: newUploadGambarUtama,
                                                        }));
                                                    };
                                                }}

                                                fileName={fileNames[index]}
                                                setFileName={(name) => {
                                                    let newFileNames = [...fileNames];
                                                    newFileNames[index] = name;
                                                    setFileNames(newFileNames);
                                                }}
                                            />
                                        )}
                                    </div>
                                ))}

                                {uploadButtons.length === uploadGambarUtama.length && uploadButtons.length < 3 && (
                                    <Button
                                        disabled={formMode === FM.VIEW}
                                        className="col-span-3 mb-[-5px]"
                                        variant="soft"
                                        color="success"
                                        sx={{ height: "0", marginTop: "22px" }}
                                        onClick={() => setUploadButtons(prevState => [...prevState, prevState.length])}
                                    >
                                        <AddOutlined className='h-4 w-4' />
                                    </Button>
                                )}

                                <AccordionUploadGambar/>
                            </div>
                        </TitleCard>
                    </div>
                </div>

                <div className="grid grid-cols-12 col-span-12 gap-x-6 mb-[25px]">
                    <TitleCard className="col-span-10" title="Preview" isBordered={false}>
                        <Card className="col-span-12" sx={{ padding: '0', border: '2px dashed #9ca3af' }}>
                            <div className="rounded-md">
                                <img alt='' src={adsLayout} style={{ position: 'relative', zIndex: 1 }} />

                                <img
                                    alt=''
                                    src={adsLayout}
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        background: `url("${uploadBackground}")`,
                                        filter: selectedDataState?.blurBg ? 'blur(5px)' : 'none',
                                        zIndex: 0,
                                    }}
                                />

                                <Box className="absolute z-10 right-0 top-0 w-[45.8%] h-full bg-white rounded-md overflow-y-auto">
                                    <div className="p-5">
                                        <Typography level="h3" className="text-center py-[10px]" style={{ position: 'sticky', top: 0, background: 'white', zIndex: 1000 }}>
                                            <span className="font-MontserratBold">{selectedDataState?.title || "Judul Iklan (Title)"}</span>
                                        </Typography>
                                        <Typography className="text-justify" style={{ whiteSpace: 'pre-line' }}>
                                                <span className="font-Montserrat">
                                                    {selectedDataState?.descr
                                                        ||
                                                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.\n\n" +
                                                        "But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\n" +
                                                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer tooked."
                                                    }
                                                </span>
                                        </Typography>
                                    </div>
                                </Box>

                                <SimpleSlider uploadedImages={uploadGambarUtama} key={uploadGambarUtama.length}
                                />
                            </div>
                        </Card>
                    </TitleCard>
                </div>
            </div>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}