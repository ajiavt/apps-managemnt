import React, {useCallback, useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TitleCard from "../components/Cards/TitleCard";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import ResponsiveModal from "../components/joy-ui/Modal";
import {fingerService} from "./_repository/fingerService";
import {branchService} from "./_repository/branchService";
import Selectbox from "../components/joy-ui/Selectbox";
import TableForm010400 from "./_components/TableForm010400"
import PreviewModal from "../components/joy-ui/PreviewModal";
import FormLabel from "@mui/joy/FormLabel";
import RadioButtonGroup from "../components/joy-ui/RadioButtonGroup";
import CheckboxButton from "../components/joy-ui/CheckboxButton";
import Button from "@mui/joy/Button";
import RestartAltOutlinedIcon from "@mui/icons-material/RestartAltOutlined";
import {CheckCircleOutline} from "@mui/icons-material";


/**
 * Form Pemeliharaan User Fingerprint
 */
// FM = Form Modes
const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    userId: '',
    fingerReq1: null,
    fingerReq2: null,
    fingerReq3: null,

    isFinger: false // mapped from row
};

export default function Form040100(callback, deps){
    const [errMsg, setErrMsg] = useState({});
    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [showModalSave, setShowModalSave] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [selectedDataState, setSelectedDataState] = useState(emptyData); // finger form-data

    const dispatch = useDispatch();

    //const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);
    const [fingersResponse, setFingersResponse] = useState({});
    const [branchsResponse, setBranchsResponse] = useState({});
    const [usersByBranchResponse, setUsersByBranchResponse] = useState({});


    const [branchsData, setBranchsData] = useState({})
    const [usersByBranchData, setUsersByBranchData] = useState({})
    const [fingersData, setFingersData] = useState({})

    // filter value
    const [branchFilter, setBranchFilter] = useState('')
    const [allBranchFilter, setAllBranchFilter] = useState(false);

    // input value
    const [selectedBranch, setSelectedBranch] = useState({});
    const [selectedUser, setSelectedUser] = useState({});
    const [finger1, setFinger1] = useState('') // base64 image
    const [finger2, setFinger2] = useState('')
    const [finger3, setFinger3] = useState('')
    const [fingerReq1, setFingerReq1] = useState({})
    const [fingerReq2, setFingerReq2] = useState({})
    const [fingerReq3, setFingerReq3] = useState({})

    const [findUserLoadingState, setFindUserLoadingState] = useState(false)
    const [captureLoadingState, setCaptureLoadingState] = useState(false)
    const [showModalPreview, setShowModalPreview] = useState(false);
    const [previewCapturedImage, setPreviewCapturedImage] = useState('')
    const [blankCapturedImage, setBlankCapturedImage] = useState('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAvUAAALBCAYAAADYqV42AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAABhaVRYdFNuaXBNZXRhZGF0YQAAAAAAeyJjbGlwUG9pbnRzIjpbeyJ4IjowLCJ5IjowfSx7IngiOjc1NywieSI6MH0seyJ4Ijo3NTcsInkiOjcwNX0seyJ4IjowLCJ5Ijo3MDV9XX0gmYhiAAAOPUlEQVR4Xu3WsQ3AMAzAMKf//+sxWfqEAHLRCzq7ewcAAMj6/gIAAFGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgDhTDwAAcaYeAADiTD0AAMSZegAAiDP1AAAQZ+oBACDO1AMAQJypBwCAOFMPAABxph4AAOJMPQAAxJl6AACIM/UAABBn6gEAIM7UAwBAnKkHAIA4Uw8AAHGmHgAA4kw9AADEmXoAAIgz9QAAEGfqAQAgztQDAECcqQcAgLSZByUZCWbmNgSwAAAAAElFTkSuQmCC')
    const [capturedFmdData, setCapturedFmdData] = useState('')
    const [selectedFingerId, setSelectedFingerId] = useState(0); // 1, 2, 3
    const [recentFingerId, setRecentFingerId] = useState(0); // 1, 2, 3


    const defaultFingerRequest = {
        height: 392,
        width: 357,
        resolution: 500,
        fingerPosition: 0,
        cbeffid: 3407615,
        format: "ANSI_378_2004",
        data: ''
    }
    let saveFingerRequest = {
        userId: '',
        fingerReq1: null,
        fingerReq2: null,
        fingerReq3: null
    }

    // define methods
    const fetchDataAdapter = async (service, stateSetter, loadingStateSetter) => {
        try {
            if (!loadingStateSetter)
                setLoadingState(true);
            else
                loadingStateSetter(true)


            await new Promise(resolve => setTimeout(resolve, 500));
            const { response, status } = await service();
            if (status === 0) setErrorMessage(response);
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            if (!loadingStateSetter)
                setLoadingState(false);
            else
                loadingStateSetter(false)
        }
    };

    const fetchDataUserFingers = useCallback(async () => {
        if (allBranchFilter) {
            fetchDataAdapter(fingerService.bind(null, "GET_ALL"), setFingersResponse);
        } else {
            fetchDataAdapter(fingerService.bind(null, "GET_ALL_BY_BRANCH_ID", { branchId: branchFilter }), setFingersResponse);
        }
    })


    const fetchDataBranchs = () => {
        fetchDataAdapter(branchService.bind(null, "GET_ALL"), setBranchsResponse, setFindUserLoadingState);
    }

    const fetchUsersByBranch = async () => {
        const branchId = selectedBranch
        if (!branchId) return

        try {
            setFindUserLoadingState(true)
            const {response, status} = await fingerService("GET_ALL_BY_BRANCH_ID", { branchId: branchId });
            if (status !== 0) {
                setUsersByBranchResponse(response);
                setUsersByBranchData({
                    "Daftar User": response.data
                })
            } else {
                setErrorMessage(handleError(response));
            }
        } catch (error) {
            setErrorMessage("An error occurred while fetching user fingers data.");
        } finally {
            setFindUserLoadingState(false)
        }
    }

    const fetchCaptureFinger = async () => {
        try {
            setCaptureLoadingState(true)
            const {response, status} = await fingerService("CAPTURE", { timeout: 5 });

            if (status !== 0) {
                setPreviewCapturedImage("data:image/jpeg;base64," + response.data.imageData);
                setCapturedFmdData(response.data.fmdData)
            } else {
                const err = handleError(response)
                if (err) {
                    setErrorMessage(handleError(response));
                }
            }
        } catch (error) {
            setErrorMessage("An error occurred while processing capture.");
        } finally {
            setCaptureLoadingState(false)
        }
    }

    // define effects (watchers)
    useEffect(() => {
        dispatch(setPageTitle({ title : "Pemeliharaan User Fingerprint"}))
    }, []);

    useEffect(() => {
        if (branchFilter) {
            fetchDataUserFingers();
        }
    }, [branchFilter]);

    useEffect(() => {
        if (allBranchFilter) {
            fetchDataUserFingers();
        } else {
            resetListData()
        }
    }, [allBranchFilter]);

    useEffect(() => {
        fetchDataBranchs();
    }, []);

    useEffect(() => {
        fetchUsersByBranch()
    }, [selectedBranch]);

    useEffect(() => {
        if (branchsResponse && branchsResponse.data) {
            setBranchsData({
                "Daftar Cabang": branchsResponse.data
            })
        }
    }, [branchsResponse]);

    // TODO debug gagal mapping response ke list data user
    // useEffect(() => {
    //     if (usersByBranchResponse && usersByBranchResponse.data) {
    //         console.log('usersByBranchResponse', usersByBranchResponse)
    //         setUsersByBranchData({
    //             "Daftar User": usersByBranchResponse.data
    //         })
    //         console.log('usersByBranchData', usersByBranchData)
    //     }
    // }, [usersByBranchResponse]);

    useEffect(() => {
        if (recentFingerId !== 0) {
            fetchCaptureFinger();
        }
        setRecentFingerId(0)
    }, [recentFingerId])

    useEffect(() => {
        if (errorMessage) {
            dispatch(showNotification({ message: errorMessage, status: 0 }));
        }
    }, [errorMessage]);

    /** START - EVENTS ON BUTTON*/
    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.userId);
        setFormMode(FM.VIEW);
    }, []);

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setSelectedDataState({...selectedDataState});
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleSaveClick = useCallback(() => {
        // Cek jika tidak ada data yang berubah
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            setLoadingState(true);
            const { response, status } = await fingerService("CREATE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrMsg(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);


    // TODO not work. error : Too many re-renders. React limits the number of renders to prevent an infinite loop
    const handleOpenPreview = (fingerId = 0) => {
        setPreviewCapturedImage('')
        setCapturedFmdData('')
        setRecentFingerId(fingerId)
        setSelectedFingerId(fingerId)
        setShowModalPreview(true)
    }


    // confirm capture
    const handleConfirmPreview = useCallback(async () => {
        let fingerData = {
            ...defaultFingerRequest,
            fmdData: capturedFmdData
        }

        if (selectedFingerId === 1) {
            setFinger1(previewCapturedImage)
            setFingerReq1(fingerData)

            setSelectedDataState({
                ...selectedDataState,
                fingerReq1: fingerData
            })
        } else if (selectedFingerId === 2) {
            setFinger2(previewCapturedImage)
            setFingerReq2(fingerData)

            setSelectedDataState({
                ...selectedDataState,
                fingerReq2: fingerData
            })
        } else if (selectedFingerId === 3) {
            setFinger3(previewCapturedImage)
            setFingerReq3(fingerData)

            setSelectedDataState({
                ...selectedDataState,
                fingerReq3: fingerData
            })
        }

        setShowModalPreview(false)

        // reset to blank state
        //setPreviewCapturedImage('')
        //setCapturedFmdData('')
    })

    const handleChangeBranch = useCallback(async () => {
        try {
            setFindUserLoadingState(true)
        } catch (error) {

        } finally {

        }
    })

    const handleChangeUser = useCallback(async (userId) => {
        setSelectedUser(userId)
        setSelectedDataState({ ...selectedDataState, userId: userId })
    })

    const handleAllBranchCheckboxChange = () => {
        setBranchFilter('')
        setAllBranchFilter(!allBranchFilter)
    }
    /** END - EVENTS ON BUTTON*/

    // define methods (again)
    const handleError = (response) => {
        const errorResponse = response?.data ? response.data : response
        if (errorResponse && errorResponse.code === "98") {
            dispatch(showNotification({ message: errorResponse?.message, status: 0 }));
            return errorResponse.error;
        } else if (errorResponse && ["99", "93", "94"].includes(errorResponse?.code)) {
            dispatch(showNotification({ message: errorResponse?.message, status: 0 }));
        } else { // unpredicted error
            dispatch(showNotification({ message: errorResponse?.code + ' - ' + errorResponse?.message, status: 0 }));
        }
    }

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrMsg({});

        setFinger1('')
        setFinger2('')
        setFinger3('')
    }, []);

    const resetListData = useCallback(() => {
        setFingersResponse({})
        resetFormData()
    })

    return(
        <>
            <div className="grid lg:grid-cols-12 lg:gap-x-6">
                <div className="lg:col-span-7">
                    <div className="grid lg:grid-cols-12 gap-x-2.5">
                        <Selectbox
                            datas={branchsData}
                            inputValue={branchFilter}
                            setInputValue={setBranchFilter}
                            disabled={!!allBranchFilter}
                            className="col-span-6 mt-[5px] mb-[-10px]"
                            idKey="branchId"
                            nameKey="branchNm"
                            label="Cabang"
                        />

                        <RadioButtonGroup
                            height="36.4px"
                            className="col-span-3 mt-[5px] mb-[-8px]"
                            label="Semua Cabang"
                        >
                            <CheckboxButton
                                padding="0 0 0 5px"
                                value="YA"
                                label="Ya"
                                checked={allBranchFilter}
                                onClick={handleAllBranchCheckboxChange}
                            />
                        </RadioButtonGroup>

                        <Button
                            className="col-span-3 h-[36.4px]"
                            style={{ marginTop: '24.5px' }}
                            variant="outlined"
                            color="neutral"
                            startDecorator={<RestartAltOutlinedIcon className="w-4 h-4" />}
                            onClick={() => {
                                // resetFormData();
                                setBranchFilter('');
                                setAllBranchFilter(false)
                                resetListData()
                            }}
                        >
                            Reset
                        </Button>
                    </div>
                    <TitleCard
                        className="max-h-[85vh] overflow-x-auto lg:col-span-12">
                        <TableForm010400
                            handleRowClick={handleRowClick}
                            selectedRow={selectedRow}
                            refreshTable={refreshTable}
                            isLoading={loadingState}
                            response={fingersResponse}
                        />
                    </TitleCard>
                </div>
                <div className="lg:col-span-5 mt-6">
                    <ButtonCRUD add={handleAddClick} edit={handleEditClick} />
                    <TitleCard>
                        <div className="grid grid-cols-12 gap-x-2.5 gap-y-2.5">
                            <Selectbox
                                idKey="branchId"
                                nameKey="branchNm"
                                label="Cabang"
                                className="col-span-12"
                                disabled={formMode === FM.VIEW}
                                datas={branchsData}
                                inputValue={selectedBranch}
                                setInputValue={setSelectedBranch}
                                value={selectedDataState?.branchId}
                                error={errMsg?.branchId}
                                onChange={handleChangeBranch}
                            />

                            <Selectbox
                                idKey="userId"
                                nameKey="fullName"
                                label="User"
                                className="col-span-12"
                                disabled={formMode === FM.VIEW}
                                datas={usersByBranchData}
                                inputValue={selectedUser}
                                setInputValue={setSelectedUser}
                                value={selectedDataState?.userId}
                                onChange={handleChangeUser}
                                error={errMsg?.userId}
                            />


                            <div className="col-span-6 gap-2.5">
                                <FormLabel className="col-span-12">
                                    <span className="text-[14px] font-Poppins">
                                        Fingerprint 1 {!selectedDataState.isFinger ? '' : <CheckCircleOutline color="success" className="h-4 w-4" />}
                                    </span>
                                </FormLabel>

                                <div
                                    className="rounded-md align-middle -align-center text-center content-center items-center justify-center"
                                    style={{
                                        position: "relative",
                                        height: `230px`,
                                        fontSize: `12px`,
                                        border: '1.5px solid grey'
                                    }}
                                >
                                    <span className="absolute top-0 bottom-0 left-0 right-0 items-center justify-center">
                                        <button
                                            type="button"
                                            disabled={!(formMode !== FM.VIEW && selectedUser)}
                                            className="btn btn-sm btn-primary mt-2"
                                            onClick={() => {
                                                handleOpenPreview(1)
                                            }}
                                        >
                                            {selectedDataState.isFinger ? 'Re-Capture' : 'Capture'} Fingerprint 1
                                        </button>
                                    </span>
                                    <img
                                        src={finger1 ? finger1 : blankCapturedImage}
                                        alt='Finger 1'
                                        style={{
                                            width: '100%',
                                            height: '100%',
                                            zIndex: 98
                                        }}
                                    />
                                </div>
                            </div>

                            <div className="col-span-6 gap-2.5">
                                <FormLabel className="col-span-12">
                                    <span className="text-[14px] font-Poppins">
                                        Fingerprint 2 {!selectedDataState.isFinger ? '' : <CheckCircleOutline color="success" className="h-4 w-4" />}
                                    </span>
                                </FormLabel>

                                <div
                                    className="rounded-md align-middle -align-center text-center content-center"
                                    style={{
                                        position: "relative",
                                        height: `230px`,
                                        fontSize: `12px`,
                                        border: '1.5px solid grey'
                                    }}
                                >
                                    <img
                                        src={finger2 ? finger2 : blankCapturedImage}
                                        alt='Finger 2'
                                        style={{
                                            width: '100%',
                                            height: '100%',
                                            zIndex: 98
                                        }}
                                    />
                                    <span className="absolute top-0 bottom-0 left-0 right-0 items-center justify-center">
                                        <button
                                            type="button"
                                            className="btn btn-sm btn-primary mt-2"
                                            disabled={!(formMode !== FM.VIEW && selectedUser && finger1)}
                                            onClick={() => {
                                                handleOpenPreview(2)
                                            }}
                                        >
                                            {selectedDataState.isFinger ? 'Re-Capture' : 'Capture'}  Fingerprint 2
                                        </button>
                                    </span>
                                </div>
                            </div>

                            <div className="col-span-6 gap-2.5">
                                <FormLabel className="col-span-12">
                                    <span className="text-[14px] font-Poppins">
                                        Fingerprint 3 (Konfirmasi) {!selectedDataState.isFinger ? '' : <CheckCircleOutline color="success" className="h-4 w-4" />}
                                    </span>
                                </FormLabel>

                                <div
                                    className="rounded-md align-middle -align-center text-center content-center items-center justify-center"
                                    style={{
                                        position: "relative",
                                        height: `230px`,
                                        fontSize: `12px`,
                                        border: '1.5px solid grey'
                                    }}
                                >
                                    <span className="absolute top-0 bottom-0 left-0 right-0 items-center justify-center">
                                        <button
                                            type="button"
                                            className="btn btn-sm btn-primary mt-2"
                                            disabled={!(formMode !== FM.VIEW && selectedUser && (finger1 && finger2))}
                                            onClick={() => {
                                                handleOpenPreview(3)
                                            }}
                                        >
                                            {selectedDataState.isFinger ? 'Re-Capture' : 'Capture'} Fingerprint 3
                                        </button>
                                    </span>
                                    <img
                                        className="object-cover z-40"
                                        src={finger3 ? finger3 : blankCapturedImage}
                                        alt='Finger 3'
                                        style={{
                                            width: '100%',
                                            height: '100%',
                                        }}
                                    />
                                </div>
                            </div>
                        </div>

                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode}/>
                    </TitleCard>
                </div>
            </div>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <PreviewModal
                color={"primary"}
                open={showModalPreview}
                title={"Capture Fingerprint " + selectedFingerId}
                isBase64={true}
                isImage={true}
                previewData={previewCapturedImage}
                desc="Apakah Anda yakin ingin menggunakan data ini ?"
                yesAction={handleConfirmPreview}
                noAction={() => setShowModalPreview(false)}
            />
        </>
    )
}