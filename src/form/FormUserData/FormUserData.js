import React, {useEffect, useState} from 'react'
import { useDispatch } from 'react-redux'
import { showNotification } from '../../features/common/headerSlice'
import userDataService from './repository/userDataService'
import {PersonOutline} from "@mui/icons-material";

const FormUserData = () => {

    const dispatch = useDispatch()
    const [response, setResponse] = useState({})
    const [errorMessage, setErrorMessage] = useState()
    const dateOptions = { weekday: 'long', year: '2-digit', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

    const fetchData = async () => {
        const {response, status} = await userDataService()
        if(status === 0) {
            setErrorMessage(response)
        } else {
            setResponse(response)
        }
    }

    useEffect(() => {
        if(errorMessage) {
            dispatch(showNotification({message : errorMessage, status : 0}))
        }
    }, [errorMessage])

    useEffect(() => {
        fetchData()
    }, []);

    return (
        <div className="overflow-x-auto ">
            <li>
                <table className="table">
                    <tbody>
                        <tr>
                            <td className="font-semibold">User Profil</td>
                            <td></td>
                        </tr>
                        <tr className="bg-gray-200">
                            <td>User</td>
                            <td>: {response.user}</td>
                        </tr>
                        <tr>
                            <td>Role</td>
                            <td>: {response.role}</td>
                        </tr>
                        <tr className="bg-gray-200">
                            <td>Cabang</td>
                            <td>: {response.cabang}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Buka</td>
                            <td>: {response.tanggalBuka}</td>
                            {/*<td>: {new Date(response.tanggalBuka + "T00:00:00").toLocaleDateString("id-ID", dateOptions)}</td >*/}
                            {/*Sesuaikan kode di atas sesuai kebutuhan Anda*/}
                            {/*Saat ini hanya menampilkan response.tanggalBuka sebagai string biasa*/}
                        </tr>
                    </tbody>
                </table>
            </li>
        </div>
    );
}


export default FormUserData;
