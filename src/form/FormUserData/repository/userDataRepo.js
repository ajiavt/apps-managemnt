import { apiURL } from "../../../app/url"
import { apiClientUserData } from "../../../app/axios"

const userDataRepo = async () => {
    const ACCESS_TOKEN = localStorage.getItem("access_token")
    const TOKEN_TYPE = localStorage.getItem("token_type")

    try {
        const success = await apiClientUserData.get(
            apiURL.GET_USER_DATA, {
                headers: {
                    'Authorization': `${TOKEN_TYPE} ${ACCESS_TOKEN}`,
                },
            }
        )

        return {response : success};
    }

    catch (error) {
        return {response: error};
    }
}

export default userDataRepo;