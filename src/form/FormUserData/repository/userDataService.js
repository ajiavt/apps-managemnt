import userDataRepo from "./userDataRepo";

const userDataService = async (accessToken) => {
    const {response}  = await userDataRepo();

    if(response.status === 200) {
        const tempData = response.data.data;

        // Date Formatting from String to Date
        const openDate = tempData['openDate'];
        const dtTanggal = new Date(openDate);
        const strTanggal = dateFormatter(dtTanggal);

        let newResponse = {};
        newResponse['user'] = tempData['userId'] + ' - ' + tempData['fullName'];
        newResponse['cabang'] = tempData['branchId'] + ' - ' + tempData['branchNm'];
        newResponse['cabangValue'] = tempData['branchId'];
        newResponse['cabangName'] = tempData['branchNm'];
        newResponse['role'] = tempData['roleId'] + ' - ' + tempData['roleNm'];
        newResponse['tanggalBuka'] = strTanggal;
        newResponse['oldResponse'] = tempData;

        return {response: newResponse, status: 1};
    } else {
        if(!(response && response.response && response.response.data)) {
            return {response: "Gagal terhubung ke API User Data", status: 0};
        }

        let str = response.response.data['error_description'];

        if(str.toLowerCase() === "token is expired") {
            // handled by formLeftbarService
            str = "";
        }

        let str2 = "";
        if(str) {
            str2 = str[0].toUpperCase() + str.slice(1) + "!";
        } else {
            // handled by formLeftbarService
            // str2 = "Koneksi API tidak tersambung!"
        }

        return {response: str2, status: 0};
    }
}

function dateFormatter(tanggal) {
    const tahun = tanggal.getFullYear();
    const bulan = String(tanggal.getMonth() + 1).padStart(2, '0');
    const tanggalTanggal = String(tanggal.getDate()).padStart(2, '0');
    const jam = String(tanggal.getHours()).padStart(2, '0');
    const menit = String(tanggal.getMinutes()).padStart(2, '0');
    const detik = String(tanggal.getSeconds()).padStart(2, '0');

    return tanggalTanggal + "-" + bulan + "-" + tahun + " " + jam + ":" + menit + ":" + detik;
}

export default userDataService;