import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";
import LoadingTable from "../../components/joy-ui/LoadingTable";

export default function TableForm010200({handleRowClick, selectedRow, isLoading, response}) {

    // Pemrosesan Data
    let dataRowNum = 1;

    const rowsToRender = (response?.data || [])
        .sort((a, b) => new Date(b.startedAt) - new Date(a.startedAt))
        .flatMap((data) => {
            const formattedStartedAt = new Date(data.startedAt).toLocaleString('id-ID', {
                day: '2-digit',
                month: '2-digit',
                year: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit'
            });

            const applicationText = data.application.includes("Aplikasi")
                ? data.application.replace("Aplikasi", "").trim()
                : data.application;

            const dataRow = (
                <tr
                    style={{ textAlign: "left", cursor: "pointer", whiteSpace: "pre-line", overflowWrap: "break-word" }}
                    className={`hover:bg-primary/10 ${data.accessToken === selectedRow ? 'bg-primary/10' : ''}`}
                    key={data.accessToken}
                    onClick={() => handleRowClick(data)}
                >
                    <th style={{ textAlign: "center"}}>{dataRowNum++}</th>
                    <td style={{ textAlign: "center"}}>{data.userId}</td>
                    <td style={{ textAlign: "center"}}>{data.branchId}</td>
                    <td>{data.roleNm}</td>
                    <td>{applicationText}</td>
                    <td>{formattedStartedAt}</td>
                    <td style={{ textAlign: "center"}}>{data.deviceId}</td>
                </tr>
            );

            return [dataRow];
        }) || [];

    return (
        <div className={`relative rounded-xl -m-[20px] ${isLoading ? 'h-full' : ''}`}>
            <table className={`table table-xs table-fixed ${isLoading ? 'h-full' : ''}`}>
                <thead style={{
                    position: "sticky",
                    top: -20,
                    backgroundImage: `url(${abstractImage})`,
                    backgroundSize: '50%',
                }}>
                <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word",}}>
                    <th width="5%" style={{ textAlign: "center"}}></th>
                    <td width="9%" style={{ textAlign: "center"}}>User Id</td>
                    <td width="7%" style={{ textAlign: "center"}}>Kd. Cab</td>
                    <td width="22%">Nama Wewenang</td>
                    <td width="10%">Aplikasi</td>
                    <td width="18%">Dimulai Pada</td>
                    <td style={{ textAlign: "center"}}>Device Id</td>
                </tr>
                </thead>

                <tbody>
                {isLoading && <LoadingTable span={7} />}
                {!isLoading && (rowsToRender) }
                </tbody>
            </table>
        </div>
    );
}
