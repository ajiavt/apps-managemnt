import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";
import LoadingTable from "../../components/joy-ui/LoadingTable";

export default function TableForm020100({handleRowClick, selectedRow, isLoading, response}) {

    // Pemrosesan Data
    let dataRowNum = 1;

    const rowsToRender = response?.data?.flatMap((data) => {
        const dataRow = (
            <tr
                style={{ textAlign: "center", cursor: "pointer", whiteSpace: "pre-line", overflowWrap: "break-word"}}
                className={`hover:bg-primary/10 ${data.id === selectedRow ? 'bg-primary/10' : ''}`}
                key={data.id}
                onClick={() => handleRowClick(data)}
            >
                <th>{dataRowNum++}</th>
                <td>{data.id}</td>
                <td style={{ textAlign: "left" }}>{data.name}</td>
            </tr>
        );

        return [dataRow];
    }) || [];

    return (
        <div className={`relative rounded-xl -m-[20px] ${isLoading ? 'h-full' : ''}`}>
            <table className={`table table-xs table-fixed ${isLoading ? 'h-full' : ''}`}>
                <thead style={{
                    position: "sticky",
                    top: -20,
                    backgroundImage: `url(${abstractImage})`,
                    backgroundSize: '50%',
                }}>
                <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                    <th width="9%"></th>
                    <th width="16%">Role Id</th>
                    <th style={{ textAlign: "left" }}>Nama Role</th>
                </tr>
                </thead>

                <tbody>
                {isLoading && <LoadingTable span={3} />}
                {!isLoading && (rowsToRender) }
                </tbody>
            </table>
        </div>
    );
}
