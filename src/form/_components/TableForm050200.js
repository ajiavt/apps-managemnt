import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";

export default function TableForm050200({handleRowClick, selectedRow, isLoading, response}) {

    // Pemrosesan Data
    let dataRowNum = 1;

    const rowsToRender = response?.data?.flatMap((data) => {
        const dataRow = (
            <tr
                style={{ cursor: "pointer" }}
                className={`hover:bg-primary/10 ${data.deviceId === selectedRow ? 'bg-primary/10' : ''}`}
                key={data.deviceId}
                onClick={() => handleRowClick(data)}
            >
                <th style={{ textAlign: "center" }}>{dataRowNum++}</th>
                <td>{data.name}</td>
                <td style={{ textAlign: "center"}}>{data.branchId}</td>
                <td>{data.deviceId}</td>
                <td>{data.deviceIdParent}</td>
                <td>{data.descr}</td>
                <td style={{ textAlign: "center" }}>{data.seq}</td>
            </tr>
        );

        return [dataRow];
    }) || [];

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-full">
            { isLoading && (<LoadingComponent/>)}
            { !isLoading &&
                (
                    <table className="table table-xs table-fixed">
                        <thead style={{
                            position: "sticky",
                            top: -20,
                            backgroundImage: `url(${abstractImage})`,
                            backgroundSize: '50%',
                        }}>
                        <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", }}>
                            <th width="4%" style={{ textAlign: "center"}}></th>
                            <th width="15%">Nama Device</th>
                            <th width="6%" style={{ textAlign: "center"}}>Kd. Cab</th>
                            <th width="24%">Serial Number</th>
                            <th width="24%">Serial Number Parent</th>
                            <th>Deskripsi</th>
                            <th width="5%" style={{ textAlign: "center" }}>Seq</th>
                        </tr>
                        </thead>

                        <tbody>
                        {rowsToRender}
                        </tbody>
                    </table>
                )
            }
        </div>
    );
}
