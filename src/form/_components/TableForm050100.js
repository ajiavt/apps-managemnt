import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";

export default function TableForm050100({handleRowClick, selectedRow, isLoading, response, filteredResult}) {
    // note: response?.data to response (filteredResult)

    let dataRowNum = 1;
    const rowsToRender = filteredResult?.sort((a, b) => (a.parmId > b.parmId ? 1 : -1)) // Urutkan secara ascending berdasarkan parmId
        .flatMap((data) => {
            const dataRow = (
                <tr
                    style={{ textAlign: "center", cursor: "pointer" }}
                    className={`hover:bg-primary/10 ${data.parmId === selectedRow ? 'bg-primary/10' : ''}`}
                    key={data.parmId}
                    onClick={() => {
                        handleRowClick(data)
                    }}
                >
                    <th style={{ textAlign: "center" }} width="6%">{dataRowNum++}</th>
                    <td style={{ textAlign: "left" }}>{data.parmId}</td>
                    <td style={{ textAlign: "left" }}>{data.typeData}</td>
                    <td style={{ textAlign: "left" }}>{data.parmNm}</td>
                </tr>
            );
            return [dataRow];
        }) || [];

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-full">
            { isLoading && (<LoadingComponent className="h-[85vh]"/>)}
            { !isLoading &&
                (
                    <table className="table table-xs table-fixed">
                        <thead style={{
                            position: "sticky",
                            top: -20,
                            backgroundImage: `url(${abstractImage})`,
                            backgroundSize: '50%',
                        }}>
                        <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                            <th width="6%"></th>
                            <th style={{ textAlign: "left" }} width="30%">Parm Id</th>
                            <th style={{ textAlign: "left" }} width="20%">Type Data</th>
                            <th style={{ textAlign: "left" }}>Parm Name</th>
                        </tr>
                        </thead>

                        <tbody>
                        {rowsToRender}
                        </tbody>
                    </table>
                )
            }
        </div>
    );
}
