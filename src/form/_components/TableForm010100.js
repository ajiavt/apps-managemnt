import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";
import LoadingTable from "../../components/joy-ui/LoadingTable";

export default function TableForm010100({handleRowClick, selectedRow, isLoading, userResponse}) {

    // Pemrosesan Data
    let dataRowNum = 1;

    const rowsToRender = userResponse?.data?.flatMap((data) => {
        const applications = data.application.map(app => {
            // Menghapus awalan 'FE_' dari nilai app
            const cleanedApp = app.replace('FE_', '');

            // Melakukan penggantian nilai berdasarkan kondisi tertentu
            switch (cleanedApp) {
                case 'BE':
                    return 'UM';
                case 'CUSTOMER':
                    return 'Customer';
                case 'TELLER':
                    return 'Office';
                default:
                    return cleanedApp;
            }
        }).join(', ');

        const dataRow = (
            <tr
                style={{ textAlign: "center", cursor: "pointer", whiteSpace: "pre-line", overflowWrap: "break-word" }}
                className={`hover:bg-primary/10 ${data.userId === selectedRow ? 'bg-primary/10' : ''}`}
                key={data.userId}
                onClick={() => handleRowClick(data)}
            >
                <th>{dataRowNum++}</th>
                <td>{data.status === 1 ? "Aktif" : "Tidak Aktif"}</td>
                <td align="left">{data.userId}</td>
                <td style={{ textAlign: "left" }}>{data.fullName}</td>
                <td>{data.branchId}</td>
                <td>{data.roleId}</td>
                <td>{data.iaNbr}</td>
                <td style={{ textAlign: "left" }}>{applications}</td>
            </tr>
        );

        return [dataRow];
    }) || [];

    return (
        <div className={`relative rounded-xl -m-[20px] ${isLoading ? 'h-full' : ''}`}>
            <table className={`table table-xs table-fixed ${isLoading ? 'h-full' : ''}`}>
                <thead style={{
                    position: "sticky",
                    top: -20,
                    backgroundImage: `url(${abstractImage})`,
                    backgroundSize: '50%',
                }}>
                <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                    <th width="5%"></th>
                    <th width="9%">Status</th>
                    <th width="9%" align="left">User Id</th>
                    <th>Nama Lengkap</th>
                    <th width="7%">Kd. Cab</th>
                    <th width="7%">Role Id</th>
                    <th>No. Rek. IA</th>
                    <th>Aplikasi</th>
                </tr>
                </thead>

                <tbody>
                {isLoading && <LoadingTable span={8} />}
                {!isLoading && (rowsToRender)}
                </tbody>
            </table>
        </div>
    );
}
