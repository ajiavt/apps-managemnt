import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";

export default function TableForm040100({handleRowClick, selectedRow, isLoading, response}) {

    // Pemrosesan Data
    let dataRowNum = 1;

    const rowsToRender = response?.data?.flatMap((data) => {
        const dataRow = (
            <tr
                style={{ textAlign: "center", cursor: "pointer" }}
                className={`hover:bg-primary/10 ${data.code === selectedRow ? 'bg-primary/10' : ''}`}
                key={data.code}
                onClick={() => handleRowClick(data)}
            >
                <th className="pl-[15px]">{dataRowNum++}</th>
                <td>{data.code}</td>
                <td style={{ textAlign: "left" }}>{data.path}</td>
                <td style={{ textAlign: "left" }}>{data.desc}</td>
            </tr>
        );

        return [dataRow];
    }) || [];

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-full">
            { isLoading && (<LoadingComponent/>)}
            { !isLoading &&
                (
                    <table className="table table-xs table-fixed">
                        <thead style={{
                            position: "sticky",
                            top: -20,
                            backgroundImage: `url(${abstractImage})`,
                            backgroundSize: '50%',
                        }}>
                        <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                            <th width="4%"></th>
                            <th width="12%">Kode API</th>
                            <th style={{ textAlign: "left" }}>Path API</th>
                            <th style={{ textAlign: "left" }}>Deskripsi</th>
                        </tr>
                        </thead>

                        <tbody>
                        {rowsToRender}
                        </tbody>
                    </table>
                )
            }
        </div>
    );
}
