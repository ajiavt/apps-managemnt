import AccordionSummary, {accordionSummaryClasses} from "@mui/joy/AccordionSummary";
import AccordionDetails, {accordionDetailsClasses} from "@mui/joy/AccordionDetails";
import Accordion from "@mui/joy/Accordion";
import {InfoOutlined} from "@mui/icons-material";
import AccordionGroup from "@mui/joy/AccordionGroup";
import React from "react";

export default function AccordionUploadGambar() {

    return (
        <AccordionGroup
            className="col-span-12"
            variant="outlined"
            transition="0.2s"
            sx={{
                opacity: '80%',
                fontSize: '14px',
                borderRadius: 'lg',
                [`& .${accordionSummaryClasses.button}:hover`]: {
                    bgcolor: 'transparent',
                },
                [`& .${accordionDetailsClasses.content}`]: {
                    boxShadow: (theme) => `inset 0 1px ${theme.vars.palette.divider}`,
                    [`&.${accordionDetailsClasses.expanded}`]: {
                        paddingBlock: '0.75rem',
                    },
                },
            }}
        >
            <Accordion>
                <AccordionSummary>
                    <span>
                        <InfoOutlined sx={{width: '17px', paddingRight: '5px'}}/>Ketentuan Upload Gambar
                    </span>
                </AccordionSummary>
                <AccordionDetails variant="soft">
                    <table>
                        <tr>
                            <td>- Maksimal ukuran gambar 1 mb</td>
                        </tr>
                        <tr>
                            <td>- Rekomendasi ukuran gambar 1280 x 720</td>
                        </tr>
                    </table>
                </AccordionDetails>
            </Accordion>
        </AccordionGroup>
    );

}