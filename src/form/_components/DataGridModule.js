import React, { useState, useEffect } from 'react';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";

const DataGridModule =
    ({
         data
         , columns
         , selectedData = []
         , selection
         , setSelection
         , loadingState
         , disableQuickFilter=true
         , disableColumnMenu=true
         , disableColumnSelector=true
         , disableColumnFilter=true
         , disableExport=true
         , density="compact"
         , pageSize=[]
     }) => {

    useEffect(() => {
        if (selectedData?.length > 0) {
            const selectedIds = selectedData.map(item => item.grpId);
            const selectedIndices = data.reduce((indices, row) => {
                if (selectedIds.includes(row.id)) {
                    indices.push(row.id);
                }
                return indices;
            }, []);

            setSelection(selectedIndices);
        }
    }, [selectedData, data]);

    return (
        <div style={{ height: '100%', width: '100%' }}>
            {loadingState && (
                <div className="w-full h-full">
                    <div className="absolute flex shadow-base-300 justify-center items-center h-[95%] w-[95%] ">
                        <div className="flex-none">
                            <span className="loading loading-dots loading-lg text-primary"></span>
                        </div>
                    </div>
                </div>
            )}
            {!loadingState && (
                <>
                    {columns.length > 0 && data !== undefined && data.length > 0 ? (
                        <DataGrid
                            disableColumnMenu={disableColumnMenu}
                            disableColumnSelector={disableColumnSelector}
                            disableColumnFilter={disableColumnFilter}
                            checkboxSelection={selection ? true : false}
                            disableExport={true}
                            density={density}
                            sx={{ border: 'none' }}
                            rows={data}
                            columns={columns}
                            rowSelectionModel={selection}
                            onRowSelectionModelChange={setSelection}
                            sx={{
                                '&.MuiDataGrid-root': {
                                    fontFamily: 'Poppins, sans-serif',
                                },
                                '.MuiDataGrid-cell' : {
                                    cursor: 'pointer',
                                },
                            }}
                            pageSizeOptions={pageSize} // Specify available page size options
                            slots={{ toolbar: GridToolbar }}
                            slotProps={{
                                toolbar: {
                                    printOptions: { disableToolbarButton: true }, // Disable print option
                                    csvOptions: { disableToolbarButton: disableExport },
                                    showQuickFilter: !disableQuickFilter,
                                },
                            }}
                        />
                    ) : (
                        <div className="h-full w-full flex justify-center items-center text-lg opacity-20">
                            Data tidak ditemukan. Silahkan pilih data yang lain.
                        </div>
                    )}
                </>
            )}
        </div>
    );
};

export default DataGridModule;
