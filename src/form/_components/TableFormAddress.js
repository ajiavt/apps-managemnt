import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";

export default function TableForm050200({isLoading, response}) {

    // Pemrosesan Data
    let dataRowNum = 1;

    const rowsToRender = response?.data?.flatMap((data) => {
        const dataRow = (
            <tr
                style={{ textAlign: "center"}}
                className='hover:bg-primary/10'
                key={data.code}
            >
                <th className="pl-[15px]">{dataRowNum++}</th>
                <td>{data.code}</td>
                <td style={{ textAlign: "left" }}>{data.descr}</td>
            </tr>
        );

        return [dataRow];
    }) || [];

    // Check if rowsToRender is empty
    const tableBodyContent = rowsToRender.length === 0 ? (
        <tr style={{ textAlign: "center" }} className='hover:bg-primary/10'>
            <td colSpan="3">-- Tidak ada data yang ditampilkan 😅 --</td>
        </tr>
    ) : rowsToRender;

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-full">
            { isLoading && (<LoadingComponent/>)}
            { !isLoading &&
                (
                    <table className="table table-xs table-fixed">
                        <thead style={{
                            position: "sticky",
                            top: -20,
                            backgroundImage: `url(${abstractImage})`,
                            backgroundSize: '50%',
                        }}>
                        <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                            <th width="4%"></th>
                            <th>Kode</th>
                            <th style={{ textAlign: "left" }}>Deskripsi</th>
                        </tr>
                        </thead>

                        <tbody>
                        {tableBodyContent}
                        </tbody>
                    </table>
                )
            }
        </div>
    );
}