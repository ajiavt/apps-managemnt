import React, { useRef } from 'react';
import 'jspdf-autotable';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from '../../components/joy-ui/LoadingComponent';

const numberFormatter = new Intl.NumberFormat('id-ID', {
    // style: 'currency',
    // currency: 'IDR',
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
});

const TableForm060300 = ({ isLoading, response }) => {
    const pdfRef = useRef();
    let prevTeller = null;

    const renderRows = () => {
        let dataRowNum = 1;

        // Handle the new JSON structure
        return Array.isArray(response?.data)
            ? response?.data?.map((userTeller, userTellerIndex) =>
                Array.isArray(userTeller?.details)
                    ? userTeller.details.map((row, index) => (
                        <React.Fragment key={index}>
                            {userTeller.userTeller !== prevTeller && (
                                <tr
                                    className="bg-base-200 text-black font-semibold"
                                    style={{
                                        position: 'sticky',
                                        top: '29px',
                                        zIndex: 40, // Menambahkan zIndex agar elemen tetap di atas kontennya
                                    }}
                                >
                                    <td colSpan={11} style={{ textAlign: 'left' }}>
                                        ID Teller : {prevTeller = userTeller.userTeller}
                                    </td>
                                </tr>
                            )}

                            <tr>
                                {/* Adjust the fields according to your new JSON structure */}
                                <td style={{ textAlign: 'center' }}>{dataRowNum++}</td>
                                <td style={{ textAlign: 'left' }}>{row.txId}</td>
                                <td style={{ textAlign: 'left' }}>{row.accNbr}</td>
                                <td style={{ textAlign: 'left' }}>{row.fullName}</td>
                                <td style={{ textAlign: 'center' }}>{row.txCcy}</td>
                                <td style={{ textAlign: 'left' }}>{row.txMsg}</td>
                                <td style={{ textAlign: 'center' }}>{row.dbCr}</td>
                                <td style={{ textAlign: 'right' }}>
                                    {row.dbCr === 0 ? numberFormatter.format(row.txAmount) : '0'}
                                </td>
                                <td style={{ textAlign: 'right' }}>
                                    {row.dbCr === 1 ? numberFormatter.format(row.txAmount) : '0'}
                                </td>
                                <td style={{ textAlign: 'right' }}>{numberFormatter.format(row.txAmount)}</td>
                                <td style={{ textAlign: 'center' }}>{row.userAuth}</td>
                            </tr>
                        </React.Fragment>
                    ))
                    : null
            )
            : [];
    };

    const renderFooterRow = () => {
        let totalDebet = 0;
        let totalKredit = 0;
        let totalData = 0;


        response?.data?.forEach((userTeller) => {
            // Check if userTeller.details is an array
            if (Array.isArray(userTeller.details)) {
                // Loop through userTeller.details
                userTeller.details.forEach((row, index) => {
                    const amount = parseFloat(row.txAmount) || 0;
                    totalDebet += row.dbCr === 0 ? amount : 0;
                    totalKredit += row.dbCr === 1 ? amount : 0;
                    totalData += 1;
                });
            }
        });

        return (
            <tr style={{ position: 'sticky', bottom: '-20px' }} className="bg-base-200 text-black font-semibold">
                <td style={{ textAlign: 'left', paddingLeft: '11px', }} colSpan="3">
                    {totalData + ' total data.'}
                </td>
                <td></td>
                <td style={{ textAlign: 'right' }} colSpan="3">Total</td>
                <td style={{ textAlign: 'right' }}>{numberFormatter.format(totalDebet)}</td>
                <td style={{ textAlign: 'right' }}>{numberFormatter.format(totalKredit)}</td>
                <td></td>
                <td></td>
            </tr>
        );
    };

    const tableBodyContent = renderRows().length === 0 ? (
        <tr style={{ textAlign: 'center' }} className='h-[498px]'>
            <td colSpan="11" className="text-lg opacity-20">
                Tidak ada data yang ditampilkan.
            </td>
        </tr>
    ) : (
        <>
            {renderRows()}
            {renderFooterRow()}
        </>
    )

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-full">
            {isLoading && <LoadingComponent className="h-[75vh]" />}
            {!isLoading && (
                <>
                    <div ref={pdfRef}>
                        <table className="table table-xs table-fixed" id="table">
                            <thead
                                style={{
                                    position: 'sticky',
                                    top: -20,
                                    backgroundImage: `url(${abstractImage})`,
                                    backgroundSize: '50%',
                                    zIndex: 50,
                                }}
                            >
                            <tr style={{ whiteSpace: 'pre-line', overflowWrap: 'break-word' }}>
                                <th rowSpan={2} style={{ textAlign: 'center' }} width="3%">No.</th>
                                <td rowSpan={2} style={{ textAlign: 'left' }} width="9%">Id Transaksi</td>
                                <td rowSpan={2} style={{ textAlign: 'left' }} width="12%">No Rekening</td>
                                <td rowSpan={2} style={{ textAlign: 'left' }}>Nama Rekening</td>
                                <td rowSpan={2} style={{ textAlign: 'center' }} width="4%">Val Tx</td>
                                <td rowSpan={2} style={{ textAlign: 'left' }}>Keterangan</td>
                                <td rowSpan={2} style={{ textAlign: 'center' }} width="3.5%">DB/CR</td>
                                <td colSpan={2} style={{ textAlign: 'center' }}>Mutasi</td>
                                <td rowSpan={2} style={{ textAlign: 'right' }} width="12%">Mutasi Val Transaksi</td>
                                <td rowSpan={2} style={{ textAlign: 'center' }} width="5.2%">User Spv</td>
                            </tr>
                            <tr
                                style={{
                                    textAlign: 'right',
                                    whiteSpace: 'pre-line',
                                    overflowWrap: 'break-word',
                                }}
                            >
                                <td width="12%">Debet</td>
                                <td width="12%">Kredit</td>
                            </tr>
                            </thead>

                            <tbody>{tableBodyContent}</tbody>
                        </table>
                    </div>
                </>
            )}
        </div>
    );
};

export default TableForm060300;
