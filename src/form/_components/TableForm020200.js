import React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from '../../components/joy-ui/LoadingComponent';
import Checkbox from '@mui/material/Checkbox';
import LoadingTable from "../../components/joy-ui/LoadingTable";

export default function TableForm020200
    ({
         isLoading,
         response,
         checkedState,
         setCheckedState,
     }) {

    let dataRowNum = 1;

    const handleCheckboxChange = (code, method) => {
        setCheckedState((prevState) => ({
            ...prevState,
            [code]: {
                ...prevState[code],
                [method]: !prevState[code]?.[method], // toggle value
            },
        }));
    };

    const renderCheckbox = (code, method) => (
        <Checkbox
            value={checkedState[code]?.[method]}
            checked={checkedState[code]?.[method] ?? false}
            onChange={() => handleCheckboxChange(code, method)}
        />
    );

    const renderRow = (data) => (
        <tr
            key={data.code}
            style={{ textAlign: 'center', cursor: 'pointer', whiteSpace: 'pre-line', overflowWrap: 'break-word'}}
            className="hover:bg-primary/10"
        >
            <th>{dataRowNum++}</th>
            <td style={{ textAlign: 'left' }}>{data.code}</td>
            <td style={{ textAlign: 'left' }}>{data.path}</td>
            <td style={{ textAlign: 'left' }}>{data.desc}</td>
            {['POST', 'GET', 'PUT', 'DELETE', 'PATCH', 'TRACE', 'OPTIONS'].map(
                (method) => (
                    <td key={method}>{renderCheckbox(data.code, method)}</td>
                )
            )}
        </tr>
    );

    const rowsToRender = (response?.data || []).slice().sort((a, b) => a.code - b.code).map(renderRow);

    return (
        <div className={`relative rounded-xl -m-[20px] ${isLoading ? 'h-full' : ''}`}>
            <table className={`table table-xs table-fixed ${isLoading ? 'h-full' : ''}`}>
                <thead
                    style={{
                        position: 'sticky',
                        top: -20,
                        backgroundImage: `url(${abstractImage})`,
                        backgroundSize: '50%',
                        zIndex: 50,
                    }}
                >
                <tr
                    style={{
                        whiteSpace: 'pre-line',
                        overflowWrap: 'break-word',
                        textAlign: 'center',
                    }}
                >
                    <th width="4%"></th>
                    <th style={{ textAlign: 'left' }} width="7%">Kode API</th>
                    <th style={{ textAlign: 'left' }}>Path API</th>
                    <th style={{ textAlign: 'left' }}>Deskripsi</th>
                    {['POST', 'GET', 'PUT', 'DELETE', 'PATCH', 'TRACE', 'OPTIONS'].map(
                        (method) => (
                            <th key={method} width="6.2%" className={getColor(method)}>
                                {method}
                            </th>
                        )
                    )}
                </tr>
                </thead>

                <tbody>
                {isLoading && <LoadingTable span={11} />}
                {!isLoading && (rowsToRender) }
                </tbody>
            </table>
        </div>
    );
}

function getColor(method) {
    switch (method) {
        case 'POST':
            return 'text-green-500';
        case 'GET':
            return 'text-blue-500';
        case 'PUT':
            return 'text-yellow-500';
        case 'DELETE':
            return 'text-red-500';
        default:
            return 'text-purple-500';
    }
}
