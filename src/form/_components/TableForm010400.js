import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";

export default function TableForm040100({handleRowClick, selectedRow, isLoading, response}) {

    let dataRowNum = 1;
    const rowsToRender = response?.data
        ?.sort((a, b) => (a.userId > b.userId ? 1 : -1)) // Urutkan secara ascending berdasarkan parmId
        .flatMap((data) => {
            const dataRow = (
                <tr
                    style={{ textAlign: "center", cursor: "pointer" }}
                    className={`hover:bg-primary/10 ${data.userId === selectedRow ? 'bg-primary/10' : ''}`}
                    key={data.userId}
                    onClick={() => handleRowClick(data)}
                >
                    <th style={{ textAlign: "center" }} width="6%">{dataRowNum++}</th>
                    <td style={{ textAlign: "left" }}>{data.branchId}</td>
                    <td style={{ textAlign: "left" }}>{data.userId}</td>
                    <td style={{ textAlign: "left" }}>{data.fullName}</td>
                    <td style={{ textAlign: "left" }}>{data.isFinger ? `SUDAH` : `BELUM`}</td>
                </tr>
            );

            return [dataRow];
        }) || [];

    const tableBodyContent = () => {
        if (!(response.data && response.data.length > 0)) {
            return (<tr style={{ textAlign: 'center' }} className='h-[498px]'>
                <td colSpan="5" className="text-lg opacity-20">
                    Tidak ada data yang ditampilkan.
                </td>
            </tr>)
        } else {
            return (
                <>
                    {rowsToRender}
                </>
            )
        }
    }

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-full">
            { isLoading && (<LoadingComponent className="h-[85vh]"/>)}
            { !isLoading &&
                (
                    <table className="table table-xs table-fixed">
                        <thead style={{
                            position: "sticky",
                            top: -20,
                            backgroundImage: `url(${abstractImage})`,
                            backgroundSize: '50%',
                        }}>
                        <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                            <th width="6%"></th>
                            <th style={{ textAlign: "left" }}>Kode Cabang</th>
                            <th style={{ textAlign: "left" }} width="30%">Kode User</th>
                            <th style={{ textAlign: "left" }}>User Name</th>
                            <th style={{ textAlign: "left" }}>Status Rekam</th>
                        </tr>
                        </thead>

                        <tbody>
                        {tableBodyContent()}
                        </tbody>
                    </table>
                )
            }
        </div>
    );
}
