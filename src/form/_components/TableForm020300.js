import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";
import Checkbox from '@mui/material/Checkbox';
import { useEffect } from 'react';
import LoadingTable from "../../components/joy-ui/LoadingTable";

export default function TableForm020300
    ({
         isLoading,
         response,
         checkedState,
         cbHeaderChecked= false,
         cbhState = false,
         originalCheckedState,
         setCheckedState,
         setCbHeaderChecked,
         setCbhState,
         lastUserClick,
         setLastUserClick,
         setOriginalCheckedState,
         originalDataState,
         setOriginalDataState,
         menuResponse,
         selectedDataState,
         setSelectedDataState,
     }) {

    const convertCbhStateToOriginalCheckedState = (cbhState, isChecked) => {
        const result = { ...originalCheckedState };

        if(lastUserClick === 'cbAll' && cbHeaderChecked) {
            Object.entries(result).forEach(([parentId, childStates]) => {
                result[parentId] = {};

                Object.keys(childStates).forEach(childId => {
                    result[parentId][childId] = isChecked;
                });
            });

            return result;
        }

        return { ...checkedState };
    };

    const handleCheckboxChange = (parentId, childId, subChildId) => {
        const resultCbhStateToOriginalCheckedState = convertCbhStateToOriginalCheckedState(cbhState, true);
        setCheckedState(resultCbhStateToOriginalCheckedState);

        setLastUserClick('cbListItem');
        setCbhState({});
        setCbHeaderChecked(false);

        setCheckedState(prevState => {
            let newState = {...prevState};
            let initOCS = {...newState};
            let parentsToRemove = [];
            let childsToRemove = [];

            if (subChildId) {
                if (!newState[parentId] || typeof newState[parentId] === 'boolean') {
                    newState[parentId] = {};
                }
                if (!newState[parentId][childId] || typeof newState[parentId][childId] === 'boolean') {
                    newState[parentId][childId] = {};
                }
                newState[parentId][childId][subChildId] = !newState[parentId][childId][subChildId];

                const allSubChildUnchecked = Object.values(newState[parentId][childId]).every(v => !v);
                if (allSubChildUnchecked) {
                    newState[parentId][childId] = false;

                    const allChildUnchecked = Object.values(newState[parentId]).every(v => !v);
                    if (allChildUnchecked) {
                        newState[parentId] = false;
                    }
                }
            } else if (childId) {
                if (!newState[parentId] || typeof newState[parentId] === 'boolean') {
                    newState[parentId] = {};
                }
                newState[parentId][childId] = !newState[parentId][childId];

                const allChildUnchecked = Object.values(newState[parentId]).every(v => !v);
                if (allChildUnchecked) {
                    newState[parentId] = false;
                }
            } else {
                newState[parentId] = !newState[parentId];

                Object.keys(originalCheckedState).forEach(parentIdKey => {
                    if(parentId === parentIdKey) {
                        parentsToRemove = [];
                        initOCS[parentIdKey] = {};

                        let countChild = 0;
                        let countProblemChild = 0;
                        Object.keys(originalCheckedState[parentIdKey]).forEach(childIdKey => {
                            countChild++;
                            initOCS[parentIdKey][childIdKey] = newState[parentIdKey];

                            if(initOCS[parentIdKey][childIdKey] === undefined || initOCS[parentIdKey][childIdKey] === false) {
                                countProblemChild++;
                            }
                        });

                        if(countChild === countProblemChild) {
                            parentsToRemove.push(parentIdKey);
                        }

                        parentsToRemove.forEach(parentIdToRemove => {
                            delete initOCS[parentIdToRemove];
                        });

                        newState = { ...initOCS }
                    }
                });
            }

            return newState;
        });
    };

    const handleCbHeaderChange = (event) => {
        setLastUserClick('cbAll');
        const isChecked = event.target.checked;
        setCheckedState({});
        setCbHeaderChecked(isChecked);

        setCbhState((prevCbhState) => {
            let newCbhState = { ...prevCbhState };
            response.data.forEach((parent) => {
                newCbhState[parent.menuId] = isChecked;

                if (parent.menuChild) {
                    parent.menuChild.forEach((child) => {
                        newCbhState[child.menuId] = isChecked;

                        if (child.menuChild) {
                            child.menuChild.forEach((subChild) => {
                                newCbhState[subChild.menuId] = isChecked;
                            });
                        }
                    });
                }
            });

            return newCbhState;
        });
    };

    useEffect(() => {
        if(Object.keys(selectedDataState).length !== 0) {
            if (JSON.stringify(selectedDataState?.menus) === JSON.stringify(menuResponse?.data)) {
                setCbHeaderChecked(true);
            }
        } else {
            setCbHeaderChecked(false);
        }
    }, [selectedDataState])

    // Pemrosesan Data
    let parentRowNum = 1;
    let childRowNum = 1;
    let subChildRowNum = 1;

    const rowsToRender = response?.data?.flatMap((parent) => {
        const parentRow = (
            <tr
                style={{ textAlign: "center", cursor: "pointer" }}
                className={`hover:bg-primary/10 bg-neutral/10`}
                key={parent.menuId}
                onClick={(e) => { e.preventDefault(); handleCheckboxChange(parent.menuId); }}
            >
                <th>
                    <Checkbox
                        id={`cbParent-${parent.menuId}`}
                        value={parent.menuId}
                        style={{margin: 0, padding: 0}}
                        checked={(checkedState[parent.menuId] ?? false) || (cbhState[parent.menuId] ?? false)}
                        onChange={() => handleCheckboxChange(parent.menuId)}
                    />
                </th>
                <th className="pl-[15px]">{parentRowNum++}</th>
                <td colSpan="3" style={{ textAlign: "left" }}>{parent.name}</td>
                <td>{parent.menuParent}</td>
                <td>{parent.menuId}</td>
            </tr>
        );

        // Child Rows
        const childRows =
            [...(parent?.menuChild?.map((child) => {
                // Grand Child Rows
                const subChildRows = child?.menuChild?.map((subChild) => (
                    <tr
                        style={{ textAlign: "center", cursor: "pointer" }}
                        className={`hover:bg-primary/10`}
                        key={subChild.menuId}
                        onClick={(e) => { e.preventDefault(); handleCheckboxChange(parent.menuId, child.menuId, subChild.menuId); }}
                    >
                        <th>
                            <Checkbox
                                id={`cbSubChild-${subChild.menuId}`}
                                value={subChild.menuId}
                                style={{margin: 0, padding: 0}}
                                checked={(checkedState[parent.menuId]?.[child.menuId]?.[subChild.menuId] ?? false) || (cbhState[subChild.menuId] ?? false)}
                                onChange={() => handleCheckboxChange(parent.menuId, child.menuId, subChild.menuId)}
                            />
                        </th>
                        <th></th>
                        <th></th>
                        <th>{`${parentRowNum - 1}.${childRowNum}.${subChildRowNum++}`}</th>
                        <td style={{ textAlign: "left" }}>{subChild.name}</td>
                        <td>{subChild.menuParent}</td>
                        <td>{subChild.menuId}</td>
                    </tr>)
                ) || [];

                const fragmentKey = `${child.menuId}-fragment`;

                return (
                    <React.Fragment key={fragmentKey}>
                        {/* Return the child row */}
                        <tr
                            style={{ textAlign: "center", cursor: "pointer" }}
                            className={`hover:bg-primary/10`}
                            key={child.menuId}
                            onClick={(e) => { e.preventDefault(); handleCheckboxChange(parent.menuId, child.menuId); }}
                        >
                            <th>
                                <Checkbox
                                    id={`cbChild-${child.menuId}`}
                                    value={child.menuId}
                                    style={{margin: 0, padding: 0}}
                                    checked={(checkedState[parent.menuId]?.[child.menuId] ?? false) || (cbhState[child.menuId] ?? false)}
                                    onChange={() => handleCheckboxChange(parent.menuId, child.menuId)}
                                />
                            </th>
                            <th></th>
                            <th>{`${parentRowNum - 1}.${childRowNum++}`}</th>
                            <td colSpan="2" style={{ textAlign: "left" }}>{child.name}</td>
                            <td>{child.menuParent}</td>
                            <td>{child.menuId}</td>
                        </tr>

                        {/* Return the grand child rows */}
                        { subChildRows }
                    </React.Fragment>
                );
            }) || [])];

        childRowNum = 1;
        subChildRowNum = 1;

        return [parentRow, ...childRows];
    }) || [];

    return (
        <div className={`relative rounded-xl -m-[20px] ${isLoading ? 'h-full' : ''}`}>
            <table className={`table table-xs table-fixed ${isLoading ? 'h-full' : ''}`}>
                <thead style={{
                    position: "sticky",
                    top: -20,
                    backgroundImage: `url(${abstractImage})`,
                    backgroundSize: '50%',
                    zIndex: 50,
                }}>
                <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                    <th width="4%">
                        <Checkbox
                            id="cbHeader"
                            value={"ALL"}
                            style={{ margin: 0, padding: 0 }}
                            checked={cbHeaderChecked}
                            onChange={handleCbHeaderChange}
                        />
                    </th>
                    <th width="4%"></th>
                    <th width="4%"></th>
                    <th width="5%"></th>
                    <th>Nama Menu</th>
                    <th width="16%">Menu Parent</th>
                    <th width="16%">Menu Id</th>
                </tr>
                </thead>

                <tbody>
                {isLoading && <LoadingTable span={7} />}
                {!isLoading && (rowsToRender)}
                </tbody>
            </table>
        </div>
    );
}
