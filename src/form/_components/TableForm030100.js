import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";

export default function TableForm030100({handleRowClick, selectedRow, isLoading, response}) {

    // Pemrosesan Data
    let parentRowNum = 1;
    let childRowNum = 1;
    let grandChildRowNum = 1;

    const rowsToRender = response?.data?.flatMap((parent) => {
        const parentRow = (
            <tr
                style={{ textAlign: "center", cursor: "pointer" }}
                className={`hover:bg-neutral/10 ${parent.menuId === selectedRow ? 'bg-neutral/10' : ''}`}
                key={parent.menuId}
                onClick={() => handleRowClick(parent)}
            >
                <th className="pl-[15px]">{parentRowNum++}</th>
                <td colSpan="3" style={{ textAlign: "left" }}>{parent.name}</td>
                <td>{parent.menuParent}</td>
                <td>{parent.menuId}</td>
                <td>{parent.pathFE}</td>
                <td>{parent.seq}</td>
            </tr>
        );

        // Child Rows
        const childRows =
            [...(parent?.menuChild?.map((child) => {
                // Grand Child Rows
                const grandChildRows = child?.menuChild?.map((grandChild) => (
                    <tr
                        style={{ textAlign: "center", cursor: "pointer" }}
                        className={`hover:bg-primary/10 ${grandChild.menuId === selectedRow ? 'bg-primary/10' : ''}`}
                        key={grandChild.menuId}
                        onClick={() => handleRowClick(grandChild)}
                    >
                        <th></th>
                        <th></th>
                        <th>{`${parentRowNum - 1}.${childRowNum}.${grandChildRowNum++}`}</th>
                        <td style={{ textAlign: "left" }}>{grandChild.name}</td>
                        <td>{grandChild.menuParent}</td>
                        <td>{grandChild.menuId}</td>
                        <td>{grandChild.pathFE}</td>
                        <td>{grandChild.seq}</td>
                    </tr>)
                ) || [];

                const fragmentKey = `${child.menuId}-fragment`;

                return (
                    <React.Fragment key={fragmentKey}>
                        {/* Return the child row */}
                        <tr
                            style={{ textAlign: "center", cursor: "pointer" }}
                            className={`hover:bg-primary/10 ${child.menuId === selectedRow ? 'bg-primary/10' : ''}`}
                            key={child.menuId}
                            onClick={() => handleRowClick(child)}
                        >
                            <th></th>
                            <th>{`${parentRowNum - 1}.${childRowNum++}`}</th>
                            <td colSpan="2" style={{ textAlign: "left" }}>{child.name}</td>
                            <td>{child.menuParent}</td>
                            <td>{child.menuId}</td>
                            <td>{child.pathFE}</td>
                            <td>{child.seq}</td>
                        </tr>

                        {/* Return the grand child rows */}
                        { grandChildRows }
                    </React.Fragment>
                );
            }) || [])];

        childRowNum = 1;
        grandChildRowNum = 1;

        return [parentRow, ...childRows];
    }) || [];

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-full">
            { isLoading && (<LoadingComponent/>)}
            { !isLoading &&
                (
                    <table className="table table-xs table-fixed">
                        <thead style={{
                            position: "sticky",
                            top: -20,
                            backgroundImage: `url(${abstractImage})`,
                            backgroundSize: '50%',
                        }}>

                        <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                            <th width="4%"></th>
                            <th width="4%"></th>
                            <th width="5%"></th>
                            <th>Nama Menu</th>
                            <th width="12%">Menu Parent</th>
                            <th width="12%">Menu Id</th>
                            <th width="26%">Path FE</th>
                            <th width="8%">Seq</th>
                        </tr>
                        </thead>

                        <tbody>
                        {rowsToRender}
                        </tbody>
                    </table>
                )
            }
        </div>
    );
}
