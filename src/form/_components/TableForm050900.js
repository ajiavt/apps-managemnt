import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingComponent from "../../components/joy-ui/LoadingComponent";
import LoadingTable from "../../components/joy-ui/LoadingTable";

export default function TableForm050900({handleRowClick, selectedRow, isLoading, response}) {

    // Pemrosesan Data
    let dataRowNum = 1;

    const rowsToRender = response?.data?.flatMap((data) => {
        const dataRow = (
            <tr
                style={{ textAlign: "center", cursor: "pointer" }}
                className={`hover:bg-primary/10 ${data.id === selectedRow ? 'bg-primary/10' : ''}`}
                key={data.id}
                onClick={() => handleRowClick(data)}
            >
                <th>{dataRowNum++}</th>
                <td style={{ textAlign: "left" }}>{data.description}</td>
            </tr>
        );

        return [dataRow];
    }) || [];

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-[114%]">
            <table className={`table table-xs table-fixed
            ${isLoading || !response.data || response.data.length === 0 ? 'h-full' : ''}`}
            >
                <thead style={{
                    position: "sticky",
                    top: -20,
                    backgroundImage: `url(${abstractImage})`,
                    backgroundSize: '50%',
                }}>
                <tr style={{whiteSpace: "pre-line", overflowWrap: "break-word", textAlign: "center"}}>
                    <th width="12%"></th>
                    <th style={{ textAlign: "left" }}>Deskripsi</th>
                </tr>
                </thead>

                <tbody>
                {isLoading && <LoadingTable span={2} />}
                {!isLoading && (rowsToRender)}
                {!isLoading && (!response.data || response.data.length === 0) ? (
                    <tr style={{ textAlign: 'center', borderBottom: 'none', }}>
                        <td colSpan="2" className="text-lg opacity-20 pt-[20px]">
                            Tidak ada data yang ditampilkan.
                        </td>
                    </tr>
                ) : null}
                </tbody>
            </table>
        </div>
    );
}
