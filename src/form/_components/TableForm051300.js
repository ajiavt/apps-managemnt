import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingTable from "../../components/joy-ui/LoadingTable";

export default function TableForm051300({ handleRowClick, selectedRow, isLoading, response }) {

    let rowsNum = 1;
    const rowsHeader = (
        <tr style={{ whiteSpace: 'pre-line', textOverflow: 'ellipsis' }}>
            <th width={'11%'}  align={'center'}>No.</th>
            <th align={'left'}>Jenis Printer</th>
            <th align={'left'}>ID Produk</th>
            <th align={'left'}>ID Grup</th>
        </tr>
    );

    const rowsBody = (response?.data || []).flatMap((data, index) => (
        <React.Fragment key={data.grpId+data.prodId+data.typePrinter}>
            <tr key={data.grpId+data.prodId+data.typePrinter}
                onClick={() => handleRowClick(data)}
                className={`hover:bg-primary/10 ${data.grpId+data.prodId+data.typePrinter === selectedRow ? 'bg-primary/10' : ''}`}
                style={{ cursor: "pointer" }}
            >
                <td align={'center'}>{rowsNum++}</td>
                <td align={'left'}>{data?.typePrinter}</td>
                <td align={'left'}>{data?.prodId}</td>
                <td align={'left'}>{data?.grpId}</td>
            </tr>
        </React.Fragment>
    ));

    return (
        <div className="relative rounded-xl shadow-inner shadow-base-300 -m-[20px] h-[137.5%]">
            <table className={`table table-xs table-fixed
            ${isLoading || !response.data || response.data.length === 0 ? 'h-full' : ''}`}
            >
                <thead style={{
                    top: -20,
                    zIndex: 50,
                    position: 'sticky',
                    backgroundImage: `url(${abstractImage})`,
                    backgroundSize: '50%',
                }}>
                {rowsHeader}
                </thead>

                <tbody>
                {isLoading && <LoadingTable span={4} />}
                {!isLoading && (rowsBody)}
                {!isLoading && (!response.data || response.data.length === 0) ? (
                    <tr style={{ textAlign: 'center', borderBottom: 'none', }}>
                        <td colSpan="4" className="text-lg opacity-20 pt-[20px]">
                            Tidak ada data yang ditampilkan.
                        </td>
                    </tr>
                ) : null}
                </tbody>
            </table>
        </div>
    );
}
