import * as React from 'react';
import abstractImage from '../../assets/images/abstract2.jpg';
import LoadingTable from "../../components/joy-ui/LoadingTable";

export default function TableForm051200({ handleRowClick, selectedRow, isLoading, response }) {

    let rowsNum = 1;
    const rowsHeader = (
        <tr style={{ whiteSpace: 'pre-line', textOverflow: 'ellipsis' }}>
            <th width={'7%'}  align={'center'}>No.</th>
            <th width={'11%'} align={'center'}>Status</th>
            <th width={'11%'} align={'center'}>ID Grup</th>
            <th width={'11%'} align={'center'}>ID Produk</th>
            <th width={'17%'} align={'center'}>Tipe Printer</th>
            <th>Deskripsi</th>
        </tr>
    );

    const rowsBody = (response?.data || []).flatMap((data, index) => (
        <React.Fragment key={data.grpId+data.prodId+data.typePrinter}>
            <tr key={data.grpId+data.prodId+data.typePrinter}
                onClick={() => handleRowClick(data)}
                className={`hover:bg-primary/10 ${data.grpId+data.prodId+data.typePrinter === selectedRow ? 'bg-primary/10' : ''}`}
                style={{ cursor: "pointer" }}
            >
                <td align={'center'}>{rowsNum++}</td>
                <td align={'center'}>{data?.status ? "Aktif" : "Tidak Aktif"}</td>
                <td align={'center'}>{data?.grpId}</td>
                <td align={'center'}>{data?.prodId}</td>
                <td align={'center'}>{data?.typePrinter}</td>
                <td>{data?.description}</td>
            </tr>
        </React.Fragment>
    ));

    return (
        <div className={`relative rounded-xl -m-[20px] ${isLoading ? 'h-full' : ''}`}>
            <table className={`table table-xs table-fixed ${isLoading ? 'h-full' : ''}`}>
                <thead style={{top: -20, zIndex: 50, position: 'sticky', backgroundImage: `url(${abstractImage})`, backgroundSize: '50%',}}>
                {rowsHeader}
                </thead>

                <tbody>
                {isLoading && <LoadingTable span={6} />}
                {!isLoading && (rowsBody)}
                </tbody>
            </table>
        </div>
    );
}
