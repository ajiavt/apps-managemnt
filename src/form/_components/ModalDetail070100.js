import * as React from 'react';
import Grid from "../../components/Grid";
import Textbox from "../../components/joy-ui/Textbox";
import Textarea from "../../components/joy-ui/Textarea";
import Box from "@mui/joy/Box";

export default function ModalDetail070100({data, grpNm}) {

    function formatDate(dateString) {
        const date = new Date(dateString);
        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const year = date.getFullYear();
        return `${day}-${month}-${year}`;
    }

    return (
        <Grid smallGap>
            <Textbox className="col-span-3" label={"No. KTP"} value={data?.cifReq?.idNbr} readOnly={true}/>
            <Textbox className="col-span-5" label={"Nama Lengkap"} value={data?.cifReq?.fullName} readOnly={true}/>
            <Textbox className="col-span-4" label={"Tempat/Tgl Lahir"} value={(data?.cifReq?.brtPlace?.trim()) + `/` + formatDate(data?.cifReq?.brtDt)} readOnly={true}/>
            <Textbox className="col-span-3" label={"No. Ponsel"} value={data?.cifReq?.phoneNumber} readOnly={true}/>
            <Textbox className="col-span-5" label={"Nama Pekerjaan"} value={data?.cifReq?.jobNm} readOnly={true}/>
            <Textbox className="col-span-4" label={"Status Nasabah"} value={data?.cifReq?.flgShare || "Nasabah"} readOnly={true}/>
            <Textarea className="col-span-6" label={"Alamat"} value={data?.cifReq?.addr?.trim()} readOnly={true} maxRows={2}/>
            <Textarea className="col-span-6" label={"Keterangan Transaksi"} value={data?.txMsg} readOnly={true} maxRows={2}/>
            <Textbox className="col-span-6" label={"Jenis Transaksi"} value={grpNm} readOnly={true}/>
            <Textbox className="col-span-6" label={"Nama Penerima"} value={data?.accDst?.nickNm} readOnly={true}/>
            <Textbox className="col-span-6" label={"Nominal Transaksi"} value={`Rp. ` + (data?.amount)} readOnly={true}/>
            <Textbox className="col-span-6" label={"Rekening Tujuan"} value={data?.accDst?.accNbr} readOnly={true}/>
            <span className="col-span-3"/>
            <div className="col-span-6">
                <span className="text-[14px] font-Poppins w-full text-center">
                    Bukti Tanda Tangan
                </span>

                <Box sx={{
                    border: '0.8px solid var(--joy-palette-neutral-outlinedBorder, var(--joy-palette-neutral-300, #CDD7E1))',
                    borderRadius: '6px',
                    boxShadow: 'var(--joy-shadowRing, 0 0 #000),0px 1px 2px 0px rgba(var(--joy-shadowChannel, 21 21 21) / var(--joy-shadowOpacity, 0.08))',

                }}>
                    <img src={`data:image/jpeg;base64,${data.signature}`} alt="signature" />
                </Box>
            </div>
        </Grid>
    );
}
