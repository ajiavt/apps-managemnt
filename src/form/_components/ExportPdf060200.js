// ExportToPdfRekap.js
import jsPDF from 'jspdf';

const numberFormatter = new Intl.NumberFormat('id-ID', {
    // style: 'currency',
    // currency: 'IDR',
    style: 'decimal',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
});

const exportToPDF = (response) => {
    const unit = 'pt';
    const size = 'A4';
    const orientation = 'landscape';

    const doc = new jsPDF(orientation, unit, size);

    // Fungsi untuk mengukur lebar teks
    doc.getTextWidth = function (text) {
        const fontSize = this.internal.getFontSize();
        const textWidth = this.getStringUnitWidth(text) * fontSize / this.internal.scaleFactor;
        return textWidth;
    };

    // Fungsi untuk header di tengah
    const header = function (data) {
        const headerText = "Table Export";
        const fontSize = 18;
        const textWidth = doc.getTextWidth(headerText);
        const pageWidth = doc.internal.pageSize.width;
        const x = (pageWidth - textWidth) / 2;

        doc.setFontSize(fontSize);
        doc.setTextColor(40);
        doc.setFont('normal');
        doc.text(headerText, x, 30);
    };

    // Fungsi untuk footer
    const footer = function(data) {
        const pageCount = doc.internal.getNumberOfPages();
        const str = "Page " + data.pageNumber + " of " + pageCount;
        const fontSize = 12;
        const pageWidth = doc.internal.pageSize.width;
        const x = pageWidth - doc.getTextWidth(str) - 20;

        const totalDebet = response?.data?.details?.reduce((acc, row) => {
            const amount = parseFloat(row.txAmount) || 0;
            return row.dbCr === 0 ? acc + amount : acc;
        }, 0);

        const totalKredit = response?.data?.details?.reduce((acc, row) => {
            const amount = parseFloat(row.txAmount) || 0;
            return row.dbCr === 1 ? acc + amount : acc;
        }, 0);

        const totalMutasiValTransaksi = response?.data?.details?.reduce((acc, row) => {
            const amount = parseFloat(row.txAmount) || 0;
            return acc + amount;
        }, 0);

        const totalText = `Total Debet: ${numberFormatter.format(totalDebet)}, Total Kredit: ${numberFormatter.format(totalKredit)}, Total Mutasi: ${numberFormatter.format(totalMutasiValTransaksi)}`;

        doc.setFontSize(fontSize);
        doc.text(totalText, 20, doc.internal.pageSize.height - 10);

        doc.setFontSize(fontSize);
        doc.text(str, x, doc.internal.pageSize.height - 10);
    };

    const columns = [
        { header: 'No.', dataKey: 'no' },
        { header: 'Id Transaksi', dataKey: 'idTransaksi' },
        { header: 'No Rekening', dataKey: 'noRekening' },
        { header: 'Nama Rekening', dataKey: 'namaRekening' },
        { header: 'Val Tx', dataKey: 'valTx' },
        { header: 'Keterangan', dataKey: 'keterangan' },
        { header: 'DB/CR', dataKey: 'dbCr' },
        { header: 'Debet', dataKey: 'debet' },
        { header: 'Kredit', dataKey: 'kredit' },
        { header: 'Mutasi Val Transaksi', dataKey: 'mutasiValTransaksi' },
        { header: 'User Spv', dataKey: 'userSpv' },
    ];

    const rows = response?.data?.details?.map((row, index) => ({
        no: index + 1,
        idTransaksi: row.txId,
        noRekening: row.accNbr,
        namaRekening: row.fullName,
        valTx: row.txCcy,
        keterangan: row.txMsg,
        dbCr: row.dbCr,
        debet: row.dbCr === 0 ? numberFormatter.format(row.txAmount) : '0',
        kredit: row.dbCr === 1 ? numberFormatter.format(row.txAmount) : '0',
        mutasiValTransaksi: numberFormatter.format(row.txAmount),
        userSpv: row.userAuth,
    })) || [];

    // Panggil fungsi header dan footer pada setiap halaman
    doc.autoTable({
        head: [columns.map(col => col.header)],
        body: rows.map(row => columns.map(col => row[col.dataKey])),
        startY: 60,
        didDrawPage: function (data) {
            header(data);
            footer(data);
        },
        margin: { top: 30, bottom: 50, left: 20, right: 20 },
        theme: 'grid',
    });

    const currentDate = new Date();
    const formattedDate = currentDate.toLocaleDateString('en-GB', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
    }).replace(/\//g, '-');

    doc.save(`Report Rincian Aktivitas_${formattedDate}.pdf`);
};

export default exportToPDF;
