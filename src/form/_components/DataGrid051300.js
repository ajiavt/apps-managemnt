// DataGrid051300.js
import React, {useCallback, useState} from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import {
    GridRowModes,
    DataGrid,
    GridToolbarContainer,
    GridActionsCellItem,
    GridRowEditStopReasons,
} from '@mui/x-data-grid';

const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
let counter = 0;

export default function DataGrid051300
     ({
         formMode,
         rows,
         setRows,
     }) {

    const [rowModesModel, setRowModesModel] = useState({});

    const handleRowEditStop = (params, event) => {
        if (params.reason === GridRowEditStopReasons.rowFocusOut) {
            event.defaultMuiPrevented = true;
        }
    };

    const handleClick = () => {
        let id = "idkey-" + counter++;
        setRows((oldRows) => [...oldRows, { id, field: '', align: '', width: '', seq: counter, isNew: true }]);
        setRowModesModel((oldModel) => ({
            ...oldModel,
            [id]: { mode: GridRowModes.Edit, fieldToFocus: 'field' },
        }));
    };

    const handleEditClick = (id) => () => {
        setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
    };

    const handleSaveClick = (id) => () => {
        setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
    };

    const handleDeleteClick = (id) => () => {
        setRows(rows.filter((row) => row.id !== id));
    };

    const handleCancelClick = (id) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View, ignoreModifications: true },
        });

        const editedRow = rows.find((row) => row.id === id);
        if (editedRow.isNew) {
            setRows(rows.filter((row) => row.id !== id));
        }
    };

    const processRowUpdate = (newRow) => {
        const updatedRow = { ...newRow, isNew: false };
        setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
        return updatedRow;
    };

    const handleRowModesModelChange = (newRowModesModel) => {
        setRowModesModel(newRowModesModel);
    };

    const columns = [
        {
            field: 'seq',
            headerName: 'Urutan',
            width: 75,
            editable: false,
            align: 'center',
            headerAlign: 'center',
        },
        {
            field: 'field',
            headerName: 'Name',
            width: 150,
            editable: false,
        },
        {
            field: 'width',
            headerName: 'Width (mm)',
            type: 'number',
            width: 125,
            align: 'center',
            headerAlign: 'center',
            editable: formMode === FM.VIEW ? false : true,
        },
        {
            field: 'align',
            headerName: 'Align',
            width: 90,
            editable: formMode === FM.VIEW ? false : true,
            type: 'singleSelect',
            align: 'center',
            headerAlign: 'center',
            valueOptions: ['left', 'center', 'right'],
        },
        {
            field: 'status',
            headerName: 'Show',
            width: 90,
            editable: formMode === FM.VIEW ? false : true,
            type: 'singleSelect',
            valueOptions: [true, false],
            align: 'center',
            headerAlign: 'center',
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Aksi',
            cellClassName: 'actions',
            getActions: ({ id }) => {
                const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            icon={<SaveIcon />}
                            label="Save"
                            sx={{
                                color: 'primary.main',
                            }}
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            icon={<CancelIcon />}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ];
                }

                return [
                    <GridActionsCellItem
                        disabled={formMode === FM.VIEW}
                        icon={<EditIcon />}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    // <GridActionsCellItem
                    //     disabled={formMode === FM.VIEW}
                    //     icon={<DeleteIcon />}
                    //     label="Delete"
                    //     onClick={handleDeleteClick(id)}
                    //     color="inherit"
                    // />,
                ];
            },
        },
    ];

    const CustomToolbar = useCallback(() => {
        return (
            <GridToolbarContainer>
                <Button color="primary" startIcon={<AddIcon />} onClick={handleClick} disabled={formMode === FM.VIEW}>
                    Add Column
                </Button>
            </GridToolbarContainer>
        );
    });

    return (
        <Box
            sx={{
                height: 230,
                width: '100%',
                '& .actions': {
                    color: 'text.secondary',
                },
                '& .textPrimary': {
                    color: 'text.primary',
                },
            }}
        >
            <DataGrid
                density="compact"
                rows={rows}
                columns={columns}
                editMode="row"
                rowModesModel={rowModesModel}
                onRowModesModelChange={handleRowModesModelChange}
                onRowEditStop={handleRowEditStop}
                processRowUpdate={processRowUpdate}
                // slots={{
                //     toolbar: CustomToolbar,
                // }}
                slotProps={{
                    toolbar: { setRows, setRowModesModel, formMode: formMode },
                }}
                initialState={{
                    pagination: {
                        paginationModel: {
                            pageSize: 8,
                        },
                    },
                }}
                sx={{
                    '&.MuiDataGrid-root': {
                        fontFamily: 'Poppins, sans-serif',
                    },
                }}
                disableRowSelectionOnClick
                disableColumnMenu
                disableColumnSelector
                disableColumnFilter
            />
        </Box>
    );
}
