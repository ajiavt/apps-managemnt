import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm030100 from "./_components/TableForm030100";
import Textbox from "../components/joy-ui/Textbox";
import Intbox from "../components/joy-ui/Intbox";
import { menuService } from "./_repository/menuService";
import RadioButtonGroup from "../components/joy-ui/RadioButtonGroup";
import RadioButton from "../components/joy-ui/RadioButton";
import Selectbox from "../components/joy-ui/Selectbox";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";

// FM = Form Modes
const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {menuParent: null, menuId: null, name: null, seq: null, pathFE: null, menuType: null,};

export default function Form030100(){

    const [errMsg, setErrMsg] = useState({});
    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [inputValue, setInputValue] = useState({});

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    const fetchData = useCallback(async () => {
        try{
            setLoadingState(true);

            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await menuService("GET_ALL");

            if (status === 0)
                setErrorMessage(response);
            else
                setResponse(response);

        } catch (error){
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData, refreshTable]);

    useEffect(() => {
        if (errorMessage) {
            dispatch(showNotification({ message: errorMessage, status: 0 }));
        }
    }, [errorMessage]);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Pemeliharaan Menu"}))
    }, []);

    useEffect(() => {
    }, [selectedDataState]);

    {/** START - EVENTS ON BUTTON*/}
    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.menuId);
        setFormMode(FM.VIEW);
    }, []);

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setSelectedDataState({...selectedDataState, menuType: 0});
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.menuId !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.menuId !== null) {
            setLoadingState(true);
            const { response, status } = await menuService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrMsg(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        // Cek jika tidak ada data yang berubah
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode === FM.EDIT && JSON.stringify(selectedDataState.menuId) === JSON.stringify(selectedDataState.menuParent)) {
            dispatch(showNotification({message : "Tidak boleh menjadi parent untuk diri sendiri!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const data = {
                menuChild: [], // You might need to adjust this depending on your requirements
                menuId: selectedDataState.menuId,
                menuParent: selectedDataState.menuParent,
                name: selectedDataState.name,
                pathFE: selectedDataState.pathFE,
                seq: Number(selectedDataState.seq),
                menuType: selectedDataState.menuType,
            };
            setLoadingState(true);
            const { response, status } = await menuService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrMsg(error);
            }

            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);
    {/** END - EVENTS ON BUTTON*/}

    const handleError = (response) => {
        // 98 = Error pada inputan
        if(response?.data?.code === "98") {
            return response?.data?.error;
        }

        else if (response?.data?.code === "99" || response?.data?.code === "93" || response?.data?.code === "94") {
            dispatch(showNotification({message : response?.data?.message, status : 0}))
        }

        else {
            return response;
        }
    }

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrMsg({});
        setInputValue('');
    }, []);

    const listParents = {
        "Parents": response?.data?.map(item => ({menuId: item.menuId, name: item.name}))
    };

    const listChilds = response?.data?.reduce((acc, curr) => {
        const childMenuIds = curr.menuChild
            ? curr.menuChild.map(child => ({menuId: child.menuId, name: child.name}))
            : [];
        return {...acc, [curr.name]: childMenuIds};
    }, {});

    return(
        <>
            <div className="grid lg:grid-cols-12 lg:gap-x-6">
                <TitleCard className="max-h-[85vh] overflow-x-auto lg:col-span-8">
                    <TableForm030100
                        handleRowClick={handleRowClick}
                        selectedRow={selectedRow}
                        refreshTable={refreshTable}
                        isLoading={loadingState}
                        response={response}
                    />
                </TitleCard>


                <div className="lg:col-span-4 mt-6">
                    <ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick} loadingState={loadingState}/>
                    <TitleCard>
                        <RadioButtonGroup disabled={formMode === FM.VIEW} error={errMsg?.menuType} label="Tipe Menu" mandatory>
                            <div className="grid lg:grid-cols-2 w-full">
                                <RadioButton
                                    checked={selectedDataState?.menuType === 0}
                                    disabled={formMode === FM.VIEW}
                                    value="0"
                                    label="Parent"
                                    onClick={() => {
                                        setSelectedDataState({...selectedDataState, menuType: 0});
                                    }}
                                />

                                <RadioButton
                                    checked={selectedDataState?.menuType === 1}
                                    disabled={formMode === FM.VIEW}
                                    value="1"
                                    label="Child"
                                    onClick={() => {
                                        setSelectedDataState({...selectedDataState, menuType: 1});
                                    }}
                                />

                                <RadioButton
                                    checked={selectedDataState?.menuType === 2}
                                    disabled={formMode === FM.VIEW}
                                    value="2"
                                    label="Sub-child"
                                    className="col-span-2"
                                    onClick={() => {
                                        setSelectedDataState({...selectedDataState, menuType: 2});
                                    }}
                                />
                            </div>
                        </RadioButtonGroup>

                        <Textbox
                            hidden={false}
                            mandatory
                            maxLength={50}
                            disabled={formMode === FM.VIEW}
                            id="txtMenuName"
                            label="Nama Menu"
                            name="name"
                            value={selectedDataState?.name}
                            error={errMsg?.name}
                            onChange={(e) => setSelectedDataState({...selectedDataState, name:e.target.value})}/>

                        <Selectbox
                            inputValue={inputValue}
                            setInputValue={setInputValue}
                            hidden={selectedDataState?.menuType === 0}
                            mandatory
                            key={renderKey}
                            disabled={formMode === FM.VIEW || selectedDataState?.menuType === 0}
                            autoFocus={formMode !== FM.VIEW}
                            id="txtMenuParent"
                            idKey="menuId"
                            nameKey="name"
                            label="Menu Parent"
                            value={selectedDataState?.menuParent}
                            datas={selectedDataState?.menuType === 1 ? listParents : (selectedDataState?.menuType === 2 ? listChilds : null)}
                            error={errMsg?.menuParent}
                            onChange={(child) => setSelectedDataState({...selectedDataState, menuParent:child})}
                        />

                        <Textbox
                            hidden={selectedDataState?.menuType !== null}
                            tooltip="Menu Id otomatis generate dari sistem."
                            maxLength={6}
                            disabled={true}
                            id="txtMenuID"
                            label="Menu Id"
                            value={selectedDataState?.menuId}
                            error={errMsg?.menuId}
                            onChange={(e) => setSelectedDataState({...selectedDataState, menuId:e.target.value})}/>

                        <Textbox
                            hidden={selectedDataState?.menuType !== null}
                            tooltip="Path FE otomatis generate dari sistem."
                            maxLength={17}
                            disabled={true}
                            id="txtPathFE"
                            label="Path FE"
                            value={selectedDataState?.pathFE}
                            error={errMsg?.pathFE}
                            onChange={(e) => setSelectedDataState({...selectedDataState, pathFE:e.target.value})}/>

                        <Intbox
                            hidden={false}
                            mandatory
                            maxLength={2}
                            disabled={formMode === FM.VIEW}
                            id="txtSeq"
                            label="Sequence"
                            name="seq"
                            value={selectedDataState?.seq}
                            error={errMsg?.seq}
                            onChange={(e) => setSelectedDataState({...selectedDataState, seq:e.target.value})}/>

                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode} loadingState={loadingState}/>
                    </TitleCard>
                </div>
            </div>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}