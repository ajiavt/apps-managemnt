import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm020500 from "./_components/TableForm020500";
import Textbox from "../components/joy-ui/Textbox";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import { roleMappingService } from "./_repository/roleMappingService";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import Grid from "../components/Grid";
import {roleService} from "./_repository/roleService";
import Selectbox from "../components/joy-ui/Selectbox";

const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    roleIdCore: null,
    roleId: null,
};

export default function Form020500(){

    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [roleResponse, setRoleResponse] = useState({});
    const [inputValue, setInputValue] = useState({});

    const dispatch = useDispatch();
    const [response, setResponse] = useState({});
    const [errorMessage, setErrorMessage] = useState(null);

    const fetchData = useCallback(async (service, stateSetter, ...params) => {
        setLoadingState(true);
        try {
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchRolesMapping = useCallback(() => {
        fetchData(roleMappingService, setResponse, "GET_ALL");
    }, []);

    const fetchRoles = useCallback(() => {
        fetchData(roleService, setRoleResponse, "GET_ALL");
    }, []);

    useEffect(() => {
        fetchRolesMapping()
    }, [refreshTable]);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Mapping Wewenang Core Banking"}))
        fetchRoles()
    }, []);

    {/** START - EVENTS ON BUTTON*/}
    const handleRowClick = useCallback((data) => {
        setSelectedDataState(data);
        setSelectedRow(data.roleIdCore+data.roleId);
        setFormMode(FM.VIEW);
    }, []);

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);
        setSelectedDataState({...selectedDataState});
    }, []);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.roleIdCore !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.roleIdCore !== null) {
            setLoadingState(true);
            const { response, status } = await roleMappingService("DELETE", selectedDataState);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        // Cek jika tidak ada data yang berubah

        console.log("originalDataState")
        console.log(originalDataState)
        console.log("selectedDataState")
        console.log(selectedDataState)
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);  // Tampilkan modal
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const data = {
                roleIdCore: selectedDataState.roleIdCore,
                roleId: selectedDataState.roleId,
            };
            setLoadingState(true);
            const { response, status } = await roleMappingService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }

            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [formMode, selectedDataState]);
    {/** END - EVENTS ON BUTTON*/}

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.VIEW);
        setErrorMessage( null);
    }, []);

    const listRoles = {
        "Daftar Wewenang": roleResponse?.data
    };

    return(
        <>
            <Grid>
                <TitleCard className="max-h-[85vh] overflow-x-auto col-span-3">
                    <TableForm020500
                        handleRowClick={handleRowClick}
                        selectedRow={selectedRow}
                        refreshTable={refreshTable}
                        isLoading={loadingState}
                        response={response}
                    />
                </TitleCard>

                <div className="col-span-4 mt-6">
                    {/*<ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick} loadingState={loadingState} />*/}
                    <ButtonCRUD add={handleAddClick} del={handleDeleteClick} loadingState={loadingState} />

                    <TitleCard>
                        <Textbox
                            mandatory
                            key={renderKey}
                            autoFocus={formMode !== FM.VIEW}
                            disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                            label="Wewenang Core"
                            value={selectedDataState?.roleIdCore}
                            error={errorMessage?.roleIdCore}
                            onChange={(e) => setSelectedDataState({...selectedDataState, roleIdCore:e.target.value})}/>

                        <Selectbox
                            mandatory
                            disabled={formMode === FM.VIEW}
                            inputValue={inputValue}
                            setInputValue={setInputValue}
                            value={selectedDataState?.roleId}
                            className="mt-[5px]"
                            idKey="id"
                            nameKey="name"
                            label="Wewenang Aplikasi"
                            datas={listRoles}
                            error={errorMessage?.roleId}
                            onChange={(e) => setSelectedDataState({...selectedDataState, roleId:e})}
                        />

                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode} loadingState={loadingState}/>
                    </TitleCard>
                </div>
            </Grid>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
}