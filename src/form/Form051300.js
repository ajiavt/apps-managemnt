import React, { useEffect, useState, useCallback, useMemo } from 'react';
import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm051300 from "./_components/TableForm051300";
import Textbox from "../components/joy-ui/Textbox";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import { printerTxService } from "./_repository/printerTxService";
import { parmService } from "./_repository/parmService";
import { productService } from "./_repository/productService";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import Grid from "../components/Grid";
import RadioButtonGroup from "../components/joy-ui/RadioButtonGroup";
import CheckboxButton from "../components/joy-ui/CheckboxButton";
import Intbox from "../components/joy-ui/Intbox";
import Box from "@mui/joy/Box";
import Selectbox from "../components/joy-ui/Selectbox";
import NorthOutlinedIcon from '@mui/icons-material/NorthOutlined';
import WestOutlinedIcon from '@mui/icons-material/WestOutlined';
import FormatSizeOutlinedIcon from '@mui/icons-material/FormatSizeOutlined';
import HeightOutlinedIcon from '@mui/icons-material/HeightOutlined';
import TableRowsOutlinedIcon from '@mui/icons-material/TableRowsOutlined';
import InsertPageBreakOutlinedIcon from '@mui/icons-material/InsertPageBreakOutlined';
import LineWeightOutlinedIcon from '@mui/icons-material/LineWeightOutlined';
import DataGrid051300 from "./_components/DataGrid051300";

const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    description: "",
    details: [],
    grpId: 0,
    fontSize: 0,
    lineHeight: 0,
    marginLeft: 0,
    marginTop: 0,
    pageBreak: 0,
    pageBreakHeight: 0,
    prodId: "",
    rowCount: 0,
    status: false,
    typePrinter: "",
};

const Form051300 = () => {

    const [refreshTable, setRefreshTable] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [originalPreviewState, setOriginalPreviewState] = useState(emptyData);
    const [printerTxResponse, setPrinterTxResponse] = useState({});
    const [typePrinter, setTypePrinter] = useState({});
    const [grupEform, setGrupEform] = useState({});
    const [productEform, setProductEform] = useState({});
    const [parmButab, setParmButab] = useState({});
    const [productName, setProductName] = useState({});
    const [inputValue, setInputValue] = useState({});
    const [inputValue2, setInputValue2] = useState({});
    const [inputValue3, setInputValue3] = useState({});
    const [rows, setRows] = useState([]);
    const [errorMessage, setErrorMessage] = useState(null);

    const dispatch = useDispatch();

    const fetchData = useCallback(async (service, stateSetter, isDetail=true, ...params) => {
        try {
            setLoadingState(true);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { response, status } = await service(...params);
            if (status === 0) setErrorMessage(handleError(response));
            else stateSetter(isDetail ? response.data : response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const fetchPrinterTx = useCallback(() => {
        fetchData(printerTxService, setPrinterTxResponse, false, "GET_ALL");
    }, []);

    const fetchPrinterTxDetail = useCallback((data) => {
        fetchData(printerTxService, setSelectedDataState, true, "GET_1", data);
    }, []);

    const fetchJenisPrinter = useCallback(() => {
        fetchData(parmService, setTypePrinter, false, "GET_1", "TYPEPRINTER");
    }, []);

    const fetchGrupEform = useCallback(() => {
        fetchData(parmService, setGrupEform, false, "GET_1", "EFORMGRPAPPRV");
    }, []);

    const fetchProductEform = useCallback(() => {
        fetchData(parmService, setProductEform, false, "GET_1", "EFORMPRD");
    }, []);

    const fetchProductName = useCallback((prodId) => {
        fetchData(productService, setProductName, false, "GET_1", prodId);
    }, []);

    const fetchParmButab = useCallback(() => {
        fetchData(parmService, setParmButab, false, "GET_1", "DTLBUTAB");
    }, []);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Konfigurasi Butab Printer"}))
        fetchPrinterTx();
        fetchJenisPrinter();
        fetchGrupEform();
        fetchProductEform();
        fetchParmButab();
    }, []);

    useEffect(() => {
        fetchPrinterTx();
    }, [refreshTable]);

    const handleRowClick = useCallback((data) => {
        setRows([]);
        setSelectedRow(data.grpId+data.prodId+data.typePrinter);
        setFormMode(FM.VIEW);
        fetchPrinterTxDetail(data)
    }, []);

    useEffect(() => {
        if(rows.length === 0) {
            const sortedDetails = selectedDataState?.details?.sort((a, b) => a.seq - b.seq);
            const finalDetails = sortedDetails.map(item => ({
                ...item,
                id: item.field
            }));

            setRows(finalDetails);
        }
    }, [selectedDataState]);

    useEffect(() => {
        if(rows.length > 0) {
            setSelectedDataState(prevState => ({
                ...prevState,
                details: rows.map(item => ({
                    field: item.field,
                    width: item.width,
                    align: item.align,
                    seq: item.seq,
                    status: item.status,
                }))
            }));
        }
    }, [rows]);

    const handleAddClick = useCallback(() => {
        setSelectedDataState(emptyData);
        setSelectedRow(null);
        setFormMode(FM.ADD);
        setRenderKey(prevKey => prevKey + 1);

        if (parmButab?.data?.length > 0) {
            const initialRows = [];
            const details = [];

            for (let index = 0; index < parmButab?.data?.length; index++) {
                const field = parmButab?.data[index];

                initialRows.push({
                    id: field,
                    field: field,
                    align: "center",
                    width: 50,
                    seq: index + 1,
                    status: true,
                });

                details.push({
                    id: field,
                    field: field,
                    width: 50,
                    align: "center",
                    seq: index + 1,
                    status: true,
                });
            }

            setRows(initialRows);

            setSelectedDataState(prevState => ({
                ...prevState,
                details,
            }));
        }
    }, [parmButab]);

    const handleEditClick = useCallback(() => {
        if(selectedRow) {
            setFormMode(FM.EDIT);
            setRenderKey(prevKey => prevKey + 1);
            setOriginalDataState({...selectedDataState});
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState, formMode]);

    const handleDeleteClick = useCallback(() => {
        if (selectedDataState.grpId !== null) {
            setShowModalDelete(true);  // Tampilkan modal
        } else {
            dispatch(showNotification({message : "Tidak ada data yang dipilih!", status : 2}));
        }
    }, [selectedDataState]);

    const handleConfirmDelete = useCallback(async () => {
        if (selectedDataState.grpId !== null) {
            setLoadingState(true);
            const { response, status } = await printerTxService("DELETE", selectedDataState);
            if (status === 0) {
                setErrorMessage(handleError(response));
            }
            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : response?.message, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);  // Toggle the state to trigger re-fetching of data in the table
                }
                resetFormData();
            }
            setLoadingState(false);
            setShowModalDelete(false);
        }
    }, [selectedDataState]);

    const handleSaveClick = useCallback(() => {
        if (formMode === FM.EDIT && JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({message : "Tidak ada data yang berubah!", status : 2}));
            return;
        }

        if (formMode !== FM.VIEW) {
            setShowModalSave(true);
        }
    }, [formMode, originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        if (formMode !== FM.VIEW) {
            const updatedRows = rows?.map(({ id, isNew, ...rest }) => rest);

            const data = {
                description: selectedDataState.description,
                fontSize: parseInt(selectedDataState.fontSize),
                lineHeight: parseInt(selectedDataState.lineHeight),
                marginLeft: parseInt(selectedDataState.marginLeft),
                marginTop: parseInt(selectedDataState.marginTop),
                pageBreak: parseInt(selectedDataState.pageBreak),
                pageBreakHeight: parseInt(selectedDataState.pageBreakHeight),
                rowCount: parseInt(selectedDataState.rowCount),
                status: selectedDataState.status,
                typePrinter: selectedDataState.typePrinter,
                grpId: parseInt(selectedDataState.grpId),
                prodId: selectedDataState.prodId,
                details: updatedRows,
            };

            setLoadingState(true);
            const { response, status } = await printerTxService((formMode === FM.EDIT ? "UPDATE" : "INSERT"), data);
            if (status === 0) {
                let error = handleError(response);
                setErrorMessage(error);
            }

            else{
                if (response.code === '00') {
                    dispatch(showNotification({message : `Data berhasil ${formMode === FM.EDIT ? "diupdate" : "ditambahkan"}!`, status : 1}));
                    setRefreshTable(refreshTable => !refreshTable);
                }
                resetFormData();
            }
            setShowModalSave(false);
            setLoadingState(false);
        }
    }, [rows, formMode, selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setSelectedRow(null);
        setErrorMessage(null);
        setFormMode(FM.VIEW);
        setInputValue('');
        setInputValue2('');
        setInputValue3('');
        setRows([]);
    }, []);

    const listJenisPrinter = {
        "Jenis Printer": typePrinter?.data?.map((item, index) => ({ id: item, name: '' }))
    };

    const listGrupEform = {
        "Grup": grupEform?.data?.map((item, index) => ({ id: item.id, name: item.desc }))
    };

    useEffect(() => {
        if (Object.keys(productEform).length !== 0) {
            fetchProductName(productEform?.data?.prdSvgId);
        }
    }, [productEform]);

    const [listProdukEform, setListProdukEform] = useState({
        "Produk": [
            {
                "id": productEform?.data?.prdSvgId,
                "name": "",
            }
        ]
    });

    useEffect(() => {
        setListProdukEform({
            "Produk": [
                {
                    "id": productEform?.data?.prdSvgId,
                    "name": productName?.data?.prodNm || "",
                }
            ]
        });
    }, [productEform, productName]);

    const updatedRows = rows?.filter(row => row.status).map(({ id, isNew, ...rest }) => rest);
    const totalColumnWidth = updatedRows.reduce((total, row) => total + row.width, 0);
    const boxWidth = totalColumnWidth; // Tambahkan margin atau padding sesuai kebutuhan

    return(
        <>
            <Grid>
                <Grid className="col-span-4">
                    <TitleCard span={12} className="h-[144px]">
                        <TableForm051300
                            handleRowClick={handleRowClick}
                            selectedRow={selectedRow}
                            refreshTable={refreshTable}
                            isLoading={loadingState}
                            response={printerTxResponse}
                        />
                    </TitleCard>

                    <TitleCard className="col-span-12">
                        <Grid smallGap>
                            <Intbox
                                startWith={<NorthOutlinedIcon/>}
                                endWith={'mm'}
                                maxLength={3}
                                span={6}
                                value={selectedDataState?.marginTop}
                                label="Margin Top"
                                error={errorMessage?.marginTop}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, marginTop: parseInt(e.target.value)})}
                                autoFocus={formMode !== FM.VIEW}
                            />

                            <Intbox
                                startWith={<FormatSizeOutlinedIcon/>}
                                endWith={'pt'}
                                maxLength={2}
                                span={6}
                                value={selectedDataState?.fontSize}
                                label="Font Size"
                                error={errorMessage?.fontSize}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, fontSize: parseInt(e.target.value)})}
                                autoFocus={formMode !== FM.VIEW}
                            />

                            <Intbox
                                startWith={<WestOutlinedIcon/>}
                                endWith={'mm'}
                                maxLength={3}
                                span={6}
                                value={selectedDataState?.marginLeft}
                                label="Margin Left"
                                error={errorMessage?.marginLeft}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, marginLeft: parseInt(e.target.value)})}
                                autoFocus={formMode !== FM.VIEW}
                            />

                            <Intbox
                                startWith={<InsertPageBreakOutlinedIcon/>}
                                endWith={'baris'}
                                maxLength={3}
                                span={6}
                                value={selectedDataState?.pageBreak}
                                label="Page Break"
                                error={errorMessage?.pageBreak}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, pageBreak: parseInt(e.target.value)})}
                                autoFocus={formMode !== FM.VIEW}
                            />

                            <Intbox
                                startWith={<LineWeightOutlinedIcon/>}
                                endWith={'mm'}
                                maxLength={7}
                                span={6}
                                value={selectedDataState?.lineHeight}
                                label="Line Height"
                                error={errorMessage?.lineHeight}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, lineHeight: parseInt(e.target.value)})}
                                autoFocus={formMode !== FM.VIEW}
                            />

                            <Intbox
                                startWith={<HeightOutlinedIcon/>}
                                endWith={'mm'}
                                maxLength={7}
                                span={6}
                                value={selectedDataState?.pageBreakHeight}
                                label="Page Break Height"
                                error={errorMessage?.pageBreakHeight}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, pageBreakHeight: parseInt(e.target.value)})}
                                autoFocus={formMode !== FM.VIEW}
                            />

                            <span className="col-span-3"/>

                            <Intbox
                                startWith={<TableRowsOutlinedIcon/>}
                                endWith="baris"
                                maxLength={3}
                                span={6}
                                value={selectedDataState?.rowCount}
                                label="Jumlah Baris"
                                error={errorMessage?.rowCount}
                                disabled={formMode === FM.VIEW}
                                mandatory
                                onChange={(e) => setSelectedDataState({...selectedDataState, rowCount: parseInt(e.target.value)})}
                                autoFocus={formMode !== FM.VIEW}
                            />
                        </Grid>
                    </TitleCard>

                    <div className="col-span-12 mt-[10px]">
                        <ButtonResetSave reset={resetFormData} save={handleSaveClick} formMode={formMode} loadingState={loadingState}/>
                    </div>
                </Grid>

                <Grid className="col-span-7 mt-6">
                    <div className="col-span-12">
                        <ButtonCRUD add={handleAddClick} edit={handleEditClick} del={handleDeleteClick} loadingState={loadingState}/>
                    </div>
                    <TitleCard className={'col-span-12'}>
                        <Grid smallGap>
                            <RadioButtonGroup
                                height="32px"
                                span={3}
                                label="Status"
                                rgClassName={'-mt-[3.5px]'}
                                mandatory
                                disabled={formMode === FM.VIEW}
                            >
                                <CheckboxButton
                                    padding="0 5px 0 5px"
                                    checked={selectedDataState?.status || false}
                                    label="Aktif"
                                    disabled={formMode === FM.VIEW}
                                    onClick={() => {
                                        setSelectedDataState({...selectedDataState, status: !selectedDataState?.status});
                                    }}
                                />
                            </RadioButtonGroup>

                            <Selectbox
                                inputValue={inputValue}
                                setInputValue={setInputValue}
                                span={3}
                                mandatory
                                disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                                label="Jenis Printer"
                                idKey="id"
                                nameKey=""
                                value={selectedDataState?.typePrinter}
                                datas={listJenisPrinter}
                                error={errorMessage?.typePrinter}
                                onChange={(child) => setSelectedDataState({...selectedDataState, typePrinter:child})}
                            />

                            <Selectbox
                                inputValue={inputValue3}
                                setInputValue={setInputValue3}
                                span={6}
                                mandatory
                                disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                                label="ID Produk"
                                idKey="id"
                                nameKey="name"
                                value={selectedDataState?.prodId}
                                datas={listProdukEform}
                                error={errorMessage?.prodId}
                                onChange={(child) => setSelectedDataState({...selectedDataState, prodId:child})}
                            />

                            <Selectbox
                                inputValue={inputValue2}
                                setInputValue={setInputValue2}
                                span={6}
                                mandatory
                                disabled={formMode === FM.VIEW || formMode === FM.EDIT}
                                label="ID Grup"
                                idKey="id"
                                nameKey="name"
                                value={selectedDataState?.grpId}
                                datas={listGrupEform}
                                error={errorMessage?.grpId}
                                onChange={(child) => setSelectedDataState({...selectedDataState, grpId:child})}
                            />

                            <Textbox
                                key={renderKey}
                                autoFocus={formMode !== FM.VIEW}
                                maxLength={50}
                                className={"col-span-6"}
                                value={selectedDataState?.description}
                                label="Deskripsi"
                                error={errorMessage?.description}
                                disabled={formMode === FM.VIEW}
                                onChange={(e) => setSelectedDataState({...selectedDataState, description:e.target.value})}
                            />
                        </Grid>
                    </TitleCard>

                    <div className="col-span-12">
                        <TitleCard>
                            <DataGrid051300
                                rows={rows}
                                setRows={setRows}
                                formMode={formMode}
                            />
                        </TitleCard>
                    </div>
                </Grid>

                <TitleCard className="col-span-11 mb-6"
                           title="Preview"
                           isBordered={false}
                >
                    <Box style={{
                        position: "relative",
                        border: '2px dashed #9ca3af',
                        width: `${boxWidth}mm`, // Menyebabkan lebar Box 100%
                    }}>
                        <table
                            style={{
                                fontFamily: "CourierPrime",
                                fontSize: `${selectedDataState.fontSize}pt`,
                                marginTop: `${selectedDataState.marginTop}mm`,
                                marginLeft: `${selectedDataState.marginLeft}mm`,
                                lineHeight: `${selectedDataState.lineHeight}mm`,
                                position: "relative",
                                border: '2px dashed black',
                                tableLayout: "auto",
                            }}
                        >
                            <thead>
                            <tr>
                                {updatedRows
                                    .filter((row) => row.status) // Filter rows with status true
                                    .map((row) => (
                                        <th key={row.field} align={row.align} style={{ width: `${row.width}mm` }}>
                                            {row.field}
                                        </th>
                                    ))}
                            </tr>
                            </thead>

                            <tbody>
                            {Array.from({ length: selectedDataState.rowCount }, (_, rowIndex) => (
                                <tr
                                    key={rowIndex}
                                    style={{
                                        height: (rowIndex+1) % selectedDataState.pageBreak === 0 ? `${selectedDataState.pageBreakHeight}mm` : 'auto',
                                        background: (rowIndex+1) % selectedDataState.pageBreak === 0 ? `hsl(var(--wa) / 0.1)` : '',
                                    }}
                                >
                                    {updatedRows
                                        .filter((cell) => cell.status) // Filter cells with status true
                                        .map((cell, cellIndex) => (
                                            <td key={cell.field} align={cell.align}>
                                                {rowIndex + 1}-{cellIndex + 1}
                                            </td>
                                        ))}
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </Box>
                </TitleCard>
            </Grid>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />

            <ResponsiveModal
                color={"danger"}
                open={showModalDelete}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menghapus data?"
                yesAction={handleConfirmDelete}
                noAction={() => setShowModalDelete(false)}
            />
        </>
    )
};

export default Form051300;
