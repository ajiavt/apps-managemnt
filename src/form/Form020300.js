import TitleCard from "../components/Cards/TitleCard";
import { useDispatch } from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import { setPageTitle, showNotification } from "../features/common/headerSlice";
import TableForm020300 from "./_components/TableForm020300";
import { menuService } from "./_repository/menuService";
import Selectbox from "../components/joy-ui/Selectbox";
import ResponsiveModal from "../components/joy-ui/Modal";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import { roleService } from "./_repository/roleService";
import Grid from "../components/Grid";

const Form020300 = () => {
    const [errorMessage, setErrorMessage] = useState(null);
    const [refreshTable, setRefreshTable] = useState(false);
    const [loadingState, setLoadingState] = useState(false);
    const [showModalSave, setShowModalSave] = useState(false);
    const [selectedDataState, setSelectedDataState] = useState({});
    const [originalDataState, setOriginalDataState] = useState({});
    const [menuResponse, setMenuResponse] = useState({});
    const [roleResponse, setRoleResponse] = useState({});
    const [checkedState, setCheckedState] = useState({});
    const [originalCheckedState, setOriginalCheckedState] = useState({});
    const [cbHeaderChecked, setCbHeaderChecked] = useState(false);
    const [inputValue, setInputValue] = useState({});
    const [cbhState, setCbhState] = useState({});
    const [lastUserClick, setLastUserClick] = useState('');

    const dispatch = useDispatch();

    const fetchData = async (service, stateSetter) => {
        try {
            setLoadingState(true);
            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await service();
            if (status === 0) setErrorMessage(response);
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    };

    const fetchDataMenu = useCallback(() => {
        fetchData(menuService.bind(null, "GET_ALL"), setMenuResponse);
    }, []);

    const fetchDataRole = useCallback(() => {
        fetchData(roleService.bind(null, "GET_ALL"), setRoleResponse);
    }, []);

    useEffect(() => {
        fetchDataMenu();
        fetchDataRole();
    }, [fetchDataMenu, fetchDataRole, refreshTable]);

    useEffect(() => {
        if (lastUserClick !== '') {
            const finalResultMenuResponse = resultMenuResponse.filter(menu => menu !== undefined);
            setSelectedDataState({ ...selectedDataState, menus: finalResultMenuResponse }); // to Save
        }
    }, [lastUserClick, cbhState, checkedState]);

    useEffect(() => {
        dispatch(setPageTitle({ title: "Generate Menu" }));
    }, []);

    useEffect(() => {
        if (Object.keys(menuResponse).length !== 0 && Array.isArray(menuResponse?.data)) {
            setOriginalCheckedState(convertToMenuStructure(menuResponse.data));
        }

        if (Object.keys(originalDataState).length !== 0) {
            setCbHeaderChecked(JSON.stringify(originalDataState?.menus) === JSON.stringify(menuResponse?.data));
        } else {
            setCbHeaderChecked(false);
        }
    }, [originalDataState, menuResponse]);

    const handleSaveClick = useCallback(() => {
        if (JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({ message: "Tidak ada data yang berubah!", status: 2 }));
            return;
        }
        setShowModalSave(true);
    }, [originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        setLoadingState(true);
        const data = {
            id: selectedDataState.id,
            name: selectedDataState.name,
            menus: selectedDataState.menus,
        };

        const { response, status } = await roleService("GENERATE_MENUS", data);
        if (status === 0) {
            let error = handleError(response);
            setErrorMessage(error);
        } else {
            if (response.code === '00') {
                dispatch(showNotification({ message: response?.message, status: 1 }));
                setRefreshTable(refreshTable => !refreshTable);
            }
            resetFormData();
        }
        setShowModalSave(false);
        setLoadingState(false);
    }, [selectedDataState]);

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const convertToMenuStructure = (menus) => {
        return menus.reduce((result, menu) => {
            const menuObj = {};

            if (menu.menuChild && menu.menuChild.length > 0) {
                menu.menuChild.forEach((child) => {
                    const childObj = {};

                    // Check for subChild
                    if (child.menuChild && child.menuChild.length > 0) {
                        child.menuChild.forEach((subChild) => {
                            childObj[subChild.menuId] = true;
                        });
                    }

                    menuObj[child.menuId] = Object.keys(childObj).length > 0 ? childObj : true;
                });
            }
            result[menu.menuId] = menuObj;
            return result;
        }, {});
    };

    const handleSelectboxWewenang = useCallback(async (child) => {
        setCheckedState({});
        setCbhState({});
        setOriginalDataState({});
        setSelectedDataState({});
        try {
            setLoadingState(true);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { get1, status } = await roleService("GET_1", child);
            if (status === 0) {
                let error = handleError(get1);
                setErrorMessage(error);
                setLoadingState(false);
                return;
            }
            setSelectedDataState({
                ...selectedDataState,
                id: get1?.data?.id,
                name: get1?.data?.name
            });
            if (get1?.data?.menus) {
                const processedMenus = convertToMenuStructure(get1?.data?.menus);
                setCheckedState(processedMenus);
                setOriginalDataState(get1?.data);
            }
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);

    const resultMenuResponse = Array.isArray(menuResponse?.data) ? menuResponse?.data?.map(menu => {
        const menuId = menu.menuId;
        let isChecked;
        if (lastUserClick === '') {
            return;
        } else if (lastUserClick === 'cbAll') {
            isChecked = cbhState[menuId];
        } else {
            isChecked = checkedState[menuId];
        }

        if (isChecked === undefined || isChecked === false) {
            return undefined;
        }

        const updatedMenu = { ...menu };

        if (updatedMenu.menuChild) {
            updatedMenu.menuChild = updatedMenu.menuChild.filter(subMenu => {
                const subMenuId = subMenu.menuId;
                return (lastUserClick === 'cbAll' && cbhState[menuId]) ||
                    (checkedState[menuId] && checkedState[menuId][subMenuId]);
            });

            if (updatedMenu.menuChild.length === 0) {
                delete updatedMenu.menuChild;
            }
        }

        return updatedMenu;
    }) : [];

    const resetFormData = useCallback(() => {
        setSelectedDataState({});
        setOriginalDataState({});
        setErrorMessage(null);
        setCheckedState({});
        setCbhState({});
        setCbHeaderChecked(false);
        setLastUserClick('');
        setInputValue('');
    }, []);

    const listRoles = {
        "Daftar Wewenang": roleResponse.data
    };

    return (
        <>
            <Grid>
                <div className="col-span-8 ">
                    <Grid smallGap>
                        <Selectbox
                            inputValue={inputValue}
                            setInputValue={setInputValue}
                            className="mt-[5px] mb-[-10px] col-span-6"
                            idKey="id"
                            nameKey="name"
                            label="Wewenang"
                            datas={listRoles}
                            error={errorMessage?.id}
                            onChange={handleSelectboxWewenang}
                        />
                        <ButtonResetSave
                            className="col-span-6 h-[36.4px] mt-[15px]"
                            reset={resetFormData}
                            save={handleSaveClick}
                            loadingState={loadingState}
                        />
                    </Grid>
                    <TitleCard className="overflow-x-auto w-[80%] h-[75vh] mt-[15px]">
                        <TableForm020300
                            refreshTable={refreshTable}
                            isLoading={loadingState}
                            response={menuResponse}
                            checkedState={checkedState}
                            cbhState={cbhState}
                            cbHeaderChecked={cbHeaderChecked}
                            originalCheckedState={originalCheckedState}
                            originalDataState={originalDataState}
                            lastUserClick={lastUserClick}
                            setLastUserClick={setLastUserClick}
                            setCheckedState={setCheckedState}
                            setCbhState={setCbhState}
                            setCbHeaderChecked={setCbHeaderChecked}
                            setOriginalCheckedState={setOriginalCheckedState}
                            setOriginalDataState={setOriginalDataState}
                            menuResponse={menuResponse}
                            selectedDataState={selectedDataState}
                            setSelectedDataState={setSelectedDataState}
                        />
                    </TitleCard>
                </div>
            </Grid>
            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />
        </>
    );
};

export default Form020300;