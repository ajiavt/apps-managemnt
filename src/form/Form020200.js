import TitleCard from "../components/Cards/TitleCard";
import {useDispatch} from "react-redux";
import React, { useEffect, useState, useCallback } from 'react';
import {setPageTitle, showNotification} from "../features/common/headerSlice";
import TableForm020200 from "./_components/TableForm020200";
import Textbox from "../components/joy-ui/Textbox";
import ResponsiveModal from "../components/joy-ui/Modal";
import Textarea from "../components/joy-ui/Textarea";
import ButtonCRUD from "../components/joy-ui/ButtonCRUD";
import ButtonResetSave from "../components/joy-ui/ButtonResetSave";
import Selectbox from "../components/joy-ui/Selectbox";
import {roleService} from "./_repository/roleService";
import { pathApiService } from "./_repository/pathApiService";
import Grid from "../components/Grid";

// FM = Form Modes
const FM = {VIEW: 'view', EDIT: 'edit', ADD: 'add'};
const emptyData = {
    code: null,
    path: null,
    desc: null,
};

export default function Form020200(){

    const [selectedRow, setSelectedRow] = useState(null);
    const [formMode, setFormMode] = useState(FM.VIEW);
    const [renderKey, setRenderKey] = useState(0);
    const [loadingState, setLoadingState] = useState(false)
    const [selectedDataState, setSelectedDataState] = useState(emptyData);
    const [originalDataState, setOriginalDataState] = useState(emptyData);
    const [showModalSave, setShowModalSave] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [checkedState, setCheckedState] = useState({});
    const [originalCheckedState, setOriginalCheckedState] = useState({});
    const [inputValue, setInputValue] = useState({});
    const [roleResponse, setRoleResponse] = useState({});
    const [pathApiResponse, setPathApiResponse] = useState({});

    const dispatch = useDispatch();
    const [errorMessage, setErrorMessage] = useState(null);

    const fetchData = async (service, stateSetter) => {
        try {
            setLoadingState(true);
            await new Promise(resolve => setTimeout(resolve, 1000));
            const { response, status } = await service();
            if (status === 0) setErrorMessage(response);
            else stateSetter(response);
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    };

    const fetchDataMenu = useCallback(() => {
        fetchData(pathApiService.bind(null, "GET_ALL"), setPathApiResponse);
    }, []);

    const fetchDataRole = useCallback(() => {
        fetchData(roleService.bind(null, "GET_ALL"), setRoleResponse);
    }, []);

    const resultPathApiResponse = Array.isArray(pathApiResponse?.data) ? pathApiResponse?.data?.map(pathApi => {
        return { ...pathApi };
    }) : [];

    useEffect(() => {
        fetchDataMenu();
        fetchDataRole();
    }, [fetchDataMenu, fetchDataRole]);

    useEffect(() => {
        const finalResultMenuResponse = resultPathApiResponse.filter(pathApi => pathApi !== undefined);
        if(finalResultMenuResponse.length > 0) {
            setSelectedDataState({ ...selectedDataState, accesses: finalResultMenuResponse }); // to Save
        }
     }, [checkedState]);

    useEffect(() => {
        dispatch(setPageTitle({ title : "Generate Akses API"}))
    }, []);

    useEffect(() => {
        if (Object.keys(pathApiResponse).length !== 0 && Array.isArray(pathApiResponse?.data)) {
            setOriginalCheckedState(convertToState(pathApiResponse.data, true));
        }
    }, [originalDataState, pathApiResponse]);

    {/** START - EVENTS ON BUTTON*/}
    const handleSaveClick = useCallback(() => {
        if (JSON.stringify(originalDataState) === JSON.stringify(selectedDataState)) {
            dispatch(showNotification({ message: "Tidak ada data yang berubah!", status: 2 }));
            return;
        }
        setShowModalSave(true);
    }, [originalDataState, selectedDataState]);

    const handleConfirmSave = useCallback(async () => {
        setLoadingState(true);

        const data = {
            id: selectedDataState.id,
            name: selectedDataState.name,
            accesses: Object.keys(checkedState).flatMap((code) => {
                const methods = Object.keys(checkedState[code]).filter((method) => checkedState[code][method]);

                const urls = selectedDataState.accesses
                    .filter((access) => access.code === code)
                    .map((access) => {
                        return {
                            code: access.code,
                            path: access.path,
                            desc: access.desc,
                        };
                    });

                return methods.map((method) => {
                    return {
                        method,
                        urls: urls.length > 0 ? urls : null,
                    };
                });
            }).reduce((acc, curr) => {
                const existingMethod = acc.find(item => item.method === curr.method);
                if (existingMethod) {
                    existingMethod.urls = existingMethod.urls.concat(curr.urls);
                } else {
                    acc.push(curr);
                }
                return acc;
            }, [])
        };

        const { response, status } = await roleService("GENERATE_ACCESS", data);
        if (status === 0) {
            let error = handleError(response);
            setErrorMessage(error);
        } else {
            if (response.code === '00') {
                dispatch(showNotification({ message: response?.message, status: 1 }));
            }
            resetFormData();
        }
        setShowModalSave(false);
        setLoadingState(false);
    }, [selectedDataState]);
    {/** END - EVENTS ON BUTTON*/}

    const handleError = (response) => {
        const { code, message } = response?.data || {};
        if (code === "98") {
            return response?.data?.error;
        } else if (code === "94") {
            dispatch(showNotification({ message, status: 2 }));
        } else {
            dispatch(showNotification({ message, status: 0 }));
        }
    };

    const resetFormData = useCallback(() => {
        setSelectedDataState(emptyData);
        setOriginalDataState(emptyData);
        setErrorMessage(null);
        setCheckedState({});
        setInputValue('');
    }, []);

    const convertToState = (accesses, state) => {
        const processedAccess = {};

        if (Array.isArray(accesses)) {
            accesses.forEach((access) => {
                if (Array.isArray(access.urls)) {
                    access.urls.forEach((url) => {
                        if (!processedAccess[url.code]) {
                            processedAccess[url.code] = {};
                        }
                        processedAccess[url.code][access.method] = true;
                    });
                }
            });
        }

        return processedAccess;
    };

    const handleSelectboxWewenang = useCallback(async (child) => {
        setCheckedState({});
        setOriginalDataState(emptyData);
        setSelectedDataState(emptyData);

        try {
            setLoadingState(true);
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const { get1, status } = await roleService("GET_1", child);
            if (status === 0) {
                let error = handleError(get1);
                setErrorMessage(error);
                setLoadingState(false);
                return;
            }
            setSelectedDataState({
                ...selectedDataState,
                id: get1?.data?.id,
                name: get1?.data?.name
            });
            if (get1?.data?.accesses) {
                const processedMenus = convertToState(get1?.data?.accesses, true);
                setCheckedState(processedMenus);
                setOriginalDataState(get1?.data);
            }
        } catch (error) {
            console.error("Error fetching data:", error);
            setErrorMessage("An error occurred while fetching data.");
        } finally {
            setLoadingState(false);
        }
    }, []);


    const listRoles = {
        "Daftar Wewenang": roleResponse.data
    };

    return(
        <>
            <Grid>
                <div className="col-span-12">
                    <Grid smallGap>
                        <Selectbox
                            inputValue={inputValue}
                            setInputValue={setInputValue}
                            className="mt-[5px] mb-[-10px] col-span-6"
                            idKey="id"
                            nameKey="name"
                            label="Wewenang"
                            datas={listRoles}
                            error={errorMessage?.id}
                            onChange={handleSelectboxWewenang}
                        />
                        <ButtonResetSave
                            className="col-span-6 h-[36.4px] mt-[15px]"
                            reset={resetFormData}
                            save={handleSaveClick}
                            loadingState={loadingState}
                        />
                    </Grid>
                    <TitleCard className="overflow-x-auto h-[75vh] mt-[15px]">
                        <TableForm020200
                            setCheckedState={setCheckedState}
                            checkedState={checkedState}
                            isLoading={loadingState}
                            response={pathApiResponse}
                        />
                    </TitleCard>
                </div>
            </Grid>

            <ResponsiveModal
                color={"primary"}
                open={showModalSave}
                title="Konfirmasi"
                desc="Apakah Anda yakin ingin menyimpan data?"
                yesAction={handleConfirmSave}
                noAction={() => setShowModalSave(false)}
            />
        </>
    )
}