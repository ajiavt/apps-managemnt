import appLogo from '../../assets/images/Olibs Hybrid/logo.png';

function LandingIntro({backgroundColor}){
    return(
        <div className={`hero min-h-full rounded-l-xl ${backgroundColor}`}>
            <img alt='' src={appLogo} className="w-[3rem] py-5 md:w-1/2"/>
        </div>
    )
  }

  export default LandingIntro