/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    "./node_modules/react-tailwindcss-datepicker/dist/index.esm.js"
  ],
  // darkMode: ["class", '[data-theme="dark"]'],
  theme: {
    extend: {
      gridTemplateColumns: {
        '16': 'repeat(16, minmax(0, 1fr))',
        '89': 'repeat(89, minmax(0, 1fr))',
      },
      gridColumn: {
        'span-14': 'span 14 / span 14',
        'span-15': 'span 15 / span 15',
        'span-17': 'span 17 / span 17',
        'span-18': 'span 18 / span 18',
        'span-21': 'span 21 / span 21',
        'span-24': 'span 24 / span 24',
        'span-25': 'span 25 / span 25',
        'span-26': 'span 26 / span 26',
        'span-27': 'span 27 / span 27',
        'span-29': 'span 29 / span 29',
      },
      colors: {
        "base":"white",
        "blue-dark":"var(--blue-dark)",
        "blue-tur":"var(--blue-tur)",
        "gray-primary":"var(--gray-primary)",
        "yellow-primary":"var(--yellow-primary)",
        "gray-dark":"var(--gray-dark)",
        "white-bg":"var(--white-background)",
        "gray-light":"var(--gray-light)"
      },
      screens: {
        'small-height': { 'raw' :'(max-height: 475px)' },
        'small-width': { 'raw' :'(max-width: 300px)' },
      },
      width: {
        'sidebaropen': 'calc(100vw - 14rem)',
        'sidebar': '14rem',
      },
      height: {
        'navbaropen': 'calc(100vh - 4rem)',
      },
      left: {
        'sidebar': '14rem',
      },
      fontFamily: {
        'MontserratBold': "MontserratBold",
        'MontserratLight': "MontserratLight",
        'Montserrat': "Montserrat",
        'PoppinsBold': "PoppinsBold",
        'PoppinsLight': "PoppinsLight",
        'PoppinsItalic': "PoppinsItalic",
        'Poppins': "Poppins",
        'CourierPrime': "CourierPrime",
      },
    },
  },
  plugins: [
      require("@tailwindcss/typography"),
      require("daisyui"),
      'mui-unused-classes'
  ],
  rules: {
    'mui-unused-classes/unused-classes': 2
  },
  daisyui: {
    themes: [
      {
        mytheme: {
          'primary': '#570df8',
          'primary-focus': '#4506cb',
          'primary-content': '#ffffff',
          'secondary': '#f000b8',
          'secondary-focus': '#bd0091',
          'secondary-content': '#ffffff',
          'accent': '#37cdbe',
          'accent-focus': '#2aa79b',
          'accent-content': '#ffffff',
          'neutral': '#3d4451',
          'neutral-focus': '#2a2e37',
          'neutral-content': '#ffffff',
          'base-100': '#ffffff',
          'base-200': '#f9fafb',
          'base-300': '#d1d5db',
          'base-content': '#1f2937',
          'info': '#2094f3',
          'success': '#009485',
          'warning': '#ff9900',
          'error': '#ff5724',
        },
      },

    ],
  },
}